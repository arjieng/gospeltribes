﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.OS;
using ImageCircle.Forms.Plugin.Droid;
using Plugin.CrossPlatformTintedImage.Android;
using Plugin.CurrentActivity;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using SQLite.Net.Platform.XamarinAndroid;
using Android.Content;
using Acr.UserDialogs;
using Plugin.FirebasePushNotification;
using System.Threading.Tasks;
using Plugin.Badge;

namespace gospeltribe.Droid
{
    [Activity(Label = "Gospel Tribes", Icon = "@mipmap/icon2", Theme = "@style/splashscreen", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, LaunchMode = LaunchMode.SingleTop)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "gospeltribe")]
    public class MainActivity : FormsAppCompatActivity
    {

        int counter = 0;
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            SaveContacts.GetContext = () => this;
            ClipBoard.GetContext = () => this;
            AppIcon.GetContext = () => this;

            var stateList = new Android.Content.Res.ColorStateList(
                new int[][]
                {
                    new int[] { Android.Resource.Attribute.StateChecked },
                    new int[] { Android.Resource.Attribute.StateEnabled }
                },
                new int[]
                {
                    new Android.Graphics.Color(Color.Red.ToAndroid()), //Selected
                    new Android.Graphics.Color(Color.FromHex("#FFFFFF").ToAndroid()) //Normal
                }
            );


            // Scaling
            var density = Resources.DisplayMetrics.Density;
            App.screenWidth = Resources.DisplayMetrics.WidthPixels / density;
            App.screenHeight = (Resources.DisplayMetrics.HeightPixels / density) - 26;
            App.appScale = density;

            BottomTabbedPageRenderer.BackgroundColor = Color.FromHex("#BEBEC1").ToAndroid();
            BottomTabbedPageRenderer.FontSize = (float?)12;
            BottomTabbedPageRenderer.IconSize = 25;
            BottomTabbedPageRenderer.ItemTextColor = stateList;
            BottomTabbedPageRenderer.ItemIconTintList = stateList;
            BottomTabbedPageRenderer.ItemPadding = new Thickness(0, 5, 0, 3);
            BottomTabbedPageRenderer.BottomBarHeight = 47.ScaleWidth();

            BottomTabbedPageRenderer.MenuItemIconSetter = (arg1, arg2, arg3) =>
            {
                BottomTabbedPageRenderer.DefaultMenuItemIconSetter.Invoke(arg1, arg2, arg3);
            };




            string esv = FileAccessHelper.GetLocalFilePath("esv.sqlite3");
            string hcsb = FileAccessHelper.GetLocalFilePath("hcsb.sqlite3");
            string kjv = FileAccessHelper.GetLocalFilePath("kjv.sqlite3");
            string nkjv = FileAccessHelper.GetLocalFilePath("nkjv.sqlite3");
            string nlt = FileAccessHelper.GetLocalFilePath("nlt.sqlite3");
            string niv = FileAccessHelper.GetLocalFilePath("niv2011.sqlite3");

            global::Xamarin.Forms.Forms.Init(this, bundle);
            //CrossBadge.Current.SetBadge(20000);
            //Splash Screen
            base.Window.RequestFeature(WindowFeatures.ActionBar);
            base.SetTheme(Resource.Style.MainTheme);

            Websockets.Droid.WebsocketConnection.Link();
            XamForms.Controls.Droid.Calendar.Init();
            ICPlatform.Init();
            ImageCircleRenderer.Init();
            TintedImageRenderer.Init();
            UserDialogs.Init(this);
            CrossCurrentActivity.Current.Init(this, bundle);
            Rg.Plugins.Popup.Popup.Init(this, bundle);
            Xamarin.FormsGoogleMaps.Init(this, bundle);


            base.OnCreate(bundle);
            Plugin.InputKit.Platforms.Droid.Config.Init(this, bundle);
            LoadApplication(new App(esv, hcsb, kjv, nkjv, nlt, new SQLitePlatformAndroid(), niv));

            //var notificationManager = this.GetSystemService(Context.NotificationService) as NotificationManager;

            //Notification.Builder builder = new Notification.Builder(this, "DefaultChannel")
            //  .SetContentTitle("hello")
            //  .SetTicker("hello")
            //  .SetNumber(App.BadgeCounter)
            //  .SetSmallIcon(Resource.Mipmap.Icon2);

            //var notification = builder.Build();
            //notificationManager.Notify(int.MinValue, notification);

            MessagingCenter.Subscribe<App>(this, "UpdateAppIconBadge", HandleAction);

            FirebasePushNotificationManager.ProcessIntent(this, Intent);
            Window.SetSoftInputMode(SoftInput.AdjustResize);
        }


        void HandleAction(App obj)
        {
            App.BadgeCounter = (App.InboxBadgeCounter + App.PrayersBadgeCounter + App.EventsBadgeCounter + App.StoryBadgeCounter + App.MembersBadgeCounter) - 1;
            var notificationManager = this.GetSystemService(Context.NotificationService) as NotificationManager;

            Notification.Builder builder = new Notification.Builder(this, "DefaultChannel")
              .SetContentTitle("hello")
              .SetTicker("hello")
              .SetNumber(App.BadgeCounter)
              .SetSmallIcon(Resource.Mipmap.Icon2);

            var notification = builder.Build();
            notificationManager.Notify(0, notification);
        }


        public static MainActivity Current { private set; get; }
        public static readonly int PickImageId = 1000;
        public TaskCompletionSource<string> PickImageTaskCompletionSource { set; get; }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == PickImageId)
            {

                if ((resultCode == Result.Ok) && (data != null))
                {
                    PickImageTaskCompletionSource.SetResult(data.DataString);
                }
                else
                {
                    PickImageTaskCompletionSource.SetResult(null);
                }
            }
            ICPlatform.OnActivityResult(requestCode, resultCode, data);
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            FirebasePushNotificationManager.ProcessIntent(this, intent);
        }
    }
}
