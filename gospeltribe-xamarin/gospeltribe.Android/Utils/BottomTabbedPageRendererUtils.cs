﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Com.Ittianyu.Bottomnavigationviewex;
using Android.Support.Design.Internal;

namespace gospeltribe.Droid
{
    using RelativeLayout = Android.Widget.RelativeLayout;
    using Platform = Xamarin.Forms.Platform.Android.Platform;

    public static class BottomTabbedPageRendererUtils
    {
        public static Rectangle CreateRect(this Context context, int width, int height)
        {
            return new Rectangle(
                    0, 0,
                    context.FromPixels(width),
                    context.FromPixels(height)
                );
        }

        public static void HandlePagesChanged(this BottomTabbedPageRenderer renderer)
        {
            renderer.SetupBottomBar();
            renderer.SetupTabItems();

            if (renderer.Element.Children.Count == 0)
            {
                return;
            }

            EnsureTabIndex(renderer);
        }

        static void EnsureTabIndex(BottomTabbedPageRenderer renderer)
        {
            var rootLayout = (RelativeLayout)renderer.GetChildAt(0);
            var bottomNav = (BottomNavigationViewEx)rootLayout.GetChildAt(1);
            var menu = (BottomNavigationMenu)bottomNav.Menu;

            var itemIndex = menu.FindItemIndex(bottomNav.SelectedItemId);
            var pageIndex = renderer.Element.Children.IndexOf(renderer.Element.CurrentPage);
            if (pageIndex >= 0 && pageIndex != itemIndex && pageIndex < bottomNav.ItemCount)
            {
                var menuItem = menu.GetItem(pageIndex);
                bottomNav.SelectedItemId = menuItem.ItemId;

                if (BottomTabbedPageRenderer.ShouldUpdateSelectedIcon && BottomTabbedPageRenderer.MenuItemIconSetter != null)
                {
                    BottomTabbedPageRenderer.MenuItemIconSetter?.Invoke(menuItem, renderer.Element.CurrentPage.Icon, true);

                    if (renderer.LastSelectedIndex != pageIndex)
                    {
                        var lastSelectedPage = renderer.Element.Children[renderer.LastSelectedIndex];
                        var lastSelectedMenuItem = menu.GetItem(renderer.LastSelectedIndex);
                        BottomTabbedPageRenderer.MenuItemIconSetter?.Invoke(lastSelectedMenuItem, lastSelectedPage.Icon, false);
                        renderer.LastSelectedIndex = pageIndex;
                    }
                }
                else if (renderer.LastSelectedIndex != pageIndex)
                {
                    renderer.LastSelectedIndex = pageIndex;
                }
            }
        }

        public static void SwitchPage(this BottomTabbedPageRenderer renderer, IMenuItem item)
        {
            var rootLayout = (RelativeLayout)renderer.GetChildAt(0);
            var bottomNav = (BottomNavigationViewEx)rootLayout.GetChildAt(1);
            var menu = (BottomNavigationMenu)bottomNav.Menu;

            var index = menu.FindItemIndex(item.ItemId);
            var pageIndex = index % renderer.Element.Children.Count;
            var currentPageIndex = renderer.Element.Children.IndexOf(renderer.Element.CurrentPage);

            if (pageIndex == currentPageIndex)
            {

                renderer.Element.Children[currentPageIndex].Navigation?.PopToRootAsync();
            }
            else
            {
                renderer.Element.CurrentPage = renderer.Element.Children[pageIndex];
            }

            for (int i = 0; i < 5; i++)
            {
                if (i != index)
                {
                    bottomNav.SetItemBackground(i, ChangeBackground(i, 0));
                }
                else
                {
                    bottomNav.SetItemBackground(i, ChangeBackground(i, 1));

                }
            }

        }

        public static int ChangeBackground(int selectedPage, int isSelected)
        {
            int selectedImage = 0;
            switch (selectedPage)
            {
                case 0:
                    switch (isSelected)
                    {
                        case 0:
                            selectedImage = Resource.Drawable.inboxUnselected;
                            break;
                        case 1:
                            selectedImage = Resource.Drawable.inboxSelected;
                            break;
                    }
                    break;
                case 1:
                    switch (isSelected)
                    {
                        case 0:
                            selectedImage = Resource.Drawable.prayersUnselected;
                            break;
                        case 1:
                            selectedImage = Resource.Drawable.prayersSelected;
                            break;
                    }
                    break;
                case 2:
                    switch (isSelected)
                    {
                        case 0:
                            selectedImage = Resource.Drawable.eventsUnselected;
                            break;
                        case 1:
                            selectedImage = Resource.Drawable.eventsSelected;
                            break;
                    }
                    break;
                case 3:
                    switch (isSelected)
                    {
                        case 0:
                            selectedImage = Resource.Drawable.storyUnselected;
                            break;
                        case 1:
                            selectedImage = Resource.Drawable.storySelected;
                            break;
                    }
                    break;
                case 4:
                    switch (isSelected)
                    {
                        case 0:
                            selectedImage = Resource.Drawable.membersUnselected;
                            break;
                        case 1:
                            selectedImage = Resource.Drawable.membersSelected;
                            break;
                    }
                    break;
            }

            return selectedImage;
        }


        public static void Layout(this BottomTabbedPageRenderer renderer, int width, int height)
        {
            var rootLayout = (RelativeLayout)renderer.GetChildAt(0);
            var bottomNav = (BottomNavigationViewEx)rootLayout.GetChildAt(1);

            var Context = renderer.Context;

            rootLayout.Measure(
                MeasureSpecFactory.MakeMeasureSpec(width, MeasureSpecMode.Exactly),
                MeasureSpecFactory.MakeMeasureSpec(height, MeasureSpecMode.AtMost));

            ((IPageController)renderer.Element).ContainerArea = Context.CreateRect(rootLayout.MeasuredWidth, rootLayout.GetChildAt(0).MeasuredHeight);

            rootLayout.Measure(
                MeasureSpecFactory.MakeMeasureSpec(width, MeasureSpecMode.Exactly),
                MeasureSpecFactory.MakeMeasureSpec(height, MeasureSpecMode.Exactly));
            rootLayout.Layout(0, 0, rootLayout.MeasuredWidth, rootLayout.MeasuredHeight);

            if (renderer.Element.Children.Count == 0)
            {
                return;
            }

            int tabsHeight = bottomNav.MeasuredHeight;

            var item = (ViewGroup)bottomNav.GetChildAt(0);
            item.Measure(
                MeasureSpecFactory.MakeMeasureSpec(width, MeasureSpecMode.Exactly),
                MeasureSpecFactory.MakeMeasureSpec(tabsHeight, MeasureSpecMode.Exactly));

            item.Layout(0, 0, width, tabsHeight);
            int item_w = width / item.ChildCount;
            for (int i = 0; i < item.ChildCount; i++)
            {
                var frame = (FrameLayout)item.GetChildAt(i);
                frame.Measure(
                MeasureSpecFactory.MakeMeasureSpec(item_w, MeasureSpecMode.Exactly),
                MeasureSpecFactory.MakeMeasureSpec(tabsHeight, MeasureSpecMode.Exactly));
                frame.Layout(i * item_w, 0, i * item_w + item_w, tabsHeight);

                var imgView = bottomNav.GetIconAt(i);
                var baselayout = frame.GetChildAt(1);
                if (BottomTabbedPageRenderer.VisibleTitle == false)
                {
                    baselayout.Visibility = ViewStates.Gone;
                    //Icon Height
                    int imgH = imgView.LayoutParameters.Height;
                    //Icon Width
                    int imgW = Math.Min(imgView.LayoutParameters.Width, item_w - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Left) - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Right));

                    int imgTop = (tabsHeight - imgH - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemSpacing)) / 2;
                    int imgLeft = (item_w - imgW) / 2;

                    switch (BottomTabbedPageRenderer.ItemAlign)
                    {
                        case ItemAlignFlags.Default:
                        case ItemAlignFlags.Top:
                            imgTop = (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Top);
                            break;
                        case ItemAlignFlags.Bottom:
                            imgTop = tabsHeight - imgH - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Bottom);
                            break;
                    }
                    //layout icon
                    imgView.Measure(MeasureSpecFactory.MakeMeasureSpec(imgW, MeasureSpecMode.Exactly), MeasureSpecFactory.MakeMeasureSpec(imgH, MeasureSpecMode.Exactly));
                    imgView.Layout(imgLeft, imgTop, imgW + imgLeft, imgH + imgTop);
                    continue;
                }

                if (baselayout != null)
                {
                    baselayout.Visibility = ViewStates.Visible;
                    if (baselayout.GetType() == typeof(BaselineLayout))
                    {
                        //Container text
                        var basel = (BaselineLayout)baselayout;
                        //Small text
                        var small = bottomNav.GetSmallLabelAt(i);
                        //Large text
                        var large = bottomNav.GetLargeLabelAt(i);

                        //Height Container text
                        int baselH = Math.Max(small.Height, large.Height);
                        //width Container text
                        int baselW = Math.Min(small.Width, item_w - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Left) - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Right));
                        //Icon Height
                        int imgH = imgView.LayoutParameters.Height;
                        //Icon Width
                        int imgW = Math.Min(imgView.LayoutParameters.Width, item_w - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Left) - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Right));

                        int imgTop = (tabsHeight - imgH - baselH - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemSpacing)) / 2;
                        int imgLeft = (item_w - imgW) / 2;
                        int topBaseLine = imgTop + imgH + (int)Context.ToPixels(BottomTabbedPageRenderer.ItemSpacing);
                        int leftBaseLine = (item_w - baselW) / 2;

                        switch (BottomTabbedPageRenderer.ItemAlign)
                        {
                            case ItemAlignFlags.Default:
                                imgTop = (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Top);
                                topBaseLine = tabsHeight - baselH - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Bottom);
                                break;
                            case ItemAlignFlags.Top:
                                imgTop = (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Top);
                                topBaseLine = imgTop + imgH + (int)Context.ToPixels(BottomTabbedPageRenderer.ItemSpacing);
                                break;
                            case ItemAlignFlags.Bottom:
                                imgTop = tabsHeight - imgH - baselH - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemSpacing) - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Bottom);
                                topBaseLine = imgTop + imgH + (int)Context.ToPixels(BottomTabbedPageRenderer.ItemSpacing);
                                break;
                        }
                        //layout icon, text
                        imgView.Measure(MeasureSpecFactory.MakeMeasureSpec(imgW, MeasureSpecMode.Exactly), MeasureSpecFactory.MakeMeasureSpec(imgH, MeasureSpecMode.Exactly));
                        imgView.Layout(imgLeft, imgTop, imgW + imgLeft, imgH + imgTop);
                        basel.Measure(MeasureSpecFactory.MakeMeasureSpec(baselW, MeasureSpecMode.Exactly), MeasureSpecFactory.MakeMeasureSpec(tabsHeight, MeasureSpecMode.Exactly));
                        basel.Layout(leftBaseLine, topBaseLine, leftBaseLine + baselW, topBaseLine + baselH);

                        //text break
                        var breaktext = small.Paint.BreakText(small.Text, true, item_w - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Right) - (int)Context.ToPixels(BottomTabbedPageRenderer.ItemPadding.Left), null);
                        var text = small.Text;
                        if (text.Length > breaktext)
                        {
                            small.Text = text.Substring(0, breaktext - 1);
                            large.Text = text.Substring(0, breaktext - 1);
                        }
                    }
                }
            }
        }

        public static void SetupTabItems(this BottomTabbedPageRenderer renderer, BottomNavigationViewEx bottomNav)
        {
            var Element = renderer.Element;
            var menu = (BottomNavigationMenu)bottomNav.Menu;
            menu.ClearAll();

            var tabsCount = Math.Min(Element.Children.Count, bottomNav.MaxItemCount);
            for (int i = 0; i < tabsCount; i++)
            {
                var page = Element.Children[i];
                //bottomNav.SetItemBackground(i, ChangeBackground(i, 0));
                var menuItem = menu.Add(0, i, 0, "");
                var setter = BottomTabbedPageRenderer.MenuItemIconSetter ?? BottomTabbedPageRenderer.DefaultMenuItemIconSetter;
                setter.Invoke(menuItem, "Careta", renderer.LastSelectedIndex == i);
            }

            for (int i = 0; i < tabsCount; i++)
            {
                //if (i != index)
                //{
                var e = Element.Children[i];
                e.Icon = null; e.Title = null;
                bottomNav.SetItemBackground(i, ChangeBackground(i, (i == 0 ? 1 : 0)));

            }


            if (Element.Children.Count > 0)
            {
                bottomNav.EnableShiftingMode(false);//remove shifting mode
                bottomNav.EnableItemShiftingMode(false);//remove shifting mode
                bottomNav.EnableAnimation(false);//remove animation
                bottomNav.SetTextVisibility(BottomTabbedPageRenderer.VisibleTitle.HasValue ? BottomTabbedPageRenderer.VisibleTitle.Value : true);
                if (BottomTabbedPageRenderer.Typeface != null)
                {
                    bottomNav.SetTypeface(BottomTabbedPageRenderer.Typeface);
                }
                if (BottomTabbedPageRenderer.IconSize.HasValue)
                {
                    bottomNav.SetIconSize(BottomTabbedPageRenderer.IconSize.Value, BottomTabbedPageRenderer.IconSize.Value);
                }
                if (BottomTabbedPageRenderer.FontSize.HasValue)
                {
                    bottomNav.SetTextSize(BottomTabbedPageRenderer.FontSize.Value);
                }

                bottomNav.TextAlignment = Android.Views.TextAlignment.Center;
            }
        }

        public static BottomNavigationViewEx SetupBottomBar(this BottomTabbedPageRenderer renderer, Android.Widget.RelativeLayout rootLayout, BottomNavigationViewEx bottomNav, int barId)
        {
            if (bottomNav != null)
            {
                rootLayout.RemoveView(bottomNav);
                bottomNav.SetOnNavigationItemSelectedListener(null);
            }

            var barParams = new Android.Widget.RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MatchParent,
                BottomTabbedPageRenderer.BottomBarHeight.HasValue ? (int)rootLayout.Context.ToPixels(BottomTabbedPageRenderer.BottomBarHeight.Value) : ViewGroup.LayoutParams.WrapContent);
            barParams.AddRule(LayoutRules.AlignParentBottom);
            bottomNav = new BottomNavigationViewEx(rootLayout.Context)
            {
                LayoutParameters = barParams,
                Id = barId
            };
            if (BottomTabbedPageRenderer.BackgroundColor.HasValue)
            {
                bottomNav.SetBackgroundColor(BottomTabbedPageRenderer.BackgroundColor.Value);
            }
            if (BottomTabbedPageRenderer.ItemIconTintList != null)
            {
                bottomNav.ItemIconTintList = BottomTabbedPageRenderer.ItemIconTintList;
            }
            if (BottomTabbedPageRenderer.ItemTextColor != null)
            {
                bottomNav.ItemTextColor = BottomTabbedPageRenderer.ItemTextColor;
            }
            if (BottomTabbedPageRenderer.ItemBackgroundResource.HasValue)
            {
                bottomNav.ItemBackgroundResource = BottomTabbedPageRenderer.ItemBackgroundResource.Value;
            }

            bottomNav.SetOnNavigationItemSelectedListener(renderer);
            rootLayout.AddView(bottomNav, 1, barParams);

            return bottomNav;
        }

        public static void ChangePage(this BottomTabbedPageRenderer renderer, FrameLayout pageContainer, Page page)
        {
            renderer.Context.HideKeyboard(renderer);

            if (page == null)
            {
                return;
            }

            if (Platform.GetRenderer(page) == null)
            {
                //Platform.SetRenderer(page, Platform.CreateRenderer(page));
                //Platform.CreateRenderer()
                var render = Platform.CreateRendererWithContext(page, renderer.Context);
                Platform.SetRenderer(page, render);
            }
            //var pageContent = Platform.GetRenderer(page).ViewGroup;
            var pageContent = Platform.GetRenderer(page).View;
            pageContainer.AddView(pageContent);
            if (pageContainer.ChildCount > 1)
            {
                pageContainer.RemoveViewAt(0);
            }

            EnsureTabIndex(renderer);
        }

        public static RelativeLayout CreateRoot(this BottomTabbedPageRenderer renderer, int barId, int pageContainerId, out FrameLayout pageContainer)
        {
            var rootLayout = new RelativeLayout(renderer.Context)
            {
                LayoutParameters = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent),
            };
            var pageParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            pageParams.AddRule(LayoutRules.Above, barId);

            pageContainer = new FrameLayout(renderer.Context)
            {
                LayoutParameters = pageParams,
                Id = pageContainerId
            };

            rootLayout.AddView(pageContainer, 0, pageParams);

            return rootLayout;
        }
    }
}
