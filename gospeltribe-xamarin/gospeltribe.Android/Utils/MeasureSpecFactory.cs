﻿using System;
using Android.Views;

namespace gospeltribe.Droid
{
    internal static class MeasureSpecFactory
    {
        public static int GetSize(int measureSpec){
            const int modeMask = 0x3 << 30;
            return measureSpec & ~modeMask;
        }

        public static int MakeMeasureSpec(int size, MeasureSpecMode mode){
            return size + (int)mode;
        }
    }
}
