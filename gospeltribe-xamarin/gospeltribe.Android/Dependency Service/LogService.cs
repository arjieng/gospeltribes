﻿using System;
using gospeltribe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(LogService))]
namespace gospeltribe.Droid
{
    public class LogService : ILogServices
    {
        public void Log(string log)
        {
            Console.WriteLine(log);
        }
    }
}

