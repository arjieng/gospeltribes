﻿using System;
using Android.Content;
using gospeltribe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ClipBoard))]
namespace gospeltribe.Droid
{
    public class ClipBoard : IClipBoard
    {
        internal static Func<Context> GetContext { get; set; }
        public string GetTextFromClipBoard()
        {
            Context context = GetContext();
            var clipboardmanager = (ClipboardManager)context.GetSystemService(Context.ClipboardService);
            var item = clipboardmanager.PrimaryClip.GetItemAt(0);
            var text = item.Text;
            return text;
        }

        public void SendTextToClipboard(string text)
        {
            Context context = GetContext();
            // Get the Clipboard Manager
            var clipboardManager = (ClipboardManager)context.GetSystemService(Context.ClipboardService);

            // Create a new Clip
            var clip = ClipData.NewPlainText("YOUR_TITLE_HERE", text);

            // Copy the text
            clipboardManager.PrimaryClip = clip;
        }
    }
}
