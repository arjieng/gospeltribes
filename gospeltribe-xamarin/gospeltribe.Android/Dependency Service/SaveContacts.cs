using System;
using Android.App;
using Android.Content;
using Android.Provider;
using Android.Widget;
using gospeltribe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(SaveContacts))]
namespace gospeltribe.Droid
{
    public class SaveContacts : iSaveContacts
    {
        internal static Func<Context> GetContext { get; set; }
        public SaveContacts()
        {
        }

        public void SaveContact(string name, string number, ProfilePage profilePage)
        {
            Context context = GetContext();
            var activity = context as Activity;
            var intent = new Intent(Intent.ActionInsert);
            intent.SetType(ContactsContract.Contacts.ContentType);
            intent.PutExtra(ContactsContract.Intents.Insert.Name, name);
            intent.PutExtra(ContactsContract.Intents.Insert.Phone, number);
            activity.StartActivity(intent);
            Toast.MakeText(activity, "Contact Saved", ToastLength.Short).Show();
        }
    }
}
