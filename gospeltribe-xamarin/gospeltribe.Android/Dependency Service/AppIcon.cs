﻿using System;
using Android.App;
using Android.Content;
using Badge.Plugin;
using gospeltribe.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppIcon))]
namespace gospeltribe.Droid
{
    public class AppIcon : iAppIcon
    {
        readonly string _title = "";
        const int BadgeNotificationId = int.MinValue;
        internal static Func<Context> GetContext { get; set; }

        public void ChangeAppIcon()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var notificationManager = GetContext().GetSystemService(Context.NotificationService) as NotificationManager;

                App.BadgeCounter = (App.InboxBadgeCounter + App.PrayersBadgeCounter + App.EventsBadgeCounter + App.StoryBadgeCounter + App.MembersBadgeCounter);
                App.Log("THIS IS THE COUNTER NOW: " + App.BadgeCounter);

                var builder = new Notification.Builder(GetContext(), "DefaultChannel")
                .SetContentTitle(_title)
                .SetTicker(_title)
                .SetNumber(App.BadgeCounter)
                .SetSmallIcon(Resource.Mipmap.Icon2);
                var notification = builder.Build();
                notificationManager.Notify(BadgeNotificationId, notification);
            });
        }
    }
}