﻿using System;
using Android.Content;
using Android.Graphics;
using gospeltribe;
using gospeltribe.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace gospeltribe.Droid.Renderers
{
	public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if(Control != null){
                Control.SetPadding(0, 0, 0, 0);
                Control.SetAllCaps(false);
            }
        }
    }
}
