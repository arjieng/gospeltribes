﻿using System;
using System.ComponentModel;
using System.Reflection;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Support.Design.Internal;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using gospeltribe;
using gospeltribe.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;

[assembly: ExportRenderer(typeof(CustomTabbedPage), typeof(CustomTabbedPageRenderer))]
namespace gospeltribe.Droid
{
    public class CustomTabbedPageRenderer : TabbedPageRenderer
    {
        
        BottomNavigationView _bottomNavigationView;
        public CustomTabbedPageRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TabbedPage> e)
        {
            base.OnElementChanged(e);

            if(e.NewElement != null){
                _bottomNavigationView = (BottomNavigationView)typeof(TabbedPageRenderer).GetField("_bottomNavigationView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(this);
                try{
                    var menuView = _bottomNavigationView.GetChildAt(0) as BottomNavigationMenuView;

                    if (menuView == null)
                        return;
                    var shiftMode = menuView.Class.GetDeclaredField("mShiftingMode");

                    shiftMode.Accessible = true;
                    shiftMode.SetBoolean(menuView, false);
                    shiftMode.Accessible = false;
                    shiftMode.Dispose();

                    for (int i = 0; i < menuView.ChildCount; i++)
                    {
                        var item = menuView.GetChildAt(i) as BottomNavigationItemView;
                        if (item == null)
                            continue;
                        item.SetShiftingMode(false);
                        item.SetChecked(item.ItemData.IsChecked);
                        if(i == 0){
                            item.SetBackgroundColor(Color.FromHex("#00feff").ToAndroid());
                        }
                    }

                    menuView.UpdateMenuView();
                }catch (Exception ex)
                {
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if(e.PropertyName == "CurrentPage"){
                try
                {
                    var menuView = _bottomNavigationView.GetChildAt(0) as BottomNavigationMenuView;

                    if (menuView == null)
                        return;

                    for (int i = 0; i < menuView.ChildCount; i++)
                    {
                        var item = menuView.GetChildAt(i) as BottomNavigationItemView;
                        if (item == null)
                            continue;
                        
                        if(i == menuView.SelectedItemId){
                            item.SetBackgroundColor(Color.FromHex("#00feff").ToAndroid());
                        }else{
                            item.SetBackgroundColor(Color.White.ToAndroid());
                        }
                    }

                    //menuView.UpdateMenuView();
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}
