﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Util;
using Android.Views.InputMethods;
using Android.Widget;
using gospeltribe;
using gospeltribe.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace gospeltribe.Droid.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        CustomEntry entry;
        public CustomEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            entry = (CustomEntry)this.Element;
            if(this.Control != null){
                //Control.SetPadding(0, 0, 0, 0);
                //this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);


                switch (entry.DesignType)
                {
                    case DesignType.Flat:
                        if (!entry.IsPassword)
                            Control.InputType = Android.Text.InputTypes.TextFlagCapWords;
                        if (entry.IsCurvedCornersEnabled)
                        {
                            // creating gradient drawable for the curved background
                            var _gradientBackground = new GradientDrawable();
                            _gradientBackground.SetShape(ShapeType.Rectangle);
                            _gradientBackground.SetColor(entry.BackgroundColor.ToAndroid());
                            _gradientBackground.SetStroke(entry.BorderWidth, entry.BorderColor.ToAndroid());
                            _gradientBackground.SetCornerRadius(DpToPixels(this.Context, Convert.ToSingle(entry.CornerRadius)));
                            Control.SetBackground(_gradientBackground);
                        }
                        Control.SetPadding((int)DpToPixels(this.Context, Convert.ToSingle(12)), Control.PaddingTop, (int)DpToPixels(this.Context, Convert.ToSingle(12)), Control.PaddingBottom);
                        break;
                    case DesignType.Materialize:
                        if (!entry.IsPassword)
                            Control.InputType = Android.Text.InputTypes.TextFlagCapWords;
                        Control.SetPadding(0, 0, 0, 0);
                        this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        break;
                }

                Element.Focused += Element_Focused;
                ChangeCapitalizationType();
                InvokeEntryCompleted();
            }

            if (e.OldElement != null)
            {
                Element.Focused -= Element_Focused;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            entry = (CustomEntry)this.Element;

            ChangeCapitalizationType();

            InvokeEntryCompleted();
        }

        void Element_Focused(object sender, FocusEventArgs e)
        {
            var imm = (InputMethodManager)Context.GetSystemService(Context.InputMethodService);
            imm.ShowSoftInput(Control, ShowFlags.Implicit);
        }

        void ChangeCapitalizationType()
        {
            if (entry != null)
            {
                switch (entry.AutoCapitalization)
                {
                    case AutoCapitalizationType.Words:
                        Control.InputType = InputTypes.TextFlagCapWords;
                        break;
                    case AutoCapitalizationType.Sentences:
                        Control.InputType = InputTypes.TextFlagCapSentences;
                        break;
                    case AutoCapitalizationType.All:
                        Control.InputType = InputTypes.TextFlagCapCharacters;
                        break;
                }
            }
        }

        void InvokeEntryCompleted()
        {
            if (entry != null)
            {
                Control.EditorAction += (object tV, TextView.EditorActionEventArgs args) =>
                {
                    entry.InvokeCompleted();
                };
            }
        }

        public static float DpToPixels(Context context, float valueInDp)
        {
            DisplayMetrics metrics = context.Resources.DisplayMetrics;
            return TypedValue.ApplyDimension(ComplexUnitType.Dip, valueInDp, metrics);
        }

        //protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        //{
        //    base.OnElementChanged(e);
        //    CustomEntry entry = (CustomEntry)this.Element;

        //    if (this.Control != null)
        //    {
        //        switch (entry.AutoCapitalization)
        //        {
        //            case AutoCapitalizationType.Words:
        //                Control.InputType = Android.Text.InputTypes.TextFlagCapWords;
        //                break;
        //            case AutoCapitalizationType.Sentences:
        //                Control.InputType = Android.Text.InputTypes.TextFlagCapSentences;
        //                break;
        //            case AutoCapitalizationType.All:
        //                Control.InputType = Android.Text.InputTypes.TextFlagCapCharacters;
        //                break;
        //        }

        //        switch (entry.DesignType)
        //        {
        //            case DesignType.Flat:
        //                Control.InputType = Android.Text.InputTypes.TextFlagCapWords;
        //                if (entry.IsCurvedCornersEnabled)
        //                {
        //                    // creating gradient drawable for the curved background
        //                    var _gradientBackground = new GradientDrawable();
        //                    _gradientBackground.SetShape(ShapeType.Rectangle);
        //                    _gradientBackground.SetColor(entry.BackgroundColor.ToAndroid());
        //                    // Thickness of the stroke line  
        //                    _gradientBackground.SetStroke(entry.BorderWidth, entry.BorderColor.ToAndroid());
        //                    // Radius for the curves  

        //                    _gradientBackground.SetCornerRadius(DpToPixels(this.Context, Convert.ToSingle(entry.CornerRadius)));
        //                    // set the background of the   
        //                    Control.SetBackground(_gradientBackground);
        //                }
        //                Control.SetPadding((int)DpToPixels(this.Context, Convert.ToSingle(12)), Control.PaddingTop, (int)DpToPixels(this.Context, Convert.ToSingle(12)), Control.PaddingBottom);
        //                break;
        //            case DesignType.Materialize:
        //                //Control.mas
        //                Control.InputType = Android.Text.InputTypes.TextFlagCapWords;
        //                Control.SetPadding(0, 0, 0, 0);
        //                this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);

        //                break;
        //        }

        //        Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);

        //        switch(e.NewElement.StyleId){
        //            case "C":
        //                Control.Gravity = Android.Views.GravityFlags.Center;
        //                break;
        //            case "LC":
        //                Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;
        //                break;
        //            default:
        //                Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;
        //                break;
        //        }

        //        if (entry != null)
        //        {
        //            SetReturnType(entry);

        //            Control.EditorAction += (object sender, TextView.EditorActionEventArgs args) =>
        //            {
        //                if (entry.ReturnType != ReturnType.Next)
        //                    entry.Unfocus();

        //                entry.InvokeCompleted();
        //            };
        //        }
        //    }
        //}

        //public static float DpToPixels(Context context, float valueInDp)
        //{
        //    DisplayMetrics metrics = context.Resources.DisplayMetrics;
        //    return TypedValue.ApplyDimension(ComplexUnitType.Dip, valueInDp, metrics);
        //}

        //private void SetReturnType(CustomEntry entry)
        //{
        //    ReturnType type = entry.ReturnType;

        //    switch (type)
        //    {
        //        case ReturnType.Go:
        //            Control.ImeOptions = ImeAction.Go;
        //            Control.SetImeActionLabel("Go", ImeAction.Go);
        //            break;
        //        case ReturnType.Next:
        //            Control.ImeOptions = ImeAction.Next;
        //            Control.SetImeActionLabel("Next", ImeAction.Next);
        //            break;
        //        case ReturnType.Send:
        //            Control.ImeOptions = ImeAction.Send;
        //            Control.SetImeActionLabel("Send", ImeAction.Send);
        //            break;
        //        case ReturnType.Search:
        //            Control.ImeOptions = ImeAction.Search;
        //            Control.SetImeActionLabel("Search", ImeAction.Search);
        //            break;
        //        default:
        //            Control.ImeOptions = ImeAction.Done;
        //            Control.SetImeActionLabel("Done", ImeAction.Done);
        //            break;
        //    }
        //}


    }
}