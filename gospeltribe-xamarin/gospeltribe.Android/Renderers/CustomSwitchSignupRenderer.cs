﻿using System;
using Android.Content;
using Android.Graphics;
using gospeltribe.Droid.Renderers;
using gospeltribe;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomSwitchSignup), typeof(CustomSwitchSignupRenderer))]
namespace gospeltribe.Droid.Renderers
{
    public class CustomSwitchSignupRenderer : SwitchRenderer
    {
        public CustomSwitchSignupRenderer(Context context) : base(context)
        {
        }

        private Android.Graphics.Color aqua = new Android.Graphics.Color(0, 255, 255);

        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);

            if(this.Control != null)
            {
                this.Control.ThumbDrawable.SetColorFilter(aqua, PorterDuff.Mode.SrcAtop);
            }
        }
    }
}
