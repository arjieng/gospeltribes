﻿using System;
using Android.Widget;
using Android.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Com.Ittianyu.Bottomnavigationviewex;
using Android.Support.Design.Internal;
using Android.Content;
using System.Collections.Generic;
using gospeltribe;
using gospeltribe.Droid;
using Newtonsoft.Json.Linq;

[assembly: ExportRenderer(typeof(BottomTabbedPage), typeof(BottomTabbedPageRenderer))]
namespace gospeltribe.Droid
{
    using RelativeLayout = Android.Widget.RelativeLayout;
    public partial class BottomTabbedPageRenderer : VisualElementRenderer<BottomTabbedPage>
    {
        public static readonly Action<IMenuItem, FileImageSource, bool> DefaultMenuItemIconSetter = (menuItem, icon, selected) =>
           {
               var tabIconId = ResourceManagerEx.IdFromTitle(icon, ResourceManager.DrawableClass);
               menuItem.SetIcon(tabIconId);
           };

        public static bool ShouldUpdateSelectedIcon;
        public static Action<IMenuItem, FileImageSource, bool> MenuItemIconSetter;
        public static float? BottomBarHeight;

        RelativeLayout rootLayout;
        FrameLayout pageContainer;
        BottomNavigationViewEx bottomNav;
        readonly int barId;

        IPageController TabbedController => Element as IPageController;

        public int LastSelectedIndex { get; internal set; }

        protected readonly Dictionary<Element, BadgeView> BadgeViews = new Dictionary<Element, BadgeView>();

        public BottomTabbedPageRenderer(Context context) : base(context)
        {
            AutoPackage = false;

            barId = GenerateViewId();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<BottomTabbedPage> e)
        {
            base.OnElementChanged(e);

            Cleanup(e.OldElement);
            Cleanup(Element);

            if (e.OldElement != null)
            {
                e.OldElement.ChildAdded -= PagesChanged;
                e.OldElement.ChildRemoved -= PagesChanged;
                e.OldElement.ChildrenReordered -= PagesChanged;
            }

            if (e.NewElement == null)
            {
                return;
            }

            UpdateIgnoreContainerAreas();

            if (rootLayout == null)
            {
                SetupNativeView();
            }


            this.HandlePagesChanged();
            SwitchContent(Element.CurrentPage);

            Element.ChildAdded += PagesChanged;
            Element.ChildRemoved += PagesChanged;
            Element.ChildrenReordered += PagesChanged;


            Element.ChildAdded += OnTabAdded;
        }

        private void OnTabAdded(object sender, ElementEventArgs e)
        {
            var page = e.Element as Page;
            if (page == null)
                return;

            var tabIndex = Element.Children.IndexOf(page);
            AddTabBadge(tabIndex);
        }

        void PagesChanged(object sender, EventArgs e)
        {
            this.HandlePagesChanged();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == nameof(TabbedPage.CurrentPage))
            {
                SwitchContent(Element.CurrentPage);
            }

            if (e.PropertyName.Equals("Renderer") || e.PropertyName.Equals("BindingContext"))
            {
                for (var i = 0; i < bottomNav.ItemCount; i++)
                {
                    AddTabBadge(i);
                }
            }
        }

        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();
            TabbedController?.SendAppearing();
        }

        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();
            TabbedController?.SendDisappearing();
        }

        protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
        {
            var width = right - left;
            var height = bottom - top;

            base.OnLayout(changed, left, top, right, bottom);

            if (width <= 0 || height <= 0)
            {
                return;
            }

            this.Layout(width, height);
        }

        void AddTabBadge(int tabIndex)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var element = Element.Children[tabIndex];
                var view = (ViewGroup)((ViewGroup)bottomNav.GetChildAt(0)).GetChildAt(tabIndex);
                var badgeView = (view as ViewGroup)?.FindChildOfType<BadgeView>();
                if (badgeView == null)
                {
                    var imageView = (view as ViewGroup)?.FindChildOfType<ImageView>();
                    var badgeTarget = imageView?.Drawable != null
                                                ? (Android.Views.View)imageView
                                                : (view as ViewGroup)?.FindChildOfType<TextView>();

                    badgeView = new BadgeView(Context, badgeTarget);
                }
                BadgeViews[element] = badgeView;

                //get text
                var badgeText = TabBadge.GetBadgeText(element);
                badgeView.Text = badgeText;

                // set color if not default
                var tabColor = TabBadge.GetBadgeColor(element);
                if (tabColor != Color.Default)
                {
                    badgeView.BadgeColor = tabColor.ToAndroid();
                }

                // set text color if not default
                var tabTextColor = TabBadge.GetBadgeTextColor(element);
                if (tabTextColor != Color.Default)
                {
                    badgeView.TextColor = tabTextColor.ToAndroid();
                }

                // set font if not default
                var font = TabBadge.GetBadgeFont(element);
                if (font != Font.Default)
                {
                    badgeView.Typeface = font.ToTypeface();
                }

                var margin = TabBadge.GetBadgeMargin(element);
                badgeView.SetMargins((float)margin.Left, (float)margin.Top, (float)margin.Right, (float)margin.Bottom);

                // set position
                badgeView.Postion = TabBadge.GetBadgePosition(element);

                element.PropertyChanged += OnTabbedPagePropertyChanged;
            });
        }

        private void OnTabbedPagePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && Element != null)
            {
                Element.ChildAdded -= PagesChanged;
                Element.ChildRemoved -= PagesChanged;
                Element.ChildrenReordered -= PagesChanged;

                if (rootLayout != null)
                {
                    //TODO Cleanup
                    RemoveAllViews();
                    foreach (Page pageToRemove in Element.Children)
                    {
                        IVisualElementRenderer pageRenderer = Platform.GetRenderer(pageToRemove);

                        if (pageRenderer != null)
                        {
                            pageRenderer.ViewGroup.RemoveFromParent();
                            pageRenderer.Dispose();
                        }
                    }

                    if (bottomNav != null)
                    {
                        bottomNav.SetOnNavigationItemSelectedListener(null);
                        bottomNav.Dispose();
                        bottomNav = null;
                    }
                    rootLayout.Dispose();
                    rootLayout = null;
                }
            }
            Cleanup(Element);
            base.Dispose(disposing);
        }

        internal void SetupNativeView()
        {
            rootLayout = this.CreateRoot(barId, GenerateViewId(), out pageContainer);

            AddView(rootLayout);
        }

        void SwitchContent(Page page)
        {
            this.ChangePage(pageContainer, page);
        }

        void UpdateIgnoreContainerAreas()
        {
            foreach (IPageController child in Element.Children)
            {
                child.IgnoresContainerArea = false;
            }
        }

        private void Cleanup(TabbedPage page)
        {
            if (page == null)
            {
                return;
            }

            foreach (var tab in page.Children)
            {
                tab.PropertyChanged -= OnTabbedPagePropertyChanged;
            }

            page.ChildRemoved -= OnTabRemoved;
            page.ChildAdded -= OnTabAdded;

            BadgeViews.Clear();
        }

        private void OnTabRemoved(object sender, ElementEventArgs e)
        {
            e.Element.PropertyChanged -= OnTabbedPagePropertyChanged;
            BadgeViews.Remove(e.Element);
        }
    }
}
