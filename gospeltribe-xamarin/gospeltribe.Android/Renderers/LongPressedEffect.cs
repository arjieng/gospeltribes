﻿using System;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using gospeltribe.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.Linq;
using static Android.Views.View;

[assembly: ResolutionGroupName("GT")]
[assembly: ExportEffect(typeof(LongPressedEffect), "LongPressedEffect")]
namespace gospeltribe.Droid
{
    public class LongPressedEffect : PlatformEffect
    {
        bool _attached;
        gospeltribe.LongPressedEffect effect;
        FancyGestureListener _listener;
        GestureDetector _detector;

        public LongPressedEffect(){
            _listener = new FancyGestureListener();
            _detector = new GestureDetector(_listener);
        }

        protected override void OnAttached()
        {
            if (!_attached)
            {
                if (Control != null)
                {
                    Control.GenericMotion += HandleGenericMotion;
                    Control.Touch += HandleTouch;
                }
                else
                {
                    Container.GenericMotion += HandleGenericMotion;
                    Container.Touch += HandleTouch;
                }

                _attached = true;
                try
                {
                    effect = (gospeltribe.LongPressedEffect)Element.Effects.FirstOrDefault(e => e is gospeltribe.LongPressedEffect);
                    _listener.tapLongTap = effect;
                    _listener.element = Element;

                }
                catch (Exception ex)
                {
                    App.Log("Cannot set property on attached control. Error: " + ex.Message);
                }
            }
        }


        void HandleTouch(object sender, TouchEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

        void HandleGenericMotion(object sender, GenericMotionEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

        protected override void OnDetached()
        {
            if (_attached)
            {
                if (Control != null)
                {
                    Control.GenericMotion -= HandleGenericMotion;
                    Control.Touch -= HandleTouch;
                }
                else
                {
                    Container.GenericMotion -= HandleGenericMotion;
                    Container.Touch -= HandleTouch;
                }
                _attached = false;
            }
        }
    }

    public class FancyGestureListener : GestureDetector.SimpleOnGestureListener
    {
        public gospeltribe.LongPressedEffect tapLongTap { get; set; }
        public Element element { get; set; }

        public override void OnLongPress(MotionEvent e)
        {
            //Console.WriteLine("OnLongPress");
            tapLongTap.SendLongTapEvent(element);
            base.OnLongPress(e);
        }

        public override bool OnDoubleTap(MotionEvent e)
        {
            //Console.WriteLine("OnDoubleTap");
            return base.OnDoubleTap(e);
        }

        public override bool OnDoubleTapEvent(MotionEvent e)
        {
            //Console.WriteLine("OnDoubleTapEvent");
            return base.OnDoubleTapEvent(e);
        }

        public override bool OnSingleTapUp(MotionEvent e)
        {
            //Console.WriteLine("OnSingleTapUp");
            return base.OnSingleTapUp(e);
        }

        public override bool OnDown(MotionEvent e)
        {
            //Console.WriteLine("OnDown");
            return base.OnDown(e);
        }

        public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            //Console.WriteLine("OnFling");
            return base.OnFling(e1, e2, velocityX, velocityY);
        }

        public override bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            //Console.WriteLine("OnScroll");
            return base.OnScroll(e1, e2, distanceX, distanceY);
        }

        public override void OnShowPress(MotionEvent e)
        {
            //Console.WriteLine("OnShowPress");
            base.OnShowPress(e);
        }

        public override bool OnSingleTapConfirmed(MotionEvent e)
        {
            //Console.WriteLine("OnSingleTapConfirmed");
            tapLongTap.SendTapEvent(element);
            return base.OnSingleTapConfirmed(e);
        }
    }

}