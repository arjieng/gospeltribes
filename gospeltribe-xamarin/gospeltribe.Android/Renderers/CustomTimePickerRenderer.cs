﻿using System;
using Android.Content;
using gospeltribe;
using gospeltribe.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace gospeltribe.Droid.Renderers
{
    public class CustomTimePickerRenderer : TimePickerRenderer
    {
        public CustomTimePickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);
            if (this.Control != null)
            {
                this.Control.SetPadding(0, 0, 0, 0);
                this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
        }
    }
}
