﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public class DataClass : INotifyPropertyChanged
    {
        static DataClass dataClass;

        public static DataClass GetInstance{
            get{
                if(dataClass == null){
                    dataClass = new DataClass();
                }
                return dataClass;
            }
        }


        UserModel _user;
        public UserModel user
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("user") && _user == null)
                {
                    JObject json = JObject.Parse(Application.Current.Properties["user"].ToString());
                    _user = JsonConvert.DeserializeObject<UserModel>(json.ToString());
                }
                return _user;
            }
            set
            {
                _user = value;
                Application.Current.Properties["user"] = JsonConvert.SerializeObject(_user);
                Application.Current.SavePropertiesAsync();
                OnPropertyChanged();
            }
        }

        string _group_code;
        public string GroupCode{
            get { 
                if (Application.Current.Properties.ContainsKey("group_code") && string.IsNullOrEmpty(_group_code))
                {
                    _group_code = Application.Current.Properties["group_code"].ToString();
                }
                return _group_code;
            }
            set { 
                _group_code = value;
                Application.Current.Properties["group_code"] = _group_code;
                Application.Current.SavePropertiesAsync();
                OnPropertyChanged();
            }
        }


        string _group_id;
        public string GroupId
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("group_id") && string.IsNullOrEmpty(_group_id))
                {
                    _group_id = Application.Current.Properties["group_id"].ToString();
                }
                return _group_id;
            }
            set
            {
                _group_id = value;
                Application.Current.Properties["group_id"] = _group_id;
                Application.Current.SavePropertiesAsync();
                OnPropertyChanged();
            }
        }

        string _token;
        public string token
        {
            get
            {
                OnPropertyChanged(nameof(token));
                if (Application.Current.Properties.ContainsKey("token") && string.IsNullOrEmpty(_token))
                {
                    _token = Application.Current.Properties["token"]?.ToString();
                }
                return _token;
            }
            set
            {
                _token = value;
                Application.Current.Properties["token"] = _token;
                Application.Current.SavePropertiesAsync();
                OnPropertyChanged();
            }
        }

        string _clientId { get; set; }
        public string clientId
        {
            set
            {
                _clientId = value;
                Application.Current.Properties["client_id"] = _clientId;
                Application.Current.SavePropertiesAsync();
            }
            get
            {
                if (String.IsNullOrEmpty(_clientId) && Application.Current.Properties.ContainsKey("client_id"))
                {
                    _clientId = Application.Current.Properties["client_id"].ToString();
                }
                return _clientId;
            }
        }

        string _uid { get; set; }
        public string uid
        {
            set
            {
                _uid = value;
                Application.Current.Properties["uid"] = _uid;
                Application.Current.SavePropertiesAsync();
            }
            get
            {
                if (String.IsNullOrEmpty(_uid) && Application.Current.Properties.ContainsKey("uid"))
                {
                    _uid = Application.Current.Properties["uid"].ToString();
                }
                return _uid;
            }
        }

        public async void Logout()
        {
            Application.Current.Properties.Remove("token");
            await Save();
        }

        public async Task Save()
        {
            await Application.Current.SavePropertiesAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string name = "")
        {
            if (PropertyChanged == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
