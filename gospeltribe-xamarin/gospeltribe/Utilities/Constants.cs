﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace gospeltribe
{
    public class Constants
    {
        //MEASUREMENTS
        public static readonly double NAVIGATION_HEIGHT = Device.RuntimePlatform == Device.iOS ? 54.ScaleHeight() : 46.ScaleHeight();

        //COLORS
        public static readonly Color THEME_COLOR = Color.FromHex("#00feff");
        public static readonly Color LABEL_GRAY = Color.FromHex("#C4C3C8");
        public static readonly Color LABEL_BLACK = Color.FromHex("#1D1D26");
        public static readonly Color BOXVIEW_DIVIDERCOLOR = Color.FromHex("#C4C3C8");

        //CONVERSATION FIRSTLOAD IOS
        public static ObservableCollection<string> groupNames = new ObservableCollection<string>();

        //FONT FAMILIES
        public static readonly string GOTHAM = Device.RuntimePlatform == Device.iOS ? "Gotham-Black" : "Fonts2/Gotham-Black.otf#Gotham Black";
        public static readonly string GOTHAM_BOLD = Device.RuntimePlatform == Device.iOS ? "Gotham-Bold" : "Fonts2/Gotham-Bold.otf#Gotham Bold";
        public static readonly string GOTHAM_LIGHT = Device.RuntimePlatform == Device.iOS ? "Gotham-Light" : "Fonts2/Gotham-Light.otf#Gotham Light";
        public static readonly string GOTHAM_MEDIUM = Device.RuntimePlatform == Device.iOS ? "Gotham-Medium" : "Fonts2/Gotham-Medium.otf#Gotham Medium";

        //Navigate from
        public static bool IS_FROM_PROFILE = false;

        //API

        public static readonly string IP_ADDRESS = "api.gospeltribes.com";
        public static readonly string domain = "http://" + IP_ADDRESS;
        public static readonly string orig = domain;


        public static readonly string host = orig + "/api";
        public static readonly string SIGN_UP = host + "/v1/auth";

        public static readonly string CHECK_GROUP = host + "/check_group";
        public static readonly string SIGN_IN = host + "/v1/auth/sign_in";

        public static readonly string SEND_MESSAGE = host + "/send_message";
        public static readonly string GET_MESSAGE = host + "/get_message";
        public static readonly string SET_READ = host + "/set_read";
        public static string REMOVE_PROFILE(int id){
            return host + "/remove_profile?user_id=" + id + "&group_id=" + DataClass.GetInstance.GroupId;
        }

        public static string FORGOT_PASSWORD = orig + "/forgot_password";
        public static string RESET_PASSWORD = host + "/reset_password";
        public static string RETRIEVE_PASSWORD = host + "/retrieve_username";


        public static readonly string REMOVE_EVENT = host + "/remove_event";
        public static readonly string REMOVE_GROUP_IMAGE = host + "/remove_group_image";

        public static readonly string EDIT_PROFILE_MAIN = host + "/update_profile";
        public static readonly string EDIT_PROFILE_ACCOUNT = host + "/update_account";
        public static readonly string UPLOAD_USER_AVATAR = host + "/upload_user_avatar";

        public static readonly string CREATE_PRAYER = host + "/create_prayer";
        public static readonly string UPDATE_PRAYER = host + "/update_prayer";
        public static readonly string ADD_PRAYER_COMMENT = host + "/add_prayer_comment";

        public static string ADD_STUDY_MODULE_COMMENT = host + "/add_study_module_comment";
        public static string GET_STUDY_MODULE_COMMENT = host + "/get_study_module_comment?study_module_id=";
        public static string GET_STUDY_MODULE = host + "/get_study_module?group_id=";
        public static string REMOVE_STUDY_MODULE = host + "/delete_study_module?group_id=";

        public static string STUDY_MODULE = host + "/study_module";
        public static string EDIT_STUDY_MODULE = host + "/edit_study_module";

        public static string GET_ATTENDEES = host + "/attendees?event_id=";

        public static string GROUP_MEDIUM = host + "/group_medium?group_id=";
        public static string ADD_EVENT = host + "/add_event";
        public static string UPDATE_EVENT = host + "/update_event";

        public static string REMOVE_MESSAGE = host + "/remove_message?message_id=";
        public static string EDIT_MESSAGE = host + "/edit_message";

        public static string ADD_GROUP = host + "/add_group?group_code=";
        public static string ADD_GROUP_MEDIA = host + "/add_group_image";

        public static string DELETE_PRAYER = host + "/delete_prayer?prayer_id=";
        public static string DELETE_PRAYER_COMMENT = host + "/delete_prayer_comment?prayer_comment_id=";
        public static string DELETE_STUDY_MODULE_COMMENT = host + "/delete_study_module_comment";
        public static string CHANGE_CHATROOM_DETAILS = host + "/change_chatroom_details";

        public static string GET_BADGES = host + "/get_unread_badges";
        public static string SET_BADGES = host + "/set_unread_badges";
        public static string SET_READ_BADGES = host + "/set_read_badges";

        public static string PUSH_NOTIF_SETTING_CHANGE = host + "/update_user_notification_settings";

        public static string HIDE_PRAYER(int id)
        {
            return host + "/hide_prayer?prayer_id=";
        }

        public static string RENAME_GROUP(string name)
        {
            return host + "/rename_group?group_id=" + DataClass.GetInstance.GroupId + "&group_name=" + name;
        }

        public static string JOIN_EVENT(int id)
        {
            return host + "/add_attendee?event_id=" + id + "&user_id=" + DataClass.GetInstance.user.id;
        }

        public static string UNJOIN_EVENT(int id)
        {
            return host + "/remove_attendee?event_id=" + id + "&user_id=" + DataClass.GetInstance.user.id;
        }

        public static string GET_ALL_EVENTS()
        {
            return host + "/all_events?group_id=" + DataClass.GetInstance.GroupId + "&user_id=" + DataClass.GetInstance.user.id;
        }

        public static string UPDATE_UDID(string udid, string last_token = "")
        {
            //iOS = 1
            //Android = 2
            var phone_type = Device.RuntimePlatform.Equals(Device.iOS) ? "1" : "2";
            return host + "/update_udid?user_id=" + DataClass.GetInstance.user.id + "&udid=" + udid + "&group_id=" + (string.IsNullOrEmpty(DataClass.GetInstance.GroupId) ? "" : DataClass.GetInstance.GroupId) + (string.IsNullOrEmpty(last_token) ? "" : ("&last_token=" + last_token)) + $"&phone_type={phone_type}";
        }

        public static string GET_USER_PRAYERS(int id)
        {
            return host + "/prayers?user_id=" + id + "&group_id=" + DataClass.GetInstance.GroupId;
        }

        public static string GET_PRAYER_COMMENTS(int id)
        {
            return host + "/prayer_comments?prayer_id=" + id;
        }

        public static string GET_CHATROOMS()
        {
            return host + "/get_chatrooms?group_code=" + (string.IsNullOrEmpty(DataClass.GetInstance.GroupCode) ? "" : DataClass.GetInstance.GroupCode) + "&gender=" + (string.IsNullOrEmpty(DataClass.GetInstance.user.gender) ? "" : DataClass.GetInstance.user.gender);
        }

        public static string GET_ALL_MEMBERS()
        {
            return host + "/group/" + (string.IsNullOrEmpty(DataClass.GetInstance.GroupCode) ? "" : DataClass.GetInstance.GroupCode) + "/group_members";
        }
        public static string GET_ALL_PRAYERS()
        {
            return host + "/get_all_prayers?group_id=" + (string.IsNullOrEmpty(DataClass.GetInstance.GroupId) ? "" : DataClass.GetInstance.GroupId);
        }

        public static string GAnsweredP()
        {
            return host + "/answered_prayers?group_id=" + DataClass.GetInstance.GroupId;
        }

        public static string GUnansweredP()
        {
            return host + "/unanswered_prayers?group_id=" + DataClass.GetInstance.GroupId;
        }

        public static string GET_USER_INFO(int id)
        {
            return host + "/get_user_info?user_id=" + id;
        }

        public static string GET_EVENT_INFOS = host + "/get_event?event_id=";
        public static string GET_USER_MEDIA(int id, string fp)
        {
            return host + "/user_images?group_id=" + DataClass.GetInstance.GroupId + "&user_id=" + id + "&from_profile=" + fp;
        }

        public static string LIKE_MESSAGE = host + "/like_message?chatroom_message_id=";
        public static string UNLIKE_MESSAGE(int cmid){
            return host + "/unlike_message?chatroom_message_id=" + cmid + "&user_id=" + DataClass.GetInstance.user.id;
        }


        //SOCKET SUBSCRIPTIONS
        public static string INBOX_PAGE()
        {
            return "/group/" + DataClass.GetInstance.GroupId + "/chatrooms";
        }

        public static string CHATROOMS(int id)
        {
            return "/group/" + DataClass.GetInstance.GroupId + "/chatrooms/" + id;
        }
    }
}
