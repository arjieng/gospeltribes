﻿using System;
using System.IO;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public class ImageCropper
    {
        public static ImageCropper Current { get; set; }

        public ImageCropper()
        {
            Current = this;
        }

        public enum CropShapeType
        {
            Rectangle,
            Oval
        };

        public CropShapeType CropShape { get; set; } = CropShapeType.Rectangle;

        public int AspectRatioX { get; set; } = 0;

        public int AspectRatioY { get; set; } = 0;

        public string PageTitle { get; set; } = null;

        public string SelectSourceTitle { get; set; } = "Select source";

        public string TakePhotoTitle { get; set; } = "Take Photo";

        public string PhotoLibraryTitle { get; set; } = "Photo Library";
        
        public Action<string> Success { get; set; }

        public Action Faiure { get; set; }

        public async void Show(Page page, string imageFile = null)
        {
            if (imageFile == null)
            {
                await CrossMedia.Current.Initialize();

                MediaFile file;

                string action = await page.DisplayActionSheet(SelectSourceTitle, "Cancel", null, TakePhotoTitle, PhotoLibraryTitle);
                if (action == TakePhotoTitle)
                {
                    var status = await PermissionHelper.CheckPermissions(Permission.Camera);
                    if (status == PermissionStatus.Granted)
                    {
                        var status2 = await PermissionHelper.CheckPermissions(Permission.Photos);
                        if (status2 == PermissionStatus.Granted)
                        {
                            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                            {
                                await page.DisplayAlert("No Camera", ":( No camera available.", "OK");
                                Faiure?.Invoke();
                                return;
                            }

                            file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions() { SaveToAlbum = true, PhotoSize = PhotoSize.Custom, CustomPhotoSize = 50 });
                        }
                        else
                        {
                            file = null;
                            Application.Current?.MainPage?.DisplayAlert("Cannot take photo", "", "OK");
                        }
                    }
                    else
                    {
                        file = null;
                        Application.Current?.MainPage?.DisplayAlert("Cannot take photo", "", "OK");
                    }

                }
                else if (action == PhotoLibraryTitle)
                {
                    var status = await PermissionHelper.CheckPermissions(Permission.Photos);
                    if (status == PermissionStatus.Granted)
                    {
                        if (!CrossMedia.Current.IsPickPhotoSupported)
                        {
                            await page.DisplayAlert("Error", "This device is not supported to pick photo.", "OK");
                            Faiure?.Invoke();
                            return;
                        }

                        file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions { PhotoSize = PhotoSize.Custom, CustomPhotoSize = 50 });
                    }
                    else
                    {
                        file = null;
                        Application.Current?.MainPage?.DisplayAlert("Cannot select photo", "", "OK");
                    }
                }
                else
                {
                    Faiure?.Invoke();
                    return;
                }

                if (file == null)
                {
                    Faiure?.Invoke();
                    return;
                }

                imageFile = file.Path;
            }

            DependencyService.Get<IImageCropperWrapper>().ShowFromFile(this, imageFile);
        }
    }
}
