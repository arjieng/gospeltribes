﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace gospeltribe
{
    public class FileReader : IFileReader
    {
        WeakReference<IFileConnector> _fileReaderDelegate;
        public IFileConnector FileReaderDelegate{
            get{
                IFileConnector fileReaderDelegate;
                return _fileReaderDelegate.TryGetTarget(out fileReaderDelegate) ? fileReaderDelegate : null;
            }
            set{
                _fileReaderDelegate = new WeakReference<IFileConnector>(value);
            }
        }

        public async Task ReadFile(string fileName, CancellationToken ct)
        {
            var assembly = typeof(FileReader).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("gospeltribe.Files." + fileName);
            using(var reader = new StreamReader(stream)){
                var json = await reader.ReadToEndAsync();
                FileReaderDelegate?.ReceiveJSONData(JObject.Parse(json), ct);
            }
        }

        public Task WriteFile(string fileName, string json, CancellationToken ct)
        {
            throw new NotImplementedException();
        }
    }
}
