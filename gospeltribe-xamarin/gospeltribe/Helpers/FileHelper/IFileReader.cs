﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace gospeltribe
{
    public interface IFileReader
    {
        Task WriteFile(string fileName, string json, CancellationToken ct);
        Task ReadFile(string fileName, CancellationToken ct);
    }
}
