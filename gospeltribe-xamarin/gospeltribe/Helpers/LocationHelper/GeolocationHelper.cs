﻿using System;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public class GeolocationHelper
    {
        public bool IsLocationAvailable(){
            if (!CrossGeolocator.IsSupported)
                return false;
            return CrossGeolocator.Current.IsGeolocationAvailable;
        }

        public bool IsLocationEnabled(){
            return CrossGeolocator.Current.IsGeolocationEnabled;
        }

        public async Task<Position> GetCurrentLocation(){
            Position position = null;

            var status = await PermissionHelper.CheckPermissions(Permission.Location);
            if (status == PermissionStatus.Granted)
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;

                    position = await locator.GetLastKnownLocationAsync();

                    if (position != null)
                    {
                        return position;
                    }

                    if (!locator.IsGeolocationEnabled || !locator.IsGeolocationAvailable)
                    {
                        return null;
                    }

                    position = await locator.GetPositionAsync(TimeSpan.FromSeconds(2), null, true);

                    if (position != null)
                    {
                        return position;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    App.Log(ex.ToString());
                    return null;
                }
            }
            else
            {
                Application.Current?.MainPage?.DisplayAlert("Cannot fetch location", "", "OK");
                return null;
            }
        }
    }
}
