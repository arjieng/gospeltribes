﻿using System;
using System.Collections.Generic;

namespace gospeltribe
{
    public interface IFayeExtension
    {
        void AddOutgoingExtension(ref Dictionary<string, object> message);
    }
}
