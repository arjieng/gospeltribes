﻿using System;
namespace gospeltribe
{
    public interface IImageCropperWrapper
    {
        void ShowFromFile(ImageCropper imageCropper, string imageFile);
    }
}
