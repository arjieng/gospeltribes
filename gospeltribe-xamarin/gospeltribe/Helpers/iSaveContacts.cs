using System;
namespace gospeltribe
{
    public interface iSaveContacts
    {
        void SaveContact(string name, string number, ProfilePage profilePage);
    }
}
