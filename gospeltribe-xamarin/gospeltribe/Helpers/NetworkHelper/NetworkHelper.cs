﻿using System;
using System.Threading.Tasks;
using Plugin.Connectivity;

namespace gospeltribe
{
    public class NetworkHelper : INetworkHelper
    {
        public NetworkHelper()
        {
        }

        public bool HasInternet()
        {
            if (!CrossConnectivity.IsSupported)
                return true;
#if DEBUG
            if (Constants.IP_ADDRESS.Equals("107.172.143.213"))
            {
                return CrossConnectivity.Current.IsConnected;
            }
            else
            {
                return true;
            }
#else
            return CrossConnectivity.Current.IsConnected;
#endif
        }

        public async Task<bool> IsHostReachable()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }



            //var reachable = await CrossConnectivity.Current.IsReachable("http://192.168.2.123:3000");
            //return reachable;
            //return true;
            //var reachable = await CrossConnectivity.Current.IsRemoteReachable(Constants.domain, int.Parse(Constants.port));
            //return reachable;
            return true;

        }
    }
}
