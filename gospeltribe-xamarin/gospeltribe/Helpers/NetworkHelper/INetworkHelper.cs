﻿using System;
using System.Threading.Tasks;

namespace gospeltribe
{
    public interface INetworkHelper
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}
