﻿using System;
using System.Globalization;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace gospeltribe
{
    public static class ValidationHelper
    {
        static bool invalid;
        public static bool IsValidEmail(this string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid email format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }


        public static bool IsValidUsername(string username){
            if (username.Length >= 6)
                return true;
            else
                return false;
        }

        public static bool IsValidPassword(string password){
            if (password.Length >= 6)
                return true;
            else
                return false;
        }

        public static bool IsValidName(string name){
            return Regex.IsMatch(name, @"^[A-Za-z]+[\s][A-Za-z]+[.][A-Za-z]+$");
        }

        public static string TruncateText(this string value, int maxChars){
            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
        }

        public static string MakeUnicode(this string num)
        {
            string numbers = "(";
            foreach (var c in num)
            {
                //numbers += c;
                switch (c)
                { 
                    case '0':
                        //numbers += "\u2070";
                        numbers += "0";
                        break;
                    case '1':
                        //numbers += "\u00B9";
                        numbers += "1";
                        break;
                    case '2':
                        //numbers += "\u00B2";
                        numbers += "2";
                        break;
                    case '3':
                        //numbers += "\u00B3";
                        numbers += "3";
                        break;
                    case '4':
                        //numbers += "\u2074";
                        numbers += "4";
                        break;
                    case '5':
                        //numbers += "\u2075";
                        numbers += "5";
                        break;
                    case '6':
                        //numbers += "\u2076";
                        numbers += "6";
                        break;
                    case '7':
                        //numbers += "\u2077";
                        //numbers += "⁷";
                        numbers += "7";
                        break;
                    case '8':
                        //numbers += "\u2078";
                        numbers += "8";
                        break;
                    case '9':
                        //numbers += "\u2079";
                        numbers += "9";
                        break;
                }
            }
            return numbers + ")";
        }
    }
}
