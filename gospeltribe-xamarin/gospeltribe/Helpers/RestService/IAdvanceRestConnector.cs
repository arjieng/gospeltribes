﻿using System;
using Newtonsoft.Json.Linq;

namespace gospeltribe
{
    public interface IAdvanceRestConnector
    {
        void ReceiveJSONData(JObject jsonData, int wsType);
        void ReceiveTimeoutError(string title, string error, int wsType);
    }
}
