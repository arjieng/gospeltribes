﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace gospeltribe
{
    public class AdvanceRestService
    {
        NetworkHelper networkHelper;
        WeakReference<IAdvanceRestConnector> _webServiceDelegate;

        public AdvanceRestService(){
            networkHelper = new NetworkHelper();
        }


        public IAdvanceRestConnector WebServiceDelegate
        {
            get
            {
                IAdvanceRestConnector webServiceDelegate;
                return _webServiceDelegate.TryGetTarget(out webServiceDelegate) ? webServiceDelegate : null;
            }

            set
            {
                _webServiceDelegate = new WeakReference<IAdvanceRestConnector>(value);
            }
        }

        public async Task GetRequest(string url, CancellationToken ct, int wsType, int timeout = 100)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var handler = new TimeoutHandler
                    {
                        DefaultTimeout = TimeSpan.FromSeconds(100),
                        InnerHandler = new HttpClientHandler()
                    };

                    using (var client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.MaxResponseContentBufferSize = 256000;
                        client.DefaultRequestHeaders.Add("access-token", DataClass.GetInstance.token);
                        client.DefaultRequestHeaders.Add("uid", DataClass.GetInstance.uid);
                        client.DefaultRequestHeaders.Add("client", DataClass.GetInstance.clientId);
                        client.Timeout = Timeout.InfiniteTimeSpan;

                        var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));

                        request.SetTimeout(TimeSpan.FromSeconds(timeout)); //Per-request timeout

                        using (var response = await client.SendAsync(request, ct))
                        {
                            await RequestAsync(response, ct, wsType);
                        }
                    }
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable", "Please try again later.", wsType);
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection and try again!", wsType);
            }
        }

        public async Task PostRequestAsync(string url, string dictionary, CancellationToken ct, int wsType, int timeout = 100)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var handler = new TimeoutHandler
                    {
                        DefaultTimeout = TimeSpan.FromSeconds(100),
                        InnerHandler = new HttpClientHandler()
                    };

                    using (var client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.MaxResponseContentBufferSize = 256000;
                        client.DefaultRequestHeaders.Add("access-token", DataClass.GetInstance.token);
                        client.DefaultRequestHeaders.Add("uid", DataClass.GetInstance.uid);
                        client.DefaultRequestHeaders.Add("client", DataClass.GetInstance.clientId);
                        client.Timeout = Timeout.InfiniteTimeSpan;

                        var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url));

                        request.SetTimeout(TimeSpan.FromSeconds(timeout)); //Per-request timeout

                        request.Content = new StringContent(dictionary, Encoding.UTF8, "application/json");

                        using (var response = await client.SendAsync(request, ct))
                        {
                            await RequestAsync(response, ct, wsType);
                        }
                    }
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable", "Please try again later.", wsType);
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection and try again!", wsType);
            }
        }

        public async Task MultiPartDataContentAsync(string url, string dictionary, CancellationToken ct, System.IO.Stream image = null, string key = "user", int wsType = 0, Action<long, long> action = null, bool is_video = false)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var handler = new TimeoutHandler
                    {
                        DefaultTimeout = TimeSpan.FromSeconds(100),
                        InnerHandler = new HttpClientHandler()
                    };

                    using (var client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.MaxResponseContentBufferSize = 256000;
                        client.DefaultRequestHeaders.Add("access-token", DataClass.GetInstance.token);
                        client.DefaultRequestHeaders.Add("uid", DataClass.GetInstance.uid);
                        client.DefaultRequestHeaders.Add("client", DataClass.GetInstance.clientId);
                        client.Timeout = Timeout.InfiniteTimeSpan;

                        var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url));
                        request.SetTimeout(TimeSpan.FromSeconds(100));

                        MultipartFormDataContent multipartFormData = new MultipartFormDataContent();
                        JToken json = JObject.Parse(dictionary)[key];
                        foreach (var obj in json)
                        {
                            string[] jsonKey = obj.Path.Split('.');
                            if ((jsonKey[1].ToString() == "image" || jsonKey[1].ToString() == "avatar" || jsonKey[1].ToString() == "files" || jsonKey[1].ToString().Equals("chatroom_image")) && image != null)
                            {
                                string keyName = "\"" + jsonKey[0] + "[" + jsonKey[1] + "]\"";
                                StreamContent content = new StreamContent(image);
                                content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = (jsonKey[1].ToString() == "files" ? "\"" + obj.First + "\"" : "\"upload-image." + (is_video ? "mp4" : "jpeg") + "\""), Name = keyName };
                                multipartFormData.Add(content);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(obj.First.ToString()))
                                {
                                    string keyName = "\"" + jsonKey[0] + "[" + jsonKey[1] + "]" + "\"";
                                    StringContent content = new StringContent(obj.First.ToString(), Encoding.UTF8);
                                    content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
                                    multipartFormData.Add(content);
                                }
                            }
                        }

                        var progressContent = new ProgressableStreamContent(multipartFormData, 256000, (arg1, arg2) =>
                        {
                            //(100/(arg2 / arg1))
                            action?.Invoke(arg1, arg2);
                        });

                        using (var response = await client.PostAsync(url, progressContent, ct))
                        {
                            await RequestAsync(response, ct, wsType);
                        }
                    }
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable", "Please try again later.", wsType);
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection and try again!", wsType);
            }

        }

        async Task RequestAsync(HttpResponseMessage response, CancellationToken ct, int wsType)
        {
            App.statusCode = response.StatusCode;
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                // ===========
                if (response.Headers.Contains("access-token"))
                {
                    var token = response.Headers.GetValues("access-token").FirstOrDefault();
                    DataClass.GetInstance.token = token;
                    App.Log("Token: " + DataClass.GetInstance.token);
                }

                if (response.Headers.Contains("client"))
                {
                    var clientId = response.Headers.GetValues("client").FirstOrDefault();
                    DataClass.GetInstance.clientId = clientId;
                    App.Log("ClientId: " + DataClass.GetInstance.clientId);
                }

                if (response.Headers.Contains("uid"))
                {
                    var uid = response.Headers.GetValues("uid").FirstOrDefault();
                    DataClass.GetInstance.uid = uid;
                    App.Log("Uid: " + DataClass.GetInstance.uid);
                }
                // ===========

                WebServiceDelegate?.ReceiveJSONData(JObject.Parse(result), wsType);
            }
            else
            {
                App.Log($"status code is : {response.StatusCode}");

                var errorResult = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var error_result = JObject.Parse(errorResult);
                string errors = string.Empty;

                if (error_result["errors"] != null)
                {
                    var list = JsonConvert.DeserializeObject<List<string>>(error_result["errors"].ToString());
                    errors = string.Join("\n", list);
                }
                else
                    errors += error_result["error"].ToString();

                WebServiceDelegate?.ReceiveTimeoutError("Error!", errors, wsType);
            }
        }
    }
}
