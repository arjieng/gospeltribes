﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Interop;

namespace gospeltribe
{
    public class BibleHelper
    {
        SQLiteAsyncConnection dbConn;
        int version;
        public BibleHelper(ISQLitePlatform sqlitePlatform, string dbPath, int version)
        {
            this.version = version;
            if (dbConn == null)
            {
                var connectionFunc = new Func<SQLiteConnectionWithLock>(() =>
                    new SQLiteConnectionWithLock
                    (
                        sqlitePlatform,
                        new SQLiteConnectionString(dbPath, false)
                    ));

                dbConn = new SQLiteAsyncConnection(connectionFunc);

                switch(version){
                    case 1:
                        dbConn.CreateTableAsync<esv_books>();
                        dbConn.CreateTableAsync<esv_verses>();
                        break;
                    case 2:
                        dbConn.CreateTableAsync<hcsb_books>();
                        dbConn.CreateTableAsync<hcsb_verses>();
                        break;
                    case 3:
                        dbConn.CreateTableAsync<kjv_books>();
                        dbConn.CreateTableAsync<kjv_verses>();
                        break;
                    case 4:
                        dbConn.CreateTableAsync<nkjv_books>();
                        dbConn.CreateTableAsync<nkjv_verses>();
                        break;
                    case 5:
                        dbConn.CreateTableAsync<nlt_books>();
                        dbConn.CreateTableAsync<nlt_verses>();
                        break;
                    case 6:
                        dbConn.CreateTableAsync<niv_books>();
                        dbConn.CreateTableAsync<niv_verses>();
                        break;
                }
            }
        }

        public async Task<List<esv_books>> GetAllEsvBooks()
        {
            return await dbConn.Table<esv_books>().ToListAsync();
        }

        public Task<List<esv_verses>> GetEsvVerses(string osis, int chapter)
        {
            return dbConn.QueryAsync<esv_verses>("SELECT * FROM [esv_verses] WHERE [book] = '" + osis + "' AND [verse] LIKE '" + chapter + ".%'");
        }

        public Task<List<esv_verses>> GetEsvMultiVerse(int from, int to){
            return dbConn.QueryAsync<esv_verses>("SELECT * FROM [esv_verses] WHERE [id] BETWEEN " + from + " AND " + to);
        }

        public async Task<List<hcsb_books>> GetAllHcsbBooks()
        {
            return await dbConn.Table<hcsb_books>().ToListAsync();
        }

        public Task<List<hcsb_verses>> GetHcsbVerses(string osis, int chapter)
        {
            return dbConn.QueryAsync<hcsb_verses>("SELECT * FROM [hcsb_verses] WHERE [book] = '" + osis + "' AND [verse] LIKE '" + chapter + ".%'");
        }

        public async Task<List<kjv_books>> GetAllKjvBooks()
        {
            return await dbConn.Table<kjv_books>().ToListAsync();
        }

        public Task<List<kjv_verses>> GetKjvVerses(string osis, int chapter)
        {
            return dbConn.QueryAsync<kjv_verses>("SELECT * FROM [kjv_verses] WHERE [book] = '" + osis + "' AND [verse] LIKE '" + chapter + ".%'");
        }

        public async Task<List<nkjv_books>> GetAllNkjvBooks()
        {
            return await dbConn.Table<nkjv_books>().ToListAsync();
        }

        public Task<List<nkjv_verses>> GetNkjvVerses(string osis, int chapter)
        {
            return dbConn.QueryAsync<nkjv_verses>("SELECT * FROM [nkjv_verses] WHERE [book] = '" + osis + "' AND [verse] LIKE '" + chapter + ".%'");
        }

        public async Task<List<nlt_books>> GetAllNltBooks()
        {
            return await dbConn.Table<nlt_books>().ToListAsync();
        }

        public Task<List<nlt_verses>> GetNltVerses(string osis, int chapter)
        {
            return dbConn.QueryAsync<nlt_verses>("SELECT * FROM [nlt_verses] WHERE [book] = '" + osis + "' AND [verse] LIKE '" + chapter + ".%'");
        }

        public async Task<List<niv_books>> GetNivBooks()
        {
            return await dbConn.Table<niv_books>().ToListAsync();
        }

        public Task<List<niv_verses>> GetNivVerses(string osis, int chapter)
        {
            return dbConn.QueryAsync<niv_verses>("SELECT * FROM [niv_verses] WHERE [book] = '" + osis + "' AND [verse] LIKE '" + chapter + ".%'");
        }
    }
}
