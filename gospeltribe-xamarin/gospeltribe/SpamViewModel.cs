﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace gospeltribe
{
    public class SpamViewModel : AbstractViewModel
    {
        public IList<CommandViewModel> Commands { get; } = new List<CommandViewModel>();


        public SpamViewModel(IUserDialogs dialogs) : base(dialogs)
        {
            this.Commands = new List<CommandViewModel>
            {
                new CommandViewModel
                {
                    Text = "Loading",
                    Command = this.LoadingCommand(MaskType.Black)
                },
                new CommandViewModel
                {
                    Text = "Loading (Clear)",
                    Command = this.LoadingCommand(MaskType.Clear)
                },
                new CommandViewModel
                {
                    Text = "Loading (Gradient)",
                    Command = this.LoadingCommand(MaskType.Gradient)
                },
                new CommandViewModel
                {
                    Text = "Loading (None)",
                    Command = this.LoadingCommand(MaskType.None)
                },
                new CommandViewModel
                {
                    Text = "Progress",
                    Command = new Command(async () =>
                    {
                        var cancelled = false;

                        using (var dlg = this.Dialogs.Progress("Test Progress", () => cancelled = true))
                        {
                            while (!cancelled && dlg.PercentComplete < 100)
                            {
                                await Task.Delay(TimeSpan.FromMilliseconds(500));
                                dlg.PercentComplete += 2;
                            }
                        }
                        this.Result(cancelled ? "Progress Cancelled" : "Progress Complete");
                    })
                },
                new CommandViewModel
                {
                    Text = "Progress (No Cancel)",
                    Command = new Command(async () =>
                    {
                        using (var dlg = this.Dialogs.Progress("Progress (No Cancel)"))
                        {
                            
                            while (dlg.PercentComplete < 100)
                            {
                                await Task.Delay(TimeSpan.FromSeconds(1));
                                dlg.PercentComplete += 20;
                                dlg.Title = $"Loading ({dlg.PercentComplete}%)";
                            }
                        }
                    })
                },
                new CommandViewModel
                {
                    Text = "Loading (No Cancel)",
                    Command = new Command(async () =>
                    {
                        using (this.Dialogs.Loading("Loading (No Cancel)"))
                            await Task.Delay(TimeSpan.FromSeconds(3));
                    })
                },
                new CommandViewModel
                {
                    Text = "Loading To Success",
                    Command = new Command(async () =>
                    {
                        using (this.Dialogs.Loading("Test Loading"))
                            await Task.Delay(3000);
                    })
                },
                new CommandViewModel
                {
                    Text = "Manual Loading",
                    Command = new Command(async () =>
                    {
                        this.Dialogs.ShowLoading("Manual Loading");
                        await Task.Delay(3000);
                        this.Dialogs.HideLoading();
                    })
                }
            };
        }


        ICommand LoadingCommand(MaskType mask)
        {
            return new Command(async () =>
            {
                var cancelSrc = new CancellationTokenSource();
                var config = new ProgressDialogConfig()
                    .SetTitle("Loading")
                    .SetIsDeterministic(false)
                    .SetMaskType(mask)
                    .SetCancel(onCancel: cancelSrc.Cancel);

                using (this.Dialogs.Progress(config))
                {
                    try
                    {
                        await Task.Delay(TimeSpan.FromSeconds(5), cancelSrc.Token);
                    }
                    catch { }
                }
                this.Result(cancelSrc.IsCancellationRequested ? "Loading Cancelled" : "Loading Complete");
            });
        }
    }

    public class CommandViewModel
    {
        public string Text { get; set; }
        public ICommand Command { get; set; }
    }

    public abstract class AbstractViewModel : INotifyPropertyChanged
    {
        protected AbstractViewModel(IUserDialogs dialogs)
        {
            this.Dialogs = dialogs;
        }


        protected IUserDialogs Dialogs { get; }


        protected virtual void Result(string msg)
        {
            this.Dialogs.Alert(msg);
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
