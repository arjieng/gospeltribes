﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public class EntryBehavior : Behavior<CustomEntry>
    {
        string entryPlaceholder;

        protected override void OnAttachedTo(CustomEntry bindable)
        {
            base.OnAttachedTo(bindable);
                
            //bindable.Focused += Bindable_Focused;
            //bindable.Unfocused += Bindable_Unfocused;

            //bindable.TextChanged += Bindable_TextChanged;
        }

        protected override void OnDetachingFrom(CustomEntry bindable)
        {
            base.OnDetachingFrom(bindable);

            bindable.Focused -= Bindable_Focused;
            bindable.Unfocused -= Bindable_Unfocused;
            //bindable.TextChanged -= Bindable_TextChanged;
        }

        protected void Bindable_Focused(object sender, FocusEventArgs e)
        {
            CustomEntry entryField = (CustomEntry)sender;
            OnEntry_Behavior(entryField, 1, int.Parse(entryField.ClassId));
        }

        void Bindable_Unfocused(object sender, FocusEventArgs e)
        {
            CustomEntry entryField = (CustomEntry)sender;
            entryField.PlaceholderColor = Color.FromHex("#C4C3C8");

            if (!String.IsNullOrEmpty(entryPlaceholder))
            {
                entryField.Placeholder = entryPlaceholder;
            }

            if (String.IsNullOrEmpty(entryField.Text))
            {
                OnEntry_Behavior(entryField, 0, int.Parse(entryField.ClassId));
            }
        }

        void OnEntry_Behavior(CustomEntry entry, int isFocused, int entryId)
        {
            Label label = new Label();
            var parent = entry.Parent as Grid;

            switch (entryId)
            {
                case 1:
                    label = parent.FindByName<Label>("nameLabel");
                    break;
                case 2:
                    label = parent.FindByName<Label>("usernameLabel");
                    break;
                case 3:
                    label = parent.FindByName<Label>("passwordLabel");
                    break;
                case 4:
                    label = parent.FindByName<Label>("confirmLabel");
                    break;
                case 5:
                    label = parent.FindByName<Label>("codeLabel");
                    break;
                case 6:
                    label = parent.FindByName<Label>("firstNameLabel");
                    break;
                case 7:
                    label = parent.FindByName<Label>("lastNameLabel");
                    break;
                case 8:
                    label = parent.FindByName<Label>("addressLabel");
                    break;
                case 9:
                    label = parent.FindByName<Label>("phoneLabel");
                    break;
                case 10:
                    label = parent.FindByName<Label>("birthLabel");
                    break;
                case 11:
                    label = parent.FindByName<Label>("emailLabel");
                    break;
                case 12:
                    label = parent.FindByName<Label>("genderLabel");
                    break;
                case 13:
                    label = parent.FindByName<Label>("stateLabel");
                    break;
                case 14:
                    label = parent.FindByName<Label>("zipLabel");
                    break;

            }

            label.Opacity = isFocused;

            if (isFocused == 1)
            {
                entryPlaceholder = label.Text;
                entry.Focus();
                entry.Placeholder = String.Empty;
            }
        }
    }
}
