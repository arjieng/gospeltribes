﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace gospeltribe
{
    public class LongPressedEffect : RoutingEffect
    {
        public LongPressedEffect() : base("GT.LongPressedEffect")
        {
        }

        public event Action<object> OnTapEvent;
        public event Action<object> OnLongTapEvent;

        public void SendTapEvent(object sender)
        {
            OnTapEvent?.Invoke(sender);
        }

        public void SendLongTapEvent(object sender)
        {
            OnLongTapEvent?.Invoke(sender);
        }
    }
}
