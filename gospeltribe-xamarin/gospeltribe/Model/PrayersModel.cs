﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace gospeltribe
{
    public class PrayersModel : INotifyPropertyChanged
    {
        string _image, _subject, _prayer, _how_is_it_answered;
        DateTime _create_at;
        int _id, _shareTo, _is_answered;
        UserModel _user;
        bool _is_hidden;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string image
        {
            get { if (string.IsNullOrEmpty(_image)) { _image = "DefaultProfile"; } return _image; }
            set { _image = value; }
        }

        public string subject
        {
            get { return _subject.ToUpper(); }
            set { _subject = value; }
        }

        public string details
        {
            get { return _prayer; }
            set { _prayer = value; }
        }

        public int share_to
        {
            get { return _shareTo; }
            set { _shareTo = value; }
        }

        public UserModel user
        {
            get { return _user; }
            set { _user = value; }
        }

        public int is_answered
        {
            get { return _is_answered; }
            set { _is_answered = value; OnPropertyChanged(); }
        }

        public string answered_details
        {
            get { return _how_is_it_answered; }
            set { _how_is_it_answered = value; OnPropertyChanged(); }
        }

        public bool is_hidden
        {
            get { return _is_hidden; }
            set { _is_hidden = value; OnPropertyChanged(); }
        }

        public DateTime created_at
        {
            get { return _create_at; }
            set { _create_at = value; OnPropertyChanged(); }
        }

        public string get_date{
            get { return created_at.ToString("MMMM dd, yyyy"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }


    public class PrayerCommentModel : INotifyPropertyChanged
    {
        UserModel _user = new UserModel();
        string _body;
        int _id;
        DateTime _created_at;

        public int id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }

        public string comment
        {
            get { return _body; }
            set { _body = value; OnPropertyChanged(); }
        }

        public UserModel user
        {
            get { return _user; }
            set { _user = value; OnPropertyChanged(); }
        }

        public DateTime created_at
        {
            get { return _created_at; }
            set { _created_at = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}