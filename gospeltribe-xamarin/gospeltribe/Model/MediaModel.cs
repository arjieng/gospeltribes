﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace gospeltribe
{
    public class MediaModel : INotifyPropertyChanged
    {
        int _id;
        UserModel _user;
        string _media, _description, _video;
        bool _is_video = false;

        public int id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }

        public UserModel user
        {
            get { return _user; }
            set { _user = value; OnPropertyChanged(); }
        }

        public string media
        {
            get { return string.IsNullOrEmpty(_media) ? "DefaultProfile" : (_media.Contains(Constants.orig) ? _media : Constants.orig + _media); }
            set { _media = value; OnPropertyChanged(); }
        }

        public string description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged(); }
        }

        public bool is_video
        {
            get { return _is_video; }
            set { _is_video = value; OnPropertyChanged(); }
        }

        public string video
        {
            get { return Constants.orig + _video; }
            set { _video = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
