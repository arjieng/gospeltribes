﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace gospeltribe
{
    public class InboxModel : INotifyPropertyChanged
    {
        string _groupImage, _groupName, _lastMessage;
        int _id, _group_id, _chatroom_type, _notif_count;

        public int id{
            get { return _id; }
            set{ _id = value; OnPropertyChanged(); }
        }

        public int group_id
        {
            get { return _group_id; }
            set { _group_id = value; OnPropertyChanged(); }
        }

        public string chatroom_image
        {
            get { return string.IsNullOrEmpty(_groupImage) ? "DefaultProfile" : (_groupImage.Contains(Constants.orig) ? _groupImage : Constants.orig + _groupImage); }
            set { _groupImage = value; OnPropertyChanged(); }
        }

        public string chatroom_name
        {
            get { return _groupName; }
            set { _groupName = value; OnPropertyChanged(); }
        }

        public int chatroom_type{
            get { return _chatroom_type; }
            set { _chatroom_type = value; OnPropertyChanged(); }
        }

        public string last_message
        {
            get { if(string.IsNullOrEmpty(_lastMessage)){ _lastMessage = "Send a message to " + (_groupName.Equals("GROUP TEXT") ? "entire group" : _groupName.ToLower()); } return _lastMessage; }
            set { _lastMessage = value; OnPropertyChanged(); }
        }

        public int notif_count{
            get { return _notif_count; }
            set { _notif_count = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}