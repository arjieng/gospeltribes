﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace gospeltribe
{
    public class EventsModel : INotifyPropertyChanged
    {
        int _id;
        string _subject = null, _details = null, _image = null, _link = null, _location = null, _subtitle = null, _repeat = null, _repeat_days = null;
        DateTime _date, _start_date, _end_date;
        bool _has_attended;

        public int id
        {
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }

        public string location
        {
            get { return _location; }
            set { _location = value; OnPropertyChanged(); }
        }

        public string subtitle
        {
            get { return _subtitle; }
            set { _subtitle = value; OnPropertyChanged(); }
        }

        public DateTime date
        {
            get { return _date; }
            set { _date = value; OnPropertyChanged(); }
        }

        public DateTime start_date
        {
            get { return _start_date; }
            set { _start_date = value; OnPropertyChanged(); }
        }

        public DateTime end_date
        {
            get { return _end_date; }
            set { _end_date = value; OnPropertyChanged(); }
        }

        public string event_date
        {
            get { return start_date.ToString("MMMM dd, yyyy"); }
        }

        public string event_date_event_view
        {
            get
            {
                if (!start_date.Date.Equals(end_date.Date))
                {
                    return start_date.ToString("MMMM dd") + " - " + end_date.ToString("MMMM dd");
                }
                return start_date.ToString("MMMM dd");
            }
        }

        public string event_time_event_view
        {
            get
            {
                string time = start_date.ToString("hh:mm tt");
                if (!start_date.TimeOfDay.Equals(end_date.TimeOfDay))
                {
                    time = time + " - " + end_date.ToString("hh:mm tt"); 
                }
                return time;
            }
        }

        public string subject
        {
            get { return _subject; }
            set { _subject = value; OnPropertyChanged(); }
        }

        public string link
        {
            get { return _link; }
            set { _link = value; OnPropertyChanged(); }
        }

        public string details
        {
            get { return _details; }
            set { _details = value; OnPropertyChanged(); }
        }

        public string image
        {
            get { return Constants.orig + _image; }
            set { _image = value; OnPropertyChanged(); }
        }

        public bool has_attended
        {
            get { return _has_attended; }
            set { _has_attended = value; OnPropertyChanged(); }
        }

        public string repeat
        {
            get { return _repeat; }
            set { _repeat = value; OnPropertyChanged(); }
        }

        public string repeat_days
        {
            get { return _repeat_days; }
            set { _repeat_days = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
