﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace gospeltribe
{
    public class MessagesModel : INotifyPropertyChanged
    {
        UserModel _user;
        ConversationPage _conversation;
        int _id, _like_count;
        string _mediaImage, _message;
        Stream _this_image;
        string _date_sent;
        bool _has_liked;

        public int id
        {
            get { return _id; }
            set{ _id = value; OnPropertyChanged("id"); }
        }

        public UserModel user{
            get { return _user; }
            set { _user = value; OnPropertyChanged("usesr"); }
        }

        public string message{
            get { return _message; }
            set { _message = value; OnPropertyChanged("message"); }
        }

        public string media{
            get { return Constants.orig + _mediaImage; }
            set { _mediaImage = value; OnPropertyChanged("media"); }
        }

        public Stream the_image{
            get{ return _this_image; }
            set { _this_image = value; OnPropertyChanged("the_image"); }
        }

        public string date_sent{
            get { return _date_sent; }
            set { _date_sent = value; OnPropertyChanged("date_sent"); }
        }

        public int like_count{
            get { return _like_count; }
            set { _like_count = value; OnPropertyChanged("like_count"); }
        }

        public bool has_liked{
            get { return _has_liked; }
            set { _has_liked = value; OnPropertyChanged("has_liked"); }
        }
        //public ConversationPage conversation{
        //    get { return _conversation; }
        //    set{ _conversation = value; OnPropertyChanged(); }
        //}
        bool _IsLoading = true;
        public bool IsLoading{
            get { return _IsLoading; }
            set{ _IsLoading = value; OnPropertyChanged("IsLoading"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
