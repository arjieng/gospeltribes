﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace gospeltribe
{
    public class GroupModel : INotifyPropertyChanged
    {
        
        string _group_name, _group_code, _group_image;
        int _id;

        public int id{
            get { return _id; }
            set { _id = value; OnPropertyChanged(); }
        }
        public string group_name
        {
            get { return _group_name; }
            set { _group_name = value; OnPropertyChanged(); }
        }

        public string group_code
        {
            get { return _group_code; }
            set { _group_code = value; OnPropertyChanged(); }
        }

        public string group_image
        {
            get { if (string.IsNullOrEmpty(_group_image)) { _group_image = "DefaultProfile"; } return _group_image; }
            set { _group_image = value; OnPropertyChanged(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
