﻿using System;
using System.Text.RegularExpressions;

namespace gospeltribe
{

    public class Scripture
    {
        public string human { get; set; }
        public int chapter { get; set; }
        public int verse_num_from { get; set; }
        public int verse_num_to { get; set; }
        public string verse { get; set; }
        public string script { get { return (string.IsNullOrEmpty(bible_version) ? "" : ("(" + bible_version + ")" + human + " " + chapter + ":" + verse_num_from + (verse_num_to != 0 ? "-" + verse_num_to : ""))); } }
        public string bible_version { get; set; }

    }

    public class StudyModules
    {
        string _files;
        public int id { get; set; }
        public Scripture scripture { get; set; }
        public string question { get; set; }
        public string link { get; set; }
        public string files { get { return Constants.orig + _files; } set { _files = value; } }

        public string yid{
            get{
                string[] s = link.Split('/');
                string youtubeID = "";
                if (s[2].Contains("youtube"))
                {
                    //youtubeID = s[3].Split('=')[1];

                    youtubeID = s[3].Split('=')[1].Split('&')[0];
                }

                if (s[2] == "youtu.be")
                {
                    youtubeID = s[3].Split('&')[0];
                }
                return youtubeID;
            }
        }
    }

    //ESV
    public class esv_books
    {
        public int number { get; set; }
        public string osis { get; set; }
        public string human { get; set; }
        public int chapters { get; set; }
    }

    public class esv_verses
    {
        Regex removeLeadingZeroesReg = new Regex(@"^0+(?=\d)");
        string _tv = "";
        public int id { get; set; }
        public string book { get; set; }
        public string verse { get; set; }
        public string unformatted { get; set; }
        public string trimmed_verses
        {
            get
            {
                if (string.IsNullOrEmpty(_tv))
                {
                    string[] words = verse.Split('.');
                    if (words[1].Length == 2)
                    {
                        words[1] += "0";
                    }
                    if (words[1].Length == 1)
                    {
                        words[1] += "00";
                    }
                    return removeLeadingZeroesReg.Replace(words[1], "");
                }
                return "0";

            }
            set
            {
                _tv = value;
            }
        }
    }
    //HCSB
    public class hcsb_books
    {
        public int number { get; set; }
        public string osis { get; set; }
        public string human { get; set; }
        public int chapters { get; set; }
    }

    public class hcsb_verses
    {
        Regex removeLeadingZeroesReg = new Regex(@"^0+(?=\d)");
        string _tv = "";
        public int id { get; set; }
        public string book { get; set; }
        public string verse { get; set; }
        public string unformatted { get; set; }
        public string trimmed_verses
        {
            get
            {
                if (string.IsNullOrEmpty(_tv))
                {
                    string[] words = verse.Split('.');
                    if (words[1].Length == 2)
                    {
                        words[1] += "0";
                    }
                    if (words[1].Length == 1)
                    {
                        words[1] += "00";
                    }
                    return removeLeadingZeroesReg.Replace(words[1], "");
                }
                return "0";

            }
            set
            {
                _tv = value;
            }
        }
    }
    //KJV
    public class kjv_books
    {
        public int number { get; set; }
        public string osis { get; set; }
        public string human { get; set; }
        public int chapters { get; set; }
    }

    public class kjv_verses
    {
        Regex removeLeadingZeroesReg = new Regex(@"^0+(?=\d)");
        string _tv = "";
        public int id { get; set; }
        public string book { get; set; }
        public string verse { get; set; }
        public string unformatted { get; set; }
        public string trimmed_verses
        {
            get
            {
                if (string.IsNullOrEmpty(_tv))
                {
                    string[] words = verse.Split('.');
                    if (words[1].Length == 2)
                    {
                        words[1] += "0";
                    }
                    if (words[1].Length == 1)
                    {
                        words[1] += "00";
                    }
                    return removeLeadingZeroesReg.Replace(words[1], "");
                }
                return "0";

            }
            set
            {
                _tv = value;
            }
        }
    }
    //NKJV
    public class nkjv_books
    {
        public int number { get; set; }
        public string osis { get; set; }
        public string human { get; set; }
        public int chapters { get; set; }
    }

    public class nkjv_verses
    {
        Regex removeLeadingZeroesReg = new Regex(@"^0+(?=\d)");
        string _tv = "";
        public int id { get; set; }
        public string book { get; set; }
        public string verse { get; set; }
        public string unformatted { get; set; }
        public string trimmed_verses
        {
            get
            {
                if (string.IsNullOrEmpty(_tv))
                {
                    string[] words = verse.Split('.');
                    if (words[1].Length == 2)
                    {
                        words[1] += "0";
                    }
                    if (words[1].Length == 1)
                    {
                        words[1] += "00";
                    }
                    return removeLeadingZeroesReg.Replace(words[1], "");
                }
                return "0";

            }
            set
            {
                _tv = value;
            }
        }
    }
    //NLT
    public class nlt_books
    {
        public int number { get; set; }
        public string osis { get; set; }
        public string human { get; set; }
        public int chapters { get; set; }
    }

    public class nlt_verses
    {
        Regex removeLeadingZeroesReg = new Regex(@"^0+(?=\d)");
        string _tv = "";
        public int id { get; set; }
        public string book { get; set; }
        public string verse { get; set; }
        public string unformatted { get; set; }
        public string trimmed_verses
        {
            get
            {
                if (string.IsNullOrEmpty(_tv))
                {
                    string[] words = verse.Split('.');
                    if (words[1].Length == 2)
                    {
                        words[1] += "0";
                    }
                    if (words[1].Length == 1)
                    {
                        words[1] += "00";
                    }
                    return removeLeadingZeroesReg.Replace(words[1], "");
                }
                return "0";

            }
            set
            {
                _tv = value;
            }
        }
    }

    //NIV
    public class niv_books
    {
        public int number { get; set; }
        public string osis { get; set; }
        public string human { get; set; }
        public int chapters { get; set; }
    }

    public class niv_verses
    {
        Regex removeLeadingZeroesReg = new Regex(@"^0+(?=\d)");
        string _tv = "";
        public int id { get; set; }
        public string book { get; set; }
        public string verse { get; set; }
        public string unformatted { get; set; }
        public string trimmed_verses
        {
            get
            {
                if (string.IsNullOrEmpty(_tv))
                {
                    string[] words = verse.Split('.');
                    if (words[1].Length == 2)
                    {
                        words[1] += "0";
                    }
                    if (words[1].Length == 1)
                    {
                        words[1] += "00";
                    }
                    return removeLeadingZeroesReg.Replace(words[1], "");
                }
                return "0";

            }
            set
            {
                _tv = value;
            }
        }
    }
}
