﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace gospeltribe
{
    public class UserModel : INotifyPropertyChanged
    {
        string _email = null, _username = null, _first_name = null, _last_name = null, _phone = null, _address = null, _gender = null, _birthdate = null, _city = null, _state = null, _zip = null, _ful_name;
        int _id, _role;
        Avatar _avatar = new Avatar();
        Setting _settings;

        public string email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged(); }
        }

        public string city
        {
            get { return _city; }
            set { _city = value; OnPropertyChanged(); }
        }

        public string state
        {
            get { return _state; }
            set { _state = value; OnPropertyChanged(); }
        }

        public string zip
        {
            get { return _zip; }
            set { _zip = value; OnPropertyChanged(); }
        }

        public string username
        {
            get { return _username; }
            set { _username = value; OnPropertyChanged(); }
        }

        public string first_name
        {
            get { return _first_name; }
            set { _first_name = value; _ful_name = value + " " + _last_name; OnPropertyChanged(); }
        }

        public string last_name
        {
            get { return _last_name; }
            set { _last_name = value; _ful_name = _first_name + " " + value; OnPropertyChanged(); }
        }

        public Avatar avatar
        {
            get { return _avatar; }
            set { _avatar = value; OnPropertyChanged(); }
        }

        public string phone_number
        {
            get { return _phone; }
            set { _phone = value; OnPropertyChanged(); }
        }

        public string address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged(); }
        }

        public string gender
        {
            get { return _gender; }
            set { _gender = value; OnPropertyChanged(); }
        }

        public string birth_date
        {
            get { return _birthdate; }
            set { _birthdate = value; OnPropertyChanged(); }
        }

        public int id
        {
            set { _id = value; OnPropertyChanged(); }
            get { return _id; }
        }

        public int role
        {
            set { _role = value; OnPropertyChanged(); }
            get { return _role; }
        }

        public string full_name
        {
            get { return ((string.IsNullOrEmpty(_first_name) && string.IsNullOrEmpty(_last_name)) ? _username : _ful_name); }
            set { _ful_name = value; OnPropertyChanged(); }
        }

        public Setting settings
        {
            set { _settings = value; OnPropertyChanged(); }
            get { return _settings; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Setting : INotifyPropertyChanged
    {
        bool _inbox = false, _prayer = false, _event = false, _story = false, _member = false;

        public bool inbox{
            set { _inbox = value; OnPropertyChanged(); }
            get { return _inbox; }
        }

        public bool prayer
        {
            set { _prayer = value; OnPropertyChanged(); }
            get { return _prayer; }
        }

        public bool event_notif
        {
            set { _event = value; OnPropertyChanged(); }
            get { return _event; }
        }

        public bool story
        {
            set { _story = value; OnPropertyChanged(); }
            get { return _story; }
        }

        public bool member
        {
            set { _member = value; OnPropertyChanged(); }
            get { return _member; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Avatar : INotifyPropertyChanged
    {
        string _url;
        Thumbs _thumb_30x30, _thumb_40x40, _thumb_50x50, _thumb_70x70, _thumb_75x75, _thumb_100x100;
        //public string url { get { string image = ""; if (string.IsNullOrEmpty(_url)) { image = "DefaultProfile"; } else { image = Constants.orig + _url; } return image; } set { _url = value; OnPropertyChanged(); } }
        public string url
        {
            get
            {
                return string.IsNullOrEmpty(_url) ? "DefaultProfile" : (_url.Contains(Constants.orig) ? _url : Constants.orig + _url);
            }
            set
            {
                _url = value;
                OnPropertyChanged();
            }
        }

        public Thumbs thumb_30x30 { get { return _thumb_30x30; } set { _thumb_30x30 = value; OnPropertyChanged(); } }
        public Thumbs thumb_40x40 { get { return _thumb_40x40; } set { _thumb_40x40 = value; OnPropertyChanged(); } }
        public Thumbs thumb_50x50 { get { return _thumb_50x50; } set { _thumb_50x50 = value; OnPropertyChanged(); } }
        public Thumbs thumb_70x70 { get { return _thumb_70x70; } set { _thumb_70x70 = value; OnPropertyChanged(); } }
        public Thumbs thumb_75x75 { get { return _thumb_75x75; } set { _thumb_75x75 = value; OnPropertyChanged(); } }
        public Thumbs thumb_100x100 { get { return _thumb_100x100; } set { _thumb_100x100 = value; OnPropertyChanged(); } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    public class Thumbs : INotifyPropertyChanged
    {
        string _url;
        public string url
        {
            get
            {
                return string.IsNullOrEmpty(_url) ? "DefaultProfile" : (_url.Contains(Constants.orig) ? _url : Constants.orig + _url);
            }
            set { _url = value; OnPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
