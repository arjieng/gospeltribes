﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.FirebasePushNotification;
using Xamarin.Forms;
using XamForms.Controls;

namespace gospeltribe
{
    public partial class SpamPage : RootViewPage
    {
        public SpamPage()
        {
            InitializeComponent();
        }
    }

    public interface IClipBoard
    {
        string GetTextFromClipBoard();
        void SendTextToClipboard(string text);
    }

    public class Example{
        public string Html { get; set; }
    }
}
