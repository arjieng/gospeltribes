using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DLToolkit.Forms.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FirebasePushNotification;
using SQLite.Net.Interop;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace gospeltribe
{
    public partial class App : Application, IAdvanceRestConnector
    {

        public static GeolocationHelper locationHelper;
        AdvanceRestService restService;
        CancellationTokenSource cts;
        public static bool isIphoneX = false;
        public static float screenWidth { get; set; }
        public static float screenHeight { get; set; }
        public static float appScale { get; set; }
        public static double screenScale;
        DataClass dataClass = DataClass.GetInstance;
        public static BibleHelper esv_app { get; private set; }
        public static BibleHelper hcsb_app { get; private set; }
        public static BibleHelper kjv_app { get; private set; }
        public static BibleHelper nkjv_app { get; private set; }
        public static BibleHelper nlt_app { get; private set; }
        public static BibleHelper niv_app { get; private set; }

        public static ObservableCollection<string> colors { get; private set; }
        public static HttpStatusCode statusCode { get; set; }
        public static App app { get; set; }
        static HomePage homePage = null;


        public App(string esv, string hcsb, string kjv, string nkjv, string nlt, ISQLitePlatform sQLitePlatform, string niv)
        {
            //Plugin.InputKit.Shared.Controls.CheckBox.GlobalSetting.BorderColor = Color.Red;
            //Plugin.InputKit.Shared.Controls.CheckBox.GlobalSetting.Size = 36;
            //Plugin.InputKit.Shared.Controls.RadioButton.GlobalSetting.Color = Color.Red;
            restService = new AdvanceRestService
            {
                WebServiceDelegate = this
            };
            locationHelper = new GeolocationHelper();
            colors = new ObservableCollection<string>();
            foreach (var field in typeof(Xamarin.Forms.Color).GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                if (field != null && !String.IsNullOrEmpty(field.Name))
                    colors.Add(field.Name);
            }

            //Xamarin.Forms.Internals.Log.Listeners.Add(new DelegateLogListener((arg1, arg2) => Debug.WriteLine("Listener: " + arg2)));
            InitializeComponent();
            FlowListView.Init();

            esv_app = new BibleHelper(sQLitePlatform, esv, 1);
            hcsb_app = new BibleHelper(sQLitePlatform, hcsb, 2);
            kjv_app = new BibleHelper(sQLitePlatform, kjv, 3);
            nkjv_app = new BibleHelper(sQLitePlatform, nkjv, 4);
            nlt_app = new BibleHelper(sQLitePlatform, nlt, 5);
            niv_app = new BibleHelper(sQLitePlatform, niv, 6);

            CrossFirebasePushNotification.Current.RegisterForPushNotifications();

            if (string.IsNullOrEmpty(dataClass.token) && string.IsNullOrEmpty(dataClass.clientId) && string.IsNullOrEmpty(dataClass.uid))
            {
                Logout();
            }
            else
            {
                ShowMainPage();
            }
            app = this;
            //App.Log("Comparing: " + (new TimeSpan(0, 0, 0)).CompareTo(new TimeSpan(23, 59, 0)).ToString());
            //App.Log("Comparing: " + (new TimeSpan(23, 59, 0)).CompareTo(new TimeSpan(0, 0, 0)).ToString());

            //MainPage = new SpamPage(); 
            CrossFirebasePushNotification.Current.OnNotificationOpened += Current_OnNotificationOpened;
        }

        void Current_OnNotificationOpened(object source, Plugin.FirebasePushNotification.Abstractions.FirebasePushNotificationResponseEventArgs e)
        {
            App.Log(JsonConvert.SerializeObject(e.Data));

            foreach (var data in e.Data)
            {
                App.Log($"{data.Key} : {data.Value}");
            }


            switch (e.Data["gcm.notification.tab_name"].ToString())
            {
                case "inbox":
                    break;
                case "prayer":
                    homePage.CurrentPage = homePage.Children[1];
                    break;
                case "event":
                    homePage.CurrentPage = homePage.Children[2];
                    break;
                case "media":
                    homePage.CurrentPage = homePage.Children[3];
                    break;
                case "member":
                    homePage.CurrentPage = homePage.Children[4];
                    break;
            }
        }


        protected override void OnStart()
        {
            //AppCenter.Start("ios=9f7d9caf-43f8-4ffe-b329-631800946de5;android=b72c1328-f6bb-4346-adf4-d151f1443fde", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static void ShowMainPage()
        {
            homePage = new HomePage();
            Current.MainPage = new NavigationPage(homePage);
        }

        public static void Logout(bool toSignIn = false)
        {
            homePage = null;
            Current.MainPage = new NavigationPage(new LandingPage(toSignIn));
        }

        public static void Log(string msg, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            msg = DateTime.Now.ToString("HH:mm:ss tt") + " [Gospel Tribes]-[" + memberName + "]: " + msg;
            DependencyService.Get<ILogServices>().Log(msg);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Log(jsonData.ToString());
            Log($"this is the status code: {statusCode}");
        }

        public async void ReceiveTimeoutError(string title, string error, int wsType)
        {
            await Task.Delay(1000);
            if (statusCode.Equals(HttpStatusCode.Unauthorized))
            {
                DataClass.GetInstance.user = null;
                DataClass.GetInstance.clientId = "";
                DataClass.GetInstance.uid = "";
                DataClass.GetInstance.token = "";
                Logout();
            }
        }

        #region PushNotification
        public static void android_DidReceiveNotification(string action)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var dataClass = DataClass.GetInstance;

                if (!string.IsNullOrEmpty(dataClass.token) && !string.IsNullOrEmpty(dataClass.clientId) && !string.IsNullOrEmpty(dataClass.uid))
                {
                    switch (action)
                    {
                        case "message":
                            if (!homePage.CurrentPage.ClassId.Equals("Inbox"))
                            {
                                InboxBadgeCounter = InboxBadgeCounter + 1;
                                JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 0, BadgeCount = 1 }));
                                MessagingCenter.Send<JObject>(jObject1, "Badges");
                                SendMessagingCenterPushNotificationReceived();
                            }

                            break;
                        case "prayer":
                            if (!homePage.CurrentPage.ClassId.Equals("Prayer"))
                            {
                                PrayersBadgeCounter = PrayersBadgeCounter + 1;
                                JObject jObject11 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 1, BadgeCount = 1 }));
                                MessagingCenter.Send<JObject>(jObject11, "Badges");
                                SendMessagingCenterPushNotificationReceived();
                            }

                            break;
                        case "calendar":
                            if (!homePage.CurrentPage.ClassId.Equals("Calendar"))
                            {
                                EventsBadgeCounter = EventsBadgeCounter + 1;
                                JObject jObject21 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 2, BadgeCount = 1 }));
                                MessagingCenter.Send<JObject>(jObject21, "Badges");
                                SendMessagingCenterPushNotificationReceived();
                            }

                            break;
                        case "story":
                            if (!homePage.CurrentPage.ClassId.Equals("Media"))
                            {
                                StoryBadgeCounter = StoryBadgeCounter + 1;
                                JObject jObject31 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 3, BadgeCount = 1 }));
                                MessagingCenter.Send<JObject>(jObject31, "Badges");
                                SendMessagingCenterPushNotificationReceived();
                            }

                            break;
                        case "members":
                            if (!homePage.CurrentPage.ClassId.Equals("Member"))
                            {
                                MembersBadgeCounter = MembersBadgeCounter + 1;
                                JObject jObject41 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 4, BadgeCount = 1 }));
                                MessagingCenter.Send<JObject>(jObject41, "Badges");
                                SendMessagingCenterPushNotificationReceived();
                            }

                            break;
                    }
                }
            });
        }

        public static void iOS_DidReceiveNotification(string action)
        {
            var dataClass = DataClass.GetInstance;
            if (!string.IsNullOrEmpty(dataClass.token) && !string.IsNullOrEmpty(dataClass.clientId) && !string.IsNullOrEmpty(dataClass.uid))
            {
                switch (action)
                {
                    case "message":
                        if (!homePage.CurrentPage.ClassId.Equals("Inbox"))
                        {
                            InboxBadgeCounter = InboxBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText1))
                            {
                                homePage.BadgeText1 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText1);
                                homePage.BadgeText1 = (badgeCount + 1).ToString();
                            }
                            JObject jObject = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 0, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "prayer":
                        if (!homePage.CurrentPage.ClassId.Equals("Prayer"))
                        {
                            PrayersBadgeCounter = PrayersBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText2))
                            {
                                homePage.BadgeText2 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText2);
                                homePage.BadgeText2 = (badgeCount + 1).ToString();
                            }
                            JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 1, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject1, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "calendar":
                        if (!homePage.CurrentPage.ClassId.Equals("Calendar"))
                        {
                            EventsBadgeCounter = EventsBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText3))
                            {
                                homePage.BadgeText3 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText3);
                                homePage.BadgeText3 = (badgeCount + 1).ToString();
                            }
                            JObject jObject2 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 2, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject2, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "story":
                        if (!homePage.CurrentPage.ClassId.Equals("Media"))
                        {
                            StoryBadgeCounter = StoryBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText4))
                            {
                                homePage.BadgeText4 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText4);
                                homePage.BadgeText4 = (badgeCount + 1).ToString();
                            }
                            JObject jObject3 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 3, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject3, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "members":
                        if (!homePage.CurrentPage.ClassId.Equals("Member"))
                        {
                            MembersBadgeCounter = MembersBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText5))
                            {
                                homePage.BadgeText5 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText5);
                                homePage.BadgeText5 = (badgeCount + 1).ToString();
                            }
                            JObject jObject4 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 4, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject4, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                }
            }
        }

        public static void iOS_DidReceiveNotification(string action, string p, string ct)
        {
            var dataClass = DataClass.GetInstance;
            if (!string.IsNullOrEmpty(dataClass.token) && !string.IsNullOrEmpty(dataClass.clientId) && !string.IsNullOrEmpty(dataClass.uid))
            {
                JObject jO = JObject.Parse(p);
                if (action.Equals("message"))
                    homePage.UpdateInboxes(jO["message"]["body"].ToString(), ct);
                else if (action.Equals("story"))
                    homePage.UpdateImages(jO);

                switch (action)
                {
                    case "message":
                        if (!homePage.CurrentPage.ClassId.Equals("Inbox"))
                        {
                            InboxBadgeCounter = InboxBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText1))
                            {
                                homePage.BadgeText1 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText1);
                                homePage.BadgeText1 = (badgeCount + 1).ToString();
                            }
                            JObject jObject = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 0, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "prayer":
                        if (!homePage.CurrentPage.ClassId.Equals("Prayer"))
                        {
                            PrayersBadgeCounter = PrayersBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText2))
                            {
                                homePage.BadgeText2 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText2);
                                homePage.BadgeText2 = (badgeCount + 1).ToString();
                            }
                            JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 1, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject1, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "calendar":
                        if (!homePage.CurrentPage.ClassId.Equals("Calendar"))
                        {
                            EventsBadgeCounter = EventsBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText3))
                            {
                                homePage.BadgeText3 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText3);
                                homePage.BadgeText3 = (badgeCount + 1).ToString();
                            }
                            JObject jObject2 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 2, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject2, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "story":
                        if (!homePage.CurrentPage.ClassId.Equals("Media"))
                        {
                            StoryBadgeCounter = StoryBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText4))
                            {
                                homePage.BadgeText4 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText4);
                                homePage.BadgeText4 = (badgeCount + 1).ToString();
                            }
                            JObject jObject3 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 3, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject3, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                    case "members":
                        if (!homePage.CurrentPage.ClassId.Equals("Member"))
                        {
                            MembersBadgeCounter = MembersBadgeCounter + 1;
                            if (string.IsNullOrEmpty(homePage.BadgeText5))
                            {
                                homePage.BadgeText5 = "1";
                            }
                            else
                            {
                                int badgeCount = int.Parse(homePage.BadgeText5);
                                homePage.BadgeText5 = (badgeCount + 1).ToString();
                            }
                            JObject jObject4 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 4, BadgeCount = 1 }));
                            MessagingCenter.Send<JObject>(jObject4, "Badges");
                            SendMessagingCenterPushNotificationReceived();
                        }
                        break;
                }
            }
        }

        static void SendMessagingCenterPushNotificationReceived()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (Device.RuntimePlatform.Equals(Device.iOS))
                {
                    App.BadgeCounter = App.BadgeCounter + 1;
                    DependencyService.Get<iAppIcon>().ChangeAppIcon();
                }
                else
                    MessagingCenter.Send<App>(App.app, "UpdateAppIconBadge");
            });
        }

        #endregion

        #region BadgeParams
        public static int BadgeCounter
        {
            get
            {
                int count = 0;
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code == DataClass.GetInstance.GroupCode);
                    if (bc != null)
                    {
                        count = bc.badge_total;
                    }
                }
                return count;
            }

            set
            {
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code.Equals(DataClass.GetInstance.GroupCode));

                    if (bc != null)
                    {
                        BadgesCounter bb = bcs[bcs.IndexOf(bc)];
                        bb.badge_total = value;
                    }
                    else
                    {
                        bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = value, badges = new Badges { events = 0, inbox = 0, members = 0, prayers = 0, stories = 0 } };
                        bcs.Add(bc);
                    }
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
                else
                {
                    ObservableCollection<BadgesCounter> bcs = new ObservableCollection<BadgesCounter>();
                    BadgesCounter bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = value, badges = new Badges { events = 0, inbox = 0, members = 0, prayers = 0, stories = 0 } };
                    bcs.Add(bc);
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
            }
        }

        public static int InboxBadgeCounter
        {
            get
            {
                int count = 0;
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code == DataClass.GetInstance.GroupCode);
                    if (bc != null)
                    {
                        count = bc.badges.inbox;
                    }
                }
                return count;
            }

            set
            {
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code.Equals(DataClass.GetInstance.GroupCode));

                    if (bc != null)
                    {
                        BadgesCounter bb = bcs[bcs.IndexOf(bc)];
                        bb.badges.inbox = value;
                    }
                    else
                    {
                        bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = value, members = 0, prayers = 0, stories = 0 } };
                        bcs.Add(bc);
                    }
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
                else
                {
                    ObservableCollection<BadgesCounter> bcs = new ObservableCollection<BadgesCounter>();
                    BadgesCounter bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = value, members = 0, prayers = 0, stories = 0 } };
                    bcs.Add(bc);
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
            }
        }

        public static int PrayersBadgeCounter
        {
            get
            {
                int count = 0;
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code == DataClass.GetInstance.GroupCode);
                    if (bc != null)
                    {
                        count = bc.badges.prayers;
                    }
                }
                return count;
            }

            set
            {
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code.Equals(DataClass.GetInstance.GroupCode));

                    if (bc != null)
                    {
                        BadgesCounter bb = bcs[bcs.IndexOf(bc)];
                        bb.badges.prayers = value;
                    }
                    else
                    {
                        bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = 0, members = 0, prayers = value, stories = 0 } };
                        bcs.Add(bc);
                    }
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
                else
                {
                    ObservableCollection<BadgesCounter> bcs = new ObservableCollection<BadgesCounter>();
                    BadgesCounter bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = 0, members = 0, prayers = value, stories = 0 } };
                    bcs.Add(bc);
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
            }
        }

        public static int EventsBadgeCounter
        {
            get
            {
                int count = 0;
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code == DataClass.GetInstance.GroupCode);
                    if (bc != null)
                    {
                        count = bc.badges.events;
                    }
                }
                return count;
            }

            set
            {
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code.Equals(DataClass.GetInstance.GroupCode));

                    if (bc != null)
                    {
                        BadgesCounter bb = bcs[bcs.IndexOf(bc)];
                        bb.badges.events = value;
                    }
                    else
                    {
                        bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = value, inbox = 0, members = 0, prayers = 0, stories = 0 } };
                        bcs.Add(bc);
                    }
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
                else
                {
                    ObservableCollection<BadgesCounter> bcs = new ObservableCollection<BadgesCounter>();
                    BadgesCounter bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = value, inbox = 0, members = 0, prayers = 0, stories = 0 } };
                    bcs.Add(bc);
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
            }
        }

        public static int StoryBadgeCounter
        {
            get
            {
                int count = 0;
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code == DataClass.GetInstance.GroupCode);
                    if (bc != null)
                    {
                        count = bc.badges.stories;
                    }
                }
                return count;
            }

            set
            {
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code.Equals(DataClass.GetInstance.GroupCode));

                    if (bc != null)
                    {
                        BadgesCounter bb = bcs[bcs.IndexOf(bc)];
                        bb.badges.stories = value;
                    }
                    else
                    {
                        bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = 0, members = 0, prayers = 0, stories = value } };
                        bcs.Add(bc);
                    }
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
                else
                {
                    ObservableCollection<BadgesCounter> bcs = new ObservableCollection<BadgesCounter>();
                    BadgesCounter bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = 0, members = 0, prayers = 0, stories = value } };
                    bcs.Add(bc);
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
            }
        }

        public static int MembersBadgeCounter
        {
            get
            {
                int count = 0;
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code == DataClass.GetInstance.GroupCode);
                    if (bc != null)
                    {
                        count = bc.badges.members;
                    }
                }
                return count;
            }

            set
            {
                if (Application.Current.Properties.ContainsKey("all_badges"))
                {
                    ObservableCollection<BadgesCounter> bcs = JsonConvert.DeserializeObject<ObservableCollection<BadgesCounter>>(Application.Current.Properties["all_badges"].ToString());
                    BadgesCounter bc = bcs.FirstOrDefault(x => x.group_code.Equals(DataClass.GetInstance.GroupCode));

                    if (bc != null)
                    {
                        BadgesCounter bb = bcs[bcs.IndexOf(bc)];
                        bb.badges.members = value;
                    }
                    else
                    {
                        bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = 0, members = value, prayers = 0, stories = 0 } };
                        bcs.Add(bc);
                    }
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
                else
                {
                    ObservableCollection<BadgesCounter> bcs = new ObservableCollection<BadgesCounter>();
                    BadgesCounter bc = new BadgesCounter() { group_code = DataClass.GetInstance.GroupCode, badge_total = 0, badges = new Badges { events = 0, inbox = 0, members = value, prayers = 0, stories = 0 } };
                    bcs.Add(bc);
                    SaveProperties(bcs);
                    //Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
                    //Application.Current.SavePropertiesAsync();
                }
            }
        }

        static void SaveProperties(ObservableCollection<BadgesCounter> bcs)
        {
            Application.Current.Properties["all_badges"] = JsonConvert.SerializeObject(bcs);
            Application.Current.SavePropertiesAsync();
        }

        #endregion


        public static Predictions PreviousPredictions
        {
            get
            {
                Predictions _predictions = null;
                if (Application.Current.Properties.ContainsKey("previous_location"))
                {
                    _predictions = JsonConvert.DeserializeObject<Predictions>(Application.Current.Properties["previous_location"].ToString());
                }
                return _predictions;
            }
            set
            {
                Application.Current.Properties["previous_location"] = JsonConvert.SerializeObject(value);
                Application.Current.SavePropertiesAsync();
            }
        }

    }

    public class BadgesCounter
    {
        public string group_code { get; set; }
        public int badge_total { get; set; }
        public Badges badges { get; set; }
    }

    public class Badges
    {
        public int inbox { get; set; }
        public int prayers { get; set; }
        public int events { get; set; }
        public int stories { get; set; }
        public int members { get; set; }
    }
}
