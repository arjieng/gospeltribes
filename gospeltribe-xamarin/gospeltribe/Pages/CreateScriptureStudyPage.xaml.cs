﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class CreateScriptureStudyPage : RootViewPage, IAdvanceRestConnector
    {
        List<string> bibleVersions = new List<string>();
        List<int> chapters = new List<int>();

        List<esv_books> esv_books = new List<esv_books>();
        List<hcsb_books> hcsb_books = new List<hcsb_books>();
        List<kjv_books> kjv_books = new List<kjv_books>();
        List<nkjv_books> nkjv_books = new List<nkjv_books>();
        List<nlt_books> nlt_books = new List<nlt_books>();
        List<niv_books> niv_books = new List<niv_books>();

        List<esv_verses> esv_Verses = new List<esv_verses>();
        List<hcsb_verses> hcsb_Verses = new List<hcsb_verses>();
        List<kjv_verses> kjv_Verses = new List<kjv_verses>();
        List<nkjv_verses> nkjv_Verses = new List<nkjv_verses>();
        List<nlt_verses> nlt_Verses = new List<nlt_verses>();
        List<niv_verses> niv_Verses = new List<niv_verses>();

        Scripture scripture = new Scripture();
        InboxPage inbox;

        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        StudyModules sm;
        FileData fileData;
        bool isEdit = false;

        public CreateScriptureStudyPage(InboxPage inbox)
        {
            this.inbox = inbox;
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            //this.PageTitle = "";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);

            });

            bibleVersions.Add("English Standard Version");
            bibleVersions.Add("Christian Standard Version");
            bibleVersions.Add("King James Version");
            bibleVersions.Add("New King James Version");
            bibleVersions.Add("New Living Translation");
            bibleVersions.Add("New International Version 2011");

            bibleVersionPicker.ItemsSource = bibleVersions;
        }



        public CreateScriptureStudyPage(InboxPage inbox, StudyModules sm)
        {
            isEdit = true;
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.inbox = inbox;
            this.sm = sm;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            //this.PageTitle = "";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);

            });

            bibleVersions.Add("English Standard Version");
            bibleVersions.Add("Christian Standard Version");
            bibleVersions.Add("King James Version");
            bibleVersions.Add("New King James Version");
            bibleVersions.Add("New Living Translation");
            bibleVersions.Add("New International Version 2011");
            bibleVersionPicker.ItemsSource = bibleVersions;

            switch (sm.scripture.bible_version)
            {
                case "ESV":
                    bibleVersionPicker.SelectedIndex = 0;
                    bibleVersionPicker.SelectedItem = bibleVersions[0];
                    break;
                case "CSB":
                    bibleVersionPicker.SelectedIndex = 1;
                    bibleVersionPicker.SelectedItem = bibleVersions[1];
                    break;
                case "KJV":
                    bibleVersionPicker.SelectedIndex = 2;
                    bibleVersionPicker.SelectedItem = bibleVersions[2];
                    break;
                case "NJKV":
                    bibleVersionPicker.SelectedIndex = 3;
                    bibleVersionPicker.SelectedItem = bibleVersions[3];
                    break;
                case "NLT":
                    bibleVersionPicker.SelectedIndex = 4;
                    bibleVersionPicker.SelectedItem = bibleVersions[4];
                    break;
                case "NIV":
                    bibleVersionPicker.SelectedIndex = 5;
                    bibleVersionPicker.SelectedItem = bibleVersions[5];
                    break;
            }

            details.Text = sm.question;
            link.Text = sm.link;
        }


        void Book_SelectedIndexChanged(object sender, System.EventArgs ev)
        {
            try
            {
                verseUnformatted.Text = "";
                chapters.Clear();
                chapterVerse2.SelectedIndex = -1;
                chapterVerse2.ItemsSource = null;
                chapterVerse.SelectedIndex = -1;
                chapterVerse.ItemsSource = null;

                bookChapter.SelectedIndex = -1;
                bookChapter.ItemsSource = null;

                if (bibleBooks.SelectedIndex != -1)
                {
                    switch (bibleVersionPicker.SelectedIndex)
                    {
                        case 0:
                            var esv = esv_books[bibleBooks.SelectedIndex];
                            AddToChapter(esv.chapters);
                            break;
                        case 1:
                            var hcsv = hcsb_books[bibleBooks.SelectedIndex];
                            AddToChapter(hcsv.chapters);
                            break;
                        case 2:
                            var kjv = kjv_books[bibleBooks.SelectedIndex];
                            AddToChapter(kjv.chapters);
                            break;
                        case 3:
                            var nkjv = nkjv_books[bibleBooks.SelectedIndex];
                            AddToChapter(nkjv.chapters);
                            break;
                        case 4:
                            var nlt = nlt_books[bibleBooks.SelectedIndex];
                            AddToChapter(nlt.chapters);
                            break;
                        case 5:
                            var niv = niv_books[bibleBooks.SelectedIndex];
                            AddToChapter(niv.chapters);
                            break;
                    }

                    bookChapter.ItemsSource = chapters;

                    if (isEdit)
                    {
                        bookChapter.SelectedItem = chapters[sm.scripture.chapter - 1];
                        bookChapter.SelectedIndex = sm.scripture.chapter - 1;
                    }

                }
            }
            catch (Exception e)
            {
            }
        }

        void AddToChapter(int to)
        {
            for (int i = 1; i <= to; i++)
            {
                chapters.Add(i);
            }
        }

        async void Chapter_SelectedIndexChanged(object sender, System.EventArgs ev)
        {
            try
            {
                verseUnformatted.Text = "";
                chapterVerse2.SelectedIndex = -1;
                chapterVerse2.ItemsSource = null;
                chapterVerse.SelectedItem = -1;
                chapterVerse.ItemsSource = null;

                if (bookChapter.SelectedIndex != -1)
                {
                    switch (bibleVersionPicker.SelectedIndex)
                    {
                        case 0:
                            var a = esv_books[bibleBooks.SelectedIndex];
                            var aa = chapters[bookChapter.SelectedIndex];
                            esv_Verses = await App.esv_app.GetEsvVerses(a.osis, aa);
                            esv_Verses.Insert(0, new esv_verses { trimmed_verses = "0" });
                            chapterVerse.ItemsSource = esv_Verses;
                            chapterVerse2.ItemsSource = esv_Verses;
                            if (isEdit)
                            {
                                var si = esv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_from.ToString())).FirstOrDefault();
                                chapterVerse.SelectedIndex = esv_Verses.IndexOf(si);
                                chapterVerse.SelectedItem = si;

                                if (!sm.scripture.verse_num_to.Equals(0))
                                {
                                    var si2 = esv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_to.ToString())).FirstOrDefault();
                                    chapterVerse2.SelectedIndex = esv_Verses.IndexOf(si2);
                                    chapterVerse2.SelectedItem = si2;
                                }
                            }
                            break;
                        case 1:
                            var b = hcsb_books[bibleBooks.SelectedIndex];
                            var bb = chapters[bookChapter.SelectedIndex];
                            hcsb_Verses = await App.hcsb_app.GetHcsbVerses(b.osis, bb);
                            hcsb_Verses.Insert(0, new hcsb_verses { trimmed_verses = "0" });
                            chapterVerse.ItemsSource = hcsb_Verses;
                            chapterVerse2.ItemsSource = hcsb_Verses;
                            if (isEdit)
                            {
                                var si = hcsb_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_from.ToString())).FirstOrDefault();
                                chapterVerse.SelectedIndex = hcsb_Verses.IndexOf(si);
                                chapterVerse.SelectedItem = si;

                                if (!sm.scripture.verse_num_to.Equals(0))
                                {
                                    var si2 = hcsb_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_to.ToString())).FirstOrDefault();
                                    chapterVerse2.SelectedIndex = hcsb_Verses.IndexOf(si2);
                                    chapterVerse2.SelectedItem = si2;
                                }
                            }
                            break;
                        case 2:
                            var c = kjv_books[bibleBooks.SelectedIndex];
                            var cc = chapters[bookChapter.SelectedIndex];
                            kjv_Verses = await App.kjv_app.GetKjvVerses(c.osis, cc);
                            kjv_Verses.Insert(0, new kjv_verses { trimmed_verses = "0" });
                            chapterVerse.ItemsSource = kjv_Verses;
                            chapterVerse2.ItemsSource = kjv_Verses;
                            if (isEdit)
                            {
                                var si = kjv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_from.ToString())).FirstOrDefault();
                                chapterVerse.SelectedIndex = kjv_Verses.IndexOf(si);
                                chapterVerse.SelectedItem = si;

                                if (!sm.scripture.verse_num_to.Equals(0))
                                {
                                    var si2 = kjv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_to.ToString())).FirstOrDefault();
                                    chapterVerse2.SelectedIndex = kjv_Verses.IndexOf(si2);
                                    chapterVerse2.SelectedItem = si2;
                                }
                            }
                            break;
                        case 3:
                            var d = nkjv_books[bibleBooks.SelectedIndex];
                            var dd = chapters[bookChapter.SelectedIndex];
                            nkjv_Verses = await App.nkjv_app.GetNkjvVerses(d.osis, dd);
                            nkjv_Verses.Insert(0, new nkjv_verses { trimmed_verses = "0" });
                            chapterVerse.ItemsSource = nkjv_Verses;
                            chapterVerse2.ItemsSource = nkjv_Verses;
                            if (isEdit)
                            {
                                var si = nkjv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_from.ToString())).FirstOrDefault();
                                chapterVerse.SelectedIndex = nkjv_Verses.IndexOf(si);
                                chapterVerse.SelectedItem = si;

                                if (!sm.scripture.verse_num_to.Equals(0))
                                {
                                    var si2 = nkjv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_to.ToString())).FirstOrDefault();
                                    chapterVerse2.SelectedIndex = nkjv_Verses.IndexOf(si2);
                                    chapterVerse2.SelectedItem = si2;
                                }
                            }
                            break;
                        case 4:
                            var e = nlt_books[bibleBooks.SelectedIndex];
                            var ee = chapters[bookChapter.SelectedIndex];
                            nlt_Verses = await App.nlt_app.GetNltVerses(e.osis, ee);
                            nlt_Verses.Insert(0, new nlt_verses { trimmed_verses = "0" });
                            chapterVerse.ItemsSource = nlt_Verses;
                            chapterVerse2.ItemsSource = nlt_Verses;
                            if (isEdit)
                            {
                                var si = nlt_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_from.ToString())).FirstOrDefault();
                                chapterVerse.SelectedIndex = nlt_Verses.IndexOf(si);
                                chapterVerse.SelectedItem = si;

                                if (!sm.scripture.verse_num_to.Equals(0))
                                {
                                    var si2 = nlt_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_to.ToString())).FirstOrDefault();
                                    chapterVerse2.SelectedIndex = nlt_Verses.IndexOf(si2);
                                    chapterVerse2.SelectedItem = si2;
                                }
                            }
                            break;
                        case 5:
                            var f = niv_books[bibleBooks.SelectedIndex];
                            var ff = chapters[bookChapter.SelectedIndex];
                            niv_Verses = await App.niv_app.GetNivVerses(f.osis, ff);
                            niv_Verses.Insert(0, new niv_verses { trimmed_verses = "0" });
                            chapterVerse.ItemsSource = niv_Verses;
                            chapterVerse2.ItemsSource = niv_Verses;
                            if (isEdit)
                            {
                                var si = niv_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_from.ToString())).FirstOrDefault();
                                chapterVerse.SelectedIndex = niv_Verses.IndexOf(si);
                                chapterVerse.SelectedItem = si;

                                if (!sm.scripture.verse_num_to.Equals(0))
                                {
                                    var si2 = nlt_Verses.Where(x => x.trimmed_verses.Equals(sm.scripture.verse_num_to.ToString())).FirstOrDefault();
                                    chapterVerse2.SelectedIndex = nlt_Verses.IndexOf(si2);
                                    chapterVerse2.SelectedItem = si2;
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        void Verse_SelectedIndexChanged(object sender, System.EventArgs ev)
        {
            chapterVerse2.SelectedIndex = -1;

            if (chapterVerse.SelectedIndex == 0)
            {
                chapterVerse.SelectedIndex = -1;
                verseUnformatted.Text = "";
            }
            else
            {
                if (chapterVerse.SelectedIndex != -1)
                {
                    string verseToShow = "";
                    scripture = new Scripture();
                    switch (bibleVersionPicker.SelectedIndex)
                    {
                        case 0:
                            scripture.human = esv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(esv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = esv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "ESV";
                            verseToShow = "(" + esv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 1:
                            scripture.human = hcsb_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(hcsb_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = hcsb_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "CSB";
                            verseToShow = "(" + hcsb_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 2:
                            scripture.human = kjv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(kjv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = kjv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "KJV";
                            verseToShow = "(" + kjv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 3:
                            scripture.human = nkjv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(nkjv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = nkjv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "NJKV";
                            verseToShow = "(" + nkjv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 4:
                            scripture.human = nlt_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(nlt_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = nlt_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "NLT";
                            verseToShow = "(" + nlt_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 5:
                            scripture.human = niv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(niv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = niv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "NIV";
                            verseToShow = "(" + niv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                    }

                    verseUnformatted.Text = verseToShow;
                }
            }
        }

        void Verse2_SelectedIndexChanged(object sender, EventArgs ev)
        {

            if (chapterVerse2.SelectedIndex == 0)
            {
                chapterVerse2.SelectedIndex = -1;
                if (chapterVerse.SelectedIndex != -1)
                {
                    scripture = new Scripture();
                    string verseToShow = "";
                    switch (bibleVersionPicker.SelectedIndex)
                    {
                        case 0:
                            scripture.human = esv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(esv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = esv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "ESV";
                            verseToShow = "(" + esv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 1:
                            scripture.human = hcsb_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(hcsb_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = hcsb_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "CSB";
                            verseToShow = "(" + hcsb_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 2:
                            scripture.human = kjv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(kjv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = kjv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "KJV";
                            verseToShow = "(" + kjv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 3:
                            scripture.human = nkjv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(nkjv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = nkjv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "NJKV";
                            verseToShow = "(" + nkjv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 4:
                            scripture.human = nlt_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(nlt_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = nlt_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "NLT";
                            verseToShow = "(" + nlt_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                        case 5:
                            scripture.human = niv_books[bibleBooks.SelectedIndex].human;
                            scripture.chapter = chapters[bookChapter.SelectedIndex];
                            scripture.verse_num_from = int.Parse(niv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                            scripture.verse = niv_Verses[chapterVerse.SelectedIndex].unformatted;
                            scripture.bible_version = "NIV";
                            verseToShow = "(" + niv_Verses[chapterVerse.SelectedIndex].trimmed_verses + ") " + scripture.verse;
                            break;
                    }
                    verseUnformatted.Text = verseToShow;
                }
            }
            else
            {
                if (chapterVerse2.SelectedIndex != -1)
                {
                    if (chapterVerse.SelectedIndex == -1)
                    {
                        DisplayAlert("", "The first picker is required.", "Okay");
                        chapterVerse2.SelectedIndex = -1;
                    }
                    else
                    {

                        if (chapterVerse.SelectedIndex == chapterVerse2.SelectedIndex)
                        {
                            DisplayAlert("", "The second verse cannot be equal to the first verse", "Okay");
                            chapterVerse2.SelectedIndex = -1;
                        }
                        else if (chapterVerse.SelectedIndex > chapterVerse2.SelectedIndex)
                        {
                            DisplayAlert("", "You can only select verses below to the first verse.", "Okay");
                            chapterVerse2.SelectedIndex = -1;
                        }
                        else
                        {
                            scripture = new Scripture();
                            string verseToShow = "";
                            switch (bibleVersionPicker.SelectedIndex)
                            {
                                case 0:
                                    scripture.human = esv_books[bibleBooks.SelectedIndex].human;
                                    scripture.chapter = chapters[bookChapter.SelectedIndex];
                                    scripture.verse_num_from = int.Parse(esv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                                    scripture.verse_num_to = int.Parse(esv_Verses[chapterVerse2.SelectedIndex].trimmed_verses);
                                    scripture.bible_version = "ESV";
                                    var verses1 = esv_Verses.Where(x => x.id >= esv_Verses[chapterVerse.SelectedIndex].id && x.id <= esv_Verses[chapterVerse2.SelectedIndex].id);
                                    foreach (var v in verses1)
                                    {
                                        //verseToShow += string.Format(" {0}{1}", MakeUnicode(v.trimmed_verses), v.unformatted);
                                        verseToShow += string.Format("{2}({0}) {1}", v.trimmed_verses, v.unformatted, !string.IsNullOrEmpty(verseToShow) ? " " : string.Empty);
                                    }
                                    scripture.verse = verseToShow;
                                    break;
                                case 1:
                                    scripture.human = hcsb_books[bibleBooks.SelectedIndex].human;
                                    scripture.chapter = chapters[bookChapter.SelectedIndex];
                                    scripture.verse_num_from = int.Parse(hcsb_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                                    scripture.verse_num_to = int.Parse(hcsb_Verses[chapterVerse2.SelectedIndex].trimmed_verses);
                                    scripture.bible_version = "CSB";
                                    var verses2 = hcsb_Verses.Where(x => x.id >= hcsb_Verses[chapterVerse.SelectedIndex].id && x.id <= hcsb_Verses[chapterVerse2.SelectedIndex].id);
                                    foreach (var v in verses2)
                                    {
                                        //verseToShow += string.Format(" {0}{1}", MakeUnicode(v.trimmed_verses), v.unformatted);
                                        verseToShow += string.Format("{2}({0}) {1}", v.trimmed_verses, v.unformatted, !string.IsNullOrEmpty(verseToShow) ? " " : string.Empty);
                                    }
                                    scripture.verse = verseToShow;
                                    break;
                                case 2:
                                    scripture.human = kjv_books[bibleBooks.SelectedIndex].human;
                                    scripture.chapter = chapters[bookChapter.SelectedIndex];
                                    scripture.verse_num_from = int.Parse(kjv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                                    scripture.verse_num_to = int.Parse(kjv_Verses[chapterVerse2.SelectedIndex].trimmed_verses);
                                    scripture.bible_version = "KJV";
                                    var verses3 = kjv_Verses.Where(x => x.id >= kjv_Verses[chapterVerse.SelectedIndex].id && x.id <= kjv_Verses[chapterVerse2.SelectedIndex].id);
                                    foreach (var v in verses3)
                                    {
                                        verseToShow += string.Format("{2}({0}) {1}", v.trimmed_verses, v.unformatted, !string.IsNullOrEmpty(verseToShow) ? " " : string.Empty);
                                        //verseToShow += string.Format(" {0}{1}", MakeUnicode(v.trimmed_verses), v.unformatted);
                                    }
                                    scripture.verse = verseToShow;
                                    break;
                                case 3:
                                    scripture.human = nkjv_books[bibleBooks.SelectedIndex].human;
                                    scripture.chapter = chapters[bookChapter.SelectedIndex];
                                    scripture.verse_num_from = int.Parse(nkjv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                                    scripture.verse_num_to = int.Parse(nkjv_Verses[chapterVerse2.SelectedIndex].trimmed_verses);
                                    scripture.bible_version = "NJKV";
                                    var verses4 = nkjv_Verses.Where(x => x.id >= nkjv_Verses[chapterVerse.SelectedIndex].id && x.id <= nkjv_Verses[chapterVerse2.SelectedIndex].id);
                                    foreach (var v in verses4)
                                    {
                                        verseToShow += string.Format("{2}({0}) {1}", v.trimmed_verses, v.unformatted, !string.IsNullOrEmpty(verseToShow) ? " " : string.Empty);
                                    }
                                    scripture.verse = verseToShow;
                                    break;
                                case 4:
                                    scripture.human = nlt_books[bibleBooks.SelectedIndex].human;
                                    scripture.chapter = chapters[bookChapter.SelectedIndex];
                                    scripture.verse_num_from = int.Parse(nlt_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                                    scripture.verse_num_to = int.Parse(nlt_Verses[chapterVerse2.SelectedIndex].trimmed_verses);
                                    scripture.bible_version = "NLT";
                                    var verses5 = nlt_Verses.Where(x => x.id >= nlt_Verses[chapterVerse.SelectedIndex].id && x.id <= nlt_Verses[chapterVerse2.SelectedIndex].id);
                                    foreach (var v in verses5)
                                    {
                                        verseToShow += string.Format("{2}({0}) {1}", v.trimmed_verses, v.unformatted, !string.IsNullOrEmpty(verseToShow) ? " " : string.Empty);
                                        //verseToShow += string.Format(" {0}{1}", MakeUnicode(v.trimmed_verses), v.unformatted);
                                    }
                                    scripture.verse = verseToShow;
                                    break;
                                case 5:
                                    scripture.human = niv_books[bibleBooks.SelectedIndex].human;
                                    scripture.chapter = chapters[bookChapter.SelectedIndex];
                                    scripture.verse_num_from = int.Parse(niv_Verses[chapterVerse.SelectedIndex].trimmed_verses);
                                    scripture.verse_num_to = int.Parse(niv_Verses[chapterVerse2.SelectedIndex].trimmed_verses);
                                    scripture.bible_version = "NIV";
                                    var verses6 = niv_Verses.Where(x => x.id >= niv_Verses[chapterVerse.SelectedIndex].id && x.id <= niv_Verses[chapterVerse2.SelectedIndex].id);
                                    foreach (var v in verses6)
                                    {
                                        verseToShow += string.Format("{2}({0}) {1}", v.trimmed_verses, v.unformatted, !string.IsNullOrEmpty(verseToShow) ? " " : string.Empty);
                                        //verseToShow += string.Format(" {0}{1}", MakeUnicode(v.trimmed_verses), v.unformatted);

                                    }
                                    scripture.verse = verseToShow;
                                    break;
                            }
                            verseUnformatted.Text = verseToShow;
                        }
                    }
                }
            }
        }

        async void AddGroup_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(details.Text))
            {
                if (fileData != null)
                {
                    Stream stream = new MemoryStream(fileData.DataArray);
                    if (isEdit)
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                UserDialogs.Instance.ShowLoading("Saving");
                                cts = new CancellationTokenSource();
                                string dict = JsonConvert.SerializeObject(new { study_module = new { study_module_id = sm.id, group_id = int.Parse(DataClass.GetInstance.GroupId), human = scripture.human, chapter = scripture.chapter, verse_num_from = scripture.verse_num_from, verse_num_to = scripture.verse_num_to, verse = scripture.verse, bible_version = scripture.bible_version, question = details.Text, link = link.Text, files = fileData.FileName } });
                                await advanceRest.MultiPartDataContentAsync(Constants.EDIT_STUDY_MODULE, dict, cts.Token, stream, "study_module");
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    UserDialogs.Instance.HideLoading();
                                    cts = null;
                                });
                            }
                        });
                    }
                    else
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                UserDialogs.Instance.ShowLoading("Saving");
                                cts = new CancellationTokenSource();
                                string dict = JsonConvert.SerializeObject(new { study_module = new { group_id = int.Parse(DataClass.GetInstance.GroupId), human = scripture.human, chapter = scripture.chapter, verse_num_from = scripture.verse_num_from, verse_num_to = scripture.verse_num_to, verse = scripture.verse, bible_version = scripture.bible_version, question = details.Text, link = link.Text, files = fileData.FileName } });
                                await advanceRest.MultiPartDataContentAsync(Constants.STUDY_MODULE, dict, cts.Token, stream, "study_module");
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    UserDialogs.Instance.HideLoading();
                                    cts = null;
                                });
                            }
                        });
                    }
                }
                else
                {
                    if (isEdit)
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                UserDialogs.Instance.ShowLoading("Saving");
                                cts = new CancellationTokenSource();
                                string dict = JsonConvert.SerializeObject(new { study_module = new { study_module_id = sm.id, group_id = int.Parse(DataClass.GetInstance.GroupId), human = scripture.human, chapter = scripture.chapter, verse_num_from = scripture.verse_num_from, verse_num_to = scripture.verse_num_to, verse = scripture.verse, bible_version = scripture.bible_version, question = details.Text, link = link.Text } });
                                await advanceRest.PostRequestAsync(Constants.EDIT_STUDY_MODULE, dict, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    UserDialogs.Instance.HideLoading();
                                    cts = null;
                                });
                            }
                        });
                    }
                    else
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                UserDialogs.Instance.ShowLoading("Saving");
                                cts = new CancellationTokenSource();
                                string dict = JsonConvert.SerializeObject(new { study_module = new { group_id = int.Parse(DataClass.GetInstance.GroupId), human = scripture.human, chapter = scripture.chapter, verse_num_from = scripture.verse_num_from, verse_num_to = scripture.verse_num_to, verse = scripture.verse, bible_version = scripture.bible_version, question = details.Text, link = link.Text } });
                                await advanceRest.PostRequestAsync(Constants.STUDY_MODULE, dict, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    UserDialogs.Instance.HideLoading();
                                    cts = null;
                                });
                            }
                        });
                    }
                }

                //Device.BeginInvokeOnMainThread(async () =>
                //{
                //    cts = new CancellationTokenSource();

                //    UserDialogs.Instance.ShowLoading("Saving");

                //});
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        async void BibleVersion_SelectedIndexChanged(object sender, System.EventArgs ev)
        {
            try
            {
                verseUnformatted.Text = "";
                chapters.Clear();
                chapterVerse2.SelectedIndex = -1;
                chapterVerse2.ItemsSource = null;
                chapterVerse.SelectedIndex = -1;
                chapterVerse.ItemsSource = null;

                bookChapter.SelectedIndex = -1;
                bookChapter.ItemsSource = null;

                bibleBooks.SelectedIndex = -1;
                bibleBooks.ItemsSource = null;


                switch (bibleVersions[bibleVersionPicker.SelectedIndex])
                {
                    case "English Standard Version":
                        esv_books = await App.esv_app.GetAllEsvBooks();
                        bibleBooks.ItemsSource = esv_books;
                        if (isEdit)
                        {
                            var si = esv_books.Where(x => x.human == sm.scripture.human).FirstOrDefault();
                            bibleBooks.SelectedIndex = esv_books.IndexOf(si);
                            bibleBooks.SelectedItem = si;
                        }
                        break;
                    case "Christian Standard Version":
                        hcsb_books = await App.hcsb_app.GetAllHcsbBooks();
                        bibleBooks.ItemsSource = hcsb_books;
                        if (isEdit)
                        {
                            var si = hcsb_books.Where(x => x.human == sm.scripture.human).FirstOrDefault();
                            bibleBooks.SelectedIndex = hcsb_books.IndexOf(si);
                            bibleBooks.SelectedItem = si;
                        }
                        break;
                    case "King James Version":
                        kjv_books = await App.kjv_app.GetAllKjvBooks();
                        bibleBooks.ItemsSource = kjv_books;
                        if (isEdit)
                        {
                            var si = kjv_books.Where(x => x.human == sm.scripture.human).FirstOrDefault();
                            bibleBooks.SelectedIndex = kjv_books.IndexOf(si);
                            bibleBooks.SelectedItem = si;
                        }
                        break;
                    case "New King James Version":
                        nkjv_books = await App.nkjv_app.GetAllNkjvBooks();
                        bibleBooks.ItemsSource = nkjv_books;
                        if (isEdit)
                        {
                            var si = nkjv_books.Where(x => x.human == sm.scripture.human).FirstOrDefault();
                            bibleBooks.SelectedIndex = nkjv_books.IndexOf(si);
                            bibleBooks.SelectedItem = si;
                        }
                        break;
                    case "New Living Translation":
                        nlt_books = await App.nlt_app.GetAllNltBooks();
                        bibleBooks.ItemsSource = nlt_books;
                        if (isEdit)
                        {
                            var si = nlt_books.Where(x => x.human == sm.scripture.human).FirstOrDefault();
                            bibleBooks.SelectedIndex = nlt_books.IndexOf(si);
                            bibleBooks.SelectedItem = si;
                        }
                        break;
                    case "New International Version 2011":
                        niv_books = await App.niv_app.GetNivBooks();
                        bibleBooks.ItemsSource = niv_books;
                        if (isEdit)
                        {
                            var si = niv_books.Where(x => x.human == sm.scripture.human).FirstOrDefault();
                            bibleBooks.SelectedIndex = niv_books.IndexOf(si);
                            bibleBooks.SelectedItem = si;
                        }
                        break;
                }


            }
            catch (Exception e)
            {
            }

        }

        void Details_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Text")
            {
                if (!string.IsNullOrEmpty(details.Text))
                {
                    details.Placeholder = "";

                }
                else
                {
                    details.Placeholder = "Type details here";
                }
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    StudyModules sm = JsonConvert.DeserializeObject<StudyModules>(jsonData["study_module"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    fileData = null;
                    UserDialogs.Instance.HideLoading();
                    Navigation.PopAsync(false);
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
