﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class InboxPage : RootViewPage, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        StudyModuleView study;
        bool isHeaderLoaded = false;
        AdvanceRestService advanceRest;

        ObservableCollection<PrayerCommentModel> pcms = new ObservableCollection<PrayerCommentModel>();

        public InboxPage()
        {
            App.Log("TRIGGER TEST INBOX");
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Messages";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            if (DataClass.GetInstance.user.role == 1)
            {
                this.RightIcon = "Add";
                this.RightIconTint = Color.Black;
                this.RightButtonCommand = new Command(OpenScripturePage);
            }

            this.LeftIcon = "other_tribes";
            this.LeftIconTint = Color.Black;
            this.LeftIconWidthRequest = (double)(30.ScaleWidth());
            this.LeftIconHeightRequest = (double)(30.ScaleWidth());
            this.LeftIconContainerPadding = new Thickness(0, 0, 0, 5.ScaleHeight());
            this.LeftButtonCommand = new Command(() =>
            {
                ((HomePage)this.Parent.Parent).Navigation.PushAsync(new OtherTribesPage(), false);
            });


            listView2.HeaderTemplate = new DataTemplate(HeaderTemplate);
            listView2.Header = new StudyModules { question = "", scripture = new Scripture() };
            listView2.FlowItemsSource = pcms;
            isHeaderLoaded = true;


            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.ADD_GROUP + DataClass.GetInstance.GroupCode, cts.Token, 4);

                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            });
        }

        public void UpdateInboxes(string pp, string ct)
        {
            study.ChangeChatPreview(pp, ct);
        }

        public void RefreshPage(){
            listView2.BeginRefresh();
            pcms.Clear();
        }

        async void DeleteComment(int obj)
        {
            await Task.Run(async () =>
            {
                try
                {
                    UserDialogs.Instance.ShowLoading("Deleting Comment");
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.DELETE_STUDY_MODULE_COMMENT + "?study_module_comment_id=" + obj, cts.Token, 3);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    App.Log(ex.Message);
                    //ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        UserDialogs.Instance.HideLoading();
                        cts = null;
                    });
                }
            });
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ChangeChatPreview(string data)
        {
            study.ChangeChatPreview(data);
        }

        void OpenScripturePage()
        {
            Application.Current.MainPage?.Navigation.PushAsync(new CreateScriptureStudyPage(this), false);
        }

        public async void LoadComments(int id)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_STUDY_MODULE_COMMENT + id, cts.Token, 2);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    App.Log(ex.Message);
                    //ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        void Chatroom_Refreshing(object sender, EventArgs e)
        {
            study.LoadInbox(true);
            listView2.IsRefreshing = false;
        }

        StudyModuleView HeaderTemplate()
        {
            study = new StudyModuleView(this);
            study.InboxTapped += Inbox_ItemTapped;
            study.SendError += Send_Error;
            study.SendMessage += SendComment_Clicked;
            return study;
        }

        void Inbox_ItemTapped(object sender, EventArgs e)
        {
            Application.Current.MainPage?.Navigation.PushAsync(new ConversationPage((InboxModel)sender, ((HomePage)this.Parent.Parent)), false);
        }

        void Send_Error(object sender, EventArgs e)
        {
            var strings = (ObservableCollection<string>)sender;
            DisplayAlert(strings[0], strings[1], "Okay");
        }

        async void SendComment_Clicked(object sender, EventArgs e)
        {

            await Task.Run(async () =>
            {
                try
                {
                    string comment = sender.ToString();
                    UserDialogs.Instance.ShowLoading("Adding Comment");
                    cts = new CancellationTokenSource();
                    await advanceRest.PostRequestAsync(Constants.ADD_STUDY_MODULE_COMMENT, comment, cts.Token, 1);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        UserDialogs.Instance.HideLoading();
                        cts = null;
                    });
                }
            });
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            study.LoadInbox(false);
        }

        public void FromStudyScripture(StudyModules s)
        {
            listView2.Header = s;
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            PrayerCommentModel pcm = JsonConvert.DeserializeObject<PrayerCommentModel>(jsonData["comment"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            pcms.Add(pcm);
                            UserDialogs.Instance.HideLoading();
                            break;
                        case 2:
                            var temp = JsonConvert.DeserializeObject<ObservableCollection<PrayerCommentModel>>(jsonData["comments"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            pcms = temp;
                            listView2.FlowItemsSource = pcms;
                            break;
                        case 3:
                            pcms.Remove(pcms.Where(x => x.id.Equals(int.Parse(jsonData["study_module_comment_id"].ToString()))).First());
                            UserDialogs.Instance.HideLoading();
                            break;
                        case 4:
                            var u = DataClass.GetInstance.user;
                            if (!u.role.Equals(int.Parse(jsonData["group"]["role"].ToString())))
                            {
                                u.role = int.Parse(jsonData["group"]["role"].ToString());
                                DataClass.GetInstance.user = u;
                                //if (u.role.Equals(1))
                                //{
                                //    await DisplayAlert("Role Changed", "You are a small admin now.", "Okay");
                                //}
                                //if (u.role.Equals(2))
                                //{
                                //    await DisplayAlert("Role Changed", "You are a regular user now.", "Okay");
                                //}
                                //if (u.role.Equals(3))
                                //{
                                //    await DisplayAlert("Role Changed", "You are a church admin now.", "Okay");
                                //}
                                //App.ShowMainPage();
                            }
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}