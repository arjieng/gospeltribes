﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using XamForms.Controls;

namespace gospeltribe
{
    public partial class CalendarPage : RootViewPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        public ObservableCollection<EventsModel> events = new ObservableCollection<EventsModel>();
        CancellationTokenSource cts;
        public bool fromCreateEvent = false;
        ObservableCollection<string> colors;
        List<SpecialDate> specialDates1 = new List<SpecialDate>();
        List<SpecialDate> specialDates2 = new List<SpecialDate>();
        EventBarViews ebv;
        int hasLoadMore = 0;
        string loadMoreURL = "";

        public CalendarPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            colors = new ObservableCollection<string>();

            foreach (var field in typeof(Xamarin.Forms.Color).GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                if (field != null && !String.IsNullOrEmpty(field.Name))
                    colors.Add(field.Name);
            }

            InitializeComponent();
            listView.FlowColumnTemplate = new DataTemplate(EventBarViewsDataTemplate);
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Events";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;
            int role = DataClass.GetInstance.user.role;
            if (role == 1)
            {
                this.RightIcon = "Add";
                this.RightIconTint = Color.Black;
                this.RightButtonCommand = new Command(() =>
                {
                    Application.Current.MainPage?.Navigation.PushAsync(new CreateEventPage(this), false);
                });
            }

            this.LeftIcon = "other_tribes";
            this.LeftIconTint = Color.Black;
            this.LeftIconWidthRequest = (double)(30.ScaleWidth());
            this.LeftIconHeightRequest = (double)(30.ScaleWidth());
            this.LeftIconContainerPadding = new Thickness(0, 0, 0, 5.ScaleHeight());
            this.LeftButtonCommand = new Command(() =>
            {
                ((HomePage)this.Parent.Parent).Navigation.PushAsync(new OtherTribesPage(), false);
            });

            calendar2.StartDate = new DateTime((calendar1.StartDate.Month + 1) == 13 ? (calendar1.StartDate.Year + 1) : calendar1.StartDate.Year, (calendar1.StartDate.Month + 1) == 13 ? 1 : (calendar1.StartDate.Month + 1), 1);
        }

        void Calendar_DateClicked(object sender, DateTimeEventArgs e)
        {
            var cSD = events.Where(x => x.start_date.Date.Equals(e.DateTime.Date));
            if (cSD.Count() != 0)
            {
                IEnumerable<EventsModel> ev = events.Where(x => new DateTime(x.start_date.Year, x.start_date.Month, x.start_date.Day) == e.DateTime.Date);
                if (cSD.Count() > 1)
                {
                    Navigation.PushModalAsync(new NavigationPage(new EventPickerPage(this, ev)), false);
                }
                else
                {
                    if (ev != null)
                    {
                        Navigation.PushModalAsync(new NavigationPage(new EventsView(ev.First(), this)), false);
                    }
                }
            }
        }

        void RefreshCalendars()
        {
            calendar1.SpecialDates.Clear();
            calendar2.SpecialDates.Clear();
            calendar1.RaiseSpecialDatesChanged();
            calendar2.RaiseSpecialDatesChanged();

            specialDates1.Clear();
            specialDates1 = null;
            specialDates1 = new List<SpecialDate>();
            specialDates2.Clear();
            specialDates2 = null;
            specialDates2 = new List<SpecialDate>();



            foreach (var ev in events)
            {
                App.Log(JsonConvert.SerializeObject(ev));
                App.Log(calendar1.StartDate.Date.ToString());

                if (ev.repeat.Equals("None"))
                {
                    DateTime dateTime1 = new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day);
                    var temp_spDates1 = specialDates1.Where(x => x.Date.Equals(dateTime1.Date));
                    if (!temp_spDates1.Count().Equals(0))
                    {
                        specialDates1[specialDates1.IndexOf(temp_spDates1.FirstOrDefault())].BackgroundColor = Color.Gray;
                    }
                    else
                    {
                        var converter = new ColorTypeConverter();
                        Color result = Color.Black;
                        if (!string.IsNullOrEmpty(ev.subtitle))
                        {
                            result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                        }


                        specialDates1.Add(new SpecialDate(new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day))
                        {
                            BackgroundColor = result,
                            Selectable = true
                        });
                    }


                    DateTime dateTime2 = new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day);
                    var temp_spDates2 = specialDates2.Where(x => x.Date.Equals(dateTime2.Date));
                    if (!temp_spDates2.Count().Equals(0))
                    {
                        specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                    }
                    else
                    {
                        var converter = new ColorTypeConverter();
                        Color result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                        specialDates2.Add(new SpecialDate(new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day))
                        {
                            BackgroundColor = result,
                            Selectable = true
                        });
                    }
                }

                if (ev.repeat.Equals("Monthly"))
                {
                    DateTime dateTime = new DateTime(calendar1.StartDate.Year, calendar1.StartDate.Month, ev.start_date.Day);
                    DateTime dateTime2 = new DateTime(calendar2.StartDate.Year, calendar2.StartDate.Month, ev.start_date.Day);


                    if (dateTime.Date >= DateTime.Now.Date)
                    {
                        var temp_spDates = specialDates1.Where(x => x.Date.Equals(dateTime.Date));
                        if (!temp_spDates.Count().Equals(0))
                        {
                            if (ev.start_date.Date != ev.end_date.Date)
                            {
                                DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                                if (dateTime.Date <= endDate.Date)
                                {
                                    specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                                }
                            }
                            else
                            {
                                specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                            }
                            //if (ev.start_date.Date != ev.end_date.Date)
                            //{
                            //    if (calendar1.StartDate.Date <= ev.end_date.Date)
                            //    {
                            //        specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                            //    }
                            //}
                            //else
                            //{
                            //    specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                            //}

                        }
                        else
                        {
                            var converter = new ColorTypeConverter();
                            Color result = Color.Black;
                            if (!string.IsNullOrEmpty(ev.subtitle))
                            {
                                result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                            }

                            if (ev.start_date.Date != ev.end_date.Date)
                            {
                                DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                                if (dateTime.Date <= endDate.Date)
                                {
                                    specialDates1.Add(new SpecialDate(dateTime)
                                    {
                                        BackgroundColor = result,
                                        Selectable = true
                                    });
                                }
                            }
                            else
                            {
                                specialDates1.Add(new SpecialDate(dateTime)
                                {
                                    BackgroundColor = result,
                                    Selectable = true
                                });
                            }

                            //if (ev.start_date.Date != ev.end_date.Date)
                            //{
                            //    if (calendar1.StartDate.Date <= ev.end_date.Date)
                            //    {
                            //        specialDates1.Add(new SpecialDate(dateTime)
                            //        {
                            //            BackgroundColor = result,
                            //            Selectable = true
                            //        });
                            //    }
                            //}
                            //else
                            //{
                            //    specialDates1.Add(new SpecialDate(dateTime)
                            //    {
                            //        BackgroundColor = result,
                            //        Selectable = true
                            //    });
                            //}
                        }
                    }



                    var temp_spDates2 = specialDates2.Where(x => x.Date.Equals(dateTime2.Date));
                    if (!temp_spDates2.Count().Equals(0))
                    {
                        if (ev.start_date.Date != ev.end_date.Date)
                        {
                            DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                            if (dateTime2.Date <= endDate.Date)
                            {
                                specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                            }
                        }
                        else
                        {
                            specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                        }
                    }
                    else
                    {
                        var converter = new ColorTypeConverter();
                        Color result = (Color)converter.ConvertFromInvariantString(ev.subtitle);

                        if (ev.start_date.Date != ev.end_date.Date)
                        {
                            DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                            if(dateTime2.Date <= endDate.Date)
                            {
                                specialDates2.Add(new SpecialDate(dateTime2)
                                {
                                    BackgroundColor = result,
                                    Selectable = true
                                });
                            }
                        }
                        else
                        {
                            specialDates2.Add(new SpecialDate(dateTime2)
                            {
                                BackgroundColor = result,
                                Selectable = true
                            });
                        }
                    }
                }

                if (ev.repeat.Equals("Weekly"))
                {
                    string[] repeat_days = ev.repeat_days.Split(',');
                    var calendar1_days = AllDatesInMonth(calendar1.StartDate.Year, calendar1.StartDate.Month).Where(i => repeat_days.Contains(i.DayOfWeek.ToString()) && i.Date >= DateTime.Now.Date);
                    var calendar2_days = AllDatesInMonth(calendar2.StartDate.Year, calendar2.StartDate.Month).Where(i => repeat_days.Contains(i.DayOfWeek.ToString()) && i.Date >= DateTime.Now.Date);

                    foreach (var rp in calendar1_days)
                    {
                        DateTime dateTime = new DateTime(rp.Year, rp.Month, rp.Day);
                        var temp_spDates = specialDates1.Where(x => x.Date.Equals(dateTime.Date));
                        if (!temp_spDates.Count().Equals(0))
                        {
                            if (ev.start_date.Date != ev.end_date.Date)
                            {
                                DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                                if (dateTime.Date <= endDate.Date)
                                {
                                    specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                                }
                            }
                            else
                            {
                                specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                            }
                            //if (ev.start_date.Date != ev.end_date.Date)
                            //{
                            //    if (calendar1.StartDate.Date <= ev.end_date.Date)
                            //    {
                            //        specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                            //    }
                            //}
                            //else
                            //{
                            //    specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                            //}
                        }
                        else
                        {
                            var converter = new ColorTypeConverter();
                            Color result = Color.Black;
                            if (!string.IsNullOrEmpty(ev.subtitle))
                            {
                                result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                            }

                            if (ev.start_date.Date != ev.end_date.Date)
                            {
                                DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                                if (dateTime.Date <= endDate.Date)
                                {
                                    specialDates1.Add(new SpecialDate(dateTime)
                                    {
                                        BackgroundColor = result,
                                        Selectable = true
                                    });
                                }
                            }
                            else
                            {
                                specialDates1.Add(new SpecialDate(dateTime)
                                {
                                    BackgroundColor = result,
                                    Selectable = true
                                });
                            }

                            //if (ev.start_date.Date != ev.end_date.Date)
                            //{
                            //    if (calendar1.StartDate.Date <= ev.end_date.Date)
                            //    {
                            //        specialDates1.Add(new SpecialDate(dateTime)
                            //        {
                            //            BackgroundColor = result,
                            //            Selectable = true
                            //        });
                            //    }
                            //}
                            //else
                            //{
                            //    specialDates1.Add(new SpecialDate(dateTime)
                            //    {
                            //        BackgroundColor = result,
                            //        Selectable = true
                            //    });
                            //}
                        }
                    }

                    foreach (var rp in calendar2_days)
                    {
                        DateTime dateTime2 = new DateTime(rp.Year, rp.Month, rp.Day);
                        var temp_spDates2 = specialDates2.Where(x => x.Date.Equals(dateTime2.Date));
                        if (!temp_spDates2.Count().Equals(0))
                        {
                            if (ev.start_date.Date != ev.end_date.Date)
                            {
                                DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                                if (dateTime2.Date <= endDate.Date)
                                {
                                    specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                                }
                            }
                            else { specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray; }
                            //if (ev.start_date.Date != ev.end_date.Date)
                            //{
                            //    if (calendar2.StartDate.Date <= ev.end_date.Date)
                            //    {
                            //        specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                            //    }
                            //}
                            //else
                            //{
                            //    specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                            //}

                        }
                        else
                        {
                            var converter = new ColorTypeConverter();
                            Color result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                            if (ev.start_date.Date != ev.end_date.Date)
                            {
                                DateTime endDate = new DateTime(ev.end_date.Date.Year, ev.end_date.Date.Month, ev.end_date.Date.Day);
                                if (dateTime2.Date <= endDate.Date)
                                {
                                    specialDates2.Add(new SpecialDate(dateTime2)
                                    {
                                        BackgroundColor = result,
                                        Selectable = true
                                    });
                                }
                            }
                            else
                            {
                                specialDates2.Add(new SpecialDate(dateTime2)
                                {
                                    BackgroundColor = result,
                                    Selectable = true
                                });
                            }
                            //if (ev.start_date.Date != ev.end_date.Date)
                            //{
                            //    if (calendar2.StartDate.Date <= ev.end_date.Date)
                            //    {
                            //        specialDates2.Add(new SpecialDate(dateTime2)
                            //        {
                            //            BackgroundColor = result,
                            //            Selectable = true
                            //        });
                            //    }
                            //}
                            //else
                            //{
                            //    specialDates2.Add(new SpecialDate(dateTime2)
                            //    {
                            //        BackgroundColor = result,
                            //        Selectable = true
                            //    });
                            //}
                        }
                    }
                }


                //if (ev.repeat.Equals("None"))
                //{
                //    if (calendar1.StartDate.Month.Equals(ev.start_date.Month))
                //    {
                //        DateTime dateTime = new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day);
                //        var temp_spDates = specialDates1.Where(x => x.Date.Equals(dateTime.Date));
                //        if (!temp_spDates.Count().Equals(0))
                //        {
                //            specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                //        }
                //        else
                //        {
                //            var converter = new ColorTypeConverter();
                //            Color result = Color.Black;
                //            if (!string.IsNullOrEmpty(ev.subtitle))
                //            {
                //                result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                //            }


                //            specialDates1.Add(new SpecialDate(new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day))
                //            {
                //                BackgroundColor = result,
                //                Selectable = true
                //            });
                //        }
                //    }

                //    if (calendar2.StartDate.Month.Equals(ev.start_date.Month))
                //    {
                //        DateTime dateTime = new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day);
                //        var temp_spDates = specialDates2.Where(x => x.Date.Equals(dateTime.Date));
                //        if (!temp_spDates.Count().Equals(0))
                //        {
                //            specialDates2[specialDates2.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                //        }
                //        else
                //        {
                //            var converter = new ColorTypeConverter();
                //            Color result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                //            specialDates2.Add(new SpecialDate(new DateTime(ev.start_date.Year, ev.start_date.Month, ev.start_date.Day))
                //            {
                //                BackgroundColor = result,
                //                Selectable = true
                //            });
                //        }
                //    }
                //}

                //if (ev.repeat.Equals("Monthly"))
                //{
                //    if (calendar1.StartDate.Date >= DateTime.Now.Date)
                //    {

                //        DateTime dateTime = new DateTime(calendar1.StartDate.Year, calendar1.StartDate.Month, ev.start_date.Day);
                //        var temp_spDates = specialDates1.Where(x => x.Date.Equals(dateTime.Date));
                //        if (!temp_spDates.Count().Equals(0))
                //        {
                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar1.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                //                }
                //            }
                //            else
                //            {
                //                specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                //            }
                //        }
                //        else
                //        {
                //            var converter = new ColorTypeConverter();
                //            Color result = Color.Black;
                //            if (!string.IsNullOrEmpty(ev.subtitle))
                //            {
                //                result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                //            }

                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar1.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates1.Add(new SpecialDate(dateTime)
                //                    {
                //                        BackgroundColor = result,
                //                        Selectable = true
                //                    });
                //                }
                //            }
                //            else
                //            {
                //                specialDates1.Add(new SpecialDate(dateTime)
                //                {
                //                    BackgroundColor = result,
                //                    Selectable = true
                //                });
                //            }

                //        }
                //    }

                //    if (calendar2.StartDate.Date >= DateTime.Now.Date  )
                //    {
                //        DateTime dateTime2 = new DateTime(calendar2.StartDate.Year, calendar2.StartDate.Month, ev.start_date.Day);
                //        var temp_spDates2 = specialDates2.Where(x => x.Date.Equals(dateTime2.Date));
                //        if (!temp_spDates2.Count().Equals(0))
                //        {

                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar2.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                //                }
                //            }
                //            else
                //            {
                //                specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                //            }

                //        }
                //        else
                //        {
                //            var converter = new ColorTypeConverter();
                //            Color result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar2.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates2.Add(new SpecialDate(dateTime2)
                //                    {
                //                        BackgroundColor = result,
                //                        Selectable = true
                //                    });
                //                }
                //            }
                //            else {
                //                specialDates2.Add(new SpecialDate(dateTime2)
                //                {
                //                    BackgroundColor = result,
                //                    Selectable = true
                //                });
                //            }

                //        }
                //    }

                //}

                //if (ev.repeat.Equals("Weekly"))
                //{
                //    string[] repeat_days = ev.repeat_days.Split(',');
                //    var calendar1_days = AllDatesInMonth(calendar1.StartDate.Year, calendar1.StartDate.Month).Where(i => repeat_days.Contains(i.DayOfWeek.ToString()) && i.Date >= DateTime.Now.Date);
                //    var calendar2_days = AllDatesInMonth(calendar2.StartDate.Year, calendar2.StartDate.Month).Where(i => repeat_days.Contains(i.DayOfWeek.ToString()) && i.Date >= DateTime.Now.Date);

                //    foreach (var rp in calendar1_days)
                //    {
                //        DateTime dateTime = new DateTime(rp.Year, rp.Month, rp.Day);
                //        var temp_spDates = specialDates1.Where(x => x.Date.Equals(dateTime.Date));
                //        if (!temp_spDates.Count().Equals(0))
                //        {
                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar1.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                //                }
                //            }
                //            else
                //            {
                //                specialDates1[specialDates1.IndexOf(temp_spDates.FirstOrDefault())].BackgroundColor = Color.Gray;
                //            }
                //        }
                //        else
                //        {
                //            var converter = new ColorTypeConverter();
                //            Color result = Color.Black;
                //            if (!string.IsNullOrEmpty(ev.subtitle))
                //            {
                //                result = (Color)converter.ConvertFromInvariantString(ev.subtitle);
                //            }

                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar1.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates1.Add(new SpecialDate(dateTime)
                //                    {
                //                        BackgroundColor = result,
                //                        Selectable = true
                //                    });
                //                }
                //            }
                //            else
                //            {
                //                specialDates1.Add(new SpecialDate(dateTime)
                //                {
                //                    BackgroundColor = result,
                //                    Selectable = true
                //                });
                //            }
                //        }
                //    }

                //    foreach (var rp in calendar2_days)
                //    {
                //        DateTime dateTime2 = new DateTime(rp.Year, rp.Month, rp.Day);
                //        var temp_spDates2 = specialDates2.Where(x => x.Date.Equals(dateTime2.Date));
                //        if (!temp_spDates2.Count().Equals(0))
                //        {
                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar2.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                //                }
                //            }
                //            else
                //            {
                //                specialDates2[specialDates2.IndexOf(temp_spDates2.FirstOrDefault())].BackgroundColor = Color.Gray;
                //            }

                //        }
                //        else
                //        {
                //            var converter = new ColorTypeConverter();
                //            Color result = (Color)converter.ConvertFromInvariantString(ev.subtitle);

                //            if (ev.start_date.Date != ev.end_date.Date)
                //            {
                //                if (calendar2.StartDate.Date <= ev.end_date.Date)
                //                {
                //                    specialDates2.Add(new SpecialDate(dateTime2)
                //                    {
                //                        BackgroundColor = result,
                //                        Selectable = true
                //                    });
                //                }
                //            }
                //            else
                //            {
                //                specialDates2.Add(new SpecialDate(dateTime2)
                //                {
                //                    BackgroundColor = result,
                //                    Selectable = true
                //                });
                //            }
                //        }
                //    }

                //}

            }

            calendar1.SpecialDates = specialDates1;
            calendar2.SpecialDates = specialDates2;
            calendar1.RaiseSpecialDatesChanged();
            calendar2.RaiseSpecialDatesChanged();
        }

        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        void Calendar1_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("StartDate"))
            {
                calendar2.StartDate = new DateTime((calendar1.StartDate.Month + 1) == 13 ? (calendar1.StartDate.Year + 1) : calendar1.StartDate.Year, (calendar1.StartDate.Month + 1) == 13 ? 1 : (calendar1.StartDate.Month + 1), 1);
                RefreshCalendars();
            }
        }

        void Calendar2_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("StartDate"))
            {
                calendar1.StartDate = new DateTime((calendar2.StartDate.Month - 1) == 0 ? (calendar2.StartDate.Year - 1) : calendar2.StartDate.Year, (calendar2.StartDate.Month - 1) == 0 ? 12 : (calendar2.StartDate.Month - 1), 1);
                RefreshCalendars();
            }
        }

        EventBarViews EventBarViewsDataTemplate(){
            ebv = new EventBarViews(this);
            ebv.SetBinding(ebv.EventProperty, ".");
            return ebv;
        }

        public void RemoveEVent(EventsModel eventsModel){
            events.Remove(events.Where(x => x.id.Equals(eventsModel.id)).First());
            RefreshCalendars();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    if (fromCreateEvent){
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            listView.IsRefreshing = true;
                        });
                    }

                    await advanceRest.GetRequest(Constants.GET_ALL_EVENTS(), cts.Token, 0);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    App.Log(ex.Message);
                    //ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        void Event_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Navigation.PushModalAsync(new NavigationPage(new EventsView((EventsModel)e.Item, this)), false);
        }

        async void Events_Refreshing(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_ALL_EVENTS(), cts.Token, 0);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        public void AddEvent(EventsModel em)
        {
            //events.Add(em);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 0:
                            events = JsonConvert.DeserializeObject<ObservableCollection<EventsModel>>(jsonData["events"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                            listView.FlowItemsSource = events.Reverse().ToList();
                            listView.IsRefreshing = false;
                            fromCreateEvent = false;
                            hasLoadMore = int.Parse(jsonData["load_more"]["load_more"].ToString());
                            loadMoreURL = Constants.domain + jsonData["load_more"]["url"];
                            break;
                        case 1:
                            var temp_events = JsonConvert.DeserializeObject<ObservableCollection<EventsModel>>(jsonData["events"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            foreach (var te in temp_events) { events.Add(te); }
                            hasLoadMore = int.Parse(jsonData["load_more"]["load_more"].ToString());
                            loadMoreURL = Constants.domain + jsonData["load_more"]["url"];
                            break;
                    }

                    RefreshCalendars();
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }

        async void Event_FlowItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            if (events != null)
            {
                if (events.Any())
                {
                    if (e.Item.Equals(events.Last()))
                    {
                        if (!hasLoadMore.Equals(0))
                        {
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    //redLabel.IsVisible = true;
                                    await advanceRest.GetRequest(loadMoreURL, cts.Token, 1);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });
                        }
                    }
                }
            }
        }
    }
}