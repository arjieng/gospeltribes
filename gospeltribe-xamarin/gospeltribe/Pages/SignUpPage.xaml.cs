﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class SignUpPage : ContentPage, IAdvanceRestConnector
    {
        bool isClicked = false;
        CancellationTokenSource cts;
        DataClass dataClass = DataClass.GetInstance;
        AdvanceRestService advanceRestService;

        public SignUpPage()
        {
            advanceRestService = new AdvanceRestService();
            advanceRestService.WebServiceDelegate = this;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "ResetPassword", (obj) =>
            {
                Navigation.PushAsync(new ResetPassword(obj), false);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "ResetPassword");
        }

        public SignUpPage(string code)
        {
            InitializeComponent();
            codeEntry.Text = code;
        }

        void GroupCode_Completed(object sender, EventArgs e)
        {
            emailEntry.Focus();
        }

        void Email_Completed(object sender, EventArgs e)
        {
            usernameEntry.Focus();
        }

        void Username_Completed(object sender, EventArgs e)
        {
            passwordEntry.Focus();
        }

        void Password_Completed(object sender, EventArgs e)
        {
            confirmEntry.Focus();
        }

        void LeftButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(false);
        }

        async void SignUp_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;
            if (String.IsNullOrEmpty(codeEntry.Text))
            {
                DisplayError(codeEntry, "Group Code is required!");
                isNotError = false;
            }

            if (String.IsNullOrEmpty(emailEntry.Text))
            {
                DisplayError(emailEntry, "Email is required!");
                isNotError = false;
            }
            else
            {
                if (!emailEntry.Text.IsValidEmail())
                {
                    DisplayError(emailEntry, "Please enter a valid email!");
                    isNotError = false;
                }
            }

            if (String.IsNullOrEmpty(usernameEntry.Text))
            {
                DisplayError(usernameEntry, "Username is required!");
                isNotError = false;
            }

            if (String.IsNullOrEmpty(passwordEntry.Text))
            {
                DisplayError(passwordEntry, "Password is required!");
                isNotError = false;
            }
            else
            {
                if (passwordEntry.Text.Length < 8)
                {
                    DisplayError(passwordEntry, "Password must be 8 charactes!");
                    confirmEntry.Text = string.Empty;
                    isNotError = false;
                }
                else
                {
                    if (String.IsNullOrEmpty(confirmEntry.Text))
                    {
                        DisplayError(confirmEntry, "Confirm Password is required!");
                        isNotError = false;
                    }

                    if (passwordEntry.Text != confirmEntry.Text)
                    {
                        confirmEntry.Text = string.Empty;
                        DisplayError(passwordEntry, "Password does not match!");
                        isNotError = false;
                    }
                }
            }



            if (isNotError && isClicked == false)
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        isClicked = true;
                        cts = new CancellationTokenSource();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            loader.IsVisible = true;
                        });
                        string data = JsonConvert.SerializeObject(new { topics = "group_", user = new { group_code = codeEntry.Text, username = usernameEntry.Text, password = passwordEntry.Text, email = emailEntry.Text, phone_type = (Device.RuntimePlatform.Equals(Device.iOS) ? 1 : 2) } });
                        await advanceRestService.PostRequestAsync(Constants.SIGN_UP, data, cts.Token, 1);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            isClicked = false;
                            loader.IsVisible = false;
                            cts = null;
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
            });
        }

        void DisplayError(Entry entry, string error)
        {
            entry.Text = String.Empty;
            entry.Placeholder = error;
            entry.PlaceholderColor = Color.Red;
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            isClicked = false;
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            UserModel user = JsonConvert.DeserializeObject<UserModel>(jsonData["data"]["user"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            user.role = int.Parse(jsonData["data"]["role"].ToString());
                            dataClass.user = user;

                            GroupModel group_model = JsonConvert.DeserializeObject<GroupModel>(jsonData["data"]["group"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                            dataClass.GroupCode = group_model.group_code;
                            dataClass.GroupId = group_model.id.ToString();

                            if (Application.Current.Properties.ContainsKey("group_codes_list"))
                            {
                                Application.Current.Properties.Remove("group_codes_list");
                                Application.Current.SavePropertiesAsync();
                            }

                            ObservableCollection<string> codes = new ObservableCollection<string>();
                            codes.Add(codeEntry.Text);
                            Application.Current.Properties["group_codes_list"] = JsonConvert.SerializeObject(codes);
                            Application.Current.SavePropertiesAsync();

                            if (Application.Current.Properties.ContainsKey("group_models_list"))
                            {
                                Application.Current.Properties.Remove("group_models_list");
                                Application.Current.SavePropertiesAsync();
                            }

                            ObservableCollection<GroupModel> group_models = new ObservableCollection<GroupModel>();
                            group_models.Add(group_model);
                            Application.Current.Properties["group_models_list"] = JsonConvert.SerializeObject(group_models);
                            Application.Current.SavePropertiesAsync();


                            Navigation.PushAsync(new EditProfilePage(true));


                            break;
                    }
                }
                loader.IsVisible = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                isClicked = false;
                loader.IsVisible = false;
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
