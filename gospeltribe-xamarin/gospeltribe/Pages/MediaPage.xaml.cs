﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MediaPage : RootViewPage, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;

        ObservableCollection<MediaModel> members = new ObservableCollection<MediaModel>();

        bool fromProfile = false;
        int? user_id;

        public MediaPage(bool fromProfile, int? user_id = null)
        {
            advanceRest = new AdvanceRestService
            {
                WebServiceDelegate = this
            };
            this.user_id = user_id;
            this.fromProfile = fromProfile;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            listView.FlowColumnTemplate = new DataTemplate(MediaImageViewDataTemplate);
            this.NavBackgroundColor = Color.FromHex("#00feff");
            if (fromProfile)
            {
                this.PageTitle = "My Gallery";
            }
            else
            {
                this.PageTitle = "Story";
            }
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;
            if (fromProfile)
            {
                this.LeftIcon = "Back";
                this.LeftIconTint = Color.Black;
                this.LeftButtonCommand = new Command(OpenMaster_Tapped);
            }

            this.RightIcon = "Add";
            this.RightIconTint = Color.Black;
            this.RightButtonCommand = new Command(Add_Image);

            listView.FlowItemsSource = members;

        }

        MediaImageView miv;
        MediaImageView MediaImageViewDataTemplate(){
            
            miv = new MediaImageView(this, false);
            return miv;
        }

        public void RemoveImage(MediaModel media){
            members.Remove(members.First(x => x.id.Equals(media.id)));
        }

        public void UpdateImages(JObject parameters)
        {
            MediaModel tgm = JsonConvert.DeserializeObject<MediaModel>(parameters.ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            members.Insert(0, tgm);
        }

        private void Add_Image(object obj)
        {
            //string a = Constants.ADD_GROUP_MEDIA;
            //if (fromProfile)
            //{
                Application.Current.MainPage?.Navigation.PushAsync(new AddGroupMediaPage(this), false);
            //}
            //else
            //{
                //Application.Current.MainPage?.Navigation.PushAsync(new AddGroupMediaPage(this), false);
            //}
        }

        private void OpenMaster_Tapped(object obj)
        {
            Navigation.PopAsync(false);
        }

        public MediaPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            listView.FlowColumnTemplate = new DataTemplate(MediaImageViewDataTemplate);
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Story";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            listView.FlowItemsSource = members;

            this.RightIcon = "Add";
            this.RightIconTint = Color.Black;
            this.RightButtonCommand = new Command(Add_Image);

            this.LeftIcon = "other_tribes";
            this.LeftIconTint = Color.Black;
            this.LeftIconWidthRequest = (double)(30.ScaleWidth());
            this.LeftIconHeightRequest = (double)(30.ScaleWidth());
            this.LeftIconContainerPadding = new Thickness(0, 0, 0, 5.ScaleHeight());
            this.LeftButtonCommand = new Command(() =>
            {
                ((HomePage)this.Parent.Parent).Navigation.PushAsync(new OtherTribesPage(), false);
            });
        }


        void Item_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            if (fromProfile)
            {
                Navigation.PushModalAsync(new NavigationPage(new MediaImagePage((MediaModel)e.Item, true)), false);
            }
            else
            {
                Application.Current.MainPage?.Navigation.PushModalAsync(new NavigationPage(new MediaImagePage((MediaModel)e.Item)), false);
            }
        }

        public async void Update()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                listView.IsRefreshing = true;
            });
            if (fromProfile)
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GET_USER_MEDIA(user_id.Value, "false"), cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });

            }
            else
            {

                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GROUP_MEDIUM + DataClass.GetInstance.GroupId, cts.Token, 0);

                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();


            if (fromProfile)
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GET_USER_MEDIA(user_id.Value, "false"), cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        App.Log(ex.Message);
                        //ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
            else
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GROUP_MEDIUM + DataClass.GetInstance.GroupId, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    var temp_messages = JsonConvert.DeserializeObject<ObservableCollection<MediaModel>>(jsonData["media"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    members = temp_messages;

                    listView.IsRefreshing = false;
                    listView.FlowItemsSource = members;
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
