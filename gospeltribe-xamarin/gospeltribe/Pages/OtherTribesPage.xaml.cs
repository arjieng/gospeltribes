﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FirebasePushNotification;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class OtherTribesPage : RootViewPage, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;

        public OtherTribesPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();


            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "TRIBES";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(GoBack);

            this.RightIcon = "LogoutWord";
            this.RightIconTint = Color.Black;
            this.RightButtonCommand = new Command(SignOut);

            var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());
            group_models.Add(new GroupModel());
            listView.FlowItemsSource = group_models;
        }

        async void Group_Tapped(object obj)
        {
            GroupModel groupModel = (GroupModel)obj;

            if (string.IsNullOrEmpty(groupModel.group_code) && string.IsNullOrEmpty(groupModel.group_name))
            {
                Navigation.PushPopupAsync(new MyPage());
            }
            else
            {
                if (!groupModel.group_code.Equals(DataClass.GetInstance.GroupCode))
                {
                    var choice = await DisplayAlert("Change Group", "Are you sure you want to change group?", "Yes", "No");
                    if (choice)
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                cts = new CancellationTokenSource();
                                await advanceRest.GetRequest(Constants.ADD_GROUP + groupModel.group_code, cts.Token, 0);

                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    cts = null;
                                });
                            }
                        });
                    }
                }
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        private async void SignOut(object obj)
        {
            var choice = await DisplayAlert("Logout", "Are you sure you want to sign out?", "Yes", "No");
            if (choice)
            {
                DataClass.GetInstance.user = null;
                DataClass.GetInstance.clientId = "";
                DataClass.GetInstance.uid = "";
                DataClass.GetInstance.token = "";
                CrossFirebasePushNotification.Current.UnsubscribeAll();
                App.Logout(true);
            }
        }

        private void GoBack(object obj)
        {
            Navigation.PopAsync(false);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    var u = DataClass.GetInstance.user;
                    u.role = int.Parse(jsonData["group"]["role"].ToString());
                    DataClass.GetInstance.user = u;
                    DataClass.GetInstance.GroupCode = jsonData["group"]["group_code"].ToString();
                    DataClass.GetInstance.GroupId = jsonData["group"]["id"].ToString();
                    App.ShowMainPage();
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
