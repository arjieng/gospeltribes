﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace gospeltribe.Pages
{
    public partial class LandingPage2 : RootViewPage
    {
        public LandingPage2()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

        }
        void Stack_Tapped(object sender, TappedEventArgs e){
            StackLayout stack = (StackLayout)sender;
            switch(stack.StyleId){
                case "1":
                    caret2.IsVisible = false;
                    caret1.IsVisible = true;
                    break;
                case "2":
                    caret1.IsVisible = false;
                    caret2.IsVisible = true;
                    break;
            }
        }
    }
}
