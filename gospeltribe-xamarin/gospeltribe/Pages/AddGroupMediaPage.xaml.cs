﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class AddGroupMediaPage : RootViewPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        Stream imageStream;
        CancellationTokenSource cts;
        MediaPage mediaPage;
        ProfilePage profile;
        bool isClicked = false, fromMediaPage = false, is_video = false;

        public AddGroupMediaPage(ProfilePage profile){
            this.profile = profile;
            advanceRest = new AdvanceRestService
            {
                WebServiceDelegate = this
            };
            cts = new CancellationTokenSource();
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            this.NavBackgroundColor = Color.FromHex("#00feff");

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);
            });
        }

        public AddGroupMediaPage(MediaPage mp)
        {
            fromMediaPage = true;
            this.mediaPage = mp;
            advanceRest = new AdvanceRestService
            {
                WebServiceDelegate = this
            };
            cts = new CancellationTokenSource();
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            this.NavBackgroundColor = Color.FromHex("#00feff");

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);
            });
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        async void AddImage_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                if (imageStream != null)
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            isClicked = true;
                            cts = new CancellationTokenSource();
                            UserDialogs.Instance.ShowLoading("Saving Media");
                            string dict = JsonConvert.SerializeObject(new { group = new { id = int.Parse(DataClass.GetInstance.GroupId), image = "", description = details.Text, is_video = is_video, topics = string.Format("'group_{0}_{1}_{2}' in topics,'group_{0}_{3}_{2}' in topics", DataClass.GetInstance.GroupId, "iOS", "All", "Android") } });
                            await advanceRest.MultiPartDataContentAsync(Constants.ADD_GROUP_MEDIA, dict, cts.Token, imageStream, "group", 0, null, is_video);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }
            }
        }

        async void Image_Tapped(object sender, System.EventArgs e)
        {
            if (!isClicked)
            {
                var choice = await Application.Current.MainPage?.DisplayActionSheet(null, "Cancel", null, "Video", "Image");
                switch(choice){
                    case "Video":
                        //submit_button.Text = "POST VIDEO";
                        is_video = true;
                        image.IsVisible = false;
                        videoPlayer.IsVisible = true;
                        limitText.IsVisible = true;
                        change_button.IsVisible = true;
                        var choice1 = await Application.Current.MainPage?.DisplayActionSheet("Select Source", "Cancel", null, "Take Video", "Video Library");
                        if (choice1.Equals("Take Video"))
                        {
                            var status = await PermissionHelper.CheckPermissions(Permission.Camera);
                            if (status == PermissionStatus.Granted)
                            {
                                var status2 = await PermissionHelper.CheckPermissions(Permission.Microphone);
                                if (status2 == PermissionStatus.Granted)
                                {
                                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
                                    {
                                        await Application.Current.MainPage?.DisplayAlert("No Camera", ":( No camera available.", "OK");
                                        return;
                                    }
                                    var file = await CrossMedia.Current.TakeVideoAsync(new StoreVideoOptions { SaveToAlbum = true, PhotoSize = PhotoSize.Full, DesiredLength = TimeSpan.FromSeconds(15), Quality = VideoQuality.Medium });
                                    if (file == null)
                                        return;

                                    imageStream = file.GetStreamWithImageRotatedForExternalStorage();
                                    videoPlayer.Source = new FormsVideoLibrary.FileVideoSource { File = file.AlbumPath };
                                }
                                else
                                {
                                    Application.Current?.MainPage?.DisplayAlert("Cannot record video", "", "OK");
                                }
                            }
                            else
                            {
                                Application.Current?.MainPage?.DisplayAlert("Cannot record video", "", "OK");
                            }
                        }
                        else if (choice1.Equals("Video Library"))
                        {
                            var status = await PermissionHelper.CheckPermissions(Permission.Photos);
                            if (status == PermissionStatus.Granted)
                            {
                                if (!CrossMedia.Current.IsPickPhotoSupported)
                                {
                                    await Application.Current.MainPage?.DisplayAlert("Error", "This device is not supported to pick photo.", "OK");
                                    return;
                                }
                                var file = await CrossMedia.Current.PickVideoAsync();

                                if (file == null)
                                    return;

                                imageStream = file.GetStreamWithImageRotatedForExternalStorage();
                                videoPlayer.Source = new FormsVideoLibrary.FileVideoSource { File = file.AlbumPath };
                            }
                            else
                            {
                                Application.Current?.MainPage?.DisplayAlert("Cannot select video", "", "OK");
                            }
                        }
                        break;
                    case "Image":
                        //submit_button.Text = "POST IMAGE";
                        is_video = false;
                        image.IsVisible = true;
                        limitText.IsVisible = false;
                        videoPlayer.IsVisible = false;
                        change_button.IsVisible = false;
                        new ImageCropper()
                        {
                            Success = (imageFile) =>
                            {
                                SuccessCallback(imageFile);
                            }
                        }.Show(this);
                        break;
                }
            }
        }



        void SuccessCallback(string imageFile)
        {
            Stream stream = new FileStream(imageFile, FileMode.Open);
            imageStream = stream;
            image.Source = ImageSource.FromFile(imageFile);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.Log(jsonData.ToString());
                if (jsonData["status"].ToString().Equals("422"))
                {
                    isClicked = false;
                    UserDialogs.Instance.HideLoading();
                }
                else
                {
                    if (fromMediaPage)
                    {
                        mediaPage.Update();
                    }
                    else
                    {
                        profile.ListView_Refreshing(null, null);
                    }

                    isClicked = false;
                    UserDialogs.Instance.HideLoading();
                    Navigation.PopAsync(false);
                }

            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            isClicked = false;
        }
    }
}
