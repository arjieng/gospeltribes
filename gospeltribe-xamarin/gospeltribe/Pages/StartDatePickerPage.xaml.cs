﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class StartDatePickerPage : RootViewPage
    {
        CreateEventPage createEventPage;
        int i;
        public StartDatePickerPage(CreateEventPage createEventPage, int i = 0)
        {
            this.i = i;
            this.createEventPage = createEventPage;
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle =  i == 0 ? "Start Date" : "End Date";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);
            });
        }

        async void SelectDate_Clicked(object sender, System.EventArgs e)
        {
            if (calendar2.SelectedDate.HasValue)
            {
                if(i == 0)
                {
                    createEventPage.ChangeStartDate(calendar2.SelectedDate.Value.Year, calendar2.SelectedDate.Value.Month, calendar2.SelectedDate.Value.Day);
                }
                else
                {
                    createEventPage.ChangeEndDate(calendar2.SelectedDate.Value.Year, calendar2.SelectedDate.Value.Month, calendar2.SelectedDate.Value.Day);
                }

            }
            else
            {
                DateTime d = DateTime.Now;

                if (i == 0)
                {
                    createEventPage.ChangeStartDate(d.Year, d.Month, d.Day);
                }
                else
                {
                    createEventPage.ChangeEndDate(d.Year, d.Month, d.Day);
                }
            }

            await Task.Delay(500);
            await Navigation.PopAsync(false);
        }
    }
}
