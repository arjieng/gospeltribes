﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class SearchPage : RootViewPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        Task timer;
        Position position;
        CancellationTokenSource cts;
        CreateEventPage createEventPage;

        public SearchPage(CreateEventPage createEventPage)
        {
            this.createEventPage = createEventPage;
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;

            Device.BeginInvokeOnMainThread(async () =>
            {
                var b = App.locationHelper.GetCurrentLocation();
                await b;
                if (b.IsCompleted)
                {
                    position = b.Result;
                }
            });

            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Location";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);
            });

            if(App.PreviousPredictions != null)
            {
                ObservableCollection<Predictions> predictions = new ObservableCollection<Predictions> { new Predictions { description = App.PreviousPredictions.description, id = App.PreviousPredictions.id } };
                listView.ItemsSource = predictions;
            }
        }

        async void Prediction_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            Predictions predictions = (Predictions)e.SelectedItem;
            if (predictions != null)
            {
                createEventPage.ChangeLocation(predictions.description);
                App.PreviousPredictions = predictions;
                await Task.Delay(500);
                await Navigation.PopAsync(false);
            }
        }

        async void SearchBox_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            timer = Task.Delay(1500);
            await timer;
            if (timer.IsCompleted)
            {
                if (position != null)
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            cts = new CancellationTokenSource();
                            string url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + e.NewTextValue + "&location=" + position.Latitude + "," + position.Longitude + "&radius=1000&key=AIzaSyBWV4ntfc6-Tg7-ud7G-Cg9a8btbqZLx18";
                            await advanceRest.GetRequest(url, cts.Token, 0);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                cts = null;
                            });
                        }
                    });
                }else{
                    var b = App.locationHelper.GetCurrentLocation();
                    await b;
                    if (b.IsCompleted)
                    {
                        position = b.Result;
                        await Task.Run(async () =>
                        {
                            try
                            {
                                cts = new CancellationTokenSource();
                                string url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + e.NewTextValue + "&location=" + position.Latitude + "," + position.Longitude + "&radius=1000&key=AIzaSyBWV4ntfc6-Tg7-ud7G-Cg9a8btbqZLx18";
                                await advanceRest.GetRequest(url, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    cts = null;
                                });
                            }
                        });
                    }
                }
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("OK"))
                {
                    var json = JsonConvert.DeserializeObject<ObservableCollection<Predictions>>(jsonData["predictions"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    if (App.PreviousPredictions != null)
                    {
                        var check_exists = json.Where(x => x.id == App.PreviousPredictions.id);
                        if (check_exists.Count().Equals(0))
                            json.Insert(0, App.PreviousPredictions);
                    }
                    listView.ItemsSource = json;
                }

                if (jsonData["status"].ToString().Equals("ZERO_RESULTS"))
                {

                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
            });
        }
    }

    public class Predictions{
        public string description { get; set; }
        public string id { get; set; }
    }
}
