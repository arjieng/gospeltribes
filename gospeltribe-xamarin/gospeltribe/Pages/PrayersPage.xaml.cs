﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class PrayersPage : RootViewPage, IAdvanceRestConnector
    {
        ObservableCollection<PrayersModel> prayersFor = new ObservableCollection<PrayersModel>();
        ObservableCollection<PrayersModel> answeredPrayers = new ObservableCollection<PrayersModel>();
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        int a_hasLoadMore = 0, u_hasLoadMore = 0;
        string a_loadMoreURL = "", u_loadMoreURL = "";

        public PrayersPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            prayinFor.FlowColumnTemplate = new DataTemplate(PrayersPageView);
            answered.FlowColumnTemplate = new DataTemplate(PrayersPageView);

            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Prayers";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;
            this.RightIcon = "Add";
            this.RightIconTint = Color.Black;
            this.RightButtonCommand = new Command(() =>
            {
                Application.Current.MainPage?.Navigation.PushAsync(new CreatePrayerPage(0, new PrayersModel(), this, null), false);
            });

            this.LeftIcon = "other_tribes";
            this.LeftIconTint = Color.Black;
            this.LeftIconWidthRequest = (double)(30.ScaleWidth());
            this.LeftIconHeightRequest = (double)(30.ScaleWidth());
            this.LeftIconContainerPadding = new Thickness(0, 0, 0, 5.ScaleHeight());
            this.LeftButtonCommand = new Command(() =>
            {
                ((HomePage)this.Parent.Parent).Navigation.PushAsync(new OtherTribesPage(), false);
            });
        }

        PrayersPageView prayersPageView;

        PrayersPageView PrayersPageView()
        {
            prayersPageView = new PrayersPageView(this);
            prayersPageView.SetBinding(prayersPageView.PrayerProperty, ".");
            return prayersPageView;
        }

        public void RemovePrayer(PrayersModel prayers)
        {
            if (prayers.is_answered.Equals(1))
            {
                answeredPrayers.Remove(answeredPrayers.Where(x => x.id == prayers.id).First());
            }
            else
            {
                prayersFor.Remove(prayersFor.Where(x => x.id == prayers.id).First());
            }
            //prayersFor = prayers_u;
            //answeredPrayers = prayers_a;
        }
        //bool onFirstRefresh = false;
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    App.Log(Constants.GET_ALL_PRAYERS());
                    await advanceRest.GetRequest(Constants.GET_ALL_PRAYERS(), cts.Token, 1);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    App.Log(ex.Message);
                    //ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void AddPrayer(PrayersModel p)
        {
            prayersFor.Insert(0, p);
        }

        async void PrayinFor_Refreshing(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GUnansweredP(), cts.Token, 2);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        async void Answered_Refreshing(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GAnsweredP(), cts.Token, 3);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        public void EditPrayer(PrayersModel orig, PrayersModel newPrayer)
        {
            if (prayersFor.Contains(orig))
            {
                var c = prayersFor.Where(x => x.id == orig.id).FirstOrDefault();
                int i = prayersFor.IndexOf(c);
                prayersFor[i] = newPrayer;
            }

            if (answeredPrayers.Contains(orig))
            {
                var c = answeredPrayers.Where(x => x.id == orig.id).FirstOrDefault();
                int i = answeredPrayers.IndexOf(c);
                answeredPrayers[i] = newPrayer;
            }
        }

        public void MoveToAnswered(PrayersModel prayers)
        {
            var p = prayersFor.Where(x => x.id == prayers.id).FirstOrDefault();
            prayersFor.Remove(p);
            p.is_answered = 1;
            answeredPrayers.Insert(0, p);
        }

        void Prayer_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            //PrayersModel item = (PrayersModel)e.Item;
            //Application.Current.MainPage?.Navigation.PushAsync(new PrayersView(item, this), false);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            //App.Log(jsonData.ToString());
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            var prayers_a = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["answered"]["prayers"].ToString());
                            var prayers_u = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["unanswered"]["prayers"].ToString());
                            prayersFor = prayers_u;
                            answeredPrayers = prayers_a;

                            a_hasLoadMore = int.Parse(jsonData["answered"]["load_more"]["load_more"].ToString());
                            u_hasLoadMore = int.Parse(jsonData["unanswered"]["load_more"]["load_more"].ToString());

                            a_loadMoreURL = Constants.domain + jsonData["answered"]["load_more"]["url"].ToString();
                            u_loadMoreURL = Constants.domain + jsonData["unanswered"]["load_more"]["url"].ToString();

                            prayinFor.IsRefreshing = false;
                            answered.IsRefreshing = false;
                            prayinFor.FlowItemsSource = prayersFor;
                            answered.FlowItemsSource = answeredPrayers;
                            break;
                        case 2:
                            var pu = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["prayers"].ToString());
                            prayinFor.FlowItemsSource = pu;
                            prayinFor.IsRefreshing = false;

                            break;
                        case 3:
                            var pa = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["prayers"].ToString());
                            answered.FlowItemsSource = pa;
                            answered.IsRefreshing = false;
                            break;
                        case 4:
                            var newPU = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["prayers"].ToString());
                            foreach (var p in newPU)
                            {
                                prayersFor.Add(p);
                            }
                            u_hasLoadMore = int.Parse(jsonData["load_more"]["load_more"].ToString());
                            u_loadMoreURL = Constants.domain + jsonData["load_more"]["url"].ToString();
                            break;
                        case 5:
                            var newPA = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["prayers"].ToString());
                            foreach (var p in newPA)
                            {
                                answeredPrayers.Add(p);
                            }
                            a_hasLoadMore = int.Parse(jsonData["load_more"]["load_more"].ToString());
                            a_loadMoreURL = Constants.domain + jsonData["load_more"]["url"].ToString();
                            break;

                    }
                }
            });
        }

        async void PrayinFor_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            if (prayersFor != null)
            {
                if (prayersFor.Any())
                {
                    if (e.Item.Equals(prayersFor.Last()))
                    {
                        if (!u_hasLoadMore.Equals(0))
                        {
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    await advanceRest.GetRequest(u_loadMoreURL, cts.Token, 4);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });
                        }
                    }
                }
            }
        }

        async void Answered_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            if (answeredPrayers != null)
            {
                if (answeredPrayers.Any())
                {
                    if (e.Item.Equals(answeredPrayers.Last()))
                    {
                        if (!a_hasLoadMore.Equals(0))
                        {
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    await advanceRest.GetRequest(a_loadMoreURL, cts.Token, 5);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });
                        }
                    }
                }
            }
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
           {
               DisplayAlert(title, error, "Okay");
           });
        }
    }
}
