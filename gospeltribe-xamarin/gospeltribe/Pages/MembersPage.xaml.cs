﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MembersPage : RootViewPage, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        MembersView membersView;
        public static ObservableCollection<UserModel> members = new ObservableCollection<UserModel>();

        public MembersPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Members";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "other_tribes";
            this.LeftIconTint = Color.Black;
            this.LeftIconWidthRequest = (double)(30.ScaleWidth());
            this.LeftIconHeightRequest = (double)(30.ScaleWidth());
            this.LeftIconContainerPadding = new Thickness(0, 0, 0, 5.ScaleHeight());
            this.LeftButtonCommand = new Command(() =>
            {
                ((HomePage)this.Parent.Parent).Navigation.PushAsync(new OtherTribesPage(), false);
            });

            listView.FlowColumnTemplate = new DataTemplate(Members_View);

            members.Add(DataClass.GetInstance.user);
            listView.FlowItemsSource = members;


        }

        MembersView Members_View()
        {
            membersView = new MembersView(this){ };
            membersView.SetBinding(membersView.UserProperty, new Binding("."));

            return membersView;
        }

        public async void DisplayAction(UserModel user)
        {
            var choice = await DisplayActionSheet("", "Cancel", "Delete", "View");
            switch (choice)
            {
                case "Delete":
                    var choice_two = await DisplayAlert("Delete user?", "Do you really want to delete this user?", "Yes", "No");
                    if (choice_two)
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                UserDialogs.Instance.ShowLoading("Deleting member");
                                cts = new CancellationTokenSource();
                                await advanceRest.GetRequest(Constants.REMOVE_PROFILE(user.id), cts.Token, 1);

                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    UserDialogs.Instance.HideLoading();
                                    cts = null;
                                });
                            }
                        });
                    }
                    break;
                case "View":
                    Application.Current.MainPage?.Navigation.PushAsync(new ProfilePage(user), false);
                    break;
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        protected async override void OnAppearing()
        {
            members.Clear();
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_ALL_MEMBERS(), cts.Token, 0);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    App.Log(ex.Message);
                    //ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });

            base.OnAppearing();
        }

        async void Members_Refreshing(object sender, EventArgs e){
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_ALL_MEMBERS(), cts.Token, 0);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 0:

                            var temp_ = JsonConvert.DeserializeObject<ObservableCollection<UserModel>>(jsonData["group_members"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            members = temp_;
                            listView.FlowItemsSource = members;
                            listView.IsRefreshing = false;
                            break;
                        case 1:
                            members.Remove(members.Where(x => x.id == int.Parse(jsonData["user_id"].ToString())).FirstOrDefault());
                            UserDialogs.Instance.HideLoading();
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
