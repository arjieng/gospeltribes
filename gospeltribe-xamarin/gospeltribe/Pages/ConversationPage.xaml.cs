﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ConversationPage : RootViewPage, IAdvanceRestConnector
    {
        public ObservableCollection<MessagesModel> messagesModels = new ObservableCollection<MessagesModel>();
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        HomePage home;
        public InboxModel ee;
        int hasLoadMore = 0;
        string loadMoreUrl = "", url = "ws://" + Constants.IP_ADDRESS + ":9292/faye";

        public ConversationPage(InboxModel ee, HomePage home)
        {
            this.ee = ee;
            this.home = home;
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            chatInputBar.chatroom_id = ee.id;
            ConnectToWebsocket();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = ee.chatroom_name;
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;
            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(GoBack_Tapped);
            if (DataClass.GetInstance.user.role.Equals(1))
            {
                this.RightIcon = "Settings";
                this.RightIconTint = Color.Black;
                this.RightButtonCommand = new Command(OpenSettings);
            }

            ChatList.ItemTemplate = new MessagesTemplateSelector(this);
            if (App.isIphoneX)
            {
                AddRow();
            }

        }



        void ChatMessages_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            ChatList.SelectedItem = null;
        }

        async void ChatMessages_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            if(messagesModels != null)
            {
                if(messagesModels.Any())
                {
                    if (e.Item.Equals(messagesModels.Last()))
                    {
                        if (!hasLoadMore.Equals(0))
                        {
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    //redLabel.IsVisible = true;
                                    await advanceRest.GetRequest(loadMoreUrl, cts.Token, 1);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });
                        }
                    }
                }
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        void Handle_Focused(object obj)
        {
            if (App.isIphoneX)
            {
                myGrid.RowDefinitions.Remove(myGrid.RowDefinitions.Last());
            };
        }

        void Handle_Unfocused(object obj)
        {
            if (App.isIphoneX)
            {
                AddRow();
            };
        }

        void AddRow()
        {
            myGrid.RowDefinitions.Add(new RowDefinition { Height = 20.ScaleHeight() });
        }

        void OpenSettings(object obj)
        {
            Application.Current.MainPage?.Navigation.PushAsync(new GroupSettings(ee), false);
        }

        public void OpenEditMessagePage(MessagesModel messages)
        {
            Navigation.PushPopupAsync(new EditMessagePopupPage(messages, this), false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ChatList.ItemsSource = messagesModels;
            //Device.BeginInvokeOnMainThread(async () =>
            //{
            //    await Task.Run(async () =>
            //    {
            //        try
            //        {
            //            cts = new CancellationTokenSource();
            //            App.Log($"{Constants.GET_MESSAGE + "?chatroom_id=" + ee.id}");
            //            await advanceRest.GetRequest(Constants.GET_MESSAGE + "?chatroom_id=" + ee.id, cts.Token, 0);
            //        }
            //        catch (OperationCanceledException oce)
            //        {
            //            ShowAlerts("Task Cancelled", oce.Message);
            //        }
            //        catch (TimeoutException te)
            //        {
            //            ShowAlerts("Request Timeout", te.Message);
            //        }
            //        catch (Exception ex)
            //        {
            //            ShowAlerts("Error", ex.Message);
            //        }
            //        finally
            //        {
            //            Device.BeginInvokeOnMainThread(() =>
            //            {
            //                cts = null;
            //            });
            //        }
            //    });

            //});
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (messagesModels.Count != 0)
            {
                home.SaveLastRead(ee.id, messagesModels.First().id);
            }
        }

        private void GoBack_Tapped(object obj)
        {
            _client.SendBayeuxUnsubscribeMessage(Constants.CHATROOMS(ee.id));
            _client = null;
            Navigation.PopAsync(false);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    hasLoadMore = int.Parse(jsonData["paginate"]["load_more"].ToString());
                    loadMoreUrl = Constants.domain + jsonData["paginate"]["url"].ToString();
                    var temp = JsonConvert.DeserializeObject<ObservableCollection<MessagesModel>>(jsonData["messages"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                    switch (wsType)
                    {
                        case 0:
                            messagesModels = temp;
                            ChatList.ItemsSource = messagesModels;
                            break;
                        case 1:
                            foreach(var t in temp)
                            {
                                messagesModels.Add(t);
                            }
                            //redLabel.IsVisible = false;
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        #region CONNECT_WEBSOCKET
        FayeClient _client;

        void ConnectToWebsocket()
        {
            _client = null;
            _client = new FayeClient();
            _client.Connected += _client_Connected;
            _client.Disconnected += _client_Disconnected;
            _client.Subscribed += _client_Subscribed;
            _client.Unsubscribed += _client_Unsubscribed;
            _client.MessageReceived += _client_MessageReceived;
            _client.Connect(url);
        }

        private void _client_Unsubscribed(object sender, SubscriptionEventArgs e)
        {
            App.Log($"SUBSCRIBED TO {e.ChannelName}");
        }

        private void _client_Subscribed(object sender, SubscriptionEventArgs e)
        {
            App.Log($"SUBSCRIBED TO {e.ChannelName}");
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        App.Log($"{Constants.GET_MESSAGE + "?chatroom_id=" + ee.id}");
                        await advanceRest.GetRequest(Constants.GET_MESSAGE + "?chatroom_id=" + ee.id, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });

            });
        }

        private void _client_Connected(object sender, EventArgs e)
        {
            if (_client.IsConnected)
            {
                _client.SendBayeuxSubscribeMessage(Constants.CHATROOMS(ee.id));
            }
        }

        void _client_MessageReceived(object sender, FayeMessageEventArgs e)
        {
            App.Log("FROM CONVERSATION PAGE: " + e.Data);
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (e.ChannelName.Equals(Constants.CHATROOMS(ee.id)))
                {
                    JObject obj = JObject.Parse(e.Data);
                    var data = obj["data"];
                    switch (obj["action"].ToString())
                    {
                        case "send_message":
                            MessagesModel mmm = JsonConvert.DeserializeObject<MessagesModel>(data.ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            messagesModels.Insert(0, mmm);
                            break;

                        case "delete_message":
                            messagesModels.Remove(messagesModels.Where(x => x.id.Equals(int.Parse(data["id"].ToString()))).First());
                            break;

                        case "update_message":
                            var m = messagesModels.Where(x => x.id.Equals(int.Parse(data["id"].ToString()))).First();
                            var index = messagesModels.IndexOf(m);
                            m.message = data["message"].ToString();
                            messagesModels[index] = m;
                            break;
                    }
                }
            });
        }

        private void _client_Disconnected(object sender, EventArgs e)
        {
            _client.Connected -= _client_Connected;
            _client.Disconnected -= _client_Disconnected;
            _client.Subscribed -= _client_Subscribed;
            _client.Unsubscribed -= _client_Unsubscribed;
            _client.MessageReceived -= _client_MessageReceived;

            Device.BeginInvokeOnMainThread(ConnectToWebsocket);
        }
        #endregion
    }
}