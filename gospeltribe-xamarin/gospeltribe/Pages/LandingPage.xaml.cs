﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace gospeltribe
{
    public partial class LandingPage : ContentPage
    {
        public LandingPage(bool willGotoSignIn = false)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            bimage.WidthRequest = App.screenWidth.ScaleWidth();
            bimage.HeightRequest = App.screenHeight.ScaleHeight();
            if (willGotoSignIn)
                Navigation.PushAsync(new SignInPage(true), false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "ResetPassword", (obj) =>
            {
                Navigation.PushAsync(new ResetPassword(obj), false);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "ResetPassword");
        }

        void SignIn_Clicked(object sender, ClickedEventArgs e)
        {
            Navigation.PushAsync(new SignInPage(), false);
        }

        void SignUp_Clicked(object sender, ClickedEventArgs e)
        {
            Navigation.PushAsync(new SignUpPage(), false);
        }
    }
}
