﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class CreatePrayerPage : RootViewPage, IAdvanceRestConnector
    {
        bool isClicked = false, isTriggered = true;
        int type = 0, share_to = 0;
        PrayersPage prayersPage;
        PrayersModel prayer = new PrayersModel();
        PrayersView pv;
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;

        public CreatePrayerPage(int type, PrayersModel prayer, PrayersPage pp, PrayersView pv)
        {
            this.type = type;
            this.pv = pv;
            this.prayer = prayer;
            this.prayersPage = pp;

            advanceRest = new AdvanceRestService { WebServiceDelegate = this };

            InitializeComponent();

            SetUpNavigation(type);

            if(!App.isIphoneX){
                ddm.Margin = new Thickness(0, 280.ScaleHeight(), 40.ScaleWidth(), 0);
                ddm.Padding = new Thickness(5.ScaleWidth(), 0, 5.ScaleWidth(), 0);
            }else{
                ddm.Padding = new Thickness(5.ScaleWidth(), 0, 5.ScaleWidth(), 0);
                ddm.Margin = new Thickness(0, 275.ScaleHeight(), 45.ScaleWidth(), 0);
            }

            // "{Binding Converter={StaticResource scaleHelper}, ConverterParameter='thickness=0,285,35,0'}"
        }
        Task detail_timer;
        CancellationTokenSource timer_cts;
        async void PrayerDetails_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            ExtendedEditorControl control = (ExtendedEditorControl)sender;
            if (isTriggered)
            {
                if (string.IsNullOrEmpty(e.OldTextValue))
                {
                    if ((0 - e.NewTextValue.Length) < 0)
                    {
                        if (!e.NewTextValue.Length.Equals(1))
                        {
                            isTriggered = false;
                            var new_text = control.Text + "{{break}}";
                            control.Text = new_text.Replace("{{break}}", System.Environment.NewLine);
                            await Task.Delay(100);
                            control.Text = control.Text.Remove(control.Text.Length - 1);
                            await Task.Delay(100);
                            isTriggered = true;
                        }
                    }
                }
                else
                {
                    if ((e.OldTextValue.Length - e.NewTextValue.Length) < 0)
                    {
                        if (!(e.NewTextValue.Length - e.OldTextValue.Length).Equals(1))
                        {
                            isTriggered = false;
                            var new_text = control.Text + "{{break}}";
                            control.Text = new_text.Replace("{{break}}", System.Environment.NewLine);
                            await Task.Delay(100);
                            control.Text = control.Text.Remove(control.Text.Length - 1);
                            await Task.Delay(100);
                            isTriggered = true;
                        }
                    }
                }
            }
        }

        void SetUpNavigation(int types){
            
            NavigationPage.SetHasNavigationBar(this, false);
            this.NavBackgroundColor = Color.FromHex("#00feff");
            switch (types)
            {
                case 0:
                    this.PageTitle = "Create Prayer";
                    break;
                case 1:
                    this.PageTitle = "Edit Prayer";
                    post.Text = "SAVE";
                    this.BindingContext = this.prayer;
                    SetUpDropdown();
                    break;
                case 2:
                    this.PageTitle = "Edit Prayer";
                    post.Text = "SAVE";
                    howAnsweredBox.IsVisible = true;
                    this.BindingContext = this.prayer;
                    SetUpDropdown();
                    break;
            }
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;
            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(GO_BACK);
        }

        void SetUpDropdown(){
            switch (this.prayer.share_to)
            {
                case 0:
                    share_to = 0;
                    selected.Text = "Everyone";
                    women.BackgroundColor = Color.White;
                    men.BackgroundColor = Color.White;
                    everyone.BackgroundColor = Color.FromHex("#00feff");
                    break;
                case 1:
                    share_to = 1;
                    selected.Text = "Women Only";
                    women.BackgroundColor = Color.FromHex("#00feff");
                    men.BackgroundColor = Color.White;
                    everyone.BackgroundColor = Color.White;
                    break;
                case 2:
                    share_to = 2;
                    selected.Text = "Men Only";
                    women.BackgroundColor = Color.White;
                    men.BackgroundColor = Color.FromHex("#00feff");
                    everyone.BackgroundColor = Color.White;
                    break;
                default:
                    share_to = 0;
                    selected.Text = "Everyone";
                    women.BackgroundColor = Color.White;
                    men.BackgroundColor = Color.White;
                    everyone.BackgroundColor = Color.FromHex("#00feff");
                    break;
            }
        }

        void TextArea_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e){
            if (e.PropertyName == "Text")
            {
                ExtendedEditorControl editorControl = (ExtendedEditorControl)sender;
                if (!string.IsNullOrEmpty(editorControl.Text))
                {
                    editorControl.Placeholder = "";

                }
                else
                {
                    editorControl.Placeholder = "Write details";
                }
            }
        }

        void GO_BACK(){
            Navigation.PopAsync(false);
        }

        void Menu_Tapped(object sender, EventArgs e)
        {
            StackLayout stack = (StackLayout)sender;
            switch (stack.ClassId)
            {
                case "1":
                    share_to = 0;
                    selected.Text = "Everyone";
                    women.BackgroundColor = Color.White;
                    men.BackgroundColor = Color.White;
                    everyone.BackgroundColor = Color.FromHex("#00feff");
                    if (type != 0)
                    {
                        prayer.share_to = 0;
                    }
                    break;
                case "2":
                    share_to = 2;
                    selected.Text = "Men Only";
                    women.BackgroundColor = Color.White;
                    men.BackgroundColor = Color.FromHex("#00feff");
                    everyone.BackgroundColor = Color.White;
                    if (type != 0)
                    {
                        prayer.share_to = 2;
                    }
                    break;
                case "3":
                    share_to = 1;
                    selected.Text = "Women Only";
                    women.BackgroundColor = Color.FromHex("#00feff");
                    men.BackgroundColor = Color.White;
                    everyone.BackgroundColor = Color.White;
                    if (type != 0)
                    {
                        prayer.share_to = 1;
                    }
                    break;
            }
            caret.Rotation += 180;
            outerSpace.IsVisible = false;
            ddm.IsVisible = false;
        }

        void DropDownMenu_Tapped(object sender, TappedEventArgs e)
        {
            caret.Rotation += 180;
            if (ddm.IsVisible)
            {
                outerSpace.IsVisible = false;
                ddm.IsVisible = false;
            }
            else
            {
                outerSpace.IsVisible = true;
                ddm.IsVisible = true;
            }
        }

        void OuterSpace_TouchAction(object sender, gospeltribe.TouchActionEventArgs args)
        {
            if (args.Type == TouchActionType.Pressed)
            {
                caret.Rotation += 180;
                outerSpace.IsVisible = false;
                ddm.IsVisible = false;
            }
        }

        async void SavePrayer_Tapped(object sender, TappedEventArgs e)
        {
            bool finishedNa = true;
            if (string.IsNullOrEmpty(subject.Text))
            {
                subject.Placeholder = "Subject must not be empty";
                subject.PlaceholderColor = Color.Red;
                finishedNa = false;
            }
            if (string.IsNullOrEmpty(details.Text))
            {
                details.Placeholder = "Prayer details must not be empty";
                details.PlaceholderColor = Color.Red;
                finishedNa = false;
            }
            if (type == 2)
            {
                if (string.IsNullOrEmpty(howWasItAnswered.Text))
                {
                    howWasItAnswered.Placeholder = "This field is important";
                    howWasItAnswered.PlaceholderColor = Color.Red;
                    finishedNa = false;
                }
            }
            if (finishedNa)
            {
                if (!isClicked)
                {
                    switch (type)
                    {
                        case 0:
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    string dict0 = JsonConvert.SerializeObject(new { topics = string.Format("group_{0}", DataClass.GetInstance.GroupId), prayer = new { subject = subject.Text, details = details.Text, is_answered = 0, share_to = share_to, is_hidden = false, group_id = int.Parse(DataClass.GetInstance.GroupId) } });
                                    isClicked = true;
                                    cts = new CancellationTokenSource();
                                    UserDialogs.Instance.ShowLoading("Saving");
                                    await advanceRest.PostRequestAsync(Constants.CREATE_PRAYER, dict0, cts.Token, 1);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        isClicked = false;
                                        UserDialogs.Instance.HideLoading();
                                        cts = null;
                                    });
                                }
                            });

                            break;
                        case 1:
                            await Task.Run(async () =>
                            {
                                try
                                {

                                    string dict1 = JsonConvert.SerializeObject(new { topics = string.Format("group_{0}", DataClass.GetInstance.GroupId), prayer = new { id = prayer.id, subject = prayer.subject, details = prayer.details, share_to = prayer.share_to, group_id = int.Parse(DataClass.GetInstance.GroupId) } });
                                    isClicked = true;
                                    cts = new CancellationTokenSource();
                                    UserDialogs.Instance.ShowLoading("Saving");
                                    await advanceRest.PostRequestAsync(Constants.UPDATE_PRAYER, dict1, cts.Token, 2);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        isClicked = false;
                                        UserDialogs.Instance.HideLoading();
                                        cts = null;
                                    });
                                }
                            });
                            break;
                        case 2:
                            await Task.Run(async () =>
                            {
                                try
                                {

                                    string dict2 = JsonConvert.SerializeObject(new { topics = string.Format("group_{0}", DataClass.GetInstance.GroupId), prayer = new { id = prayer.id, subject = prayer.subject, details = prayer.details, answered_details = prayer.answered_details, share_to = prayer.share_to, group_id = int.Parse(DataClass.GetInstance.GroupId) } });
                                    isClicked = true;
                                    cts = new CancellationTokenSource();
                                    UserDialogs.Instance.ShowLoading("Saving");
                                    await advanceRest.PostRequestAsync(Constants.UPDATE_PRAYER, dict2, cts.Token, 2);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        isClicked = false;
                                        UserDialogs.Instance.HideLoading();
                                        cts = null;
                                    });
                                }
                            });
                            break;
                    }
                }
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            App.Log("FROM EDITED: " + jsonData.ToString());
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (wsType == 2)
                    {
                        PrayersModel nprayer = JsonConvert.DeserializeObject<PrayersModel>(jsonData["prayer"].ToString());
                        pv.myListView.Header = nprayer;
                    }
                    UserDialogs.Instance.HideLoading();
                    Navigation.PopAsync();
                    isClicked = false;
                });
            }
            catch(Exception ex)
            {
                ShowAlerts("Error", ex.Message);
            }

        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                isClicked = false;
            });
        }
    }
}