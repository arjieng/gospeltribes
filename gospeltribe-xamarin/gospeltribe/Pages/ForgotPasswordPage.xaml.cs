﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ForgotPasswordPage : ContentPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        bool isClicked = false;

        public ForgotPasswordPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            InitializeComponent();
        }

        void LeftButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(false);
        }

        void DisplayError(Entry entry, string error)
        {
            entry.Text = String.Empty;
            entry.Placeholder = error;
            entry.PlaceholderColor = Color.Red;
        }

        async void RetrieveUsername_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;
            if (String.IsNullOrEmpty(emailEntry.Text))
            {
                DisplayError(emailEntry, "Email is required!");
                isNotError = false;
            }
            else
            {
                if (!emailEntry.Text.IsValidEmail())
                {
                    DisplayError(emailEntry, "Please enter a valid email!");
                    isNotError = false;
                }
            }
            if (isNotError && isClicked == false)
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        isClicked = true;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            loader.IsVisible = true;
                        });
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.RETRIEVE_PASSWORD + "?email=" + emailEntry.Text, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            isClicked = false;
                            cts = null;
                        });
                    }
                });
            }
        }

        async void ResetPassword_Clicked(object sender, System.EventArgs e)
        {
            bool isNotError = true;
            if (String.IsNullOrEmpty(emailEntry.Text))
            {
                DisplayError(emailEntry, "Email is required!");
                isNotError = false;
            }
            else
            {
                if (!emailEntry.Text.IsValidEmail())
                {
                    DisplayError(emailEntry, "Please enter a valid email!");
                    isNotError = false;
                }
            }
            if (isNotError && isClicked == false)
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        isClicked = true;
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            loader.IsVisible = true;
                        });
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.FORGOT_PASSWORD + "?email=" + emailEntry.Text, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            isClicked = false;
                            cts = null;
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "ResetPassword", (obj) =>
            {
                Navigation.PushAsync(new ResetPassword(obj), false);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "ResetPassword");
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("200"))
                {
                    DisplayAlert("Email sent", "Password reset link has been sent to your email.", "Okay");
                    emailEntry.Text = string.Empty;
                }

                if (jsonData["status"].ToString().Equals("404"))
                {
                    DisplayAlert("Error", "This user does not exist in our database", "Okay");
                }
                loader.IsVisible = false;
                isClicked = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                loader.IsVisible = false;
                isClicked = false;
            });
        }
    }
}
