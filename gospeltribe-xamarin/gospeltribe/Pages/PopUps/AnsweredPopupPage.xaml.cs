﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class AnsweredPopupPage : PopupPage, IAdvanceRestConnector
    {
        PrayersPage pp;
        PrayersModel prayer;
        PrayersView pv;

        CancellationTokenSource cts;
        AdvanceRestService advanceRest;

        public AnsweredPopupPage(PrayersPage pp, PrayersModel prayer, PrayersView pv)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;

            this.prayer = prayer;
            this.pp = pp;
            this.pv = pv;
            InitializeComponent();
        }

        void ClosePopup_Tapped(object sender, EventArgs e)
        {
            Navigation.PopPopupAsync();
        }

        async void AddGroup_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(details.Text))
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        UserDialogs.Instance.ShowLoading("Saving");
                        string dict = JsonConvert.SerializeObject(new { prayer = new { id = prayer.id, answered_details = details.Text, is_answered = 1, group_id = int.Parse(DataClass.GetInstance.GroupId) } });
                        await advanceRest.PostRequestAsync(Constants.UPDATE_PRAYER, dict, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                            UserDialogs.Instance.HideLoading();
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.HideLoading();
                ClosePopup_Tapped(null, null);
                pv.ClosePage();
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
