﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;
using System.Threading;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace gospeltribe
{
    public partial class EditMessagePopupPage : PopupPage, IAdvanceRestConnector
    {
        MessagesModel messages;
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        ConversationPage conversationPage;

        public EditMessagePopupPage(MessagesModel messages, ConversationPage conversationPage)
        {
            this.messages = messages;
            this.conversationPage = conversationPage;

            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;

            InitializeComponent();

            message.Text = messages.message;
        }

        async void Save_Clicked(object sender, System.EventArgs e)
        {
            await Task.Run(async () =>
            {
                if (string.IsNullOrEmpty(message.Text))
                {
                    var choice = await DisplayAlert("Delete Message", "Do you really want to delete this message", "Yes", "No");
                    if (choice)
                    {
                        try
                        {
                            cts = new CancellationTokenSource();
                            await advanceRest.GetRequest(Constants.REMOVE_MESSAGE + messages.id + "&group_id=" + int.Parse(DataClass.GetInstance.GroupId) + "&chatroom_id=" + conversationPage.ee.id, cts.Token, 0);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                cts = null;
                            });
                        }
                    }
                }
                else
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        string dict = JsonConvert.SerializeObject(new { message = new { message_id = messages.id, body = message.Text }, group_id = int.Parse(DataClass.GetInstance.GroupId), chatroom_id = conversationPage.ee.id });
                        await advanceRest.PostRequestAsync(Constants.EDIT_MESSAGE, dict, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                }
            });
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        void ClosePopup_Tapped(object sender, EventArgs e)
        {
            Navigation.PopPopupAsync();
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                ClosePopup_Tapped(null, null);
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}