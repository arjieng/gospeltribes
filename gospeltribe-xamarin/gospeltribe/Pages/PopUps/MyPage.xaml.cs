﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MyPage : Rg.Plugins.Popup.Pages.PopupPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        ObservableCollection<string> codes = new ObservableCollection<string>();
        public MyPage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            InitializeComponent();
            codes = JsonConvert.DeserializeObject<ObservableCollection<string>>(Application.Current.Properties["group_codes_list"].ToString());
        }

        void ClosePopup_Tapped(object sender, EventArgs e){
            Navigation.PopPopupAsync();
        }

        async void AddGroup_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(codeEntry.Text))
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.ADD_GROUP + codeEntry.Text, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error){
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    if (!codes.Contains(codeEntry.Text))
                    {
                        codes.Add(codeEntry.Text);
                        var user_temp = DataClass.GetInstance.user;
                        user_temp.role = int.Parse(jsonData["group"]["role"].ToString());
                        DataClass.GetInstance.user = user_temp;
                        DataClass.GetInstance.GroupId = jsonData["group"]["id"].ToString();
                        DataClass.GetInstance.GroupCode = codeEntry.Text;


                        GroupModel group_model_new = new GroupModel { id = int.Parse(jsonData["group"]["id"].ToString()), group_code = jsonData["group"]["group_code"].ToString(), group_name = jsonData["group"]["group_name"].ToString(), group_image = jsonData["group"]["group_image"].ToString() };

                        Application.Current.Properties["group_codes_list"] = JsonConvert.SerializeObject(codes);

                        var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());
                        group_models.Add(group_model_new);
                        Application.Current.Properties["group_models_list"] = JsonConvert.SerializeObject(group_models);

                        Application.Current.SavePropertiesAsync();
                    }

                    //Navigation.PopPopupAsync();
                    App.ShowMainPage();
                }

                if (jsonData["status"].ToString() == "404")
                {
                    DisplayAlert("Group not found", jsonData["error"]["message"].ToString(), "Okay");
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
