﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class SignInPage : ContentPage, IAdvanceRestConnector
    {
        bool isClicked = false;
        CancellationTokenSource cts;
        DataClass dataClass = DataClass.GetInstance;
        GroupModel gmodel;
        AdvanceRestService advanceRestService;

        public SignInPage(bool fromLogout = false)
        {
            advanceRestService = new AdvanceRestService();
            advanceRestService.WebServiceDelegate = this;

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            if (Application.Current.Properties.ContainsKey("group_models_list") && Application.Current.Properties.ContainsKey("group_codes_list"))
            {
                codeForm.IsVisible = false;
                backButton.Source = "Back";
                backFunction = 1;
                var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());
                gmodel = group_models.First();
            }
            passwordEntry.IsPassword = true;

            if (fromLogout)
                signInButton.IsVisible = true;
        }

        void ForgotUsername_Tapped(object sender, System.EventArgs e)
        {
            App.Log("FORGOT USERNAME");
            //Navigation.PushAsync(new ForgotPasswordPage(), false);
        }

        void ForgotPassword_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ForgotPasswordPage(), false);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "ResetPassword", (obj) =>
            {
                Navigation.PushAsync(new ResetPassword(obj), false);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "ResetPassword");
        }

        void Username_Completed(object sender, EventArgs e)
        {
            passwordEntry.Focus();
        }

        void Back_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                usernameEntry.Text = string.Empty;
                passwordEntry.Text = string.Empty;
                codeForm.IsVisible = true;
                backButton.Source = "Close";
                backFunction = 0;
            });

        }

        int backFunction = 0;
        void LeftButton_Tapped(object sender, TappedEventArgs e)
        {
            switch (backFunction)
            {
                case 0:
                    Navigation.PopAsync(false);
                    break;
                case 1:
                    Back_Clicked(null, null);
                    break;
            }
        }

        void ShowAlerts(string title, string error){
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }

        void SignUp_Clicked(object sender, ClickedEventArgs e)
        {
            Navigation.PushAsync(new SignUpPage(), false);
        }

        void SignIn_Clicked(object sender, ClickedEventArgs e)
        {
            bool isNotError = true;
            if (String.IsNullOrEmpty(usernameEntry.Text))
            {
                DisplayError(usernameEntry, "Username is required!");
                isNotError = false;
            }

            if (String.IsNullOrEmpty(passwordEntry.Text))
            {
                DisplayError(passwordEntry, "Password is required!");
                isNotError = false;
            }

            if (isNotError && isClicked == false)
            {
                isClicked = true;

                try
                {
                    //Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                    //{
                        //if (!string.IsNullOrEmpty(App.UDID))
                        //{
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                loader.IsVisible = true;
                                loader1.IsVisible = true;
                                cts = new CancellationTokenSource();
                                string data = JsonConvert.SerializeObject(new { group_id = gmodel.id, username = usernameEntry.Text, password = passwordEntry.Text, phone_type = (Device.RuntimePlatform.Equals(Device.iOS) ? 1 : 2) });
                                await advanceRestService.PostRequestAsync(Constants.SIGN_IN, data, cts.Token, 2);
                            });

                    //        return false;
                    //    }
                    //    return true;
                    //});
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        isClicked = false;
                        loader1.IsVisible = false;
                        loader.IsVisible = false;
                        cts = null;
                    });
                }
            }
        }

        void Next_Clicked(object sender, ClickedEventArgs e)
        {

            bool isNotError = true;

            if (string.IsNullOrEmpty(codeEntry.Text))
            {
                DisplayError(codeEntry, "Group code is required!");
                isNotError = false;
            }

            if (isNotError && isClicked == false)
            {
                isClicked = true;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (gmodel == null || gmodel.group_code != codeEntry.Text)
                    {
                        try
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                loader1.IsVisible = true;
                            });

                            cts = new CancellationTokenSource();
                            await advanceRestService.GetRequest(Constants.CHECK_GROUP + "?group_code=" + codeEntry.Text, cts.Token, 1);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                loader1.IsVisible = false;
                                loader.IsVisible = false;
                                cts = null;
                            });
                        }
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            codeForm.IsVisible = false;
                            backButton.Source = "Back";
                            backFunction = 1;
                            isClicked = false;
                        });
                    }

                });
            }
        }

        void DisplayError(Entry entry, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                entry.Text = String.Empty;
                entry.Placeholder = error;
                entry.PlaceholderColor = Color.Red;
            });

        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            isClicked = false;
            Device.BeginInvokeOnMainThread(() =>
            {

                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            codeForm.IsVisible = false;
                            backButton.Source = "Back";
                            backFunction = 1;

                            gmodel = JsonConvert.DeserializeObject<GroupModel>(jsonData["group"].ToString());

                            break;

                        case 2:
                            UserModel user = JsonConvert.DeserializeObject<UserModel>(jsonData["data"]["user"].ToString());
                            user.role = int.Parse(jsonData["data"]["role"].ToString());
                            dataClass.user = user;
                            dataClass.GroupCode = gmodel.group_code;
                            dataClass.GroupId = gmodel.id.ToString();
                            if (Application.Current.Properties.ContainsKey("group_codes_list"))
                            {
                                var codes = JsonConvert.DeserializeObject<ObservableCollection<string>>(Application.Current.Properties["group_codes_list"].ToString());
                                var this_codes = codes.Where(x => x.Equals(gmodel.group_code)).FirstOrDefault();
                                if (!string.IsNullOrEmpty(this_codes))
                                {
                                    codes[codes.IndexOf(this_codes)] = gmodel.group_code;
                                }
                                else
                                {
                                    codes.Add(gmodel.group_code);
                                }
                                Application.Current.Properties["group_codes_list"] = JsonConvert.SerializeObject(codes);
                                Application.Current.SavePropertiesAsync();
                            }
                            else
                            {
                                ObservableCollection<string> codes = new ObservableCollection<string>();
                                codes.Add(gmodel.group_code);
                                Application.Current.Properties["group_codes_list"] = JsonConvert.SerializeObject(codes);
                                Application.Current.SavePropertiesAsync();
                            }

                            if (Application.Current.Properties.ContainsKey("group_models_list"))
                            {
                                var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());
                                var this_model = group_models.Where(x => x.id == gmodel.id).FirstOrDefault();
                                if (this_model != null)
                                {
                                    group_models[group_models.IndexOf(this_model)] = gmodel;
                                }
                                else
                                {
                                    group_models.Add(gmodel);
                                }
                                Application.Current.Properties["group_models_list"] = JsonConvert.SerializeObject(group_models);
                                Application.Current.SavePropertiesAsync();
                            }
                            else
                            {
                                ObservableCollection<GroupModel> group_models = new ObservableCollection<GroupModel>();
                                group_models.Add(gmodel);
                                Application.Current.Properties["group_models_list"] = JsonConvert.SerializeObject(group_models);
                                Application.Current.SavePropertiesAsync();
                            }

                            App.ShowMainPage();
                            break;
                    }
                }
                else if (jsonData["status"].ToString() == "404")
                {
                    switch (wsType)
                    {
                        case 1:
                            DisplayAlert("Error", jsonData["error"]["message"].ToString(), "Okay");
                            break;
                    }
                }
                loader1.IsVisible = false;
                loader.IsVisible = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                isClicked = false;
                loader.IsVisible = false;
                loader1.IsVisible = false;
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
