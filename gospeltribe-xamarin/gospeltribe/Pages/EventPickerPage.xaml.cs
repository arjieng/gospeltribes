﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class EventPickerPage : RootViewPage
    {
        CalendarPage calendarPage;
        public EventPickerPage(CalendarPage calendarPage, IEnumerable<EventsModel> events)
        {
            this.calendarPage = calendarPage;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            listView.FlowColumnTemplate = new DataTemplate(EventBarViewsDataTemplate);
            ObservableCollection<EventsModel> ev = new ObservableCollection<EventsModel>(events);
            List<EventsModel> flowItemSource = ev.OrderBy(x => x.start_date.TimeOfDay).ToList();
            listView.FlowItemsSource = flowItemSource;
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Events";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopModalAsync(false);
            });
        }

        EventBarViews ebv;
        EventBarViews EventBarViewsDataTemplate()
        {
            ebv = new EventBarViews(calendarPage);
            ebv.SetBinding(ebv.EventProperty, ".");
            return ebv;
        }

        void Event_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Navigation.PushModalAsync(new EventsView((EventsModel)e.Item, calendarPage), false);
        }
    }
}
