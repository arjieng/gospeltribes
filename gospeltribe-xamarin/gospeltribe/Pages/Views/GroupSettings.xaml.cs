﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class GroupSettings : RootViewPage, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        InboxModel ee;
        Stream imageStream = null;
        string chatroom_name = "";
        bool isClicked = false;

        public GroupSettings(InboxModel ee)
        {
            this.ee = ee;
            //ee.
            NavigationPage.SetHasNavigationBar(this, false);
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            InitializeComponent();

            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "SETTINGS";

            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(GoBack_Tapped);
        }

        async void SaveSettings_Clicked(object sender, System.EventArgs e)
        {
            if (!isClicked)
            {
                if (imageStream != null && !string.IsNullOrEmpty(chatroom_name))
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            isClicked = true;
                            cts = new CancellationTokenSource();
                            UserDialogs.Instance.ShowLoading("Saving");
                            string dict = JsonConvert.SerializeObject(new { chatroom = new { id = ee.id, chatroom_name = chatroom_name, chatroom_image = "" } });
                            await advanceRest.MultiPartDataContentAsync(Constants.CHANGE_CHATROOM_DETAILS, dict, cts.Token, imageStream, "chatroom");
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }

                if (imageStream != null && string.IsNullOrEmpty(chatroom_name))
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            isClicked = true;
                            cts = new CancellationTokenSource();
                            UserDialogs.Instance.ShowLoading("Saving");
                            string dict = JsonConvert.SerializeObject(new { chatroom = new { id = ee.id, chatroom_image = "" } });
                            await advanceRest.MultiPartDataContentAsync(Constants.CHANGE_CHATROOM_DETAILS, dict, cts.Token, imageStream, "chatroom");
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }
                if (imageStream == null && !string.IsNullOrEmpty(chatroom_name))
                {
                    if (!ee.chatroom_name.Equals(chatroom_name))
                    {
                        

                        await Task.Run(async () =>
                        {
                            try
                            {
                                isClicked = true;
                                cts = new CancellationTokenSource();
                                UserDialogs.Instance.ShowLoading("Saving");
                                string dict = JsonConvert.SerializeObject(new { chatroom = new { id = ee.id, chatroom_name = "Example" } });
                                await advanceRest.PostRequestAsync(Constants.CHANGE_CHATROOM_DETAILS, dict, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    isClicked = false;
                                    UserDialogs.Instance.HideLoading();
                                    cts = null;
                                });
                            }
                        });
                    }
                    else
                    {
                        DisplayAlert("", "Nothing was changed.", "Okay");
                    }
                }
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        private void GoBack_Tapped(object obj)
        {
            Navigation.PopAsync(false);
        }

        async void Image_Tapped(object sender, System.EventArgs e)
        {
            new ImageCropper()
            {
                CropShape = ImageCropper.CropShapeType.Oval,
                Success = (imageFile) =>
                {
                    SuccessCallback(imageFile);
                }
            }.Show(this);
        }

        void SuccessCallback(string imageFile)
        {
            Stream stream = new FileStream(imageFile, FileMode.Open);
            imageStream = stream;
            image.Source = ImageSource.FromFile(imageFile);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.HideLoading();
                DisplayAlert("Success", "Chatroom image was changed.", "Okay");
                isClicked = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }
    }
}
