﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class InboxView : ContentView
    {
        public InboxView()
        {
            InitializeComponent();
        }

        void Handle_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Text"){
                Label thisLabel = (Label)sender;
                thisLabel.Text = thisLabel.Text.TruncateText(36);
            }
        }

        public static readonly BindableProperty InboxProperty =
            BindableProperty.Create(nameof(Inbox), typeof(InboxModel), typeof(InboxView), new InboxModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as InboxView;
                view.BindingContext = view.Inbox;
            });
        public InboxModel Inbox
        {
            get { return (InboxModel)GetValue(InboxProperty); }
            set { SetValue(InboxProperty, value); }
        }
    }
}
