﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class GroupViews : StackLayout
    {
        public GroupViews()
        {
            InitializeComponent();
            BindingContextChanged += (sender, e) => {
                group_name.Text = string.Empty;
                addAnother.IsVisible = false;
                gAvatar.Source = null;
                stack.HeightRequest = -1;

                var bc = (GroupModel)BindingContext;
                if(bc != null){
                    if(string.IsNullOrEmpty(bc.group_name)){
                        if(string.IsNullOrEmpty(bc.group_code)){
                            gAvatar.IsVisible = true;
                            gAvatar.Source = "AddCircle";
                            addAnother.IsVisible = true;
                            group_name.IsVisible = false;
                        }else{
                            stack.HeightRequest = 50.ScaleWidth();
                            gAvatar.Source = bc.group_image;
                            group_name.Text = bc.group_code;
                            addAnother.IsVisible = false;
                            group_name.IsVisible = true;
                        }
                    }else{
                        stack.HeightRequest = 50.ScaleWidth();
                        gAvatar.Source = bc.group_image;
                        group_name.Text = bc.group_name;
                        addAnother.IsVisible = false;
                        group_name.IsVisible = true;
                    }
                }
            };
        }
        public event Action<object> Tapped;
        void Group_OnTapEvent(object obj)
        {
            Tapped((GroupModel)BindingContext);
        }
    }
}
