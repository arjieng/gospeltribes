﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MediaImageView : ContentView, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        MediaPage mediaPage;
        ProfilePage profilePage;
        AdvanceRestService advanceRest;

        public MediaImageView(MediaPage mediaPage, bool isFromProfile = false)
        {
            Constants.IS_FROM_PROFILE = isFromProfile;
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.mediaPage = mediaPage;
            InitializeComponent();
            this.HeightRequest = (App.screenWidth / 3).ScaleHeight();
            BindingContextChanged += MediaImageView_BindingContextChanged;
        }

        void MediaImageView_BindingContextChanged(object sender, EventArgs e)
        {
            mainPic.Source = null;
            MediaModel media = (MediaModel)BindingContext;
            if (media != null)
            {
                mainPic.Source = media.media;
            }
        }


        public MediaImageView(MediaPage mediaPage, ProfilePage profilePage, bool isFromProfile = true)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.mediaPage = mediaPage;
            this.profilePage = profilePage;
            Constants.IS_FROM_PROFILE = isFromProfile;
            InitializeComponent();
            this.HeightRequest = (App.screenWidth / 3).ScaleHeight();
            BindingContextChanged += MediaImageView_BindingContextChanged;

        }

        public MediaImageView()
        {
            InitializeComponent();
            this.HeightRequest = (App.screenWidth / 3).ScaleHeight();
            BindingContextChanged += MediaImageView_BindingContextChanged;
        }

        void Image_OnTapEvent(object obj)
        {
            MediaModel mediaModel = ((MediaModel)this.BindingContext);
            if (Constants.IS_FROM_PROFILE)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
                    Navigation.PushModalAsync(new NavigationPage(new MediaImagePage(mediaModel, true)), false);
                }
                else
                {
                    Application.Current.MainPage?.Navigation.PushModalAsync(new NavigationPage(new MediaImagePage(mediaModel, true)), false);
                }
            }
            else
            {
                Application.Current.MainPage?.Navigation.PushModalAsync(new NavigationPage(new MediaImagePage(mediaModel)), false);
            }
        }

        async void Image_OnLongTapEvent(object obj)
        {
            if(DataClass.GetInstance.user.role.Equals(1)){
                if (mediaPage != null)
                {

                    string choice = await Application.Current.MainPage?.DisplayActionSheet("Remove this image?", "Cancel", "Delete");
                    if (choice.Equals("Delete"))
                    {
                        bool choice2 = await Application.Current.MainPage?.DisplayAlert("Remove this image?", "Are you really sure you want to delete this image?", "Yes", "No");
                        if (choice2)
                        {
                            MediaModel mediaModel = ((MediaModel)this.BindingContext);

                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    UserDialogs.Instance.ShowLoading("Deleting Media");
                                    await advanceRest.GetRequest(Constants.REMOVE_GROUP_IMAGE + "?group_image_id=" + mediaModel.id, cts.Token, 0);

                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                        UserDialogs.Instance.HideLoading();
                                    });
                                }
                            });
                        }
                    }
                }
            }
        }

        void ShowAlerts(string title, string error){
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("200"))
                {
                    UserDialogs.Instance.HideLoading();
                    mediaPage.RemoveImage(((MediaModel)this.BindingContext));
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.HideLoading();
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
