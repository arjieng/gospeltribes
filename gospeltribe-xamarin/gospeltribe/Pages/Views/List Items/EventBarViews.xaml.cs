﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class EventBarViews : ContentView, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        CalendarPage calendarPage;
        public EventBarViews(CalendarPage calendarPage)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.calendarPage = calendarPage;
            InitializeComponent();

            BindingContextChanged += (sender, e) => {
                subjectLabel.TextColor = Color.Black;
                var bc = (EventsModel)BindingContext;
                if (bc != null)
                {
                    string co = App.colors.Where(x => x.Equals(bc.subtitle)).FirstOrDefault();
                    if (!string.IsNullOrEmpty(co))
                    {
                        var converter = new ColorTypeConverter();
                        Color result = (Color)converter.ConvertFromInvariantString(bc.subtitle);
                        subjectLabel.TextColor = result;
                    }
                    else
                    {
                        subjectLabel.TextColor = Color.Black;
                    }
                }
            };

        }
        void Event_OnTapEvent(object obj)
        {
            Application.Current.MainPage?.Navigation.PushModalAsync(new NavigationPage(new EventsView((EventsModel)Event, calendarPage)), false);
        }

        async void Event_OnLongTapEvent(object obj)
        {
            if (DataClass.GetInstance.user.role.Equals(1))
            {

                string choice = await Application.Current.MainPage?.DisplayActionSheet("Remove this event?", "Cancel", "Delete");
                if (choice.Equals("Delete"))
                {
                    bool choice2 = await Application.Current.MainPage?.DisplayAlert("Remove this event?", "Are you really sure you want to delete this event?", "Yes", "No");
                    if (choice2)
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                cts = new CancellationTokenSource();
                                UserDialogs.Instance.ShowLoading("Deleting Event");
                                await advanceRest.GetRequest(Constants.REMOVE_EVENT + "?event_id=" + Event.id, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    cts = null;
                                    UserDialogs.Instance.HideLoading();
                                });
                            }
                        });
                    }
                }
            }
        }

        void ShowAlerts(string title, string error){
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public BindableProperty EventProperty =
            BindableProperty.Create(nameof(Event), typeof(EventsModel), typeof(EventBarViews), new EventsModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as EventBarViews;
                view.BindingContext = view.Event;
                if (view.Event != null)
                {
                    if (!string.IsNullOrEmpty(view.details.Text))
                    {
                        if (view.details.Text.Length > 20)
                        {
                            view.details.Text = view.details.Text.Substring(0, 16) + "...";
                        }
                    }
                }
            });

        void Handle_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Text")
            {
                if (!string.IsNullOrEmpty(details.Text))
                {
                    if (details.Text.Length > 20)
                    {
                        string substring = details.Text.Substring(0, 16);

                        if (substring.Contains("\n"))
                        {
                            details.Text = substring.Split('\n')[0] + "...";
                        }
                        else
                        {
                            details.Text = substring + "...";
                        }
                    }
                }
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("200"))
                {
                    UserDialogs.Instance.HideLoading();
                    calendarPage.RemoveEVent(Event);
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public EventsModel Event
        {
            get { return (EventsModel)GetValue(EventProperty); }
            set { SetValue(EventProperty, value); }
        }

    }
}
