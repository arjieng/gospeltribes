﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class AttendeesView : StackLayout
    {
        public AttendeesView()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty UserProperty =
            BindableProperty.Create(nameof(User), typeof(UserModel), typeof(AttendeesView), new UserModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as AttendeesView;
                view.BindingContext = view.User;
                view.image.Source = null;
                if (view.User != null)
                {
                    view.image.Source = view.User.avatar.url;
                }
            });

        public UserModel User
        {
            get { return (UserModel)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }
    }
}
