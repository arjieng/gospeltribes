﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class CommentView : ContentView
    {
        public CommentView()
        {
            InitializeComponent();
            BindingContextChanged += CommentView_BindingContextChanged;
        }

        void CommentView_BindingContextChanged(object sender, EventArgs e)
        {
            userImage.Source = null;
            userFullName.Text = string.Empty;
            userMessage.Text = string.Empty;
            if (BindingContext is PrayerCommentModel comment)
            {
                userImage.Source = comment.user.avatar.url;
                userFullName.Text = comment.user.full_name;
                userMessage.FormattedText = comment.comment.ConvertToFormattedString();
            }
        }


        public static readonly BindableProperty CommentProperty =
            BindableProperty.Create(nameof(Comment), typeof(PrayerCommentModel), typeof(CommentView), new PrayerCommentModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as CommentView;
                view.BindingContext = view.Comment;
            });

        public PrayerCommentModel Comment
        {
            get { return (PrayerCommentModel)GetValue(CommentProperty); }
            set { SetValue(CommentProperty, value); }
        }

        public event Action<int> DeleteComment;

        async void LongPressed_Command(object obj)
        {

            if (DataClass.GetInstance.user.role.Equals(1))
            {
                var choice = await Application.Current.MainPage?.DisplayActionSheet($"{Comment.comment}", "Cancel", null, "Copy Comment", "Delete Comment");
                if (choice.Equals("Copy Comment"))
                {
                    DependencyService.Get<IClipBoard>().SendTextToClipboard(Comment.comment.ToString());
                    Application.Current.MainPage?.DisplayAlert("Success", "You have copied this comment", "Okay");
                }

                if (choice.Equals("Delete Comment"))
                {
                    var choice2 = await Application.Current.MainPage?.DisplayAlert("", "Do you really want to delete this comment?", "Yes", "No");
                    if (choice2)
                    {
                        DeleteComment(Comment.id);
                    }
                }

            }
            else
            {
                var choice = await Application.Current.MainPage?.DisplayActionSheet($"{Comment.comment}", "Cancel", null, "Copy Comment");
                if (choice.Equals("Copy Comment"))
                {

                    DependencyService.Get<IClipBoard>().SendTextToClipboard(Comment.comment.ToString());
                    Application.Current.MainPage?.DisplayAlert("Success", "You have copied this comment", "Okay");
                }
            }
        }
    }
}
