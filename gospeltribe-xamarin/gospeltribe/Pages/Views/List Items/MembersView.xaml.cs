﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MembersView : ContentView
    {
        MembersPage mp;
        public MembersView(MembersPage mp)
        {
            this.mp = mp;
            InitializeComponent();

            switch(Device.RuntimePlatform){
                case Device.iOS:
                    ciAvatar.BorderThickness = 2.ScaleWidth();
                    break;
                case Device.Android:
                    ciAvatar.BorderThickness = (0.5).ScaleWidth();
                    break;
            }
            BindingContextChanged += MembersView_BindingContextChanged;
        }

        void MembersView_BindingContextChanged(object sender, EventArgs e)
        {
            ciAvatar.Source = null;
            UserModel user = (UserModel)BindingContext;
            if(user != null)
            {
                ciAvatar.Source = user.avatar.url;
            }
        }


        void User_OnTapEvent(object obj)
        {
            Application.Current.MainPage?.Navigation.PushAsync(new ProfilePage(User), false);
        }

        void Settings_OnLongTapEvent(object obj)
        {
            if (DataClass.GetInstance.user.role.Equals(1) && DataClass.GetInstance.user.id != User.id)
                mp.DisplayAction(this.User);
        }

        public BindableProperty UserProperty =
            BindableProperty.Create(nameof(User), typeof(UserModel), typeof(MembersView), new UserModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as MembersView;
                view.BindingContext = view.User;
            });

        public UserModel User
        {
            get { return (UserModel)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }
    }
}
