﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ProfilePrayersView : ContentView
    {
        public ProfilePrayersView()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty PrayerProperty =
            BindableProperty.Create(nameof(Prayer), typeof(PrayersModel), typeof(ProfilePrayersView), new PrayersModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as ProfilePrayersView;
                view.BindingContext = view.Prayer;
                if (view.Prayer != null)
                {
                    if (view.Prayer.user.id != DataClass.GetInstance.user.id)
                    {
                        view.hideIcon.IsVisible = false;
                    }
                }
            });

        public PrayersModel Prayer
        {
            get { return (PrayersModel)GetValue(PrayerProperty); }
            set { SetValue(PrayerProperty, value); }
        }

        public event EventHandler HideTapped;
        void Hide_Tapped(object sender, TappedEventArgs e)
        {
            HideTapped(Prayer, null);
        }

    }
}
