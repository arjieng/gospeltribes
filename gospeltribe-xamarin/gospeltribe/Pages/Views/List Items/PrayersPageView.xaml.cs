﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class PrayersPageView : StackLayout, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        PrayersPage prayersPage;

        public PrayersPageView(PrayersPage prayersPage)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.prayersPage = prayersPage;
            InitializeComponent();

        }

        public BindableProperty PrayerProperty =
            BindableProperty.Create(nameof(Prayer), typeof(PrayersModel), typeof(PrayersPageView), new PrayersModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as PrayersPageView;
                view.image.Source = null;
                view.name.Text = String.Empty;
                view.subject.Text = String.Empty;

                view.BindingContext = view.Prayer;
                if (view.Prayer != null)
                {
                    view.image.Source = view.Prayer.user.avatar.url;
                    view.name.Text = (string.IsNullOrEmpty(view.Prayer.user.first_name) ? view.Prayer.user.username : view.Prayer.user.first_name);
                    view.subject.Text = view.Prayer.subject;

                }
            });

        public PrayersModel Prayer
        {
            get { return (PrayersModel)GetValue(PrayerProperty); }
            set { SetValue(PrayerProperty, value); }
        }
        void Prayer_OnTapEvent(object obj)
        {
            Application.Current.MainPage?.Navigation.PushAsync(new PrayersView(Prayer, prayersPage), false);
        }

        async void Prayer_OnLongTapEvent(object obj)
        {
            if (DataClass.GetInstance.user.role.Equals(1))
            {
                string choice = await Application.Current.MainPage?.DisplayActionSheet(Prayer.subject, "Cancel", "Delete");
                if (choice.Equals("Delete"))
                {
                    bool choice2 = await Application.Current.MainPage?.DisplayAlert("Delete this prayer?", "Are you really sure you want to delete this prayer?", "Yes", "No");
                    if (choice2)
                    {
                        await Task.Run(async () =>
                        {
                            try
                            {
                                cts = new CancellationTokenSource();
                                UserDialogs.Instance.ShowLoading("Deleting Prayer");
                                await advanceRest.GetRequest(Constants.DELETE_PRAYER + Prayer.id, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    cts = null;
                                    UserDialogs.Instance.HideLoading();
                                });
                            }
                        });
                    }
                }
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("200"))
                {
                    UserDialogs.Instance.HideLoading();
                    prayersPage.RemovePrayer(Prayer);
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {

            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
