﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MasterView : ContentPage
    {
        string lastId = "1";
        public MasterView()
        {
            InitializeComponent();
        }

        public event EventHandler MenuTapped;
        void Menu_Tapped(object sender, TappedEventArgs e){
            StackLayout stackLayout = (StackLayout)sender;
            (((Grid)stackLayout.Parent).Children.Where(X => X.ClassId == lastId).FirstOrDefault() as StackLayout).BackgroundColor = Color.Transparent;
            stackLayout.BackgroundColor = Color.FromHex("#3b4e6c");
            lastId = stackLayout.ClassId;
            MenuTapped(sender, e);
        }
    }
}
