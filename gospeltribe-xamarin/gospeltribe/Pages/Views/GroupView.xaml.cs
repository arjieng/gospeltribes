﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class GroupView : ContentView
    {
        public event EventHandler Tapped;
        public event EventHandler Remove;

        public GroupView()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty ImageValueProperty =
            BindableProperty.Create(nameof(ImageValue), typeof(ImageSource), typeof(GroupView), null, propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as GroupView;
                view.image.Source = view.ImageValue;
            });

        public ImageSource ImageValue
        {
            get { return (ImageSource)GetValue(ImageValueProperty); }
            set { SetValue(ImageValueProperty, value); }
        }

        void Group_Tapped(object sender, TappedEventArgs e)
        {

        }

        void Close_Tapped(object sender, TappedEventArgs e)
        {
            Remove(this, e);
        }
    }

    public class GroupObject
    {
        public GroupView groupView;
        public GroupModel groupModel;
    }
}
