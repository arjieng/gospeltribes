﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using DLToolkit.Forms.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class PrayersView : RootViewPage, IAdvanceRestConnector
    {
        ObservableCollection<PrayerCommentModel> comments = new ObservableCollection<PrayerCommentModel>();
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        internal FlowListView myListView;
        PrayersPage pp;
        int isAdmin = 0;
        PrayersViewHeader pvh;

        public bool isAnswered;
        public PrayersView(PrayersModel item, PrayersPage pp)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            Prayer = item;
            this.pp = pp;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            listView.HeaderTemplate = new DataTemplate(NewPrayersViewHeader);
            listView.Header = item;
            listView.FlowItemsSource = comments;


            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Prayer";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command((obj) =>
            {
                Navigation.PopAsync(false);
            });


            if (item.user.id == DataClass.GetInstance.user.id || DataClass.GetInstance.user.role.Equals(1))
            {
                if (item.is_answered == 0)
                {
                    if (item.user.id != DataClass.GetInstance.user.id)
                    {
                        editStack.IsVisible = false;
                        setAsAnsweredLine.IsVisible = false;
                        setAsAnsweredStack.IsVisible = false;
                        deleteLine.IsVisible = false;
                    }

                    if (!DataClass.GetInstance.user.role.Equals(1))
                    {
                        deleteLine.IsVisible = false;
                        deleteStack.IsVisible = false;
                    }

                    this.isAnswered = false;
                    this.RightIcon = "Tdots";
                    this.RightIconTint = Color.Black;
                    this.RightButtonCommand = new Command(() =>
                    {
                        if (dm.IsVisible)
                        {
                            outerSpace.IsVisible = false;
                            dm.IsVisible = false;
                        }
                        else
                        {
                            outerSpace.IsVisible = true;
                            dm.IsVisible = true;
                        }
                    });
                }
                else
                {
                    if (DataClass.GetInstance.user.role.Equals(1))
                    {
                        setAsAnsweredLine.IsVisible = false;
                        setAsAnsweredStack.IsVisible = false;
                        isAdmin = 1;
                        this.isAnswered = false;
                        this.RightIcon = "Tdots";
                        this.RightIconTint = Color.Black;
                        this.RightButtonCommand = new Command(() =>
                        {
                            if (dm.IsVisible)
                            {
                                outerSpace.IsVisible = false;
                                dm.IsVisible = false;
                            }
                            else
                            {
                                outerSpace.IsVisible = true;
                                dm.IsVisible = true;
                            }
                        });
                    }
                    else
                    {
                        this.isAnswered = true;
                        this.RightIcon = "EditPrayer";
                        this.RightIconTint = Color.Black;
                        this.RightButtonCommand = new Command(() =>
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new CreatePrayerPage(2, Prayer, pp, this), false);
                            });

                        });
                    }

                }
            }

            myListView = listView;

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GET_PRAYER_COMMENTS(item.id), cts.Token, 1);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            });

        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        async void DeleteComment(int obj)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    UserDialogs.Instance.ShowLoading("Deleting comment");
                    await advanceRest.GetRequest(Constants.DELETE_PRAYER_COMMENT + obj, cts.Token, 4);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        UserDialogs.Instance.HideLoading();
                        cts = null;
                    });
                }
            });
        }

        PrayersViewHeader NewPrayersViewHeader()
        {
            pvh = new PrayersViewHeader();
            pvh.SendComment += Send_Clicked;
            return pvh;
        }

        public void ClosePage()
        {
            Navigation.PopAsync(false);
        }

        void Item_Tapped(object sender, TappedEventArgs e)
        {
            StackLayout stack = (StackLayout)sender;
            dm.IsVisible = false;
            switch (stack.ClassId)
            {
                case "0":
                    if (isAdmin.Equals(0))
                    {
                        Navigation.PushAsync(new CreatePrayerPage(1, Prayer, pp, this), false);
                    }
                    else
                    {
                        Navigation.PushAsync(new CreatePrayerPage(2, Prayer, pp, this), false);
                    }
                    break;
                case "1":
                    Navigation.PushPopupAsync(new AnsweredPopupPage(pp, Prayer, this));
                    break;
                case "2":
                    Device.BeginInvokeOnMainThread(async () =>
                    {

                        var choice = await DisplayAlert("", "Do you really want to delete this prayer?", "Yes", "No");
                        if (choice)
                        {
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    UserDialogs.Instance.ShowLoading("Deleting prayer");
                                    await advanceRest.GetRequest(Constants.DELETE_PRAYER + Prayer.id, cts.Token, 3);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        UserDialogs.Instance.HideLoading();
                                        cts = null;
                                    });
                                }
                            });


                        }
                    });
                    break;
            }
        }

        async void Send_Clicked(object sender, EventArgs e)
        {
            string inboxModel = sender.ToString();
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    UserDialogs.Instance.ShowLoading("Adding comment");
                    await advanceRest.PostRequestAsync(Constants.ADD_PRAYER_COMMENT, inboxModel, cts.Token, 2);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        UserDialogs.Instance.HideLoading();
                        cts = null;
                    });
                }
            });
        }

        public static readonly BindableProperty PrayerProperty =
            BindableProperty.Create(nameof(Prayer), typeof(PrayersModel), typeof(PrayersView), new PrayersModel(), propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as PrayersView;
                view.BindingContext = view.Prayer;
            });

        public PrayersModel Prayer
        {
            get { return (PrayersModel)GetValue(PrayerProperty); }
            set { SetValue(PrayerProperty, value); }
        }

        void Handle_TouchAction(object sender, gospeltribe.TouchActionEventArgs args)
        {
            if (args.Type == TouchActionType.Pressed)
            {
                outerSpace.IsVisible = false;
                dm.IsVisible = false;
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {

                    switch (wsType)
                    {
                        case 1:
                            ObservableCollection<PrayerCommentModel> opc = JsonConvert.DeserializeObject<ObservableCollection<PrayerCommentModel>>(jsonData["comments"].ToString());
                            foreach (var com in opc)
                            {
                                comments.Add(com);
                            }

                            if (opc.Count == 0)
                            {
                                pvh.HideOrUnhide(false);
                            }
                            else
                            {
                                pvh.HideOrUnhide(true);
                            }

                            break;
                        case 2:
                            PrayerCommentModel pc = JsonConvert.DeserializeObject<PrayerCommentModel>(jsonData["comment"].ToString());
                            pc.user = JsonConvert.DeserializeObject<UserModel>(jsonData["user"].ToString());
                            pvh.HideOrUnhide(true);
                            UserDialogs.Instance.HideLoading();
                            comments.Insert(0, pc);
                            break;
                        case 3:
                            UserDialogs.Instance.HideLoading();
                            Navigation.PopAsync(false);
                            break;
                        case 4:
                            comments.Remove(comments.Where(x => x.id.Equals(int.Parse(jsonData["prayer_comment_id"].ToString()))).FirstOrDefault());
                            if (comments.Count == 0)
                            {
                                pvh.HideOrUnhide(false);
                            }
                            else
                            {
                                pvh.HideOrUnhide(true);
                            }
                            UserDialogs.Instance.HideLoading();
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
