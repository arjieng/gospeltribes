﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ChatInputBarView : ContentView, IAdvanceRestConnector
    {
        byte[] fromStreamFile;
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        bool isClicked = false, isTriggered = true;
        Stream streamFile;
        public int chatroom_id;
        GroupView group = new GroupView();

        public ChatInputBarView()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            InitializeComponent();
            if (Device.RuntimePlatform == Device.iOS)
            {
                this.SetBinding(HeightRequestProperty, new Binding("Height", BindingMode.OneWay, null, null, null, chatTextInput));
            }

            chatTextInput.PropertyChanged += (sender, e) =>
            {
                if(chatTextInput.LineCount >= 11)
                {
                    chatTextInput.HeightRequest = 200.ScaleHeight();
                }
                else
                {
                    chatTextInput.HeightRequest = -1;
                }
            };

            chatTextInput.TextChanged += async (sender, e) =>
            {
                if (isTriggered)
                {
                    if (string.IsNullOrEmpty(e.OldTextValue))
                    {
                        if ((0 - e.NewTextValue.Length) < 0)
                        {
                            if (!e.NewTextValue.Length.Equals(1))
                            {
                                isTriggered = false;
                                //chatTextInput.Text = chatTextInput.Text + "{{<break>}}";
                                chatTextInput.Text = chatTextInput.Text + " ";

                                await Task.Delay(100);
                                //chatTextInput.Text = chatTextInput.Text.Replace("{{<break>}}", "");
                                chatTextInput.Text = chatTextInput.Text.Remove(chatTextInput.Text.Length - 1);

                                await Task.Delay(100);
                                isTriggered = true;
                            }
                        }
                    }
                    else
                    {
                        if ((e.OldTextValue.Length - e.NewTextValue.Length) < 0)
                        {
                            if (!(e.NewTextValue.Length - e.OldTextValue.Length).Equals(1))
                            {
                                isTriggered = false;
                                //chatTextInput.Text = chatTextInput.Text + "{{<break>}}";
                                chatTextInput.Text = chatTextInput.Text + " ";
                                await Task.Delay(100);
                                //chatTextInput.Text = chatTextInput.Text.Replace("{{<break>}}", "");
                                chatTextInput.Text = chatTextInput.Text.Remove(chatTextInput.Text.Length - 1);

                                await Task.Delay(100);
                                isTriggered = true;
                            }
                        }
                    }
                }

            };
        }


        public event Action<object> Focused;
        public event Action<object> Unfocused;

        void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            Focused(this);
        }

        void Handle_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            Unfocused(this);
        }

        async void Send_Tapped(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                await Task.Run(async () =>
                {
                    if (!string.IsNullOrEmpty(chatTextInput.Text) && streamFile != null)
                    {
                        try
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                loadingIndicatorContainer.IsVisible = true;
                                textPercentage.IsVisible = true;
                            });

                            isClicked = true;
                            cts = new CancellationTokenSource();
                            string dictionary = JsonConvert.SerializeObject(new { message = new { user_id = DataClass.GetInstance.user.id, chatroom_id = this.chatroom_id, group_id = int.Parse(DataClass.GetInstance.GroupId), body = chatTextInput.Text, image = "", time_sent = DateTime.Now, topics = string.Format("group_{0}", DataClass.GetInstance.GroupId) } });
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                chatTextInput.Text = string.Empty;
                            });
                            await advanceRest.MultiPartDataContentAsync(Constants.SEND_MESSAGE, dictionary, cts.Token, streamFile, "message", 0, Progress);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                grid.Children.Remove(grid.Children.Last());
                                chatTextInput.Text = string.Empty;
                                streamFile = null;
                                fromStreamFile = null;
                                isClicked = false;
                                cts = null;
                                loadingIndicatorContainer.IsVisible = false;
                                textPercentage.IsVisible = false;
                            });

                        }
                    }

                    if (!string.IsNullOrEmpty(chatTextInput.Text) && streamFile == null)
                    {
                        try
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                loadingIndicatorContainer.IsVisible = true;
                            });
                            isClicked = true;
                            cts = new CancellationTokenSource();
                            string dictionary = JsonConvert.SerializeObject(new { topics = string.Format("group_{0}", DataClass.GetInstance.GroupId), message = new { user_id = DataClass.GetInstance.user.id, chatroom_id = this.chatroom_id, group_id = int.Parse(DataClass.GetInstance.GroupId), body = chatTextInput.Text, time_sent = DateTime.Now } });
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                chatTextInput.Text = string.Empty;
                            });
                            await advanceRest.PostRequestAsync(Constants.SEND_MESSAGE, dictionary, cts.Token, 0);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                chatTextInput.Text = string.Empty;
                                isClicked = false;
                                cts = null;
                                loadingIndicatorContainer.IsVisible = false;
                            });

                        }
                    }

                    if (string.IsNullOrEmpty(chatTextInput.Text) && streamFile != null)
                    {
                        try
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                loadingIndicatorContainer.IsVisible = true;
                                textPercentage.IsVisible = true;
                            });
                            isClicked = true;
                            cts = new CancellationTokenSource();
                            string dictionary = JsonConvert.SerializeObject(new { message = new { user_id = DataClass.GetInstance.user.id, chatroom_id = this.chatroom_id, group_id = int.Parse(DataClass.GetInstance.GroupId), body = "", image = "", time_sent = DateTime.Now, topics = string.Format("group_{0}", DataClass.GetInstance.GroupId) } });
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                chatTextInput.Text = string.Empty;
                            });
                            await advanceRest.MultiPartDataContentAsync(Constants.SEND_MESSAGE, dictionary, cts.Token, streamFile, "message", 0, Progress);

                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                grid.Children.Remove(grid.Children.Last());
                                streamFile = null;
                                fromStreamFile = null;
                                isClicked = false;
                                cts = null;
                                loadingIndicatorContainer.IsVisible = false;
                                textPercentage.IsVisible = false;
                            });
                        }
                    }
                });
            }
        }

        void Progress(long a, long b)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                textPercentage.Text = string.Format("{0:##.###}/100", ((float)(100 * a) / b));
            });
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        async void Attachment_Tapped(object sender, EventArgs e)
        {

            var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions { PhotoSize = PhotoSize.Custom, CustomPhotoSize = 50 });
            if (file == null)
                return;


            if (streamFile != null)
            {
                grid.Children.Remove(group);
            }


            ImageSource imageSource = ImageSource.FromStream(() => { var stream = file.GetStream(); return stream; });
            GroupView groupView = new GroupView { Margin = new Thickness(0, 0, 0, 5.ScaleHeight()), BackgroundColor = Color.Red, ImageValue = imageSource, HeightRequest = 80.ScaleHeight(), WidthRequest = 80.ScaleWidth(), VerticalOptions = LayoutOptions.Start, HorizontalOptions = LayoutOptions.End };
            groupView.Remove += (object sender2, EventArgs e2) =>
            {
                grid.Children.Remove((GroupView)sender2);
                fromStreamFile = null;
                this.HeightRequest = grid.HeightRequest;
            };
            group = groupView;
            grid.Children.Add(groupView, 1, 0);
            this.HeightRequest = grid.HeightRequest;
            streamFile = file.GetStreamWithImageRotatedForExternalStorage();
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                chatTextInput.Text = string.Empty;
                isClicked = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                isClicked = false;
            });
        }
    }
}