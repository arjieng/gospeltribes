﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ProfilePageHeaderView : ContentView, IAdvanceRestConnector
    {
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        MediaPage mediaPage;
        ObservableCollection<MediaModel> medias = new ObservableCollection<MediaModel>();
        ProfilePage profile;


        public event Action<UserModel> Tapped;
        async void PhoneNumber_Tapped(object sender, System.EventArgs e)
        {
            var choice = await Application.Current.MainPage?.DisplayActionSheet(User.phone_number, "Cancel", null, "Call " + User.phone_number, "Send Message", "Add to Contacts");
            if (choice.Equals("Call " + User.phone_number))
            {
                Device.OpenUri(new Uri(String.Format("tel:{0}", User.phone_number)));
            }

            if (choice.Equals("Send Message"))
            {
                Device.OpenUri(new Uri(String.Format("sms:{0}", User.phone_number)));
            }

            if (choice.Equals("Add to Contacts"))
            {
                Tapped(User);
            }
        }

        public ProfilePageHeaderView(ProfilePage profile)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.mediaPage = new MediaPage();
            this.profile = profile;
            InitializeComponent();

            listView.FlowColumnTemplate = new DataTemplate(MediaImageViewDataTemplate);

            BindingContextChanged += (sender, e) =>
            {
                UserModel u = (UserModel)BindingContext;
                if (u != null)
                {
                    if (string.IsNullOrEmpty(u.address))
                    {
                        forAddress.IsVisible = false;
                    }
                    else
                    {
                        forAddress.IsVisible = true;
                    }

                    if (string.IsNullOrEmpty(u.gender))
                    {
                        forGender.IsVisible = false;
                    }
                    else
                    {
                        forGender.IsVisible = true;
                    }
                    if (string.IsNullOrEmpty(u.phone_number))
                    {
                        forPhone.IsVisible = false;
                    }
                    else
                    {
                        forPhone.IsVisible = true;
                    }
                    if (string.IsNullOrEmpty(u.birth_date))
                    {
                        forBirthdate.IsVisible = false;
                    }
                    else
                    {
                        forBirthdate.IsVisible = true;
                    }

                    if (string.IsNullOrEmpty(u.address) && string.IsNullOrEmpty(u.gender) && string.IsNullOrEmpty(u.phone_number) && string.IsNullOrEmpty(u.birth_date))
                    {
                        forAbout.IsVisible = false;
                        forAbout2.IsVisible = false;
                        aboutLabel.IsVisible = false;
                    }
                    else
                    {
                        forAbout.IsVisible = true;
                        forAbout2.IsVisible = true;
                        aboutLabel.IsVisible = true;
                    }
                }
            };
        }

        MediaImageView miv;
        MediaImageView MediaImageViewDataTemplate()
        {
            miv = new MediaImageView(mediaPage, profile, true);
            return miv;
        }

        void AddImage_Tapped(object sender, System.EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.Navigation.PushAsync(new AddGroupMediaPage(profile), false);
            });

        }

        void SeePhotos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MediaPage(true, User.id), false);
        }


        public void HideOrUnhide(bool hasPrayers)
        {
            if (hasPrayers)
            {
                PrayersEmpty.IsVisible = false;
            }
            else
            {
                PrayersEmpty.IsVisible = true;
            }
            loader.IsVisible = false;
        }

        void Item_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Navigation.PushModalAsync(new NavigationPage(new MediaImagePage((MediaModel)e.Item, true)), false);
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    var temp_messages = JsonConvert.DeserializeObject<ObservableCollection<MediaModel>>(jsonData["media"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                    if (temp_messages.Count == 0)
                    {
                        mediaEmpty.IsVisible = true;
                        mediaContainer.IsVisible = false;
                    }
                    else
                    {
                        mediaEmpty.IsVisible = false;
                        mediaContainer.IsVisible = true;
                        if (int.Parse(jsonData["count"].ToString()) < 4)
                            see_photos_button.IsVisible = false;
                        listView.FlowItemsSource = temp_messages;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public static readonly BindableProperty UserProperty =
            BindableProperty.Create(nameof(User), typeof(UserModel), typeof(ProfilePageHeaderView), new UserModel(), propertyChanged: async (bindable, oldValue, newValue) =>
            {
                var view = bindable as ProfilePageHeaderView;
                view.BindingContext = view.User;
                if (view.User != null)
                {
                    if (view.User.id == DataClass.GetInstance.user.id)
                    {
                        view.addImageIcon.IsVisible = true;
                    }

                    await Task.Run(async () =>
                    {
                        try
                        {
                            view.cts = new CancellationTokenSource();
                            await view.advanceRest.GetRequest(Constants.GET_USER_MEDIA(view.User.id, "true"), view.cts.Token, 0);
                        }
                        catch (OperationCanceledException oce)
                        {
                            view.ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            view.ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            view.ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                view.cts = null;
                            });
                        }
                    });
                }
            });

        public UserModel User
        {
            get { return (UserModel)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }
    }
}
