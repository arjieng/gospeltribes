﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class StudyModuleHeader : ContentView
    {
        bool frmShowMore = false;

        public StudyModuleHeader()
        {
            InitializeComponent();
            BindingContextChanged += (sender, e) =>
            {
                frmShowMore = false;
                var binding = BindingContext as StudyModules;
                verse.HeightRequest = -1;
                if (string.IsNullOrEmpty(binding.question))
                {
                    nothingHere.IsVisible = true;
                    toStudy.IsVisible = false;
                }
                else
                {
                    nothingHere.IsVisible = false;
                    toStudy.IsVisible = true;
                    if (string.IsNullOrEmpty(binding.scripture.bible_version))
                    {
                        script.IsVisible = false;
                        verseContainer.IsVisible = false;
                        showMore.IsVisible = false;
                    }
                    else
                    {
                        script.IsVisible = true;
                        verseContainer.IsVisible = true;
                        showMore.IsVisible = true;
                    }
                }
            };
        }

        void Verse_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Height")
            {
                if (verse.Height >= 150.ScaleHeight() && !frmShowMore)
                {
                    verse.HeightRequest = 150.ScaleHeight();
                    showMore.IsVisible = true;
                    if (showMore.Text == "SHOW LESS")
                    {
                        showMore.Text = "SHOW MORE";
                    }
                }

                if (!(verse.Height >= 150.ScaleHeight()) && !frmShowMore)
                {
                    showMore.IsVisible = false;
                    verse.HeightRequest = -1;
                }
            }
        }

        void ShowMore_Tapped(object sender, TappedEventArgs e)
        {
            frmShowMore = true;
            if (showMore.Text == "SHOW MORE")
            {
                showMore.Text = "SHOW LESS";
                verse.HeightRequest = -1;
            }
            else
            {
                showMore.Text = "SHOW MORE";
                verse.HeightRequest = 150.ScaleHeight();
            }
        }
    }
}
