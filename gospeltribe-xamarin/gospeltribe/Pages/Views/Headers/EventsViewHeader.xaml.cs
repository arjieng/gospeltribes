﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace gospeltribe
{
    public partial class EventsViewHeader : ContentView, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        public event EventHandler JoinClicked;
        CancellationTokenSource cts;
        Plugin.Geolocator.Abstractions.Position position;
        public Label jlabel;
        string address = "";
        public EventsViewHeader()
        {
            advanceRest = new AdvanceRestService
            {
                WebServiceDelegate = this
            };

            InitializeComponent();
            jlabel = joinLabel;

            BindingContextChanged += (sender, e) =>
            {
                EventsModel model = (EventsModel)BindingContext;

                if (model != null)
                {
                    cv.IsVisible = false;

                    if (string.IsNullOrEmpty(model.image) || model.image == Constants.orig)
                    {
                        image.IsVisible = false;
                    }

                    if(!string.IsNullOrEmpty(model.location)){
                        cv.IsVisible = true;
                        address = model.location;
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            var b = App.locationHelper.GetCurrentLocation();
                            await b;
                            if (b.IsCompleted)
                            {
                                position = b.Result;
                                if (position != null)
                                {
                                    Xamarin.Forms.GoogleMaps.Position mapPosition = new Xamarin.Forms.GoogleMaps.Position(position.Latitude, position.Longitude);
                                    map.MoveToRegion(MapSpan.FromCenterAndRadius(mapPosition, Distance.FromMiles(0.1)));
                                }
                            }

                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    string url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + model.location + "&key=AIzaSyBWV4ntfc6-Tg7-ud7G-Cg9a8btbqZLx18";
                                    await advanceRest.GetRequest(url, cts.Token, 0);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });
                        });
                    }
                    if(model.has_attended){
                        jlabel.Text = "JOINED";
                    }
                }
            };
        }

        void CopyText_Tapped(object sender, System.EventArgs e)
        {
            DependencyService.Get<IClipBoard>().SendTextToClipboard(details.FormattedText.ToString());
            Application.Current.MainPage?.DisplayAlert("Success", "You have copied the event details", "Okay");
        }

        void Map_Tapped(object sender, System.EventArgs e)
        {
            switch(Device.RuntimePlatform){
                case Device.Android:
                    var uri = new Uri($"http://maps.google.com/maps?daddr={address}&directionsmode=transit");
                    Device.OpenUri(uri);
                    break;
                case Device.iOS:
                    DependencyService.Get<IMapHeler>().OpenMap($"http://maps.apple.com/?daddr={address}");
                    break;
            }

        }

        void ShowAlerts(string title, string error){
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        void Join_Clicked(object sender, TappedEventArgs e)
        {
            JoinClicked(this, null);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("OK"))
                {
                    EventsModel model = (EventsModel)BindingContext;
                    double lat = (double)jsonData["results"][0]["geometry"]["location"]["lat"];
                    double lng = (double)jsonData["results"][0]["geometry"]["location"]["lng"];
                    map.Pins.Clear();
                    Xamarin.Forms.GoogleMaps.Position p = new Xamarin.Forms.GoogleMaps.Position(lat, lng);
                    map.Pins.Add(new Pin { Type = PinType.Place, Label = model.location, Position = p });
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(p, Distance.FromMiles(0.1)));
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
