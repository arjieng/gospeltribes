﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class PrayersViewHeader : ContentView
    {
        public PrayersViewHeader()
        {
            InitializeComponent();
            commentImage.Source = DataClass.GetInstance.user.avatar.url;

        }

        public event EventHandler SendComment;
        void Send_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(commentEditor.Text))
            {
                PrayersModel prayers = (PrayersModel)BindingContext;
                string newComment = JsonConvert.SerializeObject(new { comment = new { prayer_id = prayers.id, user_id = DataClass.GetInstance.user.id, comment = commentEditor.Text } });
                SendComment(newComment, null);
                commentEditor.Text = string.Empty;
                commentEditor.Unfocus();
            }
        }

        void CopyText_Tapped1(object sender, System.EventArgs e)
        {
            DependencyService.Get<IClipBoard>().SendTextToClipboard(details.FormattedText.ToString());
            Application.Current.MainPage?.DisplayAlert("Success", "You have copied the prayer details", "Okay");
        }

        void CopyText_Tapped2(object sender, System.EventArgs e)
        {
            DependencyService.Get<IClipBoard>().SendTextToClipboard(answered_details.FormattedText.ToString());
            Application.Current.MainPage?.DisplayAlert("Success", "You have copied the prayer answer details", "Okay");
        }

        public void HideOrUnhide(bool hasComments){
            loader.IsVisible = false;
            if(hasComments){
                CommentsEmpty.IsVisible = false;
            }else{
                CommentsEmpty.IsVisible = true;
            }
        }
    }
}
