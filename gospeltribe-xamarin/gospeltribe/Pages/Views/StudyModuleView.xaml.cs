﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class StudyModuleView : ContentView, IAdvanceRestConnector
    {
        ObservableCollection<InboxModel> inbox = new ObservableCollection<InboxModel>();
        CancellationTokenSource cts;
        AdvanceRestService advanceRest;
        StudyModules sm;
        int currentStudyModuleId;
        InboxPage ib;
        bool frmShowMore = false;

        public StudyModuleView(InboxPage ip)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.ib = ip;
            InitializeComponent();

            BindingContextChanged += (sender, e) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    frmShowMore = false;
                    var binding = BindingContext as StudyModules;
                    this.sm = binding;
                    verse.HeightRequest = -1;

                    if (binding.files == Constants.orig || string.IsNullOrEmpty(binding.files))
                    {
                        filelink.IsVisible = false;
                    }
                    else
                    {
                        filelink.IsVisible = true;
                    }

                    if (string.IsNullOrEmpty(binding.question))
                    {
                        if (DataClass.GetInstance.user.role == 1)
                        {
                            editStudyModuleContainer0.IsVisible = false;
                            editStudyModuleContainer1.IsVisible = false;
                            editStudyModuleContainer2.IsVisible = false;
                            editStudyModuleContainer3.IsVisible = false;
                        }
                        toStudy.IsVisible = false;
                    }
                    else
                    {
                        if (DataClass.GetInstance.user.role == 1)
                        {
                            editStudyModuleContainer0.IsVisible = true;
                            editStudyModuleContainer1.IsVisible = true;
                            editStudyModuleContainer2.IsVisible = true;
                            editStudyModuleContainer3.IsVisible = true;
                        }

                        toStudy.IsVisible = true;
                        if (binding.scripture == null)
                        {
                            script.IsVisible = false;
                            verseContainer.IsVisible = false;
                        }
                        else
                        {
                            script.IsVisible = true;
                            verseContainer.IsVisible = true;
                        }


                        if (string.IsNullOrEmpty(binding.link))
                        {
                            wv.IsVisible = false;
                            link.IsVisible = false;
                        }
                        else
                        {
                            wv.IsVisible = true;
                            link.IsVisible = true;
                            string[] s = binding.link.Split('/');
                            var c = new HtmlWebViewSource();
                            if (s[2].Contains("youtube"))
                            {
                                wv.youtubeID = binding.yid;
                            }

                            if (s[2] == "youtu.be")
                            {
                                wv.youtubeID = binding.yid;
                            }

                        }
                    }
                });
            };
            commentAvatar.Source = DataClass.GetInstance.user.avatar.url;
        }

        public void ChangeChatPreview(string data, string ct)
        {
            inbox.First(x => x.chatroom_type.ToString().Equals(ct)).last_message = data;
        }

        public void ChangeChatPreview(string data)
        {
            JObject obj = JObject.Parse(data);
            var datadata = obj["data"];

            inbox.First(x => x.chatroom_type.ToString().Equals(datadata["chatroom_type"].ToString())).last_message = datadata["message"].ToString();
        }

        void CopyText_Tapped(object sender, System.EventArgs e)
        {
            DependencyService.Get<IClipBoard>().SendTextToClipboard(question.FormattedText.ToString());
            Application.Current.MainPage?.DisplayAlert("Success", "You have copied the study details", "Okay");
        }

        void Edit_Study_Tapped(object sender, System.EventArgs e)
        {
            Application.Current.MainPage?.Navigation.PushAsync(new CreateScriptureStudyPage(this.ib, this.sm), false);
        }

        async void Remove_Study_Tapped(object sender, System.EventArgs e)
        {
            var choice = await Application.Current.MainPage?.DisplayAlert("Remove study", "Do you want to remove study?", "Yes", "No");
            if(choice){
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.REMOVE_STUDY_MODULE + DataClass.GetInstance.GroupId, cts.Token, 10);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {

                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        bool isrefreshing;
        public async void LoadInbox(bool isrefreshing)
        {
            this.isrefreshing = isrefreshing;

            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_CHATROOMS(), cts.Token, 1);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    App.Log(ex.Message);
                    //ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }


        void Link_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Device.OpenUri(new Uri(link.Text));
            });
        }

        void FileLink_Clicked(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Device.OpenUri(new Uri(filelink.Text));
            });
        }

        public event EventHandler InboxTapped;
        public event EventHandler SendMessage;
        void Send_Clicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(commentEditor.Text))
            {
                string newComment = JsonConvert.SerializeObject(new { study_module_comment = new { study_module_id = currentStudyModuleId, user_id = DataClass.GetInstance.user.id, comment = commentEditor.Text } });

                SendMessage(newComment, null);
                commentEditor.Text = string.Empty;
                commentEditor.Unfocus();
            }
        }

        void Inbox_Tapped(object sender, System.EventArgs e)
        {
            Grid grid = (Grid)(((BoxView)sender).Parent);
            InboxModel inbox_context = (InboxModel)grid.BindingContext;

            InboxTapped(inbox_context, e);
        }

        void Inbox_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            InboxTapped(e.Item, e);
        }

        public event EventHandler SendError;

        void ShowMore_Tapped(object sender, TappedEventArgs e)
        {
            frmShowMore = true;
            if (showMore.Text == "SHOW MORE")
            {
                showMore.Text = "SHOW LESS";
                verse.HeightRequest = -1;
            }
            else
            {
                showMore.Text = "SHOW MORE";
                verse.HeightRequest = 150.ScaleHeight();
            }
        }

        void Verse_PropertyChanged1(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.PropertyName))
            {
                if (e.PropertyName.Equals("Height"))
                {
                    if (verse.Height >= 150.ScaleHeight() && !frmShowMore)
                    {
                        verse.HeightRequest = 150.ScaleHeight();
                        showMore.IsVisible = true;
                        if (showMore.Text == "SHOW LESS")
                        {
                            showMore.Text = "SHOW MORE";
                        }
                    }

                    if (!(verse.Height >= 150.ScaleHeight()) && !frmShowMore)
                    {
                        showMore.IsVisible = false;
                        verse.HeightRequest = -1;
                    }
                }
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            var temp = JsonConvert.DeserializeObject<ObservableCollection<InboxModel>>(jsonData["groups"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                            inbox.Clear();
                            listView.Children.Clear();
                            inbox = temp;
                            listView.ItemsSource = inbox;
                            await Task.Delay(1000);
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    await advanceRest.GetRequest(Constants.GET_STUDY_MODULE + DataClass.GetInstance.GroupId, cts.Token, 2);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    App.Log(ex.Message);
                                    //ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });

                            break;
                        case 2:
                            if (jsonData.ContainsKey("study_module"))
                            {

                                StudyModules ssm = JsonConvert.DeserializeObject<StudyModules>(jsonData["study_module"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                                currentStudyModuleId = ssm.id;
                                this.BindingContext = ssm;

                                ib.LoadComments(currentStudyModuleId);
                            }

                            break;
                        case 10:
                            if (DataClass.GetInstance.user.role == 1)
                            {
                                editStudyModuleContainer0.IsVisible = false;
                                editStudyModuleContainer1.IsVisible = false;
                                editStudyModuleContainer2.IsVisible = false;
                                editStudyModuleContainer3.IsVisible = false;
                            }
                            toStudy.IsVisible = false;
                            ib.RefreshPage();
                            break;

                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var data = new ObservableCollection<string>();
                data.Add(title); data.Add(error);
                SendError(data, null);
            });
        }
    }
}
