﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DLToolkit.Forms.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class EventsView : RootViewPage, IAdvanceRestConnector
    {

        CancellationTokenSource cts;
        ObservableCollection<UserModel> attendees = new ObservableCollection<UserModel>();
        bool isClicked = false;
        AdvanceRestService advanceRest;
        public EventsModel events;
        CalendarPage cp;
        public FlowListView alv;

        public EventsView(EventsModel events, CalendarPage cp)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.cp = cp;
            this.events = events;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Event";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(OpenMaster_Tapped);
            alv = listview;
            listview.Header = events;
            listview.FlowItemsSource = attendees;
            if (DataClass.GetInstance.user.role == 1)
            {
                this.RightIcon = "EditPrayer";
                this.RightIconTint = Color.Black;
                this.RightButtonCommand = new Command(() =>
                {
                    Navigation.PushAsync(new CreateEventPage(cp, this));
                });
            }

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        isClicked = true;
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GET_ATTENDEES + events.id, cts.Token, 3);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            isClicked = false;
                            cts = null;
                        });
                    }
                });
            });
        }

        async void Event_Refreshing(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    isClicked = true;
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_EVENT_INFOS + events.id, cts.Token, 4);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        isClicked = false;
                        cts = null;
                    });
                }
            });
        }

        private void OpenMaster_Tapped(object obj)
        {
            Navigation.PopModalAsync(false);
        }

        void Handle_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Navigation.PushAsync(new ProfilePage((UserModel)e.Item), false);
        }

        EventsViewHeader page;
        async void Join_Clicked(object sender, TappedEventArgs e)
        {
            this.page = (EventsViewHeader)sender;

            if (!isClicked)
            {
                switch (this.page.jlabel.Text)
                {
                    case "JOIN":
                        await Task.Run(async () =>
                        {
                            try
                            {
                                isClicked = true;
                                cts = new CancellationTokenSource();
                                await advanceRest.GetRequest(Constants.JOIN_EVENT(events.id), cts.Token, 1);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    isClicked = false;
                                    cts = null;
                                });
                            }
                        });
                        break;
                    case "JOINED":
                        await Task.Run(async () =>
                        {
                            try
                            {
                                isClicked = true;
                                cts = new CancellationTokenSource();
                                await advanceRest.GetRequest(Constants.UNJOIN_EVENT(events.id), cts.Token, 2);

                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    isClicked = false;
                                    cts = null;
                                });
                            }
                        });
                        break;
                }
                //isClicked = true;
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            attendees.Add(JsonConvert.DeserializeObject<UserModel>(jsonData["attendee"].ToString()));
                            cp.events[cp.events.IndexOf(events)].has_attended = true;
                            this.page.jlabel.Text = "JOINED";
                            break;
                        case 2:
                            if (bool.Parse(jsonData["removed"].ToString()))
                            {
                                attendees.Remove(attendees.Where(x => x.id == DataClass.GetInstance.user.id).FirstOrDefault());
                                cp.events[cp.events.IndexOf(events)].has_attended = false;
                                this.page.jlabel.Text = "JOIN";
                            }

                            break;
                        case 3:
                            attendees = JsonConvert.DeserializeObject<ObservableCollection<UserModel>>(jsonData["attendees"].ToString());
                            listview.FlowItemsSource = attendees;
                            break;
                        case 4:
                            listview.Header = JsonConvert.DeserializeObject<EventsModel>(jsonData["event"].ToString());
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    isClicked = true;
                                    cts = new CancellationTokenSource();
                                    await advanceRest.GetRequest(Constants.GET_ATTENDEES + events.id, cts.Token, 3);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        isClicked = false;
                                        cts = null;
                                    });
                                }
                            });
                            listview.IsRefreshing = false;
                            break;
                    }
                }
                isClicked = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }
    }
}
