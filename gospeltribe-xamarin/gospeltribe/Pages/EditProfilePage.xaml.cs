﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using DLToolkit.Forms.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FirebasePushNotification;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class EditProfilePage : RootViewPage, IAdvanceRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool fromSignup = false, isClicked = false;
        CancellationTokenSource cts;
        FlowListView listView;
        AdvanceRestService advanceRest;
        ProfilePage pp;
        IMedia _mediaPicker;
        string Group_name = "";
        bool group_is_clicked = false;

        public EditProfilePage(bool fromSignup = false, FlowListView listView = null, ProfilePage pp = null)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.pp = pp;
            this.listView = listView;
            this.fromSignup = fromSignup;

            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Edit Profile";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(OpenMaster_Tapped);
            this.BindingContext = dataClass.user;

            firstNameEntry.Text = dataClass.user.first_name;
            if (fromSignup)
            {
                notifications.IsVisible = false;
                accountForm.IsVisible = false;
                personalInfo.IsVisible = true;
            }

            if (!fromSignup)
            {
                inbox.IsToggled = pp.user.settings.inbox;
                prayer.IsToggled = pp.user.settings.prayer;
                event_notif.IsToggled = pp.user.settings.event_notif;
                story.IsToggled = pp.user.settings.story;
                member.IsToggled = pp.user.settings.member;
            }

            if (dataClass.user.role == 1)
            {

                groupCodeLabel.Text = dataClass.GroupCode;

                if (Application.Current.Properties.ContainsKey("group_models_list"))
                {
                    var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());
                    var this_model = group_models.Where(x => x.group_code.Equals(dataClass.GroupCode)).FirstOrDefault();
                    if (string.IsNullOrEmpty(this_model.group_name))
                    {
                        groupNameEntry.Placeholder = "Unnamed group";
                    }
                    else
                    {
                        Group_name = this_model.group_name;
                        groupNameEntry.Text = this_model.group_name;
                    }
                }
            }
            else
            {
                groupsContainer.IsVisible = false;
            }

        }

        void Gender_Changed(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            genderEntry.Text = (string)genderPicker.SelectedItem;
        }

        void Capitalize_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                CustomEntry customEntry = (CustomEntry)sender;
                customEntry.Text = (char.ToUpper(e.NewTextValue[0]) + e.NewTextValue.Substring(1));
            }
        }

        void Birth_DateSelected(object sender, Xamarin.Forms.DateChangedEventArgs e)
        {

            birthEntry.Text = e.NewDate.ToString("MMMM dd");
        }

        void Birthday_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                birthDatePicker.Focus();
            });
        }

        void Phone_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length > 13)
            {
                phoneEntry.Text = e.OldTextValue;
            }
            switch (e.NewTextValue.Length)
            {
                case 3:
                    int number;
                    if (int.TryParse(phoneEntry.Text, out number))
                    {
                        phoneEntry.Text = "(" + number + ")";
                    }
                    break;
                case 9:
                    int number2;
                    if (int.TryParse(phoneEntry.Text.Substring(5, 4), out number2))
                    {
                        phoneEntry.Text = e.OldTextValue + "-" + e.NewTextValue.Last();
                    }
                    break;
            }
            //(541) 754-3010
        }

        void Caret_Tapped(object sender, TappedEventArgs e)
        {
            StackLayout stack = (StackLayout)sender;
            Image img = (Image)stack.Children.Last();
            img.Rotation += 180;
            switch (stack.ClassId)
            {
                case "1":
                    if (personalInfo.IsVisible)
                    {
                        personalInfo.IsVisible = false;
                    }
                    else
                    {
                        personalInfo.IsVisible = true;
                    }
                    break;
                case "2":
                    if (accountInfo.IsVisible)
                    {
                        accountInfo.IsVisible = false;
                    }
                    else
                    {
                        accountInfo.IsVisible = true;
                    }
                    break;
                case "3":
                    if (notifs.IsVisible)
                    {
                        notifs.IsVisible = false;
                    }
                    else
                    {
                        notifs.IsVisible = true;
                    }
                    break;
                case "4":
                    if (groupSettings.IsVisible)
                    {
                        groupSettings.IsVisible = false;
                    }
                    else
                    {
                        groupSettings.IsVisible = true;
                    }
                    break;
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        async void GroupSettings_Clicked(object sender, EventArgs e)
        {
            if (!Group_name.Equals(groupNameEntry.Text))
            {
                if (!group_is_clicked)
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            group_is_clicked = true;
                            UserDialogs.Instance.ShowLoading("Saving Details");
                            cts = new CancellationTokenSource();
                            await advanceRest.GetRequest(Constants.RENAME_GROUP(groupNameEntry.Text), cts.Token, 4);

                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                UserDialogs.Instance.HideLoading();
                                group_is_clicked = false;
                                cts = null;
                            });
                        }
                    });
                }
            }
        }

        async void Image_Tapped(object sender, TappedEventArgs e)
        {

            new ImageCropper()
            {
                CropShape = ImageCropper.CropShapeType.Oval,
                Success = async (imageFile) =>
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            Stream stream = new FileStream(imageFile, FileMode.Open);
                            UserDialogs.Instance.ShowLoading("Saving Image");
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                loader.IsVisible = true;
                            });
                            cts = new CancellationTokenSource();
                            await advanceRest.MultiPartDataContentAsync(Constants.UPLOAD_USER_AVATAR, JsonConvert.SerializeObject(new { user = new { avatar = "" } }), cts.Token, stream, "user", 3);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }
            }.Show(this);
        }

        private async void OpenMaster_Tapped(object obj)
        {
            if (fromSignup)
            {
                var choice = await DisplayAlert("", "NOTE: Skipping this step will disable some features. Are you sure you want to skip this step?", "Yes", "No");
                if (choice)
                {
                    App.ShowMainPage();
                }
            }
            else
            {
                await Navigation.PopAsync(false);
            }

        }

        void Gender_Focused(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                genderPicker.Focus();
            });
        }

        async void Account_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                bool finishedNa = true;
                if (string.IsNullOrEmpty(usernameEntry.Text))
                {
                    DisplayError(usernameEntry, "Username is required");
                    finishedNa = false;
                }

                if (!string.IsNullOrEmpty(passwordEntry.Text) || !string.IsNullOrEmpty(confirmEntry.Text))
                {
                    if (passwordEntry.Text != confirmEntry.Text)
                    {
                        confirmEntry.Text = string.Empty;
                        DisplayError(passwordEntry, "Password does not match!");
                        finishedNa = false;
                    }
                }

                if (finishedNa)
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            isClicked = true;
                            string data = "";
                            cts = new CancellationTokenSource();
                            if (!string.IsNullOrEmpty(passwordEntry.Text) && !string.IsNullOrEmpty(confirmEntry.Text))
                            {
                                data = JsonConvert.SerializeObject(new { user = new { username = usernameEntry.Text, password = passwordEntry.Text } });
                            }
                            else
                            {
                                data = JsonConvert.SerializeObject(new { user = new { username = usernameEntry.Text } });
                            }
                            UserDialogs.Instance.ShowLoading("Saving Details");
                            await advanceRest.PostRequestAsync(Constants.EDIT_PROFILE_ACCOUNT, data, cts.Token, 2);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }
            }
        }

        async void Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                bool finishedNa = true;
                if (string.IsNullOrEmpty(genderEntry.Text))
                {
                    DisplayError(genderEntry, "Gender is required");
                    finishedNa = false;
                }

                if (string.IsNullOrEmpty(emailEntry.Text))
                {
                    DisplayError(emailEntry, "Email is required");
                    finishedNa = false;
                }

                if (finishedNa)
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            isClicked = true;
                            cts = new CancellationTokenSource();
                            UserDialogs.Instance.ShowLoading("Saving Details");

                            string data = JsonConvert.SerializeObject(new { user = new { id = dataClass.user.id, email = emailEntry.Text, phone_number = phoneEntry.Text, first_name = firstNameEntry.Text, last_name = lastNameEntry.Text, address = addressEntry.Text, gender = genderEntry.Text, city = cityEntry.Text, state = stateEntry.Text, zip = zipEntry.Text, birth_date = birthEntry.Text } });
                            await advanceRest.PostRequestAsync(Constants.EDIT_PROFILE_MAIN, data, cts.Token, 1);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }
            }
        }

        void DisplayError(Entry entry, string error)
        {
            entry.Text = String.Empty;
            entry.Placeholder = error;
            entry.PlaceholderColor = Color.Red;
        }

        async void NotifSave_Clicked(object sender, System.EventArgs e)
        {
            if (!isClicked)
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        isClicked = true;
                        cts = new CancellationTokenSource();
                        UserDialogs.Instance.ShowLoading("Saving Notification Settings");
                        string data = JsonConvert.SerializeObject(new { setting = new { user_id = dataClass.user.id, group_id = int.Parse(DataClass.GetInstance.GroupId), inbox = inbox.IsToggled, event_notif = event_notif.IsToggled, prayer = prayer.IsToggled, story = story.IsToggled, member = member.IsToggled } });
                        await advanceRest.PostRequestAsync(Constants.PUSH_NOTIF_SETTING_CHANGE, data, cts.Token, 5);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            isClicked = false;
                            UserDialogs.Instance.HideLoading();
                            cts = null;
                        });
                    }
                });
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {
                    switch (wsType)
                    {
                        case 1:
                            UserModel user = JsonConvert.DeserializeObject<UserModel>(jsonData["data"].ToString());
                            user.role = dataClass.user.role;
                            dataClass.user = user;
                            dataClass.user.email = user.email;
                            dataClass.user.first_name = user.first_name;
                            dataClass.user.last_name = user.last_name;
                            dataClass.user.full_name = user.first_name + " " + user.last_name;
                            dataClass.user.address = user.address;
                            dataClass.user.birth_date = user.birth_date;
                            dataClass.user.phone_number = user.phone_number;
                            dataClass.user.gender = user.gender;
                            dataClass.user.city = user.city;
                            dataClass.user.state = user.state;
                            dataClass.user.zip = user.zip;
                            dataClass.uid = user.email;

                            CrossFirebasePushNotification.Current.UnsubscribeAll();
                            // 0 - group_id, 1 - phone_type, 2 - gender
                            // 0 - group_id, 1 - user_id

                            if (!string.IsNullOrEmpty(user.gender))
                            {
                                CrossFirebasePushNotification.Current.Subscribe(string.Format("group_{0}_{1}_{2}", DataClass.GetInstance.GroupId, Device.RuntimePlatform.Equals(Device.iOS) ? "iOS" : "Android", user.gender));
                            }
                            CrossFirebasePushNotification.Current.Subscribe(new string[] { string.Format("group_{0}_{1}", DataClass.GetInstance.GroupId, DataClass.GetInstance.user.id), string.Format("group_{0}_{1}_{2}", DataClass.GetInstance.GroupId, Device.RuntimePlatform.Equals(Device.iOS) ? "iOS" : "Android", "All") });

                            UserDialogs.Instance.HideLoading();
                            if (fromSignup)
                            {
                                App.ShowMainPage();
                            }
                            else
                            {
                                DisplayAlert("Updated", "Information Successfully Saved", "Okay");
                            }
                            break;
                        case 2:
                            dataClass.user.username = jsonData["user"]["username"].ToString();
                            UserDialogs.Instance.HideLoading();
                            DisplayAlert("Updated", "Account Successfully Saved", "Okay");
                            break;
                        case 3:
                            UserModel nuser = JsonConvert.DeserializeObject<UserModel>(jsonData["user"].ToString());
                            nuser.role = dataClass.user.role;

                            dataClass.user = nuser;
                            loader.IsVisible = false;
                            dataClass.user.email = nuser.email;
                            dataClass.user.avatar = nuser.avatar;
                            dataClass.user.first_name = nuser.first_name;
                            dataClass.user.last_name = nuser.last_name;
                            dataClass.user.full_name = nuser.first_name + " " + nuser.last_name;
                            dataClass.user.address = nuser.address;
                            dataClass.user.birth_date = nuser.birth_date;
                            dataClass.user.phone_number = nuser.phone_number;
                            dataClass.user.gender = nuser.gender;
                            dataClass.user.city = nuser.city;
                            dataClass.user.state = nuser.state;
                            dataClass.user.zip = nuser.zip;
                            dataClass.uid = nuser.email;
                            dataClass.user.avatar = nuser.avatar;
                            if (listView != null)
                            {
                                listView.Header = nuser;
                            }

                            profileImage.Source = nuser.avatar.url;
                            UserDialogs.Instance.HideLoading();
                            DisplayAlert("Updated", "Image Successfully Saved", "Okay");
                            break;
                        case 4:
                            var new_group_info = JsonConvert.DeserializeObject<GroupModel>(jsonData["group"].ToString());

                            var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());
                            var this_model = group_models.Where(x => x.id == new_group_info.id).FirstOrDefault();
                            group_models[group_models.IndexOf(this_model)] = new_group_info;

                            Application.Current.Properties["group_models_list"] = JsonConvert.SerializeObject(group_models);
                            Application.Current.SavePropertiesAsync();
                            pp.RefreshGroups();
                            UserDialogs.Instance.HideLoading();
                            DisplayAlert("Updated", "Group Successfully Saved", "Okay");
                            break;
                        case 5:
                            pp.user.settings.event_notif = event_notif.IsToggled;
                            pp.user.settings.inbox = inbox.IsToggled;
                            pp.user.settings.member = member.IsToggled;
                            pp.user.settings.prayer = prayer.IsToggled;
                            pp.user.settings.story = story.IsToggled;
                            UserDialogs.Instance.HideLoading();
                            DisplayAlert("Updated", "Notification Settings Successfully Saved", "Okay");
                            break;
                    }
                }

                if (jsonData["status"].ToString() == "404")
                {
                    DisplayAlert("Error", jsonData["error"]["message"].ToString(), "Okay");
                }
            });
            group_is_clicked = false;
            isClicked = false;
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                isClicked = false;
                group_is_clicked = false;
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
