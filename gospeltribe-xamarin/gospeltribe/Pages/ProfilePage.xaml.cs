﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ProfilePage : RootViewPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        public UserModel user;
        ObservableCollection<PrayersModel> prayerArchive = new ObservableCollection<PrayersModel>();
        ProfilePageHeaderView pphv;


        public ProfilePage(UserModel user)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.user = user;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();

            listView.HeaderTemplate = new DataTemplate(NewProfilePageHeaderView);
            listView.Header = user;
            listView.FlowItemsSource = prayerArchive;


            this.NavBackgroundColor = Color.FromHex("#00feff");
            this.PageTitle = "Profile";
            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            if (user.id == DataClass.GetInstance.user.id)
            {
                this.RightIcon = "Edit";
                this.RightIconTint = Color.Black;
                this.RightButtonCommand = new Command(EditProfile_Clicked);

                var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());

                foreach(var models in group_models){
                    BoxView line = new BoxView { BackgroundColor = Color.FromHex("#9aa2af"), HeightRequest = 1.ScaleHeight(), HorizontalOptions = LayoutOptions.FillAndExpand };
                    groupsContainer.Children.Insert(0, line);

                    StackLayout group = new StackLayout { HeightRequest = 30.ScaleHeight(), HorizontalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.White };
                    if(models.group_code == DataClass.GetInstance.GroupCode){
                        group.BackgroundColor = Color.FromRgb(255, 255, 0);
                    }

                    Label groupName = new Label { Text = string.IsNullOrEmpty(models.group_name) ? models.group_code : models.group_name, FontFamily = Constants.GOTHAM_LIGHT, TextColor = Color.Black, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };
                    group.Children.Add(groupName);

                    var tapGestureRecognizer = new TapGestureRecognizer();
                    tapGestureRecognizer.Tapped += async (sender, e) =>
                    {
                        if (!models.group_code.Equals(DataClass.GetInstance.GroupCode))
                        {
                            var choice = await DisplayAlert("Change Group", "Are you sure you want to change group?", "Yes", "No");
                            if (choice)
                            {
                                await Task.Run(async () =>
                                {
                                    try
                                    {
                                        cts = new CancellationTokenSource();
                                        await advanceRest.GetRequest(Constants.ADD_GROUP + models.group_code, cts.Token, 4);
                                    }
                                    catch (OperationCanceledException oce)
                                    {
                                        ShowAlerts("Task Cancelled", oce.Message);
                                    }
                                    catch (TimeoutException te)
                                    {
                                        ShowAlerts("Request Timeout", te.Message);
                                    }
                                    catch (Exception ex)
                                    {
                                        ShowAlerts("Error", ex.Message);
                                    }
                                    finally
                                    {
                                        Device.BeginInvokeOnMainThread(() =>
                                        {
                                            cts = null;
                                        });
                                    }
                                });
                            }
                        }
                    };
                    group.GestureRecognizers.Add(tapGestureRecognizer);

                    groupsContainer.Children.Insert(0, group);
                }
            }

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(OpenMaster_Tapped);

            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.GET_USER_PRAYERS(user.id), cts.Token, 1);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });

            });

        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void RefreshGroups(){
            groupsContainer.Children.Clear();
            var group_models = JsonConvert.DeserializeObject<ObservableCollection<GroupModel>>(Application.Current.Properties["group_models_list"].ToString());

            foreach (var models in group_models)
            {
                BoxView line = new BoxView { BackgroundColor = Color.FromHex("#9aa2af"), HeightRequest = 1.ScaleHeight(), HorizontalOptions = LayoutOptions.FillAndExpand };
                groupsContainer.Children.Insert(0, line);
                StackLayout group = new StackLayout { HeightRequest = 30.ScaleHeight(), HorizontalOptions = LayoutOptions.FillAndExpand, BackgroundColor = Color.White };
                if (models.group_code == DataClass.GetInstance.GroupCode)
                {
                    group.BackgroundColor = Color.FromRgb(255, 255, 0);
                }
                Label groupName = new Label { Text = string.IsNullOrEmpty(models.group_name) ? models.group_code : models.group_name, FontFamily = Constants.GOTHAM_LIGHT, TextColor = Color.Black, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };
                group.Children.Add(groupName);


                var tapGestureRecognizer = new TapGestureRecognizer();
                tapGestureRecognizer.Tapped += async (sender, e) =>
                {
                    if (!models.group_code.Equals(DataClass.GetInstance.GroupCode))
                    {
                        var choice = await DisplayAlert("Change Group", "Are you sure you want to change group?", "Yes", "No");
                        if (choice)
                        {
                            await Task.Run(async () =>
                            {
                                try
                                {
                                    cts = new CancellationTokenSource();
                                    await advanceRest.GetRequest(Constants.ADD_GROUP + models.group_code, cts.Token, 4);
                                }
                                catch (OperationCanceledException oce)
                                {
                                    ShowAlerts("Task Cancelled", oce.Message);
                                }
                                catch (TimeoutException te)
                                {
                                    ShowAlerts("Request Timeout", te.Message);
                                }
                                catch (Exception ex)
                                {
                                    ShowAlerts("Error", ex.Message);
                                }
                                finally
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        cts = null;
                                    });
                                }
                            });
                        }
                    }

                };
                group.GestureRecognizers.Add(tapGestureRecognizer);

                groupsContainer.Children.Insert(0, group);
            }
        }

        void Handle_TouchAction(object sender, gospeltribe.TouchActionEventArgs args)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if(args.Type == TouchActionType.Pressed){
                    outerSpace.IsVisible = false;
                    dm.IsVisible = false;
                }
            });
        }

        ProfilePageHeaderView NewProfilePageHeaderView(){
            pphv = new ProfilePageHeaderView(this) { VerticalOptions = LayoutOptions.FillAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand, User = this.user };
            pphv.Tapped += Pphv_Tapped;

            return pphv;
        }

        public event Action<UserModel> CreateContact;
        void Pphv_Tapped(UserModel user)
        {
            CreateContact(user);
        }


        async void Hide_Tapped(object sender, System.EventArgs e)
        {
            var choice = await DisplayAlert("Hide Prayer", "Arey you sure you want to hide this prayer?", "Yes", "No");
            if (choice)
            {
                PrayersModel prayer = (PrayersModel)sender;

                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        await advanceRest.GetRequest(Constants.HIDE_PRAYER(prayer.id), cts.Token, 3);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
        }

        void AddGroup_Tapped(object sender, EventArgs e)
        {
            Navigation.PushPopupAsync(new MyPage());
        }

        public async void ListView_Refreshing(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_USER_INFO(user.id), cts.Token, 2);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });

            //this.user = DataClass.GetInstance.user;
            //listView.Header = DataClass.GetInstance.user;
            //listView.IsRefreshing = false;
        }

        async void Logout_Tapped(object sender, EventArgs e)
        {
            var choice = await DisplayAlert("Logout", "Are you sure you want to sign out?", "Yes", "No");
            if (choice)
            {
                DataClass.GetInstance.user = null;
                DataClass.GetInstance.clientId = "";
                DataClass.GetInstance.uid = "";
                DataClass.GetInstance.token = "";

                //Application.Current.Properties.Remove("group_codes_list");
                //await Application.Current.SavePropertiesAsync();
                App.Logout();
            }
        }

        void Logout_Clicked(object obj)
        {

            if (dm.IsVisible)
            {
                outerSpace.IsVisible = false;
                dm.IsVisible = false;
            }
            else
            {
                outerSpace.IsVisible = true;
                dm.IsVisible = true;
            }
        }

        void EditProfile_Clicked(object sender)
        {
            outerSpace.IsVisible = false;
            dm.IsVisible = false;
            Navigation.PushAsync(new EditProfilePage(false, listView, this), false);
        }

        private void OpenMaster_Tapped(object obj)
        {
            Navigation.PopAsync(false);
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
           {
               if (jsonData["status"].ToString() == "200")
               {
                   switch (wsType)
                   {
                       case 1:
                           var prayers = JsonConvert.DeserializeObject<ObservableCollection<PrayersModel>>(jsonData["prayers"].ToString());
                           foreach (var prayer in prayers)
                           {
                               prayerArchive.Add(prayer);
                           }
                           if (prayers.Count == 0)
                           {
                               pphv.HideOrUnhide(false);
                           }
                           else
                           {
                               pphv.HideOrUnhide(true);
                           }
                           break;
                       case 2:
                           UserModel newuser = JsonConvert.DeserializeObject<UserModel>(jsonData["data"].ToString());
                           listView.Header = newuser;
                           var members = MembersPage.members;
                           int index = members.IndexOf(members.Where(x => x.id == newuser.id).FirstOrDefault());
                           members[index] = newuser;
                           this.user = newuser;
                           listView.IsRefreshing = false;

                           break;

                       case 3:
                           PrayersModel returnedPrayer = JsonConvert.DeserializeObject<PrayersModel>(jsonData["prayer"].ToString());
                           prayerArchive.Remove(prayerArchive.Where(x => x.id == returnedPrayer.id).FirstOrDefault());
                           break;
                       case 4:
                           var u = DataClass.GetInstance.user;
                           u.role = int.Parse(jsonData["group"]["role"].ToString());
                           DataClass.GetInstance.user = u;
                           DataClass.GetInstance.GroupCode = jsonData["group"]["group_code"].ToString();
                           DataClass.GetInstance.GroupId = jsonData["group"]["id"].ToString();
                           App.ShowMainPage();
                           break;
                   }
               }
           });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
            });
        }
    }
}
