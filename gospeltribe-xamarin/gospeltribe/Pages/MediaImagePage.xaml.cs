﻿using System;
using System.Collections.Generic;
using FormsVideoLibrary;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class MediaImagePage : RootViewPage
    {
        MediaModel media;
        public MediaImagePage(MediaModel media, bool isFromProfile = false)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            this.media = media;
            BindingContext = media;

            if (media.is_video)
            {
                videoPlayer.Source = new FileVideoSource() { File = media.video };
                videoPlayer.IsVisible = true;
                thumb.IsVisible = false;
            }
            else
            {
                videoPlayer.IsVisible = false;
                thumb.IsVisible = true;
            }

            if (isFromProfile){
                profile.IsVisible = false;
            }
        }

        void CopyText_Tapped(object sender, System.EventArgs e)
        {

            DependencyService.Get<IClipBoard>().SendTextToClipboard(description.FormattedText.ToString());
            Application.Current.MainPage?.DisplayAlert("Success", "You have copied the media caption", "Okay");
        }

        void CloseButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        void Profile_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage(media.user), false);
        }
    }
}
