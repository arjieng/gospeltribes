﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.FirebasePushNotification;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class HomePage : BottomTabbedPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        public static HomePage hp;

        public HomePage()
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();
            hp = this;

            First_Open();
            CurrentPageChanged += HomePage_CurrentPageChanged;

            if (Device.RuntimePlatform.Equals(Device.Android))
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    MessagingCenter.Subscribe<JObject>(this, "Badges", BadgesMessagingCallback);
                    MessagingCenter.Subscribe<JObject>(this, "RemoveBadges", RemoveBadgesMessagingCallback);
                });
            }
        }

        void SetupNotification()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.BadgeCounter = (App.InboxBadgeCounter + App.PrayersBadgeCounter + App.EventsBadgeCounter + App.StoryBadgeCounter + App.MembersBadgeCounter);
                if (Device.RuntimePlatform.Equals(Device.iOS))
                {
                    HomePage.hp.BadgeText1 = App.InboxBadgeCounter.ToString();
                    HomePage.hp.BadgeText2 = App.PrayersBadgeCounter.ToString();
                    HomePage.hp.BadgeText3 = App.EventsBadgeCounter.ToString();
                    HomePage.hp.BadgeText4 = App.StoryBadgeCounter.ToString();
                    HomePage.hp.BadgeText5 = App.MembersBadgeCounter.ToString();
                    JObject jObject4 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 0, BadgeCount = 1 }));
                    MessagingCenter.Send<JObject>(jObject4, "Badges");
                    DependencyService.Get<iAppIcon>().ChangeAppIcon();
                }
                else
                {
                    BindingContext = new
                    {
                        aa1 = (App.InboxBadgeCounter.Equals(0) ? string.Empty : App.InboxBadgeCounter.ToString()),
                        aa2 = (App.PrayersBadgeCounter.Equals(0) ? string.Empty : App.PrayersBadgeCounter.ToString()),
                        aa3 = (App.EventsBadgeCounter.Equals(0) ? string.Empty : App.EventsBadgeCounter.ToString()),
                        aa4 = (App.StoryBadgeCounter.Equals(0) ? string.Empty : App.StoryBadgeCounter.ToString()),
                        aa5 = (App.MembersBadgeCounter.Equals(0) ? string.Empty : App.MembersBadgeCounter.ToString())
                    };
                }
            });
        }

        void RemoveBadgesMessagingCallback(JObject obj)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                int aa1 = App.InboxBadgeCounter, aa2 = App.PrayersBadgeCounter, aa3 = App.EventsBadgeCounter, aa4 = App.StoryBadgeCounter, aa5 = App.MembersBadgeCounter;

                switch (int.Parse(obj["BadgeView"].ToString()))
                {
                    case 0:
                        aa1 = 0;
                        break;
                    case 1:
                        aa2 = 0;
                        break;
                    case 2:
                        aa3 = 0;
                        break;
                    case 3:
                        aa4 = 0;
                        break;
                    case 4:
                        aa5 = 0;
                        break;
                }

                BindingContext = new { aa1 = (aa1.Equals(0) ? string.Empty : aa1.ToString()), aa2 = (aa2.Equals(0) ? string.Empty : aa2.ToString()), aa3 = (aa3.Equals(0) ? string.Empty : aa3.ToString()), aa4 = (aa4.Equals(0) ? string.Empty : aa4.ToString()), aa5 = (aa5.Equals(0) ? string.Empty : aa5.ToString()) };
            });
        }

        void BadgesMessagingCallback(JObject obj)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                int aa1 = 0, aa2 = 0, aa3 = 0, aa4 = 0, aa5 = 0;

                if (!string.IsNullOrEmpty(TabBadge.GetBadgeText(Inbox)))
                {
                    aa1 = int.Parse(TabBadge.GetBadgeText(Inbox));
                }

                if (!string.IsNullOrEmpty(TabBadge.GetBadgeText(Prayer)))
                {
                    aa2 = int.Parse(TabBadge.GetBadgeText(Prayer));
                }

                if (!string.IsNullOrEmpty(TabBadge.GetBadgeText(Calendar)))
                {
                    aa3 = int.Parse(TabBadge.GetBadgeText(Calendar));
                }

                if (!string.IsNullOrEmpty(TabBadge.GetBadgeText(Media)))
                {
                    aa4 = int.Parse(TabBadge.GetBadgeText(Media));
                }

                if (!string.IsNullOrEmpty(TabBadge.GetBadgeText(Member)))
                {
                    aa5 = int.Parse(TabBadge.GetBadgeText(Member));
                }

                switch (int.Parse(obj["BadgeView"].ToString()))
                {
                    case 0:
                        aa1 += 1;
                        break;
                    case 1:
                        aa2 += 1;
                        break;
                    case 2:
                        aa3 += 1;
                        break;
                    case 3:
                        aa4 += 1;
                        break;
                    case 4:
                        aa5 += 1;
                        break;
                }

                BindingContext = new { aa1 = (aa1.Equals(0) ? string.Empty : aa1.ToString()), aa2 = (aa2.Equals(0) ? string.Empty : aa2.ToString()), aa3 = (aa3.Equals(0) ? string.Empty : aa3.ToString()), aa4 = (aa4.Equals(0) ? string.Empty : aa4.ToString()), aa5 = (aa5.Equals(0) ? string.Empty : aa5.ToString()) };
            });
        }

        async void First_Open()
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.GET_BADGES + "?user_id=" + DataClass.GetInstance.user.id +
                                                        "&group_id=" + DataClass.GetInstance.GroupId, cts.Token, 1);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    cts = null;
                }
            });
        }

        void HomePage_CurrentPageChanged(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (CurrentPage != null)
                {
                    string tab = "";
                    bool is_editable = false;
                    switch (CurrentPage.ClassId)
                    {
                        case "Inbox":
                            tab = "inbox";
                            if (!App.InboxBadgeCounter.Equals(0))
                                is_editable = true;
                            App.BadgeCounter = App.BadgeCounter - App.InboxBadgeCounter;
                            App.InboxBadgeCounter = 0;
                            switch (Device.RuntimePlatform)
                            {
                                case Device.iOS:
                                    hp.BadgeText1 = string.Empty;
                                    break;
                                case Device.Android:
                                    JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 0 }));
                                    MessagingCenter.Send<JObject>(jObject1, "RemoveBadges");
                                    break;
                            }
                            break;
                        case "Prayer":
                            tab = "prayer";
                            if (!App.PrayersBadgeCounter.Equals(0))
                                is_editable = true;
                            App.BadgeCounter = App.BadgeCounter - App.PrayersBadgeCounter;
                            App.PrayersBadgeCounter = 0;
                            switch (Device.RuntimePlatform)
                            {
                                case Device.iOS:
                                    hp.BadgeText2 = string.Empty;
                                    break;
                                case Device.Android:
                                    JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 1 }));
                                    MessagingCenter.Send<JObject>(jObject1, "RemoveBadges");
                                    break;
                            }
                            break;
                        case "Calendar":
                            tab = "event";
                            if (!App.EventsBadgeCounter.Equals(0))
                                is_editable = true;
                            App.BadgeCounter = App.BadgeCounter - App.EventsBadgeCounter;
                            App.EventsBadgeCounter = 0;
                            switch (Device.RuntimePlatform)
                            {
                                case Device.iOS:
                                    hp.BadgeText3 = string.Empty;
                                    break;
                                case Device.Android:
                                    JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 2 }));
                                    MessagingCenter.Send<JObject>(jObject1, "RemoveBadges");
                                    break;
                            }
                            break;
                        case "Media":
                            tab = "story";
                            if (!App.StoryBadgeCounter.Equals(0))
                                is_editable = true;
                            App.BadgeCounter = App.BadgeCounter - App.StoryBadgeCounter;
                            App.StoryBadgeCounter = 0;
                            switch (Device.RuntimePlatform)
                            {
                                case Device.iOS:
                                    hp.BadgeText4 = string.Empty;
                                    break;
                                case Device.Android:
                                    JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 3 }));
                                    MessagingCenter.Send<JObject>(jObject1, "RemoveBadges");
                                    break;
                            }
                            break;
                        case "Member":
                            tab = "member";
                            if (!App.MembersBadgeCounter.Equals(0))
                                is_editable = true;
                            App.BadgeCounter = App.BadgeCounter - App.MembersBadgeCounter;
                            App.MembersBadgeCounter = 0;
                            switch (Device.RuntimePlatform)
                            {
                                case Device.iOS:
                                    hp.BadgeText5 = string.Empty;
                                    break;
                                case Device.Android:
                                    JObject jObject1 = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 4 }));
                                    MessagingCenter.Send<JObject>(jObject1, "RemoveBadges");
                                    break;
                            }
                            break;
                    }


                    if (Device.RuntimePlatform.Equals(Device.iOS))
                    {
                        JObject jObject = JObject.Parse(JsonConvert.SerializeObject(new { BadgeView = 0, BadgeCount = 1 }));
                        MessagingCenter.Send<JObject>(jObject, "Badges");
                    }

                    if (is_editable)
                        DependencyService.Get<iAppIcon>().ChangeAppIcon();

                    await Task.Run(async () =>
                    {
                        try
                        {
                            cts = new CancellationTokenSource();
                            AdvanceRestService restService = new AdvanceRestService { WebServiceDelegate = this };
                            await restService.GetRequest(Constants.SET_READ_BADGES + "?user_id=" + DataClass.GetInstance.user.id +
                                                                "&group_id=" + DataClass.GetInstance.GroupId + "&tab=" + tab, cts.Token, 2);
                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            cts = null;
                        }
                    });
                }
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        //public void

        public async void SaveLastRead(int cid, int cmid)
        {
            await Task.Run(async () =>
            {
                try
                {
                    cts = new CancellationTokenSource();
                    await advanceRest.GetRequest(Constants.SET_READ + "?chatroom_id=" + cid + "&chatroom_message_id=" + cmid, cts.Token, 0);
                }
                catch (OperationCanceledException oce)
                {
                    ShowAlerts("Task Cancelled", oce.Message);
                }
                catch (TimeoutException te)
                {
                    ShowAlerts("Request Timeout", te.Message);
                }
                catch (Exception ex)
                {
                    ShowAlerts("Error", ex.Message);
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        cts = null;
                    });
                }
            });
        }

        public void UpdateInboxes(string obj, string ct)
        {
            inboxPage.UpdateInboxes(obj, ct);
        }

        public void UpdateImages(JObject parameters)
        {
            storyPage.UpdateImages(parameters);
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (wsType.Equals(1))
                {
                    if (jsonData["status"].ToString().Equals("200"))
                    {
                        App.Log("THIS TRIGGERS: " + jsonData.ToString());
                        if (CurrentPage.Equals(this.Children[0]))
                        {
                            App.InboxBadgeCounter = 0;
                        }
                        else
                        {
                            App.InboxBadgeCounter = int.Parse(jsonData["badge"]["inbox"].ToString());
                        }

                        if (CurrentPage.Equals(this.Children[1]))
                        {
                            App.PrayersBadgeCounter = 0;
                        }
                        else
                        {
                            App.PrayersBadgeCounter = int.Parse(jsonData["badge"]["prayer"].ToString());
                        }

                        if (CurrentPage.Equals(this.Children[2]))
                        {
                            App.EventsBadgeCounter = 0;
                        }
                        else
                        {
                            App.EventsBadgeCounter = int.Parse(jsonData["badge"]["event"].ToString());
                        }

                        if (CurrentPage.Equals(this.Children[3]))
                        {
                            App.StoryBadgeCounter = 0;
                        }
                        else
                        {
                            App.StoryBadgeCounter = int.Parse(jsonData["badge"]["story"].ToString());
                        }

                        if (CurrentPage.Equals(this.Children[4]))
                        {
                            App.MembersBadgeCounter = 0;
                        }
                        else
                        {
                            App.MembersBadgeCounter = int.Parse(jsonData["badge"]["member"].ToString());
                        }




                        //App.BadgeCounter = App.InboxBadgeCounter + App.StoryBadgeCounter + App.EventsBadgeCounter + App.MembersBadgeCounter + App.PrayersBadgeCounter;
                        SetupNotification();

                        CrossFirebasePushNotification.Current.UnsubscribeAll();
                        // 0 - group_id, 1 - phone_type, 2 - gender
                        // 0 - group_id, 1 - user_id

                        if (!string.IsNullOrEmpty(DataClass.GetInstance.user.gender))
                        {
                            CrossFirebasePushNotification.Current.Subscribe(string.Format("group_{0}_{1}_{2}", DataClass.GetInstance.GroupId, Device.RuntimePlatform.Equals(Device.iOS) ? "iOS" : "Android", DataClass.GetInstance.user.gender));
                        }
                        CrossFirebasePushNotification.Current.Subscribe(new string[] { string.Format("group_{0}_{1}", DataClass.GetInstance.GroupId, DataClass.GetInstance.user.id), string.Format("group_{0}_{1}_{2}", DataClass.GetInstance.GroupId, Device.RuntimePlatform.Equals(Device.iOS) ? "iOS" : "Android", "All") });

                        App.Log(string.Join(",", CrossFirebasePushNotification.Current.SubscribedTopics));

                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }
    }
}