﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class OutgoingMessageViewCell : ViewCell, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        ConversationPage cp;
        MessagesModel messages;

        public OutgoingMessageViewCell(ConversationPage cp)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.cp = cp;
            InitializeComponent();
            BindingContextChanged += OutgoingMessageViewCell_BindingContextChanged;
            message_image.PropertyChanged += (sender, e) => {
                if(e.PropertyName.Equals("IsLoading")){
                    if(!messages.media.Equals(Constants.orig)){
                        if(!message_image.IsLoading){
                            message_image.HeightRequest = -1;
                        }
                    }
                }
            };
        }

        void Image_Tapped(object sender, System.EventArgs e)
        {
            var this_message = (MessagesModel)BindingContext;
            Application.Current.MainPage?.Navigation.PushModalAsync(new MediaImagePage(new MediaModel() { is_video = false, description = this_message.message, user = this_message.user, media = this_message.media }));
        }

        void OutgoingMessageViewCell_BindingContextChanged(object sender, EventArgs e)
        {
            profileImage.Source = null;
            senderName.Text = string.Empty;
            message.Text = string.Empty;
            message_image.Source = null;
            dateSent.Text = string.Empty;

            messages = (MessagesModel)BindingContext;

            if (messages != null)
            {

                profileImage.Source = messages.user.avatar.url;
                senderName.Text = messages.user.full_name;
                dateSent.Text = messages.date_sent;

                if (!string.IsNullOrEmpty(messages.message))
                {
                    message.IsVisible = true;
                    message.FormattedText = messages.message.ConvertToFormattedString();
                    //message.Text = messages.message;
                }
                else
                {
                    message.IsVisible = false;
                    message.Text = string.Empty;
                }

                if (messages.media.Equals(Constants.orig))
                {
                    message_image.IsVisible = false;
                    message_image.Source = null;
                }
                else
                {
                    message_image.HeightRequest = 220.ScaleHeight();
                    message_image.IsVisible = true;
                    message_image.Source = messages.media;
                }


                if (!messages.like_count.Equals(0))
                {
                    heartContainerContainer.IsVisible = true;
                    heartCount.Text = messages.like_count.ToString();
                }
                else
                {
                    heartContainerContainer.IsVisible = false;
                }
            }
            this.ForceUpdateSize();
        }

        void ShowAlerts(string title, string error){
            Device.BeginInvokeOnMainThread(async () =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        async void Message_OnLongTapEvent(object obj)
        {
            MessagesModel mm = (MessagesModel)BindingContext;
            if (DataClass.GetInstance.user.role.Equals(1))
            {
                string app = await Application.Current.MainPage?.DisplayActionSheet(mm.message, "Cancel", null, "Edit", "Delete", "Copy");
                switch (app)
                {
                    case "Delete":
                        await Task.Run(async () =>
                        {
                            try
                            {
                                cts = new CancellationTokenSource();
                                await advanceRest.GetRequest(Constants.REMOVE_MESSAGE + mm.id + "&group_id=" + int.Parse(DataClass.GetInstance.GroupId) + "&chatroom_id=" + cp.ee.id, cts.Token, 0);
                            }
                            catch (OperationCanceledException oce)
                            {
                                ShowAlerts("Task Cancelled", oce.Message);
                            }
                            catch (TimeoutException te)
                            {
                                ShowAlerts("Request Timeout", te.Message);
                            }
                            catch (Exception ex)
                            {
                                ShowAlerts("Error", ex.Message);
                            }
                            finally
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    cts = null;
                                });
                            }
                        });
                        break;
                    case "Edit":
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            cp.OpenEditMessagePage(messages);
                        });
                        break;

                    case "Copy":
                        DependencyService.Get<IClipBoard>().SendTextToClipboard(mm.message);
                        Application.Current.MainPage?.DisplayAlert("Success", "You have copied the message", "Okay");
                        break;
                }
            }
            else
            {
                string app = await Application.Current.MainPage?.DisplayActionSheet(mm.message, "Cancel", null, "Copy");
                switch (app)
                {
                    case "Copy":
                        DependencyService.Get<IClipBoard>().SendTextToClipboard(mm.message);
                        Application.Current.MainPage?.DisplayAlert("Success", "You have copied the message", "Okay");
                        break;
                }
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString() == "200")
                {

                }
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {

            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
