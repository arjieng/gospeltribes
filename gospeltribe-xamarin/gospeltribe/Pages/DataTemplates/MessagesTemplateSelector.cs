﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public class MessagesTemplateSelector : DataTemplateSelector
    {
        DataTemplate incomingDataTemplate;
        DataTemplate outgoingDataTemplate;
        DataClass dataClass = DataClass.GetInstance;
        ConversationPage cp;

        public MessagesTemplateSelector(ConversationPage cp)
        {
            this.cp = cp;
            this.incomingDataTemplate = new DataTemplate(incomingMessageViewCell);
            this.outgoingDataTemplate = new DataTemplate(outgoingMessageViewCell);
        }

        IncomingMessageViewCell incomingMessageViewCell()
        {
            return new IncomingMessageViewCell(cp);
        }

        OutgoingMessageViewCell outgoingMessageViewCell()
        {
            return new OutgoingMessageViewCell(cp);
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var messageVm = item as MessagesModel;
            if (messageVm == null)
                return null;

            return (messageVm.user.id == dataClass.user.id) ? outgoingDataTemplate : incomingDataTemplate;
        }
    }
}
