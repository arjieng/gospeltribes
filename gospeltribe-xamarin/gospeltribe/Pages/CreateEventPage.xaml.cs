﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.InputKit.Shared.Controls;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class CreateEventPage : RootViewPage, IAdvanceRestConnector
    {
        public DateTime? MyEndDate { get; set; }
        public DateTime? MyStartDate { get; set; }

        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        Stream imageStream;
        CalendarPage calendarPage = new CalendarPage();
        EventsView ev;
        bool isEdit = false, isClicked = false;
        ObservableCollection<string> colors, eventTypeSource, days;
        public CreateEventPage(CalendarPage calendarPage, EventsView ev = null)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;
            this.ev = ev;
            this.calendarPage = calendarPage;

            colors = new ObservableCollection<string> { "Red", "Orange", "Yellow", "Aqua", "Pink", "Blue", "Green", "Fuchsia", "Violet", "Brown" };
            eventTypeSource = new ObservableCollection<string> { "Monthly", "Weekly" };
            days = new ObservableCollection<string>();

            InitializeComponent();
            end_time.Time = new TimeSpan(23, 59, 0);


            highlight_color.ItemsSource = colors;
            highlight_color.SelectedItem = colors[0];
            highlight_color.SelectedIndex = 0;

            eventType.ItemsSource = eventTypeSource;
            eventType.SelectedItem = eventTypeSource[0];
            eventType.SelectedIndex = 0;

            this.BindingContext = this;
            NavigationPage.SetHasNavigationBar(this, false);
            this.NavBackgroundColor = Color.FromHex("#00feff");

            if (ev != null)
            {
                var em = ev.events;
                this.PageTitle = "Edit Event";
                this.isEdit = true;
                title.Text = em.subject;
                details.Text = em.details;
                image.Source = em.image;
                start_date.NullableDate = em.start_date;
                start_time.Time = new TimeSpan(em.start_date.Hour, em.start_date.Minute, em.start_date.Second);
                location.Text = em.location;

                if(em.start_date.TimeOfDay.Hours != em.end_date.TimeOfDay.Hours)
                //if (!em.start_date.Equals(em.end_date))
                {
                    addEndTime.IsChecked = true;
                    endTimeLabel.IsVisible = true;
                    endTimeContainer.IsVisible = true;
                    end_time.Time = new TimeSpan(em.end_date.Hour, em.end_date.Minute, em.end_date.Second);
                }

                if (!em.repeat.Equals("None"))
                {
                    addRepeat.IsChecked = true;
                    eventTypeContainer.IsVisible = true;
                    addEndDate.IsVisible = true;

                    if(em.start_date.Date != em.end_date.Date)
                    {
                        addEndDate.IsChecked = true;
                        endWhenFormsContainer.IsVisible = true;
                        end_date.NullableDate = em.end_date;
                    }

                    switch (em.repeat)
                    {
                        case "Weekly":
                            eventType.SelectedItem = eventTypeSource[1];
                            eventType.SelectedIndex = 1;
                            days = new ObservableCollection<string>(em.repeat_days.Split(','));
                            App.Log(JsonConvert.SerializeObject(days));
                            if (days.Contains("Sunday"))
                            {
                                sunday.IsChecked = true;
                            }
                            if (days.Contains("Monday"))
                            {
                                monday.IsChecked = true;
                            }
                            if (days.Contains("Tuesday"))
                            {
                                tuesday.IsChecked = true;
                            }
                            if (days.Contains("Wednesday"))
                            {
                                wednesday.IsChecked = true;
                            }
                            if (days.Contains("Thursday"))
                            {
                                thursday.IsChecked = true;
                            }
                            if (days.Contains("Friday"))
                            {
                                friday.IsChecked = true;
                            }
                            if (days.Contains("Saturday"))
                            {
                                saturday.IsChecked = true;
                            }


                            break;
                        case "Monthly":
                            eventType.SelectedItem = eventTypeSource[0];
                            eventType.SelectedIndex = 0;
                            break;
                    }
                }
            }
            else
            {
                this.PageTitle = "Create Event";
            }

            this.TitleFontFamily = Constants.GOTHAM_LIGHT;
            this.TitleFontColor = Color.Black;

            this.LeftIcon = "Back";
            this.LeftIconTint = Color.Black;
            this.LeftButtonCommand = new Command(() =>
            {
                Navigation.PopAsync(false);
            });

            bvdate.HeightRequest = start_date.HeightRequest + 20;
            bvdate.Margin = new Thickness(0, -start_date.HeightRequest - 20, 0, 0);
            bvdate2.HeightRequest = start_date.HeightRequest + 20;
            bvdate2.Margin = new Thickness(0, -start_date.HeightRequest - 20, 0, 0);

            bv.HeightRequest = location.HeightRequest + 20;
            bv.Margin = new Thickness(0, -location.HeightRequest - 20, 0, 0);
        }

        void Time_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("Time"))
            {
                if (addEndTime.IsChecked)
                {

                    //App.Log("START TIME: " + start_time.Time.Hours + start_time.Time.Minutes + 0);
                    //App.Log("END TIME: " + end_time.Time.Hours + end_time.Time.Minutes + 0);
                    //App.Log("COMPARE: " + new TimeSpan(start_time.Time.Hours, start_time.Time.Minutes, 0).CompareTo(new TimeSpan(end_time.Time.Hours, end_time.Time.Minutes, 0)).Equals(1));
                    if (new TimeSpan(start_time.Time.Hours, start_time.Time.Minutes, 0).CompareTo(new TimeSpan(end_time.Time.Hours, end_time.Time.Minutes, 0)).Equals(1) || new TimeSpan(start_time.Time.Hours, start_time.Time.Minutes, 0).CompareTo(new TimeSpan(end_time.Time.Hours, end_time.Time.Minutes, 0)).Equals(0))
                    {
                        end_time.Time = new TimeSpan(start_time.Time.Hours, start_time.Time.Minutes + 1, 0);
                    }
                }
            }
        }

        public void ChangeLocation(string address)
        {
            location.Text = address;
        }

        public void ChangeStartDate(int year, int month, int day)
        {
            start_date.NullableDate = new DateTime(year, month, day);
            if (addEndDate.IsChecked)
            {
                if (end_date.NullableDate.Value <= start_date.NullableDate.Value)
                {
                    end_date.NullableDate = start_date.NullableDate.Value.AddDays(1);
                }
            }

        }

        public void ChangeEndDate(int year, int month, int day)
        {
            end_date.NullableDate = new DateTime(year, month, day);
            if (addEndDate.IsChecked)
            {
                if (end_date.NullableDate.Value <= start_date.NullableDate.Value)
                {
                    end_date.NullableDate = start_date.NullableDate.Value.AddDays(1);
                }
            }
        }

        void StartDate_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new StartDatePickerPage(this), false);
        }

        void EndDate_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new StartDatePickerPage(this, 1), false);

        }

        void Location_Tapped(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new SearchPage(this), false);
        }

        void Image_Tapped(object sender, System.EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                new ImageCropper()
                {
                    Success = (imageFile) =>
                    {
                        SuccessCallback(imageFile);
                    }
                }.Show(this);
            });

        }

        void UploadCallback(long sent, long total)
        {
        }

        void SuccessCallback(string imageFile)
        {
            Stream stream = new FileStream(imageFile, FileMode.Open);
            imageStream = stream;
            addImageLabel.IsVisible = false;
            image.Source = ImageSource.FromFile(imageFile);
        }

        void Start_Date_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            start_date_container.BorderColor = Color.Gray;
        }

        void End_Date_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            end_date_container.BorderColor = Color.Gray;
        }

        async void Create_Event_Clicked(object sender, System.EventArgs e)
        {
            if (!isClicked)
            {
                bool isFinishedNa = true;
                if (string.IsNullOrEmpty(title.Text))
                {
                    title.Placeholder = "Title must not be empty";
                    title.PlaceholderColor = Color.Red;
                    isFinishedNa = false;
                }

                //if (string.IsNullOrEmpty(details.Text))
                //{
                //    details.Placeholder = "Details must not be empty";
                //    details.PlaceholderColor = Color.Red;
                //    isFinishedNa = false;
                //}

                if (!addRepeat.IsChecked)
                {
                    if (!start_date.NullableDate.HasValue)
                    {
                        start_date_container.BorderColor = Color.Red;
                        isFinishedNa = false;
                    }
                }
                else
                {
                    if (eventType.SelectedItem.ToString().Equals("Monthly"))
                    {
                        if (!start_date.NullableDate.HasValue)
                        {
                            start_date_container.BorderColor = Color.Red;
                            isFinishedNa = false;
                        }
                    }

                    if (eventType.SelectedItem.ToString().Equals("Weekly"))
                    {
                        if (!days.Any())
                        {
                            await DisplayAlert("Empty Field", "You must select at least one day", "OK");
                            isFinishedNa = false;
                        }
                    }
                }




                if (string.IsNullOrEmpty(location.Text))
                {
                    location.Placeholder = "Location must not be empty";
                    location.PlaceholderColor = Color.Red;
                    isFinishedNa = false;
                }

                if (highlight_color.SelectedIndex.Equals(-1))
                {
                    highlight_color_container.BorderColor = Color.Red;
                    isFinishedNa = false;
                }

                if (addEndDate.IsChecked)
                {
                    if (!end_date.NullableDate.HasValue)
                    {
                        end_date_container.BorderColor = Color.Red;
                        isFinishedNa = false;
                    }
                }

                if (isFinishedNa)
                {
                    await Task.Run(async () =>
                    {
                        try
                        {
                            UserDialogs.Instance.ShowLoading("Saving");
                            isClicked = true;
                            cts = new CancellationTokenSource();

                            string endDateVariable;
                            if (addEndTime.IsChecked)
                            {
                                if (addEndDate.IsChecked)
                                {
                                    endDateVariable = (end_date.NullableDate.HasValue ? end_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + end_time.Time.Hours + ":" + end_time.Time.Minutes + ":" + end_time.Time.Seconds + " UTC +00:00") : null);

                                }
                                else
                                {
                                    endDateVariable = (start_date.NullableDate.HasValue ? start_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + end_time.Time.Hours + ":" + end_time.Time.Minutes + ":" + end_time.Time.Seconds + " UTC +00:00") : null);
                                }
                            }
                            else
                            {
                                if (addEndDate.IsChecked)
                                {
                                    //endDateVariable = (end_date.NullableDate.HasValue ? end_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + end_time.Time.Hours + ":" + end_time.Time.Minutes + ":" + end_time.Time.Seconds + " UTC +00:00") : null);
                                    endDateVariable = (end_date.NullableDate.HasValue ? end_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + start_time.Time.Hours + ":" + start_time.Time.Minutes + ":" + start_time.Time.Seconds + " UTC +00:00") : null);
                                }
                                else
                                {
                                    endDateVariable = (start_date.NullableDate.HasValue ? start_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + start_time.Time.Hours + ":" + start_time.Time.Minutes + ":" + start_time.Time.Seconds + " UTC +00:00") : null);
                                }
                            }

                            string repeat = "None", repeatDays = "None";
                            if (addRepeat.IsChecked)
                            {
                                repeat = eventType.SelectedItem.ToString();
                                if (repeat.Equals("Weekly"))
                                {
                                    repeatDays = string.Join(",", days);
                                    if (addEndTime.IsChecked)
                                    {
                                        if (addEndDate.IsChecked)
                                        {
                                            endDateVariable = (end_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + end_time.Time.Hours + ":" + end_time.Time.Minutes + ":" + end_time.Time.Seconds + " UTC +00:00"));

                                        }
                                        else
                                        {
                                            endDateVariable = (DateTime.Now.Date.ToString("ddd, dd MMM yyy " + end_time.Time.Hours + ":" + end_time.Time.Minutes + ":" + end_time.Time.Seconds + " UTC +00:00"));
                                        }
                                    }
                                    else
                                    {
                                        if (addEndDate.IsChecked)
                                        {
                                            endDateVariable = (end_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + start_time.Time.Hours + ":" + start_time.Time.Minutes + ":" + start_time.Time.Seconds + " UTC +00:00"));
                                        }
                                        else
                                        {
                                            endDateVariable = (DateTime.Now.Date.ToString("ddd, dd MMM yyy " + start_time.Time.Hours + ":" + start_time.Time.Minutes + ":" + start_time.Time.Seconds + " UTC +00:00"));
                                        }
                                    }
                                }

                            }
                            App.Log("REPEAT STATS: " + repeat + ", " + repeatDays);
                            string startDateVariable = (addRepeat.IsChecked && repeat.Equals("Weekly")) ? (DateTime.Now.Date.ToString("ddd, dd MMM yyy " + start_time.Time.Hours + ":" + start_time.Time.Minutes + ":" + start_time.Time.Seconds + " UTC +00:00")) : (start_date.NullableDate.HasValue ? start_date.NullableDate.Value.Date.ToString("ddd, dd MMM yyy " + start_time.Time.Hours + ":" + start_time.Time.Minutes + ":" + start_time.Time.Seconds + " UTC +00:00") : null);

                            if (imageStream != null)
                            {
                                if (isEdit)
                                {

                                    string dict = JsonConvert.SerializeObject(new { events = new { event_id = ev.events.id, group_id = int.Parse(DataClass.GetInstance.GroupId), title = title.Text, details = details.Text, image = "", start_date = startDateVariable, end_date = endDateVariable, subtitle = colors[highlight_color.SelectedIndex], location = location.Text, repeat = repeat, repeat_days = repeatDays } });
                                    await advanceRest.MultiPartDataContentAsync(Constants.UPDATE_EVENT, dict, cts.Token, imageStream, "events", 0, UploadCallback);
                                }
                                else
                                {
                                    string dict = JsonConvert.SerializeObject(new { topics = string.Format("'group_{0}_{1}_{2}' in topics,'group_{0}_{3}_{2}' in topics", DataClass.GetInstance.GroupId, "iOS", "All", "Android"), events = new { group_id = int.Parse(DataClass.GetInstance.GroupId), title = title.Text, details = details.Text, image = "", start_date = startDateVariable, end_date = endDateVariable, subtitle = colors[highlight_color.SelectedIndex], location = location.Text, repeat = repeat, repeat_days = repeatDays } });
                                    await advanceRest.MultiPartDataContentAsync(Constants.ADD_EVENT, dict, cts.Token, imageStream, "events", 0, UploadCallback);
                                }
                            }
                            else
                            {
                                if (isEdit)
                                {
                                    string dict = JsonConvert.SerializeObject(new { events = new { event_id = ev.events.id, group_id = int.Parse(DataClass.GetInstance.GroupId), title = title.Text, details = details.Text, start_date = startDateVariable, end_date = endDateVariable, subtitle = colors[highlight_color.SelectedIndex], location = location.Text, repeat = repeat, repeat_days = repeatDays } });
                                    await advanceRest.PostRequestAsync(Constants.UPDATE_EVENT, dict, cts.Token, 0);
                                }
                                else
                                {
                                    string dict = JsonConvert.SerializeObject(new { topics = string.Format("'group_{0}_{1}_{2}' in topics,'group_{0}_{3}_{2}' in topics", DataClass.GetInstance.GroupId, "iOS", "All", "Android"), events = new { group_id = int.Parse(DataClass.GetInstance.GroupId), title = title.Text, details = details.Text, start_date = startDateVariable, end_date = endDateVariable, subtitle = colors[highlight_color.SelectedIndex], location = location.Text, repeat = repeat, repeat_days = repeatDays } });
                                    await advanceRest.PostRequestAsync(Constants.ADD_EVENT, dict, cts.Token, 0);
                                }
                            }


                        }
                        catch (OperationCanceledException oce)
                        {
                            ShowAlerts("Task Cancelled", oce.Message);
                        }
                        catch (TimeoutException te)
                        {
                            ShowAlerts("Request Timeout", te.Message);
                        }
                        catch (Exception ex)
                        {
                            ShowAlerts("Error", ex.Message);
                        }
                        finally
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                isClicked = false;
                                UserDialogs.Instance.HideLoading();
                                cts = null;
                            });
                        }
                    });
                }
            }

        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        void StartDate_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("NullableDate"))
            {
                start_date_container.BorderColor = Color.Gray;
                if (addEndDate.IsChecked)
                {
                    end_date.NullableDate = start_date.NullableDate.Value.AddDays(1);
                }
            }
        }

        void EndDate_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("NullableDate"))
            {
                end_date_container.BorderColor = Color.Gray;
            }
        }

        void HighLightColor_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            highlight_color_container.BorderColor = Color.Gray;
        }

        void Details_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Text")
            {
                if (!string.IsNullOrEmpty(details.Text))
                {
                    details.Placeholder = "";

                }
                else
                {
                    details.Placeholder = "Write event details";
                }
            }
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            App.Log("CREATE EVENT: " + jsonData.ToString());
            Device.BeginInvokeOnMainThread(() =>
            {
                EventsModel eventsModel = JsonConvert.DeserializeObject<EventsModel>(jsonData["event"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                if (!isEdit)
                {
                    calendarPage.AddEvent(eventsModel);
                }
                else
                {
                    ev.alv.Header = eventsModel;
                    calendarPage.events[calendarPage.events.IndexOf(calendarPage.events.First(x => x.id == ev.events.id))] = eventsModel;
                }
                calendarPage.fromCreateEvent = true;
                isClicked = false;
                UserDialogs.Instance.HideLoading();
                Navigation.PopAsync(false);
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                UserDialogs.Instance.HideLoading();
                DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }

        void AddEndTime_CheckChanged(object sender, System.EventArgs e)
        {
            if (addEndTime.IsChecked)
            {
                endTimeLabel.IsVisible = true;
                endTimeContainer.IsVisible = true;
            }
            else
            {
                endTimeLabel.IsVisible = false;
                endTimeContainer.IsVisible = false;
            }
        }

        void AddRepeat_CheckChanged(object sender, System.EventArgs e)
        {
            if (addRepeat.IsChecked)
            {
                eventTypeContainer.IsVisible = true;
                addEndDate.IsVisible = true;
            }    
            else
            {
                eventType.SelectedItem = eventTypeSource[0];
                eventType.SelectedIndex = 0;
                eventTypeContainer.IsVisible = false;
                weeklyWhenFormsContainer.IsVisible = false;
                addEndDate.IsVisible = false;
                endWhenFormsContainer.IsVisible = false;
                whenFormsContainer.IsVisible = true;
                addEndDate.IsChecked = false;
            }
        }

        void AddEndDate_CheckChanged(object sender, EventArgs e)
        {
            if (addEndDate.IsChecked)
            {
                endWhenFormsContainer.IsVisible = true;
                if (start_date.NullableDate.HasValue)
                    end_date.NullableDate = start_date.NullableDate.Value.AddDays(1);
            }
            else
            {
                endWhenFormsContainer.IsVisible = false;
                end_date.NullableDate = null;
            }
        }

        void EventType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            App.Log(eventType.SelectedItem.ToString());
            switch (eventType.SelectedItem.ToString())
            {
                case "Monthly":
                    weeklyWhenFormsContainer.IsVisible = false;
                    whenFormsContainer.IsVisible = true;
                    break;
                case "Weekly":
                    weeklyWhenFormsContainer.IsVisible = true;
                    whenFormsContainer.IsVisible = false;
                    break;
            }
        }
        void AddDays_CheckChanged(object sender, System.EventArgs e)
        {
            if (addRepeat.IsChecked)
            {
                CheckBox checkBox = (CheckBox)sender;
                if (checkBox.IsChecked)
                {
                    if (!days.Contains(checkBox.Text))
                    {
                        days.Add(checkBox.Text);
                    }
                }
                else
                {
                    days.Remove(checkBox.Text);
                }
                //App.Log("DAYS: " + string.Join(",", days));
            }
        }
    }
}
