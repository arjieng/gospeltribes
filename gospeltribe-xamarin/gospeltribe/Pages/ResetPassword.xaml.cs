﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace gospeltribe
{
    public partial class ResetPassword : ContentPage, IAdvanceRestConnector
    {
        AdvanceRestService advanceRest;
        CancellationTokenSource cts;
        string reset_token;
        bool isClicked = false;

        public ResetPassword(string reset_token)
        {
            advanceRest = new AdvanceRestService();
            advanceRest.WebServiceDelegate = this;

            this.reset_token = reset_token;
            InitializeComponent();
        }

        void LeftButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(false);
        }

        void DisplayError(Entry entry, string error)
        {
            entry.Text = String.Empty;
            entry.Placeholder = error;
            entry.PlaceholderColor = Color.Red;
        }

        async void ResetPassword_Clicked(object sender, System.EventArgs e)
        {
            bool isNotError = true;
            if (String.IsNullOrEmpty(passwordEntry.Text))
            {
                DisplayError(passwordEntry, "Password is required!");
                isNotError = false;
            }
            else
            {
                if (passwordEntry.Text.Length < 8)
                {
                    DisplayError(passwordEntry, "Password must be 8 charactes!");
                    confirmEntry.Text = string.Empty;
                    isNotError = false;
                }
                else
                {
                    if (String.IsNullOrEmpty(confirmEntry.Text))
                    {
                        DisplayError(confirmEntry, "Confirm Password is required!");
                        isNotError = false;
                    }

                    if (passwordEntry.Text != confirmEntry.Text)
                    {
                        confirmEntry.Text = string.Empty;
                        DisplayError(passwordEntry, "Password does not match!");
                        isNotError = false;
                    }
                }
            }

            if (isNotError && isClicked == false){

                await Task.Run(async () =>
                {
                    try
                    {
                        cts = new CancellationTokenSource();
                        string dictionary = JsonConvert.SerializeObject(new { user = new { reset_password_token = this.reset_token, password = passwordEntry.Text } });
                        await advanceRest.PostRequestAsync(Constants.RESET_PASSWORD, dictionary, cts.Token, 0);
                    }
                    catch (OperationCanceledException oce)
                    {
                        ShowAlerts("Task Cancelled", oce.Message);
                    }
                    catch (TimeoutException te)
                    {
                        ShowAlerts("Request Timeout", te.Message);
                    }
                    catch (Exception ex)
                    {
                        ShowAlerts("Error", ex.Message);
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            cts = null;
                        });
                    }
                });
            }
        }

        void ShowAlerts(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Application.Current.MainPage?.DisplayAlert(title, error, "Okay");
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<string>(this, "ResetPassword", (obj) =>
            {
                this.reset_token = obj;
            });
        }


        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<string>(this, "ResetPassword");
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (jsonData["status"].ToString().Equals("200"))
                {
                    DisplayAlert("Password changed", "Please sign in using your new password", "Okay");
                    Navigation.PopToRootAsync(false);
                }
                if (jsonData["status"].ToString().Equals("404"))
                {
                    DisplayAlert("Invalid Token", "Reset token is invalid", "Okay");
                }
                if (jsonData["status"].ToString().Equals("300"))
                {
                    DisplayError(passwordEntry, "Password must be 8 charactes!");
                    confirmEntry.Text = string.Empty;
                }
                isClicked = false;
                loader.IsVisible = false;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DisplayAlert(title, error, "Okay");
                isClicked = false;
                loader.IsVisible = false;
            });
        }
    }
}
