﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public class BottomTabbedPage : TabbedPage
    {
        public static readonly BindableProperty BadgeText1Property = BindableProperty.Create(nameof(BadgeText1), typeof(string), typeof(BottomTabbedPage), null);
        public string BadgeText1
        {
            get { return (string)GetValue(BadgeText1Property); }
            set { SetValue(BadgeText1Property, value); }
        }
        public static readonly BindableProperty BadgeText2Property = BindableProperty.Create(nameof(BadgeText2), typeof(string), typeof(BottomTabbedPage), null);
        public string BadgeText2
        {
            get { return (string)GetValue(BadgeText2Property); }
            set { SetValue(BadgeText2Property, value); }
        }
        public static readonly BindableProperty BadgeText3Property = BindableProperty.Create(nameof(BadgeText3), typeof(string), typeof(BottomTabbedPage), null);
        public string BadgeText3
        {
            get { return (string)GetValue(BadgeText3Property); }
            set { SetValue(BadgeText3Property, value); }
        }
        public static readonly BindableProperty BadgeText4Property = BindableProperty.Create(nameof(BadgeText4), typeof(string), typeof(BottomTabbedPage), null);
        public string BadgeText4
        {
            get { return (string)GetValue(BadgeText4Property); }
            set { SetValue(BadgeText4Property, value); }
        }
        public static readonly BindableProperty BadgeText5Property = BindableProperty.Create(nameof(BadgeText5), typeof(string), typeof(BottomTabbedPage), null);
        public string BadgeText5
        {
            get { return (string)GetValue(BadgeText5Property); }
            set { SetValue(BadgeText5Property, value); }
        }


        public static readonly BindableProperty A1Property = BindableProperty.Create(nameof(A1), typeof(NavigationPage), typeof(BottomTabbedPage), null);
        public NavigationPage A1
        {
            get { return (NavigationPage)GetValue(A1Property); }
            set { SetValue(A1Property, value); }
        }

        public static readonly BindableProperty A2Property = BindableProperty.Create(nameof(A2), typeof(NavigationPage), typeof(BottomTabbedPage), null);
        public NavigationPage A2
        {
            get { return (NavigationPage)GetValue(A2Property); }
            set { SetValue(A2Property, value); }
        }

        public static readonly BindableProperty A3Property = BindableProperty.Create(nameof(A3), typeof(NavigationPage), typeof(BottomTabbedPage), null);
        public NavigationPage A3
        {
            get { return (NavigationPage)GetValue(A3Property); }
            set { SetValue(A3Property, value); }
        }

        public static readonly BindableProperty A4Property = BindableProperty.Create(nameof(A4), typeof(NavigationPage), typeof(BottomTabbedPage), null);
        public NavigationPage A4
        {
            get { return (NavigationPage)GetValue(A4Property); }
            set { SetValue(A4Property, value); }
        }

        public static readonly BindableProperty A5Property = BindableProperty.Create(nameof(A5), typeof(NavigationPage), typeof(BottomTabbedPage), null);
        public NavigationPage A5
        {
            get { return (NavigationPage)GetValue(A5Property); }
            set { SetValue(A5Property, value); }
        }
    }
}