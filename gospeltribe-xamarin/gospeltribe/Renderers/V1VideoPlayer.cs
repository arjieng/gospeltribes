﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public interface IVideoPlayerFunctions
    {
        void pauseVideo();
        void playVideo();
    }

    public class V1VideoPlayer : WebView, IVideoPlayerFunctions
    {
        public IVideoPlayerFunctions nativeRenderer;
        public static readonly BindableProperty youtubeIDProperty = BindableProperty.Create(
            nameof(youtubeID),
            typeof(string),
            typeof(V1VideoPlayer),
            default(string),
            BindingMode.TwoWay,
            propertyChanged: (bindable, oldValue, newValue) =>
            {
                var view = bindable as V1VideoPlayer;
                var newVal = newValue as string;
                var html = "<html>" +
                    "<body style='margin:0px;padding:0px;' bgcolor='#000000'>" +
                    "<script type='text/javascript' src='https://www.youtube.com/iframe_api'></script>" +
                    "<script type='text/javascript'>" +
                    "function onYouTubeIframeAPIReady(){" +
                    "ytplayer=new YT.Player('playerId')}" +
                    "function onPlayerReady(a){" +
                    "a.target.playVideo();}" +
                    "</script>" +
                $"<iframe id='playerId' type='text/html' width=100% height=100% src='https://www.youtube.com/embed/{newVal}?enablejsapi=1&rel=0&playsinline=1&mode=opaque&autohide=1&showinfo=0' frameborder='0' allowfullscreen>" +
                    "</body>" +
                    "</html>";
                var htmlSource = new HtmlWebViewSource();
                htmlSource.Html = html;
                view.Source = htmlSource;
            }
        );

        public string youtubeID
        {
            get { return (string)GetValue(youtubeIDProperty); }
            set { SetValue(youtubeIDProperty, value); }
        }

        public void pauseVideo()
        {
            nativeRenderer.pauseVideo();
        }
        public void playVideo()
        {
            nativeRenderer.playVideo();
        }
    }
}
