﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public class CustomEntry : Entry
    {
        public new event EventHandler Completed;

        public static readonly BindableProperty ReturnTypeProperty = BindableProperty.Create(nameof(ReturnType), typeof(ReturnType), typeof(CustomEntry), ReturnType.Done, BindingMode.TwoWay);
        public ReturnType ReturnType
        {
            get { return (ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }

        public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create(nameof(AutoCapitalization), typeof(AutoCapitalizationType), typeof(CustomEntry), AutoCapitalizationType.None);
        public AutoCapitalizationType AutoCapitalization
        {
            get { return (AutoCapitalizationType)GetValue(AutoCapitalizationProperty); }
            set { SetValue(AutoCapitalizationProperty, value); }
        }

        public static readonly BindableProperty DesignTypeProperty = BindableProperty.Create(nameof(DesignType), typeof(DesignType), typeof(CustomEntry), DesignType.Default, BindingMode.TwoWay);
        public DesignType DesignType
        {
            get { return (DesignType)GetValue(DesignTypeProperty); }
        }

        #region IF_FLAT
        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(CustomEntry), Color.Gray);
        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(BorderColorProperty, value);
        }

        public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(nameof(BorderWidth), typeof(int), typeof(CustomEntry), Device.OnPlatform<int>(1, 2, 2));
        public int BorderWidth
        {
            get => (int)GetValue(BorderWidthProperty);
            set => SetValue(BorderWidthProperty, value);
        }
        public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(nameof(CornerRadius), typeof(double), typeof(CustomEntry), Device.OnPlatform<double>(6, 7, 7));
        public double CornerRadius
        {
            get => ((double)GetValue(CornerRadiusProperty)).ScaleWidth();
            set => SetValue(CornerRadiusProperty, value);
        }
        public static readonly BindableProperty IsCurvedCornersEnabledProperty = BindableProperty.Create(nameof(IsCurvedCornersEnabled), typeof(bool), typeof(CustomEntry), true);
        public bool IsCurvedCornersEnabled
        {
            get => (bool)GetValue(IsCurvedCornersEnabledProperty);
            set => SetValue(IsCurvedCornersEnabledProperty, value);
        }
        #endregion

        public static readonly BindableProperty IsSignupProperty = BindableProperty.Create(nameof(IsSignup), typeof(bool), typeof(CustomEntry), false);
        public bool IsSignup
        {
            get
            {
                return (bool)GetValue(IsSignupProperty);
            }
            set
            {
                SetValue(IsSignupProperty, value);
            }
        }


        public void InvokeCompleted()
        {
            if (this.Completed != null)
                this.Completed.Invoke(this, null);
        }
    }

    public enum DesignType
    {
        Default, Flat, Materialize
    }

    public enum ReturnType
    {
        Go, Next, Done, Send, Search
    }

    public enum AutoCapitalizationType
    {
        Words, Sentences, None, All
    }
}