﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public class RoundedBox : ContentView
    {
        public static readonly BindableProperty FillColorProperty =
            BindableProperty.Create("FillColor", typeof(Color), typeof(RoundedBox), Color.White);
        public Color FillColor      
        {      
            get { return (Color)GetValue(FillColorProperty); }      
            set { SetValue(FillColorProperty, value); }      
        } 

        public static readonly BindableProperty CornerRadiusProperty =
            BindableProperty.Create("CornerRadius", typeof(double), typeof(RoundedBox), 0.0);
        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly BindableProperty MakeCircleProperty =
            BindableProperty.Create("MakeCircle", typeof(bool), typeof(RoundedBox), false);
        public Boolean MakeCircle
        {
            get { return (Boolean)GetValue(MakeCircleProperty); }
            set { SetValue(MakeCircleProperty, value); }
        }

        public static readonly BindableProperty BorderColorProperty =
            BindableProperty.Create("BorderColor", typeof(Color), typeof(RoundedBox), Color.Transparent);
        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static readonly BindableProperty BorderWidthProperty =
            BindableProperty.Create("BorderWidth", typeof(int), typeof(RoundedBox), 1);
        public int BorderWidth
        {
            get { return (int)GetValue(BorderWidthProperty); }
            set { SetValue(BorderWidthProperty, value); }
        }
    }
}
