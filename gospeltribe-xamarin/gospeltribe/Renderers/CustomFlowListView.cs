﻿using System;
using DLToolkit.Forms.Controls;
using Xamarin.Forms;

namespace gospeltribe
{
    public class CustomFlowListView : FlowListView
    {
        public CustomFlowListView()
        {
        }

        //public CustomFlowListView(ListViewCachingStrategy cachingStrategy) : base(cachingStrategy) { }

        //public event EventHandler<ScrollStateChangedEventArgs> ScrollStateChanged;
        //public static void OnScrollStateChanged(object sender, ScrollStateChangedEventArgs e)
        //{
        //    var customListview = (CustomFlowListView)sender;
        //    customListview.ScrollStateChanged?.Invoke(customListview, e);
        //}

        //public event EventHandler<ItemTappedEventArgs> ItemTappedIOS;
        //public static void OnItemTappedIOS(object sender, ItemTappedEventArgs e)
        //{
        //    var customListview = (CustomFlowListView)sender;
        //    customListview.ItemTappedIOS?.Invoke(customListview, e);
        //}
    }

    public class ScrollStateChangedEventArgs : EventArgs
    {
        public ScrollStateChangedEventArgs(ScrollState scrollState)
        {
            this.CurScrollState = scrollState;
        }

        public enum ScrollState
        {
            Idle = 0,
            Running = 1
        }

        public ScrollState CurScrollState { get; }
    }
}
