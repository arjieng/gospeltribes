﻿using System;
using Xamarin.Forms;

namespace gospeltribe
{
    public class BDLPicker : Picker
    {
        public static readonly BindableProperty ImageProperty = BindableProperty.Create(nameof(Image), typeof(string), typeof(BDLPicker), "Caret");
        public string Image{
            get{
                return (string)GetValue(ImageProperty);
            }
            set{
                SetValue(ImageProperty, value);
            }
        }
    }
}
