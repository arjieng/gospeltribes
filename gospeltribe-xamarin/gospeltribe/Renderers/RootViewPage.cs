﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace gospeltribe
{
    public class RootViewPage : ContentPage
    {

        public RootViewPage()
        {
            //Xamarin.Forms.PlatformConfiguration.iOSSpecific.Page.SetUseSafeArea(this, true);
        }

        public static readonly BindableProperty PageTitleProperty = BindableProperty.Create("PageTitle", typeof(string), typeof(RootViewPage), null);

        public static readonly BindableProperty EntrySearchIsVisibleProperty = BindableProperty.Create("IsEntrySearchVisible", typeof(bool), typeof(RootViewPage), false);
        public static readonly BindableProperty EntrySearchTextProperty = BindableProperty.Create("EntrySearchText", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty EntrySearchPlaceholderProperty = BindableProperty.Create("EntrySearchPlaceholder", typeof(string), typeof(RootViewPage), null);

        public static readonly BindableProperty TitleFontFamilyProperty = BindableProperty.Create("TitleFontFamily", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty TitleFontColorProperty = BindableProperty.Create("TitleFontColor", typeof(Color), typeof(RootViewPage), Color.White);

        public static readonly BindableProperty LeftIconProperty = BindableProperty.Create("LeftIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty LeftIconTintProperty = BindableProperty.Create("LeftIconTint", typeof(Color), typeof(RootViewPage), Color.Transparent);

        public static readonly BindableProperty LeftIconHeightRequestProperty = BindableProperty.Create("LeftIconHeightRequest", typeof(double), typeof(RootViewPage), 16.0);
        public static readonly BindableProperty LeftIconWidthRequestProperty = BindableProperty.Create("LeftIconWidthRequest", typeof(double), typeof(RootViewPage), 16.0);
        public static readonly BindableProperty LeftIconContainerPaddingProperty = BindableProperty.Create("LeftIconContainerPadding", typeof(Thickness), typeof(RootViewPage), new Thickness(0, 0, 0, 11.ScaleHeight()));

        public static readonly BindableProperty LeftIconIsVisibleProperty = BindableProperty.Create("LeftIconIsVisible", typeof(bool), typeof(RootViewPage), true);
        public static readonly BindableProperty LeftButtonCommandProperty = BindableProperty.Create("LeftButtonCommand", typeof(ICommand), typeof(RootViewPage), null);

        public static readonly BindableProperty RightIconProperty = BindableProperty.Create("RightIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty RightIconTintProperty = BindableProperty.Create("RightIconTint", typeof(Color), typeof(RootViewPage), Color.Transparent);
        public static readonly BindableProperty RightButtonCommandProperty = BindableProperty.Create("RightButtonCommand", typeof(ICommand), typeof(RootViewPage), null);


        //public static readonly BindableProperty ImageRotationProperty = BindableProperty.Create("Rotation", typeof(int), typeof(RootViewPage), 0);


        public static readonly BindableProperty RightSubIconProperty = BindableProperty.Create("RightSubIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty RightSubIconTintProperty = BindableProperty.Create("RightSubIconTint", typeof(Color), typeof(RootViewPage), Color.Transparent);
        public static readonly BindableProperty RightSubButtonCommandProperty = BindableProperty.Create("RightSubButtonCommand", typeof(ICommand), typeof(RootViewPage), null);

        public static readonly BindableProperty NavBackgroundColorProperty = BindableProperty.Create("NavBackgroundColor", typeof(Color), typeof(RootViewPage), Color.Transparent);



        //public int ImageRotation{
        //    set { SetValue(ImageRotationProperty, value); }
        //    get { return (int)GetValue(ImageRotationProperty); }
        //}

        public string PageTitle
        {
            set { SetValue(PageTitleProperty, value); }
            get { return (string)GetValue(PageTitleProperty); }
        }

        public bool IsEntrySearchVisible
        {
            set { SetValue(EntrySearchIsVisibleProperty, value); }
            get { return (bool)GetValue(EntrySearchIsVisibleProperty); }
        }

        public string EntrySearchText
        {
            set { SetValue(EntrySearchTextProperty, value); }
            get { return (string)GetValue(EntrySearchTextProperty); }
        }

        public string EntrySearchPlaceholder
        {
            set { SetValue(EntrySearchPlaceholderProperty, value); }
            get { return (string)GetValue(EntrySearchPlaceholderProperty); }
        }

        public string TitleFontFamily
        {
            set { SetValue(TitleFontFamilyProperty, value); }
            get { return (string)GetValue(TitleFontFamilyProperty); }
        }

        public Color TitleFontColor
        {
            set { SetValue(TitleFontColorProperty, value); }
            get { return (Color)GetValue(TitleFontColorProperty); }
        }

        public string LeftIcon
        {
            set { SetValue(LeftIconProperty, value); }
            get { return (string)GetValue(LeftIconProperty); }
        }

        public double LeftIconHeightRequest
        {
            set { SetValue(LeftIconHeightRequestProperty, value); }
            get { return (double)GetValue(LeftIconHeightRequestProperty); }
        }

        public double LeftIconWidthRequest
        {
            set { SetValue(LeftIconWidthRequestProperty, value); }
            get { return (double)GetValue(LeftIconWidthRequestProperty); }
        }


        public bool LeftIconIsVisible
        {
            set { SetValue(LeftIconIsVisibleProperty, value); }
            get { return (bool)GetValue(LeftIconIsVisibleProperty); }
        }

        public ICommand LeftButtonCommand
        {
            set { SetValue(LeftButtonCommandProperty, value); }
            get { return (ICommand)GetValue(LeftButtonCommandProperty); }
        }

        public Thickness LeftIconContainerPadding{
            set { SetValue(LeftIconContainerPaddingProperty, value); }
            get{ return (Thickness)GetValue(LeftIconContainerPaddingProperty); }
        }

        public string RightSubIcon
        {
            set { SetValue(RightSubIconProperty, value); }
            get { return (string)GetValue(RightSubIconProperty); }
        }

        public ICommand RightSubButtonCommand
        {
            set { SetValue(RightSubButtonCommandProperty, value); }
            get { return (ICommand)GetValue(RightSubButtonCommandProperty); }
        }

        public string RightIcon
        {
            set { SetValue(RightIconProperty, value); }
            get { return (string)GetValue(RightIconProperty); }
        }


        public Color RightSubIconTint
        {
            set { SetValue(RightSubIconTintProperty, value); }
            get { return (Color)GetValue(RightSubIconTintProperty); }
        }

        public Color RightIconTint
        {
            set { SetValue(RightIconTintProperty, value); }
            get { return (Color)GetValue(RightIconTintProperty); }
        }

        public Color LeftIconTint
        {
            set { SetValue(RightIconTintProperty, value); }
            get { return (Color)GetValue(RightIconTintProperty); }
        }

        public ICommand RightButtonCommand
        {
            set { SetValue(RightButtonCommandProperty, value); }
            get { return (ICommand)GetValue(RightButtonCommandProperty); }
        }

        public Color NavBackgroundColor
        {
            set { SetValue(NavBackgroundColorProperty, value); }
            get { return (Color)GetValue(NavBackgroundColorProperty); }
        }
    }
}
