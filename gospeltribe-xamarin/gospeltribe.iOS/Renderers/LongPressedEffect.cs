﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Linq;
using System;
using gospeltribe.iOS;

[assembly: ResolutionGroupName("GT")]
[assembly: ExportEffect(typeof(LongPressedEffect), "LongPressedEffect")]
namespace gospeltribe.iOS
{
    public class LongPressedEffect : PlatformEffect
    {
        bool _attached;
        UILongPressGestureRecognizer longPressGestureRecognizer;
        UITapGestureRecognizer tapGestureRecognizer;
        protected override void OnAttached()
        {
            if (!_attached)
            {
                try
                {
                    var effect = (gospeltribe.LongPressedEffect)Element.Effects.FirstOrDefault(e => e is gospeltribe.LongPressedEffect);
                    longPressGestureRecognizer = new UILongPressGestureRecognizer((state) =>
                    {
                        if (state.State == UIGestureRecognizerState.Began)
                        {
                            effect.SendLongTapEvent(Element);
                        }
                    });

                    tapGestureRecognizer = new UITapGestureRecognizer(() =>
                    {
                        effect.SendTapEvent(Element);
                    });

                    Container.AddGestureRecognizer(longPressGestureRecognizer);
                    Container.AddGestureRecognizer(tapGestureRecognizer);
                }
                catch (Exception ex)
                {
                    App.Log("Cannot set property on attached control. Error: " + ex.Message);
                }

                _attached = true;
            }
        }

        protected override void OnDetached()
        {
            if (_attached)
            {
                if (longPressGestureRecognizer != null)
                {
                    Container.RemoveGestureRecognizer(longPressGestureRecognizer);
                }

                if (tapGestureRecognizer != null)
                {
                    Container.RemoveGestureRecognizer(tapGestureRecognizer);
                }
                _attached = false;
            }
        }
    }
}
