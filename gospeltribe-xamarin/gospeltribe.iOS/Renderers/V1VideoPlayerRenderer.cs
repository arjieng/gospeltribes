﻿using System;
using gospeltribe;
using gospeltribe.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(V1VideoPlayer), typeof(V1VideoPlayerRenderer))]
namespace gospeltribe.iOS
{
    public class V1VideoPlayerRenderer : WebViewRenderer, IVideoPlayerFunctions
    {
        private UIWebView nControl;
        private V1VideoPlayer xControl;

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                nControl = (UIWebView)this.NativeView;
                xControl = e.NewElement as V1VideoPlayer;

                nControl.AllowsInlineMediaPlayback = true;
                nControl.MediaPlaybackRequiresUserAction = false;

                xControl.nativeRenderer = this;

                //nControl.EvaluateJavascript("ytplayer.playVideo()");
            }
        }

        public void pauseVideo()
        {
            nControl.EvaluateJavascript("ytplayer.pauseVideo()");
        }

        public void playVideo()
        {
            nControl.EvaluateJavascript("ytplayer.playVideo()");
        }
    }
}
