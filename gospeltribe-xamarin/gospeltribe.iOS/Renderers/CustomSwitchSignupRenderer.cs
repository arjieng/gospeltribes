﻿using System;
using gospeltribe.iOS.Renderers;
using gospeltribe;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomSwitchSignup), typeof(CustomSwitchSignupRenderer))]
namespace gospeltribe.iOS.Renderers
{
    public class CustomSwitchSignupRenderer : SwitchRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);
        }
    }
}
