﻿using System;
using CoreAnimation;
using CoreGraphics;
using gospeltribe;
using gospeltribe.iOS.Renderers;
using Newtonsoft.Json.Linq;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BottomTabbedPage), typeof(BottomTabbedPageRenderer))]
namespace gospeltribe.iOS.Renderers
{
    public class BottomTabbedPageRenderer : TabbedRenderer
    {
        protected override void Dispose(bool disposing)
        {
            MessagingCenter.Unsubscribe<JObject>(this, "Badges");
            base.Dispose(disposing);
        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                MessagingCenter.Subscribe<JObject>(this, "Badges", (obj) =>
                {
                    var element = (BottomTabbedPage)e.NewElement;
                    for (int i = 0; i < TabBar.Items.Length; i++)
                    {
                        UITabBarItem tabBarItem = TabBar.Items[i];

                        switch (i)
                        {
                            case 0:
                                tabBarItem.Image = UIImage.FromBundle("inboxUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                                if (!string.IsNullOrEmpty(element.BadgeText1) && !element.BadgeText1.Equals("0"))
                                {
                                    tabBarItem.BadgeValue = element.BadgeText1;
                                }
                                else
                                {
                                    tabBarItem.BadgeValue = null;
                                }
                                break;
                            case 1:
                                tabBarItem.Image = UIImage.FromBundle("prayersUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                                if (!string.IsNullOrEmpty(element.BadgeText2) && !element.BadgeText2.Equals("0"))
                                {
                                    tabBarItem.BadgeValue = element.BadgeText2;
                                }
                                else
                                {
                                    tabBarItem.BadgeValue = null;
                                }
                                break;
                            case 2:
                                tabBarItem.Image = UIImage.FromBundle("eventsUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                                if (!string.IsNullOrEmpty(element.BadgeText3) && !element.BadgeText3.Equals("0"))
                                {
                                    tabBarItem.BadgeValue = element.BadgeText3;
                                }
                                else
                                {
                                    tabBarItem.BadgeValue = null;
                                }
                                break;
                            case 3:
                                tabBarItem.Image = UIImage.FromBundle("storyUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                                if (!string.IsNullOrEmpty(element.BadgeText4) && !element.BadgeText4.Equals("0"))
                                {
                                    tabBarItem.BadgeValue = element.BadgeText4;
                                }
                                else
                                {
                                    tabBarItem.BadgeValue = null;
                                }
                                break;
                            case 4:
                                tabBarItem.Image = UIImage.FromBundle("membersUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                                if (!string.IsNullOrEmpty(element.BadgeText5) && !element.BadgeText5.Equals("0"))
                                {
                                    tabBarItem.BadgeValue = element.BadgeText5;
                                }
                                else
                                {
                                    tabBarItem.BadgeValue = null;
                                }
                                break;
                        }
                    }

                    for (int i = 0; i < TabBar.Subviews.Length; i++)
                    {
                        UIView tabBarSubView = TabBar.Subviews[i];
                        foreach (var subviews in tabBarSubView.Subviews)
                        {
                            if (subviews.Class.Name == "_UIBadgeView")
                            {
                                CATransform3D transform3D = new CATransform3D();
                                if (i == TabBar.Subviews.Length - 1)
                                {
                                    transform3D = NewTransformation(-5, 0);
                                }
                                else
                                {
                                    transform3D = NewTransformation(-20, 0);
                                }
                                subviews.Layer.Transform = transform3D;
                            }
                        }
                    }
                });
            }
        }

        public override void ViewWillAppear(bool animated)
        {
            if (TabBar == null) return;
            if (TabBar.Items == null) return;

            UITabBar.Appearance.SelectedImageTintColor = UIColor.Black;

            base.ViewWillAppear(animated);


            CGSize size = new CGSize();
            size.Width = TabBar.Frame.Width / TabBar.Items.Length;
            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                size.Height = 55.ScaleHeight();
            }else{
                size.Height = TabBar.Frame.Height;
            }

            UITabBar.Appearance.SelectionIndicatorImage = imageWithColor(size);

            var element = (BottomTabbedPage)Element;
            for (int i = 0; i < TabBar.Items.Length; i++)
            {
                UITabBarItem tabBarItem = TabBar.Items[i];

                switch (i)
                {
                    case 0:
                        tabBarItem.Image = UIImage.FromBundle("inboxUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                        if (!string.IsNullOrEmpty(element.BadgeText1) && !element.BadgeText1.Equals("0"))
                        {
                            tabBarItem.BadgeValue =  element.BadgeText1;
                        }
                        break;
                    case 1:
                        tabBarItem.Image = UIImage.FromBundle("prayersUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                        if (!string.IsNullOrEmpty(element.BadgeText2) && !element.BadgeText2.Equals("0"))
                        {
                            tabBarItem.BadgeValue = element.BadgeText2;
                        }
                        break;
                    case 2:
                        tabBarItem.Image = UIImage.FromBundle("eventsUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                        if (!string.IsNullOrEmpty(element.BadgeText3) && !element.BadgeText3.Equals("0"))
                        {
                            tabBarItem.BadgeValue = element.BadgeText3;
                        }
                        break;
                    case 3:
                        tabBarItem.Image = UIImage.FromBundle("storyUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                        if (!string.IsNullOrEmpty(element.BadgeText4) && !element.BadgeText4.Equals("0"))
                        {
                            tabBarItem.BadgeValue = element.BadgeText4;
                        }
                        break;
                    case 4:
                        tabBarItem.Image = UIImage.FromBundle("membersUnselected").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

                        if (!string.IsNullOrEmpty(element.BadgeText5) && !element.BadgeText5.Equals("0"))
                        {
                            tabBarItem.BadgeValue = element.BadgeText5;
                        }
                        break;
                }
            }

            for (int i = 0; i < TabBar.Subviews.Length; i++)
            {
                UIView tabBarSubView = TabBar.Subviews[i];
                foreach (var subviews in tabBarSubView.Subviews)
                {
                    if (subviews.Class.Name == "_UIBadgeView")
                    {
                        CATransform3D transform3D = new CATransform3D();
                        if (i == TabBar.Subviews.Length - 1)
                        {
                            transform3D = NewTransformation(-5, 0);
                        }
                        else
                        {
                            transform3D = NewTransformation(-20, 0);
                        }
                        subviews.Layer.Transform = transform3D;
                    }
                }
            }
        }

        public override void ViewWillLayoutSubviews()
        {
            if (TabBar?.Items == null)
                return;

            base.ViewWillLayoutSubviews();

            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                TabBar.Frame = new CGRect(TabBar.Frame.X, (TabBar.Frame.Y - 50.ScaleHeight()) + (TabBar.Frame.Height - 5.ScaleHeight()), TabBar.Frame.Width, 55.ScaleHeight());
            }

        }

        CATransform3D NewTransformation(nfloat m41, nfloat m42)
        {
            CATransform3D transform3D = new CATransform3D
            {
                m11 = 1,
                m12 = 0,
                m13 = 0,
                m14 = 0,

                m21 = 0,
                m22 = 1,
                m23 = 0,
                m24 = 0,

                m31 = 0,
                m32 = 0,
                m33 = 1,
                m34 = 0,

                m41 = m41,
                m42 = m42,
                m43 = 1,
                m44 = 1
            };

            return transform3D;
        }

        public UIImage imageWithColor(CGSize size)
        {
            CGRect rect = new CGRect(0, 0, size.Width, size.Height);
            UIGraphics.BeginImageContext(size);
            using (CGContext context = UIGraphics.GetCurrentContext())
            {
                context.SetFillColor(Color.FromHex("#00feff").ToCGColor());
                context.FillRect(rect);
            }
            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return image;
        }


    }
}
