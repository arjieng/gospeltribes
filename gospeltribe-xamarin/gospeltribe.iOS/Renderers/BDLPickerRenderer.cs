﻿using System;
using gospeltribe;
using gospeltribe.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BDLPicker), typeof(BDLPickerRenderer))]
namespace gospeltribe.iOS.Renderers
{
	public class BDLPickerRenderer : PickerRenderer
    {
        public BDLPickerRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UITextBorderStyle.None;
            var element = (BDLPicker)this.Element;

            //if(this.Control != null && this.Element != null && !string.IsNullOrEmpty(element.Image)){
            //    var downArrow = UIImage.FromBundle(element.Image);
            //    Control.RightViewMode = UITextFieldViewMode.Always;
            //    Control.RightView = new UIImageView(downArrow);
            //}

            //Control.AttributedPlaceholder = new Foundation.NSAttributedString(Control.Placeholder, foregroundColor: UIColor.White);
        }
    }
}
