﻿using System;
using System.Threading;
using AddressBook;
using AddressBookUI;
using gospeltribe;
using gospeltribe.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ProfilePage), typeof(ProfilePageRenderer))]
namespace gospeltribe.iOS.Renderers
{
    public class ProfilePageRenderer : PageRenderer
    {
        ProfilePage _page;
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                _page = (ProfilePage)Element;
                _page.CreateContact += _page_CreateContact;
            }
        }


        void _page_CreateContact(UserModel user)
        {
            
            ABNewPersonViewController _newPersonController = new ABNewPersonViewController();

            var person = new ABPerson();
            person.FirstName = user.first_name;
            person.LastName = user.last_name;
            person.Nickname = user.username;
            ABMutableMultiValue<string> phones = new ABMutableStringMultiValue();
            phones.Add(user.phone_number, ABPersonPhoneLabel.Mobile);

            person.SetPhones(phones);

            _newPersonController.DisplayedPerson = person;

            _newPersonController.NewPersonComplete += (object sender, ABNewPersonCompleteEventArgs e) =>
            {
                
                NavigationPage.SetHasNavigationBar(_page, false);
                NavigationController.PopViewController(false);
                if (e.Completed)
                {
                    throwToastMethod("Contact Saved");
                }
            };

            NavigationPage.SetHasNavigationBar(_page, true);
            NavigationController.PushViewController(_newPersonController, false);

        }


        private void throwToastMethod(string message)
        {
            var toast = UIAlertController.Create(null, message, UIAlertControllerStyle.Alert);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(toast, false, null);

            //Add Delay to dismiss the alert view without any user interaction
            new Timer((state) => InvokeOnMainThread(() => toast.DismissViewController(true, null)), null, 600, Timeout.Infinite);
        }

    }
}
