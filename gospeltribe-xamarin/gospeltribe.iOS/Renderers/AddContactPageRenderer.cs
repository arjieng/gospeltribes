﻿using System;
using AddressBook;
using AddressBookUI;
using CoreGraphics;
using gospeltribe;
using gospeltribe.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AddContactPage), typeof(AddContactPageRenderer))]
namespace gospeltribe.iOS.Renderers
{
    public class AddContactPageRenderer : PageRenderer
    {
        ABNewPersonViewController _newPersonController;
        UIButton _createContact;
        UILabel _contactName;

        public AddContactPageRenderer()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = "Create a Contact";
            View.BackgroundColor = UIColor.White;

            _createContact = UIButton.FromType(UIButtonType.RoundedRect);
            _createContact.Frame = new CGRect(10, 60, 300, 50);
            _createContact.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            _createContact.SetTitle("Create a Contact", UIControlState.Normal);
            _contactName = new UILabel { Frame = new CGRect(10, 120, 300, 50) };
            _contactName.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

            View.AddSubviews(_createContact, _contactName);

            _newPersonController = new ABNewPersonViewController();

            _createContact.TouchUpInside += (sender, e) => {

                var person = new ABPerson();
                person.FirstName = "John";
                person.LastName = "Doe";

                _newPersonController.DisplayedPerson = person;

                NavigationController.PushViewController(_newPersonController, true);
            };

            _newPersonController.NewPersonComplete += (object sender, ABNewPersonCompleteEventArgs e) => {

                if (e.Completed)
                {
                    _contactName.Text = String.Format("new contact: {0} {1}", e.Person.FirstName, e.Person.LastName);
                }
                else
                {
                    _contactName.Text = "cancelled";
                }

                NavigationController.PopViewController(true);
            };
        }

        public override void ViewDidUnload()
        {
            base.ViewDidUnload();

        }

        public override bool ShouldAutorotateToInterfaceOrientation(UIInterfaceOrientation toInterfaceOrientation)
        {
            // Return true for supported orientations
            return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
        }
    }
}
