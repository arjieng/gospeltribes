﻿using System;
using CoreGraphics;
using gospeltribe;
using gospeltribe.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTabbedPage), typeof(CustomTabbedPageRenderer))]
namespace gospeltribe.iOS
{
    public class CustomTabbedPageRenderer : TabbedRenderer
    {
        public CustomTabbedPageRenderer()
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            if (TabBar == null) return;
            if (TabBar.Items == null) return;

            TabBar.UnselectedItemTintColor = UIColor.Black;
            UITabBar.Appearance.SelectedImageTintColor = UIColor.Black;

            base.ViewWillAppear(animated);

            CGSize size = new CGSize(TabBar.Frame.Width / TabBar.Items.Length, TabBar.Frame.Height);
            UITabBar.Appearance.SelectionIndicatorImage = imageWithColor(size);
            //UITabBarItem.Appearance.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White }, UIControlState.Normal);
            //UITabBarItem.Appearance.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.Black }, UIControlState.Selected);
            foreach(UITabBarItem b in TabBar.Items){
                b.BadgeColor = UIColor.Red;
                b.BadgeValue = 1.ToString();
            }

        }

        public UIImage imageWithColor(CGSize size){
            CGRect rect = new CGRect(0, 0, size.Width, size.Height);
            UIGraphics.BeginImageContext(size);
            using(CGContext context =  UIGraphics.GetCurrentContext()){
                //context.SetFillColor(UIColor.Red.CGColor);
                context.SetFillColor(Xamarin.Forms.Color.FromHex("#00feff").ToCGColor());
                //context.SetFillColor(UIColor.FromRGB(19, 173, 151).CGColor);
                context.FillRect(rect);
            }
            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return image;
        }

        void UpdateItem(UITabBarItem item, string icon){
            if (item == null) return;
            try{
                item.Image = UIImage.FromBundle(icon);
                item.Image = item.Image.ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);

            }catch(Exception ex){
                Console.WriteLine("Unable to set selected icon " + ex);
            }
        }
    }
}
