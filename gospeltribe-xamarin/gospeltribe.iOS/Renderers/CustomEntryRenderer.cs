﻿using System;
using CoreGraphics;
using gospeltribe;
using gospeltribe.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace gospeltribe.iOS.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        float animatedDistance;
        CustomEntry customEntry;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            customEntry = (CustomEntry)this.Element;

            if (this.Control == null) return;
            if(e.NewElement != null){
                switch(customEntry.DesignType){
                    case DesignType.Flat:
                        Control.LeftView = new UIView(new CGRect(0f, 0f, 9f, 20f));
                        Control.LeftViewMode = UITextFieldViewMode.Always;
                        Control.KeyboardAppearance = UIKeyboardAppearance.Dark;
                        Control.ReturnKeyType = UIReturnKeyType.Done;
                        // Radius for the curves  
                        Control.Layer.CornerRadius = Convert.ToSingle(customEntry.CornerRadius);
                        // Thickness of the Border Color  
                        Control.Layer.BorderColor = customEntry.BorderColor.ToCGColor();
                        // Thickness of the Border Width  
                        Control.Layer.BorderWidth = customEntry.BorderWidth;
                        Control.ClipsToBounds = true;
                        break;
                    case DesignType.Materialize:
                        Control.BorderStyle = UITextBorderStyle.None;
                        break;
                }
                
                Control.SpellCheckingType = UITextSpellCheckingType.No;
                Control.AutocorrectionType = UITextAutocorrectionType.No;
                Control.AutocapitalizationType = UITextAutocapitalizationType.None;

                Control.EditingDidBegin += OnEntryDidEdit;
                Control.EditingDidEnd += OnEntryDidEnd;

                if (e.NewElement.StyleId == null)
                {
                    Control.TextAlignment = UITextAlignment.Left;
                }
                else if (e.NewElement.StyleId.Equals("C"))
                {
                    Control.TextAlignment = UITextAlignment.Center;
                }
                else if (e.NewElement.StyleId.Equals("LC"))
                {
                    Control.TextAlignment = UITextAlignment.Left;
                }

                if (customEntry.AutoCapitalization == AutoCapitalizationType.Words)
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
                }
                else if (customEntry.AutoCapitalization == AutoCapitalizationType.Sentences)
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
                }
                else if (customEntry.AutoCapitalization == AutoCapitalizationType.All)
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.AllCharacters;
                }
                else
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.None;
                }

                if (customEntry != null)
                {
                    SetReturnType(customEntry);

                    Control.ShouldReturn += (UITextField tf) =>
                    {
                        customEntry.InvokeCompleted();
                        return true;
                    };
                }
            }
        }

        private void OnEntryDidEnd(object sender, EventArgs e)
        {
            UIView parentView = getParentView();
            var viewFrame = parentView.Bounds;

            viewFrame.Y = 0.0f;

            BeginAnimations(null, (IntPtr)null);
            SetAnimationBeginsFromCurrentState(true);
            SetAnimationDuration(0.3);

            parentView.Frame = viewFrame;

            CommitAnimations();
        }

        private void OnEntryDidEdit(object sender, EventArgs e)
        {
            UIView parentWindow = getParentView();
            var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
            var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

            float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
            float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
            float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
            float heightFraction = numerator / denominator;

            if (heightFraction < 0.0)
            {
                heightFraction = 0.0f;
            }
            else if (heightFraction > 1.0)
            {
                heightFraction = 1.0f;
            }

            UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
            if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
            {
                if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((216.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(216.0f * heightFraction);
                }
            }
            else
            {
                if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(162.0f * heightFraction);
                }
            }

            var viewFrame = parentWindow.Frame;
            viewFrame.Y -= animatedDistance;

            BeginAnimations(null, (IntPtr)null);
            SetAnimationBeginsFromCurrentState(true);
            SetAnimationDuration(0.3);

            parentWindow.Frame = viewFrame;

            if(customEntry.IsSignup)
            {
                viewFrame.Y = -127;
                if (App.isIphoneX)
                    viewFrame.Y = -162;
                
                parentWindow.Frame = viewFrame;
            }

            CommitAnimations();
        }

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ReturnKeyType = UIReturnKeyType.Go;
                    break;
                case ReturnType.Next:
                    Control.ReturnKeyType = UIReturnKeyType.Next;
                    break;
                case ReturnType.Send:
                    Control.ReturnKeyType = UIReturnKeyType.Send;
                    break;
                case ReturnType.Search:
                    Control.ReturnKeyType = UIReturnKeyType.Search;
                    break;
                case ReturnType.Done:
                    Control.ReturnKeyType = UIReturnKeyType.Done;
                    break;
                default:
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                    break;
            }
        }

        UIView getParentView()
        {
            UIView view = Control.Superview;

            while (view != null && !(view is UIWindow))
            {
                view = view.Superview;
            }

            return view;
        }
    }
}
