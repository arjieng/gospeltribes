﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Foundation;
using Newtonsoft.Json.Linq;
using Shiny.Jobs;
using UIKit;

namespace gospeltribe.iOS
{
    public class FetchUnreadMessagesJob : IJob, IAdvanceRestConnector
    {
        AdvanceRestService restService;
        CancellationTokenSource cts;

        public FetchUnreadMessagesJob()
        {
            this.restService = new AdvanceRestService { WebServiceDelegate = this };
        }

        public async Task<bool> Run(JobInfo jobInfo, CancellationToken cancelToken)
        {
            bool result = true;
            await Task.Run(async () =>
            {
                try
                {
                    Console.WriteLine("JOB STARTED");
                    cts = new CancellationTokenSource();
                    await restService.GetRequest(Constants.GET_BADGES + "?user_id=" + DataClass.GetInstance.user.id + "&group_id=" + DataClass.GetInstance.GroupId + "&is_testing=false", cts.Token, 1);
                }
                catch (OperationCanceledException oce)
                {
                    result = false;
                    Console.WriteLine(oce.Message);
                }
                catch (TimeoutException toe)
                {
                    result = false;
                    Console.WriteLine(toe.Message);
                }
                catch (Exception ex)
                {
                    result = false;
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    cts = null;
                    Console.WriteLine("JOB ENDED");
                }
            });
            return result;
        }

        public void ReceiveJSONData(JObject jsonData, int wsType)
        {
            this.BeginInvokeOnMainThread(() =>
            {
                int count = int.Parse(jsonData["badge"]["inbox"].ToString()) +
                            int.Parse(jsonData["badge"]["prayer"].ToString()) +
                            int.Parse(jsonData["badge"]["event"].ToString()) +
                            int.Parse(jsonData["badge"]["story"].ToString()) +
                            int.Parse(jsonData["badge"]["member"].ToString());
                App.BadgeCounter = count;
                UIApplication.SharedApplication.ApplicationIconBadgeNumber = App.BadgeCounter;
            });
        }

        public void ReceiveTimeoutError(string title, string error, int wsType)
        {

        }

        public void BeginInvokeOnMainThread(Action action)
        {
            NSRunLoop.Main.BeginInvokeOnMainThread(action.Invoke);
        }
    }
}
