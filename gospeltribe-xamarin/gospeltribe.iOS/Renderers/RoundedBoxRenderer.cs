﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using gospeltribe;
using gospeltribe.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RoundedBox), typeof(RoundedBoxRenderer))]
namespace gospeltribe.iOS
{
    public class RoundedBoxRenderer : ViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);
            if (this.Element == null) return;
            this.Element.PropertyChanged += Element_PropertyChanged;
        }

        void Element_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            try
            {
                if (NativeView != null)
                {
                    NativeView.SetNeedsDisplay();
                    NativeView.SetNeedsLayout();
                }
            }
            catch (Exception ex)
            {
            }
        }

        public override void Draw(CoreGraphics.CGRect rect){
            base.Draw(rect);
            this.LayoutIfNeeded();

            RoundedBox rcv = (RoundedBox)Element;
            //rcv.HasShadow = false;    
            rcv.Padding = new Thickness(0, 0, 0, 0);

            //this.BackgroundColor = rcv.FillColor.ToUIColor();    
            this.ClipsToBounds = true;
            this.Layer.BackgroundColor = rcv.FillColor.ToCGColor();
            this.Layer.MasksToBounds = true;
            this.Layer.CornerRadius = (nfloat)rcv.CornerRadius;
            if (rcv.MakeCircle)
            {
                this.Layer.CornerRadius = (int)(Math.Min(Element.Width, Element.Height) / 2);
            }
            this.Layer.BorderWidth = 0;

            if (rcv.BorderWidth > 0 && rcv.BorderColor.A > 0.0)
            {
                this.Layer.BorderWidth = rcv.BorderWidth;
                this.Layer.BorderColor =
                    new UIKit.UIColor(
                    (nfloat)rcv.BorderColor.R,
                    (nfloat)rcv.BorderColor.G,
                    (nfloat)rcv.BorderColor.B,
                        (nfloat)rcv.BorderColor.A).CGColor;
            }
        }
    }
}
