﻿using System;
using System.ComponentModel;
using gospeltribe.iOS.Renderers;
using gospeltribe;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomFlowListView), typeof(CustomFlowListViewRenderer))]
namespace gospeltribe.iOS.Renderers
{
    public class CustomFlowListViewRenderer : ListViewRenderer
    {
        public CustomFlowListViewRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            if (Element != null)
            {
                Control.AlwaysBounceVertical = Element.IsPullToRefreshEnabled;
                Control.Bounces = Element.IsPullToRefreshEnabled;

            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            App.Log(e.PropertyName);
            if (e.PropertyName.Equals(nameof(Element.IsPullToRefreshEnabled)))
            {
                Control.AlwaysBounceVertical = Element.IsPullToRefreshEnabled;
                Control.Bounces = Element.IsPullToRefreshEnabled;
            }
        }
    }
}
