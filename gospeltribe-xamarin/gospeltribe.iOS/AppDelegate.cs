﻿using System;
using System.Text;
using AudioToolbox;
using Foundation;
using ImageCircle.Forms.Plugin.iOS;
using Newtonsoft.Json;
using Plugin.CrossPlatformTintedImage.iOS;
using Plugin.FirebasePushNotification;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Shiny;
using Shiny.Jobs;
using SQLite.Net.Platform.XamarinIOS;
using UIKit;
using UserNotifications;
using Xamarin.Forms;

namespace gospeltribe.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate //, IAdvanceRestConnector
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            if (UIScreen.MainScreen.NativeBounds.Height == 2436)
            {
                App.isIphoneX = true;
            }
            string esv = FileAccessHelper.GetLocalFilePath("esv.sqlite3");
            string hcsb = FileAccessHelper.GetLocalFilePath("hcsb.sqlite3");
            string kjv = FileAccessHelper.GetLocalFilePath("kjv.sqlite3");
            string nkjv = FileAccessHelper.GetLocalFilePath("nkjv.sqlite3");
            string nlt = FileAccessHelper.GetLocalFilePath("nlt.sqlite3");
            string niv = FileAccessHelper.GetLocalFilePath("niv2011.sqlite3");

            //Scaling
            App.screenWidth = (float)UIScreen.MainScreen.Bounds.Width;
            App.screenHeight = (float)UIScreen.MainScreen.Bounds.Height;
            App.appScale = (float)UIScreen.MainScreen.Scale;

            global::Xamarin.Forms.Forms.Init();
            Xamarin.FormsGoogleMaps.Init("AIzaSyBWV4ntfc6-Tg7-ud7G-Cg9a8btbqZLx18");
            iOSShinyHost.Init();
            XamForms.Controls.iOS.Calendar.Init();
            Rg.Plugins.Popup.Popup.Init();
            ImageCircleRenderer.Init();
            TintedImageRenderer.Init();
            Websockets.Ios.WebsocketConnection.Link();

            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (approved, err) => {
                    // Handle approval
                });

                UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();
            }
            else
            {
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }


            LoadApplication(new App(esv, hcsb, kjv, nkjv, nlt, new SQLitePlatformIOS(), niv));
            Plugin.InputKit.Platforms.iOS.Config.Init();
            DataClass dataClass = DataClass.GetInstance;
            if (!string.IsNullOrEmpty(dataClass.token) && !string.IsNullOrEmpty(dataClass.clientId) && !string.IsNullOrEmpty(dataClass.uid))
            {
                var job = new JobInfo { Identifier = "FetchUnreadMessages", Type = typeof(FetchUnreadMessagesJob), BatteryNotLow = true, DeviceCharging = false, Repeat = true, RequiredInternetAccess = InternetAccess.Any };
                ShinyHost.Resolve<IJobManager>().Schedule(job);
            }

            FirebasePushNotificationManager.Initialize(options, true);
            FirebasePushNotificationManager.CurrentNotificationPresentationOption = UNNotificationPresentationOptions.Alert & UNNotificationPresentationOptions.Sound;


            //Device.BeginInvokeOnMainThread(async () =>
            //{
            //    if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            //    {
            //        App.Log("FROM CHECK SYSteM VErSION 8.0");
            //        UIUserNotificationType types = UIApplication.SharedApplication.CurrentUserNotificationSettings.Types;

            //        App.Log($"\nIn MyApp.iOS.Implementations.NativeHelper_iOS.PushNotificationAlertsEnabledAsync() - Allowed Push Notification Types: {types}\n");

            //        if (types.HasFlag(UIUserNotificationType.Alert)) {
            //            App.Log("HAS ALERT NOTIFICATION");
            //        } //Here I only car about Alerts being enabled but you could check for all or none of them and act accordingly
            //        App.Log("=============================");
            //    }
            //    else
            //    {
            //        App.Log("NOT FROM CHECK SYSteM VErSION 8.0");
            //        UIRemoteNotificationType types = UIApplication.SharedApplication.EnabledRemoteNotificationTypes;

            //        App.Log($"\nIn MyApp.iOS.Implementations.NativeHelper_iOS.PushNotificationAlertsEnabledAsync() - Allowed Push Notification Types: {types}\n");

            //        if (types.HasFlag(UIRemoteNotificationType.Alert))
            //        {
            //            App.Log("HAS ALERT NOTIFICATION");
            //        } //Here I only car about Alerts being enabled but you could check for all or none of them and act accordingly
            //        App.Log("=============================");
            //    }
            //});
            return base.FinishedLaunching(app, options);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            FirebasePushNotificationManager.DidRegisterRemoteNotifications(deviceToken);
        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            FirebasePushNotificationManager.RemoteNotificationRegistrationFailed(error);
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
            {
                var sound = new SystemSound(1000);
                sound.PlaySystemSound();
            }

            if (userInfo["action"].ToString().Equals("message"))
                App.iOS_DidReceiveNotification(userInfo["action"].ToString(), userInfo["params"].ToString(), userInfo["chatroom_type"].ToString());
            else if (userInfo["action"].ToString().Equals("story"))
                App.iOS_DidReceiveNotification(userInfo["action"].ToString(), userInfo["group_media"].ToString(), "0");
            else
                App.iOS_DidReceiveNotification(userInfo["action"].ToString());


            completionHandler(UIBackgroundFetchResult.NewData);
        }

        void ViewAllFOnts()
        {
            var fontList = new StringBuilder();
            var familyNames = UIFont.FamilyNames;
            foreach (var familyName in familyNames)
            {
                fontList.Append(String.Format("Family: {0}\n", familyName));
                Console.WriteLine("Family: {0}\n", familyName);
                var fontNames = UIFont.FontNamesForFamilyName(familyName);
                foreach (var fontName in fontNames)
                {
                    Console.WriteLine("\tFont: {0}\n", fontName);
                    fontList.Append(String.Format("\tFont: {0}\n", fontName));
                }
            };
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            string[] arr = url.ToString().Split(new char[] { '&' });
            if (url.ToString().Contains("reset"))
            {
                string[] reset_token_array = arr[1].Split(new char[] { '=' });
                string reset_token = reset_token_array[1];
                MessagingCenter.Send(reset_token, "ResetPassword");
            }

            return false;
        }

        public override void PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
        {
            Shiny.Jobs.JobManager.OnBackgroundFetch(completionHandler);
        }
    }

    public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        #region Constructors
        public UserNotificationCenterDelegate()
        {
        }
        #endregion

        #region Override Methods
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            // Do something with the notification
            Console.WriteLine("Active Notification: {0}", notification);

            // Tell system to display the notification anyway or use
            // `None` to say we have handled the display locally.
            completionHandler(UNNotificationPresentationOptions.Alert);
        }
        #endregion
    }
}
