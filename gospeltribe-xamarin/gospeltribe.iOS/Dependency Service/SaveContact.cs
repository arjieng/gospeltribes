using System;
using System.Threading;
using AddressBook;
using AddressBookUI;
using Contacts;
using ContactsUI;
using Foundation;
using gospeltribe.iOS.DependencyService;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: Dependency(typeof(SaveContacts))]
namespace gospeltribe.iOS.DependencyService
{
    public class SaveContacts : UIViewController, iSaveContacts
    {
        public void SaveContact(string name, string number, ProfilePage profilePage)
        {

            // ======================================================================================= //

            ABNewPersonViewController _newPersonController = new ABNewPersonViewController();
            var person = new ABPerson();
            person.FirstName = "John";
            person.LastName = "Doe";
            _newPersonController.DisplayedPerson = person;

            _newPersonController.NewPersonComplete += (object sender, ABNewPersonCompleteEventArgs e) => {
                NavigationController.PopViewController(true);
            };

            var renderer = Platform.GetRenderer(profilePage);

            if(renderer == null){
                renderer = Platform.CreateRenderer(profilePage);
            }

            var rootController = renderer.ViewController;
            rootController.NavigationController.PushViewController(_newPersonController, true);
        }

        private void throwAlertMethod(string heading, string Body)
        {
            var alertController = UIAlertController.Create(heading, Body, UIAlertControllerStyle.Alert);
            alertController.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));

            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(alertController, true, null);
        }

        private void saveContactToStoreMethod(string name, string number)
        {
            var store = new CNContactStore();
            var contact = new CNMutableContact();
            var cellPhone = new CNLabeledValue<CNPhoneNumber>(CNLabelPhoneNumberKey.Mobile, new CNPhoneNumber(number));
            var phoneNumber = new[] { cellPhone };
            contact.PhoneNumbers = phoneNumber;
            contact.GivenName = name;
            var saveRequest = new CNSaveRequest();
            saveRequest.AddContact(contact, store.DefaultContainerIdentifier);

            NSError error;
            if (store.ExecuteSaveRequest(saveRequest, out error))
            {
                throwToastMethod("Contact Saved");
            }
            else
            {
                throwToastMethod("Error saving contact");
            }

        }

        private void throwToastMethod(string message)
        {
            var toast = UIAlertController.Create(null, message, UIAlertControllerStyle.Alert);
            UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(toast, false, null);

            //Add Delay to dismiss the alert view without any user interaction
            new Timer((state) => InvokeOnMainThread(() => toast.DismissViewController(true, null)), null, 600, Timeout.Infinite);
        }
    }
}
