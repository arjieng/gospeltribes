﻿using System;
using Badge.Plugin;
using gospeltribe.iOS.DependencyService;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppIcon))]
namespace gospeltribe.iOS.DependencyService
{
    public class AppIcon : iAppIcon
    {
        public void ChangeAppIcon()
        {
            CrossBadge.Current.SetBadge(App.BadgeCounter);
        }
    }
}