﻿using System;
using gospeltribe.iOS.DependencyService;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ClipBoard))]
namespace gospeltribe.iOS.DependencyService
{
    public class ClipBoard : IClipBoard
    {
        public string GetTextFromClipBoard() => UIPasteboard.General.GetValue("public.utf8-plain-text").ToString();
        public void SendTextToClipboard(string text) => UIPasteboard.General.String = text;
    }
}
