﻿using System;
using Foundation;
using gospeltribe.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(MapHelper))]
namespace gospeltribe.iOS
{
    public class MapHelper : IMapHeler
    {
        public void OpenMap(string url)
        {
            url = url.Replace(" ", "%20");
            if(UIApplication.SharedApplication.CanOpenUrl(new NSUrl(url))){
                UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
            }else{
                new UIAlertView("Error", "Maps is not supported on this device", null, "Ok").Show();
            }
        }
    }
}
