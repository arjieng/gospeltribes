﻿using System;
using gospeltribe.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(LogService))]
namespace gospeltribe.iOS
{
    public class LogService : ILogServices
    {
        public void Log(string log)
        {
            Console.WriteLine(log);
        }
    }
}
