﻿using System;
using System.IO;
using Foundation;

namespace gospeltribe.iOS
{
    public class FileAccessHelper
    {
        public static string GetLocalFilePath(string filename)
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            string dbPath = Path.Combine(libFolder, filename);

            CopyDatabaseIfNotExists(dbPath, filename);

            return dbPath;
        }

        private static void CopyDatabaseIfNotExists(string dbPath, string filename)
        {
            if (!File.Exists(dbPath))
            {
                string[] words = filename.Split('.');
                var existingDb = NSBundle.MainBundle.PathForResource(words[0], words[1]);
                File.Copy(existingDb, dbPath);

            }
        }
    }
}
