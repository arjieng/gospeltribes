var AppCalendar = function() {

    return {
        init: function(group_url, monthly_events_list) {
            this.initCalendar(group_url, monthly_events_list);
        },

        init_drag: function(el){
            var eventObject = {
                title: $.trim(el.text()), 
                event_list_id: el.data("event_list_id"),
                from_event_list_id: el.data("from_event_list_id")
            };

            el.data('eventObject', eventObject);
            el.draggable({
                zIndex: 999,
                revert: true, 
                revertDuration: 0 
            });
        },

        add_event: function(title, id) {
            title = title.length === 0 ? "Untitled Event" : title;
            var html = $('<div class="external-event label label-default" data-from_event_list_id="' + id + '" data-event_list_id="' + id + '">' + title + '</div>');
            jQuery('#event_box').append(html);
            this.init_drag(html);
        },

        initCalendar: function(group_url, monthly_events_list) {

            if (!jQuery().fullCalendar) {
                return;
            }

            var monthlyHashes = [];
            var weeklyHashes = [];

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if (App.isRTL()) {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    // h = {
                    //     right: 'title, prev, next',
                    //     center: '',
                    //     left: 'agendaDay, agendaWeek, month, today'
                    // };
                } else {
                    $('#calendar').removeClass("mobile");
                    // h = {
                    //     right: 'title',
                    //     center: '',
                    //     left: 'agendaDay, agendaWeek, month, today, prev,next'
                    // };
                }
            } else {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    // h = {
                    //     left: 'title, prev, next',
                    //     center: '',
                    //     right: 'today,month,agendaWeek,agendaDay'
                    // };
                } else {
                    $('#calendar').removeClass("mobile");
                    // h = {
                    //     left: 'title',
                    //     center: '',
                    //     right: 'prev,next,today,month,agendaWeek,agendaDay'
                    // };
                }
            }

            var initDrag = function(el) {
                var eventObject = {
                    title: $.trim(el.text()), 
                    event_list_id: el.data("event_list_id"),
                    from_event_list_id: el.data("from_event_list_id")
                };

                el.data('eventObject', eventObject);
                el.draggable({
                    zIndex: 999,
                    revert: true, 
                    revertDuration: 0 
                });
            };



            // var addEvent = function(title) {
            //     title = title.length === 0 ? "Untitled Event" : title;
            //     var html = $('<div class="external-event label label-default">' + title + '</div>');
            //     jQuery('#event_box').append(html);
            //     initDrag(html);
            // };

            $('#external-events div.external-event').each(function() {
                initDrag($(this));
            });

            // $('#event_add').unbind('click').click(function() {
            //     var title = $('#event_title').val();
            //     addEvent(title);
            // });


            function formatAMPM(date) {
              var hours = date.getHours();
              var minutes = date.getMinutes();
              var ampm = hours >= 12 ? 'pm' : 'am';
              hours = hours % 12;
              hours = hours ? hours : 12; // the hour '0' should be '12'
              minutes = minutes < 10 ? '0'+minutes : minutes;
              var strTime = hours + ':' + minutes + ' ' + ampm;
              return strTime;
            }

            $('#event_box').html("");
            
            $('#calendar').fullCalendar('destroy'); 
            $('#calendar').fullCalendar({ 
                header: h,
                displayEventTime: false,
                defaultView: 'month',  
                slotMinutes: 15,
                editable: true,
                droppable: true, 
                drop: function(date, allDay) {
                    var originalEventObject = $(this).data('eventObject');
                    var copiedEventObject = $.extend({}, originalEventObject);

                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $(this).attr("data-class");
                    copiedEventObject.id = originalEventObject.id;
                    copiedEventObject.event_action = 0;
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                    event = copiedEventObject;
                    


                    $(".remove-item").attr("disabled", true);
                    $(".save-item").attr("disabled", true);  

                    $(".event_list_id").val(null);
                    $(".event_id").val(null);
                    $(".from_event_list_id").val(null);

                    $("#modalImage").attr("src", null);

                    $("#modalImage").attr("src", event.image);
                    
                    if(event.hasOwnProperty("event_list_id")){
                        $(".event_list_id").val(event.event_list_id);
                    }

                    if(event.hasOwnProperty("from_event_list_id")){
                        $(".from_event_list_id").val(event.from_event_list_id);
                    }
                    

                    if(event.hasOwnProperty("id")){
                        $(".event_id").val(event.id);
                    }

                    var d = event.start._d.getFullYear() + "-" + (event.start._d.getMonth() + 1) + "-" + event.start._d.getDate() + " 00:00:00";
                    $(".start_date").val(d);
                    $(".end_date").val(d);                    

                    $(".event_title").val(event.title);
                    $(".event_details").val(event.description);
                    $('#share_to').find("option").remove();
                    $("#share_to").selectpicker("refresh");
                    
                    $.ajax({
                        url: group_url,
                        method: "post",
                        data: { from_event_list_id: event.from_event_list_id },
                        success: function(e){

                            e["strings"].forEach(function(value){
                                $('#share_to').append(value);
                            });
                            $("#share_to").selectpicker("refresh");

                            $(".remove-item").attr("disabled", false);
                            $(".save-item").attr("disabled", false); 

                            $('.save-item').click();
                        }
                    });
                },
                eventClick: function(event) {

                    console.log("========================");
                    console.log(event.orig_date);
                    console.log(event.this_end);
                    console.log(event.start._d);
                    console.log("========================");

                    $(".remove-item").attr("disabled", true);
                    $(".save-item").attr("disabled", true);  
                    $(".repeat-checkbox").prop("checked", false);
                    
                    $(".repeat_days").val("None");
                    $(".repeat").val("None");
                    
                    $(".event_list_id").val(null);
                    $(".event_id").val(null);
                    $(".from_event_list_id").val(null);

                    $("#modalImage").attr("src", null);

                    $("#modalImage").attr("src", event.image);
                    
                    if(event.hasOwnProperty("event_list_id")){
                        $(".event_list_id").val(event.event_list_id);
                    }

                    if(event.r != "None"){
                        $(".repeat").val(event.r);
                        $(".repeat-checkbox").prop("checked", true);
                        $("#repeat-dropdown-container").css("display", "block");

                        $('#repeat-dropdown').find("option").remove();
                        $("#repeat-dropdown").selectpicker("refresh");
                        var src = ["Monthly", "Weekly"];
                        src.forEach(function(i){
                            $("#repeat-dropdown").append("<option " + (i == event.r ? "selected" : "") + ">" + i + "</option>");
                        });
                        $("#repeat-dropdown").selectpicker("refresh");

                        
                        if(event.r == "Weekly"){
                            $(".repeat_days").val(event.rd);
                            $("#repeat_days_container").css("display", "block");

                            var rdd = event.rd.split(",");
                            var src2 = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                            
                            $("#repeat_days_selector_id").find("option").remove();  
                            $("#repeat_days_selector_id").selectpicker("refresh");
                            
                            src2.forEach(function(i){
                                $("#repeat_days_selector_id").append("<option value=\"" + i + "\" " + (rdd.includes(i) ? "selected=\"true\"" : "") + ">" + i + "</option>");
                            }); 
                            
                            $("#repeat_days_selector_id").selectpicker("refresh");
                        }
                    }

                    if(event.hasOwnProperty("from_event_list_id")){
                        $(".from_event_list_id").val(event.from_event_list_id);
                    }

                    $('#color-picker-subtitle').find("option").remove();
                    $("#color-picker-subtitle").selectpicker("refresh");

                    $(".js-data-example-ajax").html("<option selected='true'>" + event.location + "</option>");

                    if(event.hasOwnProperty("id")){
                        $(".event_id").val(event.id);
                    }
                

                    if(event.r != "None"){
                        var d = event.orig_date.getFullYear() + "-" + (event.orig_date.getMonth() + 1) + "-" + event.orig_date.getDate() + " " + event.orig_date.getHours() + ":" + event.orig_date.getMinutes() + ":" + event.orig_date.getSeconds();
                        var ed = event.orig_date.getFullYear() + "-" + (event.orig_date.getMonth() + 1) + "-" + event.orig_date.getDate() + " " + event.this_end.getHours() + ":" + event.this_end.getMinutes() + ":" + event.this_end.getSeconds();

                        $(".start_date").val(d.toString());
                        $(".end_date").val(ed.toString());
                    }else{
                        var d = event.start._d.getFullYear() + "-" + (event.start._d.getMonth() + 1) + "-" + event.start._d.getDate() + " " + event.orig_date.getHours() + ":" + event.orig_date.getMinutes() + ":" + event.orig_date.getSeconds();
                        var ed = event.start._d.getFullYear() + "-" + (event.start._d.getMonth() + 1) + "-" + event.start._d.getDate() + " " + event.this_end.getHours() + ":" + event.this_end.getMinutes() + ":" + event.this_end.getSeconds();
                        
                        $(".start_date").val(d.toString());
                        $(".end_date").val(ed.toString());
                    }

                    if(+(new Date($(".start_date").val())) != +(new Date($(".end_date").val()))){
                        $(".add-end-time-checkbox").prop("checked", true);
                        $(".end-time-container").css('display', 'block');
                    }



                    $('.timepicker-1').timepicker('setTime', formatAMPM(event.orig_date));
                    $('.timepicker-2').timepicker('setTime', formatAMPM(event.this_end));

                    $(".event_title").val(event.title);
                    $(".event_details").val(event.description);
                    $('#share_to').find("option").remove();
                    $("#share_to").selectpicker("refresh");

                    $.ajax({
                        url: group_url,
                        method: "post",
                        data: { from_event_list_id: event.from_event_list_id },
                        success: function(e){
                            var src = ["Red", "Orange", "Yellow", "Aqua", "Pink", "Blue", "Green", "Fuchsia", "Violet", "Brown"];
                            src.forEach(function(item){
                                $("#color-picker-subtitle").append("<option " + (item == event.subtitle ? "selected" : "") + ">" + item + "</option>");
                            });
                            $("#color-picker-subtitle").selectpicker("refresh");


                            e["strings"].forEach(function(value){
                                $('#share_to').append(value);
                            });
                            
                            $("#share_to").selectpicker("refresh");

                            $(".remove-item").attr("disabled", false);
                            $(".save-item").attr("disabled", false); 
                        }
                    });                    


                    $('#basic').modal();
                    return false;
                },
                eventDrop: function(event, delta, revertFunc){
                    $(".remove-item").attr("disabled", true);
                    $(".save-item").attr("disabled", true);  

                    $(".event_list_id").val(null);
                    $(".event_id").val(null);
                    $(".from_event_list_id").val(null);

                    $("#modalImage").attr("src", null);

                    $("#modalImage").attr("src", event.image);
                    
                    if(event.hasOwnProperty("event_list_id")){
                        $(".event_list_id").val(event.event_list_id);
                    }

                    if(event.hasOwnProperty("from_event_list_id")){
                        $(".from_event_list_id").val(event.from_event_list_id);
                    }

                    if(event.r != "None"){
                        $(".repeat").val(event.r);
                        
                        if(event.r == "Weekly"){
                            $(".repeat_days").val(event.rd);
                        }
                    }



                    $('#color-picker-subtitle').find("option").remove();
                    $("#color-picker-subtitle").selectpicker("refresh");

                    $(".js-data-example-ajax").html("<option selected='true'>" + event.location + "</option>");
                    
                    var src = ["Red", "Orange", "Yellow", "Aqua", "Pink", "Blue", "Green", "Fuchsia", "Violet", "Brown"];
                    src.forEach(function(item){
                        $("#color-picker-subtitle").append("<option " + (item == event.subtitle ? "selected" : "") + ">" + item + "</option>");
                    });
                    $("#color-picker-subtitle").selectpicker("refresh");

                    if(event.hasOwnProperty("id")){
                        $(".event_id").val(event.id);
                    }

                    $(".start_date").val(event.start._d.getFullYear() + "-" + (event.start._d.getMonth() + 1) + "-" + event.start._d.getDate() + " " + event.orig_date.getHours() + ":" + event.orig_date.getMinutes() + ":" + event.orig_date.getSeconds());
                    $(".end_date").val(event.start._d.getFullYear() + "-" + (event.start._d.getMonth() + 1) + "-" + event.start._d.getDate() + " " + event.this_end.getHours() + ":" + event.this_end.getMinutes() + ":" + event.this_end.getSeconds());

                    $(".event_title").val(event.title);
                    $(".event_details").val(event.description);
                    $('#share_to').find("option").remove();
                    $("#share_to").selectpicker("refresh");

                    $.ajax({
                        url: group_url,
                        method: "post",
                        data: { from_event_list_id: event.from_event_list_id },
                        success: function(e){

                            e["strings"].forEach(function(value){
                                $('#share_to').append(value);
                            });
                            $("#share_to").selectpicker("refresh");

                            $(".remove-item").attr("disabled", false);
                            $(".save-item").attr("disabled", false); 

                            $('.save-item').click();
                            // $(".modal").modal('show');
                        }
                    });
                }, dayRender: function(date, cell){
                    monthly_events_list.forEach(function(event){
                        if(new Date() <= date._d){
                            switch(event.repeat){
                                case "Monthly":
                                    if(event.start.getDate() == date._d.getDate()){
                                        var eventHash = SHA256(event.id + date._d);
                                        if(!(eventHash in monthlyHashes)){
                                            $("#calendar").fullCalendar('renderEvent', { id: event.id, title: event.title, description: event.description, start: date._d, end: date._d, orig_date: event.orig_date, backgroundColor: event.backgroundColor, image: event.image, from_event_list_id: event.from_event_list_id, location: event.location, subtitle: event.subtitle, r: event.r, rd: event.rd, this_end: event.this_end }, true);
                                            monthlyHashes[eventHash] = true;
                                        }
                                    }
                                    break;
                                case "Weekly":
                                    var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                                    var repeat_days = event.repeat_days.split(",");
                                    if(repeat_days.includes(weekdays[date._d.getDay()])){
                                        var eventHash = SHA256(event.id + date._d);
                                        if(!(eventHash in weeklyHashes)){
                                            $("#calendar").fullCalendar('renderEvent', { id: event.id, title: event.title, description: event.description, start: date._d, end: date._d, orig_date: event.orig_date, backgroundColor: event.backgroundColor, image: event.image, from_event_list_id: event.from_event_list_id, location: event.location, subtitle: event.subtitle, r: event.r, rd: event.rd, this_end: event.this_end }, true);
                                            weeklyHashes[eventHash] = true;
                                        }
                                    }
                                    break;

                            }
                        }
                    });
                }

                // viewRender: viewRender_function
                // ,
                // events: [{
                //     title: 'All Day Event',
                //     start: new Date(y, m, 1),
                //     backgroundColor: App.getBrandColor('yellow')
                // }, {
                //     title: 'Long Event',
                //     start: new Date(y, m, d - 5),
                //     end: new Date(y, m, d - 2),
                //     backgroundColor: App.getBrandColor('green')
                // }, {
                //     title: 'Repeating Event',
                //     start: new Date(y, m, d - 3, 16, 0),
                //     allDay: false,
                //     backgroundColor: App.getBrandColor('red')
                // }, {
                //     title: 'Repeating Event',
                //     start: new Date(y, m, d + 4, 16, 0),
                //     allDay: false,
                //     backgroundColor: App.getBrandColor('green')
                // }, {
                //     title: 'Meeting',
                //     start: new Date(y, m, d, 10, 30),
                //     allDay: false,
                // }, {
                //     title: 'Lunch',
                //     start: new Date(y, m, d, 12, 0),
                //     end: new Date(y, m, d, 14, 0),
                //     backgroundColor: App.getBrandColor('grey'),
                //     allDay: false,
                // }, {
                //     title: 'Birthday Party',
                //     start: new Date(y, m, d + 1, 19, 0),
                //     end: new Date(y, m, d + 1, 22, 30),
                //     backgroundColor: App.getBrandColor('purple'),
                //     allDay: false,
                // }, {
                //     title: 'Click for Google',
                //     start: new Date(y, m, 28),
                //     end: new Date(y, m, 29),
                //     backgroundColor: App.getBrandColor('yellow'),
                //     url: 'http://google.com/',
                //     id: 1000,
                // }]
            });

        }

    };

}();

jQuery(document).ready(function() {  
   // AppCalendar.init();
});