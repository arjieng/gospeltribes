require 'faye'
require 'eventmachine'
require 'logger'
require 'open-uri'
require 'net/http'
require 'json'
class ServerTest

  def initialize()
    @logger = Logger.new STDOUT
    @logger.level = Logger::DEBUG
    begin
      @client = Faye::Client.new('tcp://107.172.143.213:9292/faye')
    rescue
      @logger.error "Error initializing Faye client"
      exit
    end
  end

  def run
    EM.run {
      g = @client.subscribe("/tesst") do |message|
        # if message["action"] == "receive_message"
        #   puts "#{message.inspect} MESSAGE HERE"
        # end
      end

      # begin
      #   message = {:channel => "/group/1/chatrooms", :data => {hello: 1}, :ext => {hello: 2}}
      #   uri = URI.parse("http://192.168.1.42:9292/faye")
      #   Net::HTTP.post_form(uri, :message => message.to_json)
      # rescue => e
      #   puts "#{e.inspect}"
      # end

      
      g.callback do |message|
        @client.publish "/tesst", {action: "send_message", data: { user_id: 39, chatroom_id: 9, body: "This is from console" }}
      end





    } 
  end
end

if __FILE__ == $0
  if ARGV.count < 0
    puts "Usage: ruby server_test.rb <user_id>"
    exit
  end

  user_id= ARGV[0]

  client = ServerTest.new
  client.run
end