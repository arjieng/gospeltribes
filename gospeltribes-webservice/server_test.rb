require_relative 'config/environment.rb'
class ServerTest
  def initialize
  end

  def get_time_remaining
  	t = DateTime.now.to_date.to_s + " 18:30:00 +0800"
  	timr = (((t.to_time - Time.now)/60).to_i + 1).to_i
  	tihr = ((((t.to_time - Time.now)/60).to_i + 1)/60).to_f.ceil
  	puts "#{tihr} hour/s and #{(timr-(tihr.to_i*60))} minute/s remaining"
  	# puts "#{timr} minutes remaining"
  end

  def get_days_remaining
    @count = 0
	@not_include = 0
	my_days = [1, 2, 3, 4, 5]
	input = [(print 'Contract End Date: '), gets.rstrip][1]
	# October 1, 2019
	((DateTime.now.to_date)..(input.to_date)).to_a.select { |k| my_days.include?(k.wday) ? (@count += 1) : @not_include += 1 }

	puts "#{@count} Working days (Holidays not included)"
	puts "#{@not_include} Sundays and Saturdays"
  end

end
# client_id
# 538614263840.684400870899
# client_secret
# 679e886d3f9a32d8de4ca2060832c01e
# signing_secret
# 1f454e86b72166a87e29e812ad06c057
# app_id
# AL4BSRLSF
# verification_token
# 76PVY2Q2OlzozYxYCoDG5u0b

if __FILE__ == $0
  client = ServerTest.new
  puts "1 - Get remaining time"
  puts "2 - Get remaining days"

  input = [(print 'Select option: '), gets.rstrip][1]
  case input.to_i
  when 1
  	client.get_time_remaining
  when 2
  	client.get_days_remaining
  else
	puts "Selected option invalid"
  end
end