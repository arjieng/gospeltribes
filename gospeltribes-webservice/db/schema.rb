# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_21_055512) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attendees", force: :cascade do |t|
    t.bigint "event_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_attendees_on_event_id"
    t.index ["user_id"], name: "index_attendees_on_user_id"
  end

  create_table "chatroom_messages", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "chatroom_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "image"
    t.string "body"
    t.datetime "time_sent"
    t.boolean "from_admin", default: false
    t.integer "counter", default: 0
    t.index ["chatroom_id"], name: "index_chatroom_messages_on_chatroom_id"
    t.index ["user_id"], name: "index_chatroom_messages_on_user_id"
  end

  create_table "chatroom_messages_likes", force: :cascade do |t|
    t.bigint "chatroom_message_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chatroom_message_id"], name: "index_chatroom_messages_likes_on_chatroom_message_id"
    t.index ["user_id"], name: "index_chatroom_messages_likes_on_user_id"
  end

  create_table "chatrooms", force: :cascade do |t|
    t.bigint "group_id"
    t.string "chatroom_name"
    t.json "chatroom_image"
    t.integer "chatroom_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_chatrooms_on_group_id"
  end

  create_table "event_lists", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "group_id"
    t.string "event_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_event_lists_on_group_id"
    t.index ["user_id"], name: "index_event_lists_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.bigint "group_id"
    t.string "title"
    t.string "details"
    t.string "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "image"
    t.datetime "end_date"
    t.datetime "start_date"
    t.bigint "from_event_list"
    t.string "subtitle"
    t.string "location"
    t.string "repeat", default: "None"
    t.string "repeat_days", default: "None"
    t.index ["group_id"], name: "index_events_on_group_id"
  end

  create_table "group_creators", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_group_creators_on_group_id"
    t.index ["user_id"], name: "index_group_creators_on_user_id"
  end

  create_table "group_media", force: :cascade do |t|
    t.bigint "group_id"
    t.bigint "chatroom_message_id"
    t.bigint "user_id"
    t.json "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.boolean "is_video", default: false
    t.json "video_name"
    t.string "content_type"
    t.index ["chatroom_message_id"], name: "index_group_media_on_chatroom_message_id"
    t.index ["group_id"], name: "index_group_media_on_group_id"
    t.index ["user_id"], name: "index_group_media_on_user_id"
  end

  create_table "group_members", force: :cascade do |t|
    t.bigint "group_id"
    t.bigint "user_id"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_group_members_on_group_id"
    t.index ["user_id"], name: "index_group_members_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "group_code"
    t.string "group_name"
    t.json "group_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "prayer_comments", force: :cascade do |t|
    t.bigint "prayer_id"
    t.bigint "user_id"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prayer_id"], name: "index_prayer_comments_on_prayer_id"
    t.index ["user_id"], name: "index_prayer_comments_on_user_id"
  end

  create_table "prayers", force: :cascade do |t|
    t.bigint "user_id"
    t.string "subject"
    t.string "details"
    t.string "answered_details"
    t.integer "is_answered"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "share_to"
    t.boolean "is_hidden"
    t.integer "group_id"
    t.boolean "from_admin", default: false
    t.integer "counter", default: 0
    t.index ["group_id"], name: "index_group_id"
    t.index ["user_id"], name: "index_prayers_on_user_id"
  end

  create_table "push_notification_tokens", force: :cascade do |t|
    t.bigint "user_id"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "phone_type"
    t.index ["user_id"], name: "index_push_notification_tokens_on_user_id"
  end

  create_table "read_statuses", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "chatroom_id"
    t.bigint "last_read_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chatroom_id"], name: "index_read_statuses_on_chatroom_id"
    t.index ["user_id"], name: "index_read_statuses_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "group_id"
    t.boolean "inbox", default: true
    t.boolean "prayer", default: true
    t.boolean "event", default: true
    t.boolean "story", default: true
    t.boolean "member", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_settings_on_group_id"
    t.index ["user_id"], name: "index_settings_on_user_id"
  end

  create_table "study_module_comments", force: :cascade do |t|
    t.bigint "study_module_id"
    t.bigint "user_id"
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["study_module_id"], name: "index_study_module_comments_on_study_module_id"
    t.index ["user_id"], name: "index_study_module_comments_on_user_id"
  end

  create_table "study_modules", force: :cascade do |t|
    t.bigint "group_id"
    t.string "human"
    t.integer "chapter"
    t.integer "verse_num_from"
    t.integer "verse_num_to"
    t.string "verse"
    t.string "bible_version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "question"
    t.string "link"
    t.json "files"
    t.index ["group_id"], name: "index_study_modules_on_group_id"
  end

  create_table "user_badges", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "group_id"
    t.integer "inbox", default: 0
    t.integer "prayer", default: 0
    t.integer "event", default: 0
    t.integer "story", default: 0
    t.integer "member", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_user_badges_on_group_id"
    t.index ["user_id"], name: "index_user_badges_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean "allow_password_change", default: false
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "first_name"
    t.string "last_name"
    t.string "address"
    t.string "phone_number"
    t.string "birth_date"
    t.string "gender"
    t.string "username"
    t.string "role"
    t.json "avatar"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "udid"
    t.integer "current_group_id"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

end
