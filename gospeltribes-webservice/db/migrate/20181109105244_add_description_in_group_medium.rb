class AddDescriptionInGroupMedium < ActiveRecord::Migration[5.2]
  def up
  	add_column :group_media, :description, :text
  end
  
  def down
  	remove_column :group_media, :description
  end
end
