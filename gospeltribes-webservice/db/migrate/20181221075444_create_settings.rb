class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
	  t.belongs_to :user
	  t.belongs_to :group
	  t.boolean :inbox, default: true
	  t.boolean :prayer, default: true
	  t.boolean :event, default: true
	  t.boolean :story, default: true
	  t.boolean :member, default: true
      t.timestamps
    end
  end
end
