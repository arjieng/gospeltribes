class ChangeImageStringToJsonInChatroom < ActiveRecord::Migration[5.2]
  def up
  	change_table :chatrooms do |t|
  		t.change :chatroom_image, 'json USING CAST(chatroom_image as json)'
  	end
  end

  def down
  	change_table :chatrooms do |t|
  		t.change :chatroom_image, :string
  	end
  end
end
