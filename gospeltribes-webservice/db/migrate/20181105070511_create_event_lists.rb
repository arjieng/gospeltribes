class CreateEventLists < ActiveRecord::Migration[5.2]
  def change
    create_table :event_lists do |t|
	  t.belongs_to :user
	  t.belongs_to :group
	  t.string :event_name
      t.timestamps
    end
  end
end
