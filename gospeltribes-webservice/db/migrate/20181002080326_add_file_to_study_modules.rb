class AddFileToStudyModules < ActiveRecord::Migration[5.2]
  def up
    add_column :study_modules, :files, :json
  end
  def down
  	remove_column :study_modules, :files
  end
end
