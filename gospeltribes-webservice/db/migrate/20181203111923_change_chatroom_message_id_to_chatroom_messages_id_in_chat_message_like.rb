class ChangeChatroomMessageIdToChatroomMessagesIdInChatMessageLike < ActiveRecord::Migration[5.2]
  def up
  	rename_column :chat_message_likes, :chatroom_message_id, :chatroom_messages_id
  end

  def down
  	rename_column :chat_message_likes, :chatroom_messages_id, :chatroom_message_id
  end
end
