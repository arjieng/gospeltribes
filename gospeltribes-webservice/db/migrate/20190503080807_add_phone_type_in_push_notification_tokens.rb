class AddPhoneTypeInPushNotificationTokens < ActiveRecord::Migration[5.2]
  def up
  	add_column :push_notification_tokens, :phone_type, :integer
  end
  
  def down
  	remove_column :push_notification_tokens, :phone_type
  end
end
