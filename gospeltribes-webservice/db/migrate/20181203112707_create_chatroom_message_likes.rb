class CreateChatroomMessageLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :chatroom_message_likes do |t|
	  t.belongs_to :chatroom_messages
	  t.belongs_to :user
      t.timestamps
    end
  end
end
