class ChangeVideoNameTypeToJson < ActiveRecord::Migration[5.2]
  def up
  	change_table :group_media do |t|
  		t.change :video_name, 'json USING CAST(video_name as json)'
  	end
  end

  def down
  	change_table :group_media do |t|
  		t.change :video_name, :string
  	end
  end
end
