class AddFromAdminAndCounterToPrayers < ActiveRecord::Migration[5.2]
	def up
		add_column :prayers, :from_admin, :boolean, :default => false
		add_column :prayers, :counter, :integer, :default => 0
	end

	def down
		remove_column :prayers, :from_admin
  		remove_column :chatroom_messages, :counter
	end
end
