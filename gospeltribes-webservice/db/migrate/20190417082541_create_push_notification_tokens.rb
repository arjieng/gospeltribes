class CreatePushNotificationTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :push_notification_tokens do |t|
	  t.belongs_to :user
	  t.string :token
      t.timestamps
    end
  end
end
