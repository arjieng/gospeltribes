class DropTableChatMessageLike < ActiveRecord::Migration[5.2]
  def up
    drop_table :chat_message_likes
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
