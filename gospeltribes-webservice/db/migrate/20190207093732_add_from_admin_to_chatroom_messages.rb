class AddFromAdminToChatroomMessages < ActiveRecord::Migration[5.2]
  def up
  	add_column :chatroom_messages, :from_admin, :boolean, :default => false
  end
  
  def down
  	remove_column :chatroom_messages, :from_admin
  end
end
