class DeleteChatroomMessageLike < ActiveRecord::Migration[5.2]
  def up
    drop_table :chatroom_message_likes
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
