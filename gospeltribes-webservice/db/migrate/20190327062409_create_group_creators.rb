class CreateGroupCreators < ActiveRecord::Migration[5.2]
  def change
    create_table :group_creators do |t|
	  t.belongs_to :user
	  t.belongs_to :group
      t.timestamps
    end
  end
end
