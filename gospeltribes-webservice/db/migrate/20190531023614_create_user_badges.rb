class CreateUserBadges < ActiveRecord::Migration[5.2]
  def change
    create_table :user_badges do |t|
	  t.belongs_to :user
	  t.belongs_to :group
	  t.integer :inbox, default: 0
	  t.integer :prayer, default: 0
	  t.integer :event, default: 0
	  t.integer :story, default: 0
	  t.integer :member, default: 0
      t.timestamps
    end
  end
end