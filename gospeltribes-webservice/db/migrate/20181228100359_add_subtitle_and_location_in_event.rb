class AddSubtitleAndLocationInEvent < ActiveRecord::Migration[5.2]
  def up
  	add_column :events, :subtitle, :string
  	add_column :events, :location, :string
  end
  
  def down
  	remove_column :events, :subtitle
  	remove_column :events, :location
  end
end
