class AddColumnDateSentInChatroomMessage < ActiveRecord::Migration[5.2]
  def up
  	add_column :chatroom_messages, :time_sent, :datetime
  end
  
  def down
  	remove_column :chatroom_messages, :time_sent
  end
end
