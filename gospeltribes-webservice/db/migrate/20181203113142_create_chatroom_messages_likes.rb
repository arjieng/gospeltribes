class CreateChatroomMessagesLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :chatroom_messages_likes do |t|
  	  t.belongs_to :chatroom_message
	  t.belongs_to :user
      t.timestamps
    end
  end
end
