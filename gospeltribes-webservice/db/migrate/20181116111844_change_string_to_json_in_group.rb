class ChangeStringToJsonInGroup < ActiveRecord::Migration[5.2]
  def up
  	change_table :groups do |t|
  		t.change :group_image, 'json USING CAST(group_image as json)'
  	end
  end

  def down
  	change_table :groups do |t|
  		t.change :group_image, :string
  	end
  end
end
