class CreateGroupMedia < ActiveRecord::Migration[5.2]
  def change
    create_table :group_media do |t|
      t.belongs_to :group
      t.belongs_to :chatroom_message
      t.belongs_to :user
      t.json :image
      t.timestamps
    end
  end
end
