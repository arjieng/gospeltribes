class AddRepeatAndRepeatDaysInEvents < ActiveRecord::Migration[5.2]
  def up
  	add_column :events, :repeat, :string, default: "None"
  	add_column :events, :repeat_days, :string, default: "None"
  end
  
  def down
  	remove_column :events, :repeat
  	remove_column :events, :repeat_days
  end
end
