class ChangeChapterStringToIntegerInStudyModule < ActiveRecord::Migration[5.2]
  def up
  	change_table :study_modules do |t|
  		t.change :chapter, 'integer USING CAST(chapter as integer)'
  	end
  end

  def down
  	change_table :study_modules do |t|
  		t.change :chapter, :string
  	end
  end
end
