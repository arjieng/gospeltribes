class CreateStudyModules < ActiveRecord::Migration[5.2]
  def change
    create_table :study_modules do |t|
      t.belongs_to :groups
      t.string :human
      t.string :chapter
      t.integer :verse_num_from
      t.integer :verse_num_to
      t.string :verse
      t.string :bible_version
      t.timestamps
    end
  end
end
