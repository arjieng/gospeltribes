class CreateChatMessageLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :chat_message_likes do |t|
	  t.belongs_to :chatroom_message
	  t.belongs_to :user
      t.timestamps
    end
  end
end
