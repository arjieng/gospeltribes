class AddContentTypeInGroupMedia < ActiveRecord::Migration[5.2]
	def up
		add_column :group_media, :content_type, :string
	end

	def down
  		remove_column :group_media, :content_type
	end
end
