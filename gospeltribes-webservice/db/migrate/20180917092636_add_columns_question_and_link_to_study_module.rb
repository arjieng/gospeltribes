class AddColumnsQuestionAndLinkToStudyModule < ActiveRecord::Migration[5.2]
  def up
  	add_column :study_modules, :question, :string
  	add_column :study_modules, :link, :string
  end
  
  def down
  	remove_column :study_modules, :question
  	remove_column :study_modules, :link
  end
end
