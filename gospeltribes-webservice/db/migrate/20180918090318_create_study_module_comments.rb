class CreateStudyModuleComments < ActiveRecord::Migration[5.2]
  def change
    create_table :study_module_comments do |t|
      t.belongs_to :study_module
      t.belongs_to :user
      t.text :comment
      t.timestamps
    end
  end
end
