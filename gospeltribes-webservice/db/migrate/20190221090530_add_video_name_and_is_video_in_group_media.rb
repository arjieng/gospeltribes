class AddVideoNameAndIsVideoInGroupMedia < ActiveRecord::Migration[5.2]
	def up
		add_column :group_media, :is_video, :boolean, :default => false
		add_column :group_media, :video_name, :string
	end

	def down
		remove_column :group_media, :is_video
  		remove_column :group_media, :video_name
	end
end
