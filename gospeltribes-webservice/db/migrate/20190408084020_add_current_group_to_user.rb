class AddCurrentGroupToUser < ActiveRecord::Migration[5.2]
  def up
  	add_column :users, :current_group_id, :integer
  end
  
  def down
  	remove_column :users, :current_group_id
  end
end
