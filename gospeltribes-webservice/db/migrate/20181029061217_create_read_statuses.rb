class CreateReadStatuses < ActiveRecord::Migration[5.2]
  def change
    create_table :read_statuses do |t|
	  t.belongs_to :user
	  t.belongs_to :chatroom
	  t.bigint :last_read_id
      t.timestamps
    end
  end
end
