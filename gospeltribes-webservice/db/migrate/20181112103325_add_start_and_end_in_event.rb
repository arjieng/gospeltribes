class AddStartAndEndInEvent < ActiveRecord::Migration[5.2]
  def up
  	add_column :events, :end_date, :datetime
  	add_column :events, :start_date, :datetime
  end
  
  def down
  	remove_column :events, :end_date
  	remove_column :events, :start_date
  end
end
