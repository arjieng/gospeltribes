class AddCounterToChatroomMessages < ActiveRecord::Migration[5.2]
  def up
  	add_column :chatroom_messages, :counter, :integer, :default => 0
  end
  
  def down
  	remove_column :chatroom_messages, :counter
  end
end
