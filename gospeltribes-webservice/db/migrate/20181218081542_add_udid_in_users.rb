class AddUdidInUsers < ActiveRecord::Migration[5.2]
  def up
  	add_column :users, :udid, :string
  end
  
  def down
  	remove_column :users, :udid
  end
end
