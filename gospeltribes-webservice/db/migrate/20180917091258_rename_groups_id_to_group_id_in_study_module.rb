class RenameGroupsIdToGroupIdInStudyModule < ActiveRecord::Migration[5.2]
  def up
  	rename_column :study_modules, :groups_id, :group_id
  end

  def down
  	rename_column :study_modules, :group_id, :groups_id
  end
end
