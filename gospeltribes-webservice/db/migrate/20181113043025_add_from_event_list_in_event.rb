class AddFromEventListInEvent < ActiveRecord::Migration[5.2]
  def up
  	add_column :events, :from_event_list, :bigint
  end
  
  def down
  	remove_column :events, :from_event_list
  end
end
