require 'eventmachine'
require 'json'
require 'faye'
require 'logger'
require_relative '../../config/environment.rb'

class GospelTribesAuthentication
	def initialize
		@logger = Logger.new STDOUT	
		@faye_server = Faye::Client.new("tcp://104.248.208.184:9292/faye")
	end
	
	def incoming(message, callback)
		puts message
		if message["channel"] == "/meta/subscribe"
			# @logger.info "SUBSCRIBED"
		elsif message["channel"] == "/meta/unsubscribe"
			# @logger.info "UNSUSBSCRIBED"
		elsif message["channel"] == "/meta/disconnect"
			# @logger.info "DISCONNECTED"
		elsif message["channel"] == "/meta/handshake"
			# @logger.info "HANDSHAKE"
		elsif message["channel"] == "/meta/connect"

		else
			data = message["data"]

			if data[:action] == "send_message" || data[:action] == "delete_message" || data[:action] == "update_role"
				command = "#{data["action"]}_command"
				method(command).call(data["data"], message["channel"])
			end
			# the_message = message["data"]
			# if JSON.is_json?(message["data"])
			# 	the_message = JSON.parse(message["data"])
			# end

			# if the_message["action"] == "send_message" || the_message["action"] == "get_messages"
			# 	command = "#{the_message["action"]}_command"
			# 	method(command).call(the_message["data"], message["channel"])
			# end
		end
		callback.call(message)
	end

	def update_role_command(message, channel)
		data = { action: "role_updated", data: message }
		@faye_server.publish(channel, data)
	end

	def send_message_command(message, channel)
		data = { action: "receive_message", data: message }
		@faye_server.publish(channel, data)
	end

	def delete_message_command
		data = { action: "remove_message", data: message }
		@faye_server.publish(channel, data)
	end
end

module JSON
  def self.is_json?(foo)
    begin
      return false unless foo.is_a?(String)
      JSON.parse(foo).all? 
    rescue JSON::ParserError
      false
    end 
  end
end