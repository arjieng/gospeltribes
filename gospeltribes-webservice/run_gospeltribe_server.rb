
# d = DateTime.now
# dd = DateTime.now
# puts d.to_date > dd.to_date


# image = MiniMagick::Image.open("aa.png")
# image.rotate "90"
# image.write "/Users/developer/Projects/client/gospeltribe/gospeltribes-webservice/aa.png"
# puts image.path


# t = Time.zone.parse('2019-02-17 12:4:0')
# puts "#{t}"
# e = Event.find 11
# puts "#{e.inspect}"
# e.update(start_date: t)
# puts "#{e.inspect}"


require_relative 'config/environment.rb'
require 'fcm'
require "mini_magick"

class ServerTest
  def initialize
    @fcm = FCM.new("AAAAQUNBeHY:APA91bFp_cFsHbGXZKhzUaBgonQUfHg01yohwZEYrvkoHNTSgUN5vdYdwTrRMuGiG8S6Ao1tqzIrgoDBKM5cacPDkkvH6QIogn6sLrARDdgkubDanuSg_6Ixn_6y6dgsertoGmOMYWEq")
  end

  def xample
    @fcm.send_to_topic_condition("'group_9_iOS_All' in topics", { priority: 'high', notification: { body: "HELLO TITLE", sound: 'default', content_available: true }, data: { action: "example" } })
  end

  def push_notif_with_topic(topic, data, title, group_id, exceptions)

    # data must have `message` params inside
    options_Android = { priority: 'high', data: data }
    options_iOS = { priority: 'high', data: data, notification: { body: title, sound: 'default', content_available: true } }
    
    group = Group.includes(group_members: [user: :settings]).find(group_id.to_i)
    members = group.group_members
    members.each do |m|
      if !m.user.nil?
        user = m.user
        if !user.settings.find_by(group_id: group_id).nil?
          user_settings = user.settings.find_by(group_id: group_id)
          if (data[:action].to_s == "message" && user_settings.inbox == false) || (data[:action].to_s == "prayer" && user_settings.prayer == false) || (data[:action].to_s == "calendar" && user_settings.event == false) || (data[:action].to_s == "story" && user_settings.story == false) || (data[:action].to_s == "members" && user_settings.member == false)
            exceptions.push("'group_#{group_id}_#{user.id}' in topics")
          end
        end
      end
    end
    
    # topic[0] = ios, topic[1] = android 
    topic_array = topic.split(",")
    response_iOS = @fcm.send_to_topic_condition("#{topic_array[0]} && !(#{exceptions.uniq.join(" || ")})", options_iOS)
    response_Android = @fcm.send_to_topic_condition("#{topic_array[1]} && !(#{exceptions.uniq.join(" || ")})", options_Android)
    puts response_iOS
    puts response_Android
  end
  # response = fcm.send_to_topic_condition("('group_9_iOS_All' in topics || 'group_9_iOS_Male' in topics || 'group_9_iOS_Female' in topics) && !('group_9_30' in topics || 'group_9_29' in topics)", options_iOS)

  def send_to_websocket
    # @faye_server = Faye::Client.new("tcp://api.gospeltribes.com:9292/faye")
    # 104.248.208.184
    begin
      message = {:channel => "/group/9/chatrooms", :data => { action: "send_message", data: { example: 11 } }}
      uri = URI.parse("http://api.gospeltribes.com:9292/faye")
      Net::HTTP.post_form(uri, :message => message.to_json)
    rescue => e
      Rails.logger.debug "#{e.inspect}"
    end
  end

end


# RUNNING THIS FILE
if __FILE__ == $0
  if ARGV.count < 0
    puts "Usage: ruby run_gospeltribe_server.rb <user_id>"
    exit
  end

  user_id = ARGV[0]

  client = ServerTest.new
  # topics = "'group_1_iOS_Female' in topics,'group_1_Android_Female' in topics,'group_2_iOS_Female' in topics,'group_2_Android_Female' in topics,'group_3_iOS_Female' in topics,'group_3_Android_Female' in topics,'group_4_iOS_Female' in topics,'group_4_Android_Female' in topics,'group_5_iOS_Female' in topics,'group_5_Android_Female' in topics,'group_6_iOS_Female' in topics,'group_6_Android_Female' in topics,'group_7_iOS_Female' in topics,'group_7_Android_Female' in topics,'group_8_iOS_Female' in topics,'group_8_Android_Female' in topics,'group_9_iOS_Female' in topics,'group_9_Android_Female' in topics,'group_10_iOS_Female' in topics,'group_10_Android_Female' in topics,'group_11_iOS_Female' in topics,'group_11_Android_Female' in topics,'group_12_iOS_Female' in topics,'group_12_Android_Female' in topics,'group_13_iOS_Female' in topics,'group_13_Android_Female' in topics,'group_14_iOS_Female' in topics,'group_14_Android_Female' in topics,'group_15_iOS_Female' in topics,'group_15_Android_Female' in topics,'group_16_iOS_Female' in topics,'group_16_Android_Female' in topics"
  # client.push_notif_with_topic(topics, {action: "message"}, "Crow has sent a message", 1, ["'group_1_23' in topics"]) 
  client.xample()
end