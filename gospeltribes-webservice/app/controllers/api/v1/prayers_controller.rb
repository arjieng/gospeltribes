class Api::V1::PrayersController < ApplicationController
	before_action :authenticate_user!, only: [:create, :answered_prayers, :unanswered_prayers, :index, :update, :add_prayer_comment, :prayer_comments, :get_all_prayers, :hide_prayer, :delete_prayer]
	before_action :authorize_user!, only: [:create, :answered_prayers, :unanswered_prayers, :index, :update, :add_prayer_comment, :prayer_comments, :get_all_prayers, :hide_prayer, :delete_prayer]
	def get_all_prayers
		@prayers_a = []
		@prayers_u = []

		share_to_query = "share_to = #{0}" + (!current_user.gender.nil? ? " OR share_to = #{current_user.gender.eql?("Female") ? 1 : 2}" : "") + " OR user_id = #{current_user.id}"
		where_query_a = "group_id = #{params[:group_id]} AND is_answered = #{1} AND (#{share_to_query})"
		where_query_u = "group_id = #{params[:group_id]} AND is_answered = #{0} AND (#{share_to_query})"

		prayers_a = Prayer.includes(:group, user: :group_members).where(where_query_a).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_answered_prayer = Prayer.where(where_query_a).count

		prayers_u = Prayer.includes(:group, user: :group_members).where(where_query_u).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_unanswered_prayer = Prayer.where(where_query_u).count

		prayers_a.each do |prayer|
			group_member = prayer.user
			if !group_member.nil?
				gm = group_member.group_members.find_by_group_id(params[:group_id])
				if !gm.nil?
					@prayers_a.push({ user: { role: gm.role, id: group_member.id, first_name: group_member.first_name, last_name: group_member.last_name, email: group_member.email, address: group_member.address, city: group_member.city, zip: group_member.zip, state: group_member.state, phone_number: group_member.phone_number, gender: group_member.gender, username: group_member.username, avatar: group_member.avatar, birth_date: group_member.birth_date }, id: prayer.id, subject: prayer.subject, details: prayer.details, answered_details: prayer.answered_details, is_answered: prayer.is_answered, share_to: prayer.share_to, is_hidden: prayer.is_hidden, created_at: prayer.created_at })
				end
			end
		end

		prayers_u.each do |prayer|
			group_member = prayer.user
			if !group_member.nil?
				gm = group_member.group_members.find_by_group_id(params[:group_id])
				if !gm.nil?
					@prayers_u.push({ user: { role: gm.role, id: group_member.id, first_name: group_member.first_name, last_name: group_member.last_name, email: group_member.email, address: group_member.address, city: group_member.city, zip: group_member.zip, state: group_member.state, phone_number: group_member.phone_number, gender: group_member.gender, username: group_member.username, avatar: group_member.avatar, birth_date: group_member.birth_date }, id: prayer.id, subject: prayer.subject, details: prayer.details, answered_details: prayer.answered_details, is_answered: prayer.is_answered, share_to: prayer.share_to, is_hidden: prayer.is_hidden, created_at: prayer.created_at })
				end
			end
		end

		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10

		@paginate_a = {
			load_more: total_answered_prayer > off_set ? 1 : 0,
			url: "/api/answered_prayers?off_set=#{off_set}&group_id=#{params[:group_id]}"
		}
		@paginate_u = {
			load_more: total_unanswered_prayer > off_set ? 1 : 0,
			url: "/api/unanswered_prayers?off_set=#{off_set}&group_id=#{params[:group_id]}"
		}
		render json: { answered: { prayers: @prayers_a, load_more: @paginate_a }, unanswered:{ prayers: @prayers_u, load_more: @paginate_u }, status: 200 }, status: 200
	end

	def answered_prayers
		@prayers = []
		share_to_query = "share_to = #{0}" + (!current_user.gender.nil? ? " OR share_to = #{current_user.gender.eql?("Female") ? 1 : 2}" : "") + " OR user_id = #{current_user.id}"
		where_query_a = "group_id = #{params[:group_id]} AND is_answered = #{1} AND (#{share_to_query})"
		
		prayers = Prayer.includes(user: :group_members).where(where_query_a).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_answered_prayer = Prayer.where(where_query_a).count
		

		prayers.each do |prayer|
			group_member = prayer.user
			if !group_member.nil?
				gm = group_member.group_members.find_by_group_id(params[:group_id])
				if !gm.nil?
					@prayers.push({ user: { role: gm.role, id: group_member.id, first_name: group_member.first_name, last_name: group_member.last_name, email: group_member.email, address: group_member.address, city: group_member.city, zip: group_member.zip, state: group_member.state, phone_number: group_member.phone_number, gender: group_member.gender, username: group_member.username, avatar: group_member.avatar, birth_date: group_member.birth_date }, id: prayer.id, subject: prayer.subject, details: prayer.details, answered_details: prayer.answered_details, is_answered: prayer.is_answered, share_to: prayer.share_to, is_hidden: prayer.is_hidden, created_at: prayer.created_at })
				end
			end
		end

		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10
		@paginate = {
			load_more: total_answered_prayer > off_set ? 1 : 0,
			url: "/api/answered_prayers?off_set=#{off_set}&group_id=#{params[:group_id]}"
		}
		render json: { prayers: @prayers, load_more: @paginate, status: 200 }, status: 200
	end

	def unanswered_prayers
		@prayers = []
		share_to_query = "share_to = #{0}" + (!current_user.gender.nil? ? " OR share_to = #{current_user.gender.eql?("Female") ? 1 : 2}" : "") + " OR user_id = #{current_user.id}"
		where_query_u = "group_id = #{params[:group_id]} AND is_answered = #{0} AND (#{share_to_query})"
		prayers = Prayer.includes(user: :group_members).where(where_query_u).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_unanswered_prayer = Prayer.where(where_query_u).count
		
		prayers.each do |prayer|
			group_member = prayer.user
			if !group_member.nil?
				gm = group_member.group_members.find_by_group_id(params[:group_id])
				if !gm.nil?
					@prayers.push({ user: { role: gm.role, id: group_member.id, first_name: group_member.first_name, last_name: group_member.last_name, email: group_member.email, address: group_member.address, city: group_member.city, zip: group_member.zip, state: group_member.state, phone_number: group_member.phone_number, gender: group_member.gender, username: group_member.username, avatar: group_member.avatar, birth_date: group_member.birth_date }, id: prayer.id, subject: prayer.subject, details: prayer.details, is_answered: prayer.is_answered, share_to: prayer.share_to, is_hidden: prayer.is_hidden, created_at: prayer.created_at })
				end
			end
		end

		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10
		@paginate = {
			load_more: total_unanswered_prayer > off_set ? 1 : 0,
			url: "/api/unanswered_prayers?off_set=#{off_set}&group_id=#{params[:group_id]}"
		}
		render json: { prayers: @prayers, load_more: @paginate, status: 200 }, status: 200
	end

	def index
		@prayers = []
		user = User.includes(:prayers).find params[:user_id]
		prayers = user.prayers.where(is_hidden: false, group_id: params[:group_id]).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_prayers = user.prayers.where(is_hidden: false).count

		prayers.each do |prayer|
			@prayers.push({ id: prayer.id, subject: prayer.subject, details: prayer.details, answered_details: prayer.answered_details, is_answered: prayer.is_answered, share_to: prayer.share_to, is_hidden: prayer.is_hidden, created_at: prayer.created_at, user: { id: user.id } })
		end

		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10
		@paginate = {
			load_more: total_prayers > off_set ? 1 : 0,
			url: "/api/prayers?user_id=#{params[:user_id]}&off_set=#{off_set}"
		}
		render json: { prayers: @prayers, load_more: @paginate, status: 200 }, status: 200
	end

	def update
		prayer = Prayer.includes(user: :group_members, group: [group_members: :user]).find params[:prayer][:id]
		prayer.update_attributes(prayer_params)


		group_member = prayer.user
		if !group_member.nil?
			gm = group_member.group_members.find_by_group_id(params[:prayer][:group_id])

			@data = { prayer: { user: { role: gm.role, id: group_member.id, first_name: group_member.first_name, last_name: group_member.last_name, email: group_member.email, address: group_member.address, city: group_member.city, zip: group_member.zip, state: group_member.state, phone_number: group_member.phone_number, gender: group_member.gender, username: group_member.username, avatar: group_member.avatar, birth_date: group_member.birth_date }, id: prayer.id, subject: prayer.subject, details: prayer.details, answered_details: prayer.answered_details, is_answered: prayer.is_answered, share_to: prayer.share_to, is_hidden: prayer.is_hidden, created_at: prayer.created_at }, status: 200, params: params }

			if params[:prayer][:is_answered]
				user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{prayer_params[:group_id].to_i}")
				if user_badges.count != 0
					user_badges.find_each do |ub|
						ub.prayer = ub.prayer + 1
						ub.save!
					end
				end

				ct = prayer_params[:share_to] == 1 ? "Female" : (prayer_params[:share_to] == 2 ? "Male" : "All")
				topics = "'#{params[:topics].to_s}_iOS_#{ct}' in topics,'#{params[:topics].to_s}_Android_#{ct}' in topics"
				send_to_topic(topics, { action: "prayer" }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has moved a prayer to answered", prayer_params[:group_id], ["'group_#{prayer_params[:group_id]}_#{current_user.id}' in topics"], "prayer")
		
				admins = User.where(role: 1)
				admins.each do |admin|
					data = { prayer: @data, email: admin.email, message: (group_member.first_name.nil? || group_member.last_name.nil? ? group_member.username.to_s : "#{group_member.first_name} #{group_member.last_name}") + "'s prayer has been answered" }
					PrayerMailer.send_email(data).deliver
				end
			end
			
			render json: @data, status: 200
		end
	end

	def create
		prayer = current_user.prayers.create(prayer_params)
		prayer.save!

		user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{prayer_params[:group_id].to_i}")
		if user_badges.count != 0
			user_badges.find_each do |ub|
				ub.prayer = ub.prayer + 1
				ub.save!
			end
		end

		ct = prayer_params[:share_to] == 1 ? "Female" : (prayer_params[:share_to] == 2 ? "Male" : "All")
		topics = "'#{params[:topics].to_s}_iOS_#{ct}' in topics,'#{params[:topics].to_s}_Android_#{ct}' in topics"
		send_to_topic(topics, { action: "prayer" }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has created a prayer", prayer_params[:group_id], ["'group_#{prayer_params[:group_id]}_#{current_user.id}' in topics"], "prayer")
		
		render json: { prayer: prayer.as_json(only: [:id, :subject, :details, :answered_details, :is_answered, :share_to, :is_hidden, :created_at]), status: 200 }, status: 200
	end

	def add_prayer_comment
		prayer = Prayer.find params[:comment][:prayer_id]
		comment = prayer.prayer_comments.create(prayer_comment_params)
		render json: { comment: comment.as_json(only: [:id, :comment, :created_at]), user: comment.user.as_json(only: [:id, :first_name, :last_name, :email, :address, :city, :zip, :state, :phone_number, :gender, :username, :avatar]), status: 200 }, status: 200
	end

	def prayer_comments
		comments = PrayerComment.includes(:user).where(prayer_id: params[:prayer_id]).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		comment_count = PrayerComment.where(prayer_id: params[:prayer_id]).count
		
		@comments = []
		comments.each do |comment|
			@comments.push({ id: comment.id, comment: comment.comment, created_at: comment.created_at, user: comment.user.as_json(only: [:id, :first_name, :last_name, :email, :address, :birth_date, :city, :zip, :state, :phone_number, :gender, :username, :avatar]) })
		end

		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10
		@paginate = {
			load_more: comment_count > off_set ? 1 : 0,
			url: "/api/prayer_comments?id=#{params[:prayer_id]}&off_set=#{off_set}"
		}
		render json: { comments: @comments, load_more: @paginate, status: 200}, status: 200	
	end

	def delete_prayer_comment
		pc = PrayerComment.find params[:prayer_comment_id]
		pc.delete
		render json: { is_deleted: true, prayer_comment_id: pc.id, status: 200 }, status: 200
	end

	def hide_prayer
		prayer = Prayer.find params[:prayer_id]
		prayer.update(is_hidden: true)
		render json: { prayer: prayer.as_json(only: [:id, :subject, :details, :answered_details, :is_answered, :share_to, :is_hidden, :created_at]), status: 200 }, status: 200
	end

	def delete_prayer
		prayr = Prayer.includes(:prayer_comments).find params[:prayer_id]
		prayr.prayer_comments.delete_all
		prayr.delete
		render json: { is_deleted: true, status: 200 }, status: 200
	end


	protected
		def prayer_params
			params.require(:prayer).permit(:subject, :details, :answered_details, :is_answered, :share_to, :is_hidden, :group_id)
		end
		def prayer_comment_params
			params.require(:comment).permit(:user_id, :comment)
		end
end