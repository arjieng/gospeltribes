class Api::V1::RegistrationsController < DeviseTokenAuth::RegistrationsController
	def create
		@user = User.find_by(username: params[:user][:username])
	    if @user.nil?
	    	@user = User.find_by(email: params[:user][:email])
	    	if @user.nil?
	    		if(params[:user][:password].to_s.length < 8)
	    			render json: { error: "Password must be 8 characters.", status: 404 }, status: 400
	    		else
	    			group = Group.includes(chatrooms: :read_statuses).find_by(group_code: params[:user][:group_code])
					if group.nil?
						render json: { error: "Group does not exist.", status: 404 }, status: 400
					else
						user = User.create(email: user_params[:email], password: user_params[:password], username: user_params[:username], role: 2, current_group_id: group.id, udid: user_params[:udid])

						client_id = SecureRandom.urlsafe_base64(nil, false)
						token     = SecureRandom.urlsafe_base64(nil, false)
						user.tokens[client_id] = {
							token: BCrypt::Password.create(token),
							expiry: (Time.now + user.token_lifespan).to_i
						}
						new_auth_header = user.build_auth_header(token, client_id)
						response.headers.merge!(new_auth_header)

						group_member = group.group_members.create(user_id: user.id, role: 2)
						group_member.save!
						if user.save!

							group.chatrooms.each do |chatroom|
								chatroom.read_statuses.create(user_id: user.id, last_read_id: 0)
							end           
							
							if params[:user][:udid].present?
								upnt = user.push_notification_tokens.create(token: user_params[:udid])
								if params[:phone_type].present?
					            	upnt.phone_type = params[:phone_type].to_i
					            	upnt.save!
					          	end
				          	end
				          	
							user.settings.create(group_id: group.id, user_id: user.id) 
							         
							user_badges = UserBadge.where("user_id != #{user.id} AND group_id = #{group.id.to_i}")
							if user_badges.count != 0
								user_badges.find_each do |ub|
									ub.member = ub.member + 1
									ub.save!
								end
							end

							topics = "'#{params[:topics].to_s}#{group.id}_iOS_All' in topics,'#{params[:topics].to_s}#{group.id}_Android_All' in topics"
							send_to_topic(topics, { action: "members" }, "#{user.first_name.nil? ? user.username : user.first_name} has joined the group", group.id, ["'group_#{group.id}_#{user.id}' in topics"], "member")
							
							render json: { data: { role: group_member.role, user: user.as_json(only: [:id, :first_name, :last_name, :email, :address, :birthdate, :city, :zip, :state, :phone_number, :gender, :username, :avatar]), group: group.as_json(only: [:id, :group_code, :group_name]) }, status: 200 }, status: 200
						else
							render json: { error: "Account not created.", status: 404 }, status: 400
						end
					end
	    		end
		    else
		    	render json: { error: "Email already exist.", status: 404 }, status: 400
		    end
		    	
	    else
			render json: { error: "Username already exist.", status: 404 }, status: 400
	    end
	end

	protected
		def user_params
			params.require(:user).permit(:email, :password, :username, :udid)
		end
end
