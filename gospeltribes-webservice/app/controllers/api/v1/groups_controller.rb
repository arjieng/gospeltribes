class Api::V1::GroupsController < ApplicationController
	before_action :authenticate_user!, only: [:member_list, :get_chatrooms, :group_medium, :get_study_module, :add_study_module_comment, :get_study_module_comment, :add_group, :add_group_image]
	before_action :authorize_user!, only: [:member_list, :get_chatrooms, :group_medium, :get_study_module, :add_study_module_comment, :get_study_module_comment, :add_group, :add_group_image]
	require "mini_magick"
	require "streamio-ffmpeg"
	def check_group
		group = Group.find_by_group_code params["group_code"]
		if group.present?
			render json: { group: group.as_json(only: [ :id, :group_code, :group_name ]), status: 200 }, status: 200
		else
			render json: { error: { message: "This group does not exist." }, status: 404 }, status: 200
		end
	end

	def get_study_module
		@sm = StudyModule.where(group_id: params[:group_id]).last
		if @sm.nil?
			render json: { error: { message: "Nothing to study." }, status: 200 }, status: 200
		else
			render json: { study_module: { id: @sm.id, scripture: { human: @sm.human, chapter: @sm.chapter, verse_num_from: @sm.verse_num_from, verse_num_to: @sm.verse_num_to, verse: @sm.verse, bible_version: @sm.bible_version }, question: @sm.question, link: @sm.link, files: @sm.files.url }, status: 200 }, status: 200
		end
	end

	def delete_study_module
		@sm = StudyModule.includes(:study_module_comments).where(group_id: params[:group_id])
		@sm.each do |sm|
			sm.study_module_comments.destroy_all
		end	
		
		render json: { removed: @sm.destroy_all, status: 200 }, status: 200
	end

	def study_module
		@sm = StudyModule.create(study_module_params)
		@sm.save!
		render json: { study_module: { scripture: { human: @sm.human, chapter: @sm.chapter, verse_num_from: @sm.verse_num_from, verse_num_to: @sm.verse_num_to, verse: @sm.verse, bible_version: @sm.bible_version }, question: @sm.question, link: @sm.link, files: @sm.files.url }, status: 200 }, status: 200
	end

	def edit_study_module
		@sm = StudyModule.find params[:study_module][:study_module_id]
		@sm.update_attributes(study_module_params)
		render json: { study_module: { scripture: { human: @sm.human, chapter: @sm.chapter, verse_num_from: @sm.verse_num_from, verse_num_to: @sm.verse_num_to, verse: @sm.verse, bible_version: @sm.bible_version }, question: @sm.question, link: @sm.link, files: @sm.files.url }, status: 200 }, status: 200
	end

	def remove_message
		cm = ChatroomMessage.includes(:group_medium).find params["message_id"]
		if !cm.group_medium.nil?
			cm.group_medium.delete
		end

		@data = { action: "delete_message", data: { id: cm.id } }
		broadcast("/group/#{params[:group_id]}/chatrooms/#{params[:chatroom_id]}", @data)

		render json: { deleted: cm.delete, status: 200 }, status: 200
	end

	def edit_message
		cm = ChatroomMessage.includes(:group_medium).find params[:message]["message_id"]
		medium = cm.group_medium
		if !medium.nil?
			if params[:message][:image].present?
				medium.update_attribute(image: params[:message][:image])
			end
		end
		cm.update_attributes(message_params)
		cm.save!

		@data = { action: "update_message", data: { id: cm.id, message: cm.body } }
		broadcast("/group/#{params[:group_id]}/chatrooms/#{params[:chatroom_id]}", @data)

		render json: { message: { id: cm.id, message: cm.body, media: (medium.nil? ? nil : medium.image.url) }, status: 200 }, status: 200
	end

	def add_group
		g = Group.find_by_group_code params["group_code"]
		if g.nil?
			render json: { error: { message: "This group does not exist." }, status: 404 }, status: 200
		else
			@gm = g.group_members.includes(:user).find_by_user_id current_user.id

			if @gm.nil?
				@gm = g.group_members.includes(:user).create(user_id: current_user.id, role: 2)
			end
			current_user.current_group_id = g.id
			current_user.save!
			render json: { group: { id: g.id, group_code: g.group_code, group_name: g.group_name, group_image: g.group_image, role: @gm.role }, status: 200 }, status: 200
		end
	end

	def delete_study_module_comment
		@smc = StudyModuleComment.find params[:study_module_comment_id]
		if @smc.present?
			@smc.delete
			render json: { removed: @smc.destroyed?, study_module_comment_id: @smc.id, status: 200 }, status: 200
		end
	end

	def get_study_module_comment
		@smc = StudyModuleComment.includes(:user).where(study_module_id: params["study_module_id"].to_i)
		smc = []
		@smc.each do |sm|
			smc.push({ id: sm.id, comment: sm.comment, created_at: sm.created_at, user: sm.user })
		end
		render json: { comments: smc, status: 200 }, status: 200
	end

	def add_study_module_comment
		@sm = StudyModule.find params[:study_module_comment][:study_module_id]
		@smc = @sm.study_module_comments.includes(:user).create(study_module_comment_params)
		@smc.save!
		render json: { comment: { id: @smc.id, comment: @smc.comment, created_at: @smc.created_at, user: @smc.user }, status: 200 }, status: 200
	end

	def group_medium
		media = GroupMedium.includes(:user).where("group_id=#{params[:group_id]} AND chatroom_message_id IS NULL").order("id DESC")
		@media = []
		media.each do |medium|
			@media.push({ media: (medium.is_video ? medium.video_name.thumb.url : medium.image.url), user: medium.user, id: medium.id, description: medium.description, is_video: medium.is_video, video: (medium.is_video ? medium.video_name.url : nil) })
		end

		render json: { media: @media, status: 200 }, status: 200
	end

	def get_chatrooms
		group = Group.includes(chatrooms: [:read_statuses, chatroom_messages: :user]).find_by_group_code params["group_code"]
		if group.present?
			@chatrooms = []
			group.chatrooms.each do |chatroom|
				the_last_message = chatroom.chatroom_messages.last
				read_status = chatroom.read_statuses.where(user_id: current_user.id).first

				if read_status.nil?
					read_status = chatroom.read_statuses.create(user_id: current_user.id, last_read_id: 0)
					read_status.save!
				end
				notif_count = (read_status.nil? ? 0 : chatroom.chatroom_messages.where("id > #{read_status.last_read_id} AND user_id != #{current_user.id}").count)

				@last_message = nil

				if the_last_message.present?
					the_last_message_user = the_last_message.user
					the_last_message_group_medium = the_last_message.group_medium
					if the_last_message_group_medium.present?
						@users_name = nil
						if current_user.id == the_last_message_user.id
							@users_name = "You"
						else
							if the_last_message_user.first_name.nil? && the_last_message_user.last_name.nil?
								@users_name = the_last_message_user.username
							elsif !the_last_message_user.first_name.nil? && the_last_message_user.last_name.nil?
								@users_name = the_last_message_user.first_name
							elsif the_last_message_user.first_name.nil? && !the_last_message_user.last_name.nil?
								@users_name = the_last_message_user.last_name
							elsif !the_last_message_user.first_name.nil? && !the_last_message_user.last_name.nil?
								@users_name = "#{the_last_message_user.first_name} #{the_last_message_user.last_name}"
							end
						end

						@last_message = "#{@users_name} sent a photo"
					else
						if !the_last_message.body.nil?
							if the_last_message.body.length > 30
								@last_message = "#{the_last_message.body[0..27]}..."
							else
								@last_message = the_last_message.body
								if the_last_message.body.include? "\n"
									last_message_array =  the_last_message.body.split "\n"
									@last_message = "#{last_message_array[0]} ..."
								end
							end
						end
					end
				end				

				case params["gender"].to_s
				when "admin"
					@chatrooms.push({ id: chatroom.id, group_id: chatroom.group_id, chatroom_name: chatroom.chatroom_name, chatroom_image: chatroom.chatroom_image.url, chatroom_type: chatroom.chatroom_type, last_message: @last_message, notif_count: notif_count })
				when "Male"
					if(chatroom.chatroom_type != 1)
						@chatrooms.push({ id: chatroom.id, group_id: chatroom.group_id, chatroom_name: chatroom.chatroom_name, chatroom_image: chatroom.chatroom_image.url, chatroom_type: chatroom.chatroom_type, last_message: @last_message, notif_count: notif_count })
					end
				when "Female"
					if(chatroom.chatroom_type != 2)
						@chatrooms.push({ id: chatroom.id, group_id: chatroom.group_id, chatroom_name: chatroom.chatroom_name, chatroom_image: chatroom.chatroom_image.url, chatroom_type: chatroom.chatroom_type, last_message: @last_message, notif_count: notif_count })
					end
				when ""
					if(chatroom.chatroom_type != 1 && chatroom.chatroom_type != 2)
						@chatrooms.push({ id: chatroom.id, group_id: chatroom.group_id, chatroom_name: chatroom.chatroom_name, chatroom_image: chatroom.chatroom_image.url, chatroom_type: chatroom.chatroom_type, last_message: @last_message, notif_count: notif_count })
					end
				end

			end
			render json: { groups: @chatrooms, status: 200 }, status: 200
		else
			render json: { error: { message: "This group does not exist." }, status: 404 }, status: 200
		end
	end

	def member_list
		group = Group.includes(group_members: [user: :settings]).find_by_group_code params[:group_id]

		@group_members = []
		@group_members_without_last_name = []

		group.group_members.each do |group_member|
			@group_member = group_member.user
			if !@group_member.nil?
				sett = @group_member.settings.find_by(group_id: group.id)
				if !@group_member.last_name.nil?
					@group_members.push({ id: @group_member.id, first_name: @group_member.first_name, last_name: @group_member.last_name, email: @group_member.email, address: @group_member.address, city: @group_member.city, zip: @group_member.zip, state: @group_member.state, phone_number: @group_member.phone_number, gender: @group_member.gender, username: @group_member.username, avatar: @group_member.avatar, role: group_member.role, birth_date: @group_member.birth_date, settings: { inbox: !sett.nil? ? sett.inbox : true, prayer: !sett.nil? ? sett.prayer : true, event_notif: !sett.nil? ? sett.event : true, story: !sett.nil? ? sett.story : true, member: !sett.nil? ? sett.member : true } })
				else
					@group_members_without_last_name.push({ id: @group_member.id, first_name: @group_member.first_name, last_name: @group_member.last_name, email: @group_member.email, address: @group_member.address, city: @group_member.city, zip: @group_member.zip, state: @group_member.state, phone_number: @group_member.phone_number, gender: @group_member.gender, username: @group_member.username, avatar: @group_member.avatar, role: group_member.role, birth_date: @group_member.birth_date, settings: { inbox: !sett.nil? ? sett.inbox : true, prayer: !sett.nil? ? sett.prayer : true, event_notif: !sett.nil? ? sett.event : true, story: !sett.nil? ? sett.story : true, member: !sett.nil? ? sett.member : true } })
				end
			end
		end

		ngm = @group_members.sort_by{ |gm| gm[:last_name] }
		ngmwln = @group_members_without_last_name.sort_by{ |gm| gm[:username] }
		fgm = ngm.concat(ngmwln)

		render json: { group_members: fgm, status: 200 }, status: 200
	end


	def add_group_image
		if params[:group][:is_video] == "True"
			movie = FFMPEG::Movie.new(params[:group][:image].tempfile.path)
			if movie.duration.to_i > 15
				render json: { errors: "Video must not exceed to 15 seconds.", status: 422 }, status: 200
			else
				gm = GroupMedium.create(group_id: params[:group][:id], user_id: current_user.id, video_name: params[:group][:image], description: params[:group][:description], is_video: true)
				gm.save!
				path = "#{Rails.public_path}#{gm.video_name.thumb.url}"
				image = MiniMagick::Image.open(path)
				image.rotate "90"
				image.write path

				user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{params[:group][:id].to_i}")
				if user_badges.count != 0
					user_badges.find_each do |ub|
						ub.story = ub.story + 1
						ub.save!
					end
				end

				media = { media: (gm.is_video ? gm.video_name.thumb.url : gm.image.url), user: gm.user, id: gm.id, description: gm.description, is_video: gm.is_video, video: (gm.is_video ? gm.video_name.url : nil) }
		
				send_to_topic(params[:group][:topics].to_s, { action: "story", group_media: media }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has added a video", params[:group][:id], ["'group_#{params[:group][:id]}_#{current_user.id}' in topics"], "media")

				render json: {gm: gm, status: 200}, status: 200
			end
		else
			gm = GroupMedium.create(group_id: params[:group][:id], user_id: current_user.id, image: params[:group][:image], description: params[:group][:description], is_video: false)
			gm.save!

			user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{params[:group][:id].to_i}")
			if user_badges.count != 0
				user_badges.find_each do |ub|
					ub.story = ub.story + 1
					ub.save!
				end
			end
			media = { media: (gm.is_video ? gm.video_name.thumb.url : gm.image.url), user: gm.user, id: gm.id, description: gm.description, is_video: gm.is_video, video: (gm.is_video ? gm.video_name.url : nil) }
			send_to_topic(params[:group][:topics].to_s, { action: "story", group_media: media }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has added a image", params[:group][:id], ["'group_#{params[:group][:id]}_#{current_user.id}' in topics"], "media")
			
			render json: {gm: gm, status: 200}, status: 200
		end
	end

	def remove_group_image
		gm = GroupMedium.find params[:group_image_id]
		if gm.present?
			gm.delete
			render json: { removed: gm.destroyed?, group_medium_id: gm.id, status: 200 }, status: 200
		end
	end

	def rename_group
		group = Group.find params[:group_id]
		group.group_name = params[:group_name]
		if group.save!
			render json: { group: group.as_json(only: [ :id, :group_code, :group_name ]), status: 200 }, status: 200
		else
			render json: { error: { message: "Group failed to save." }, status: 404 }, status: 200
		end
	end

	def change_chatroom_details
		chatroom = Chatroom.find params[:chatroom][:id]
		chatroom.update_attributes(chatroom_params)
		render json: { chatroom: { id: chatroom.id, group_id: chatroom.group_id, chatroom_name: chatroom.chatroom_name, chatroom_image: chatroom.chatroom_image.url, chatroom_type: chatroom.chatroom_type }, status: 200 }, status: 200
	end

	def example_method
		PrayerMailer.send_email().deliver

	end

	protected
		def chatroom_params
			params.require(:chatroom).permit(:chatroom_image, :chatroom_name)
		end
		def study_module_params
			params.require(:study_module).permit(:group_id, :human, :chapter, :verse_num_from, :verse_num_to, :verse, :bible_version, :question, :link, :files)			
		end
		def study_module_comment_params
			params.require(:study_module_comment).permit(:user_id, :comment)
		end
		def message_params
			params.require(:message).permit(:body)
		end
end





















