class Api::V1::EventsController < ApplicationController
	before_action :authenticate_user!, only: [:create, :index, :update, :get_attendees, :add_attendee, :remove_attendee]
	before_action :authorize_user!, only: [:create, :index, :update, :get_attendees, :add_attendee, :remove_attendee]

	def get_attendees
		attendees = Attendee.includes(:user).where(event_id: params[:event_id])
		
		@attendees = []
		attendees.each do |attendee|
			@attendees.push(attendee.user.as_json(only: [:id, :first_name, :last_name, :email, :address, :city, :zip, :state, :phone_number, :gender, :username, :avatar]))
		end
		render json: { attendees: @attendees, status: 200 }, status: 200
	end

	def add_attendee
		attendee = Attendee.includes(:user).create(event_id: params[:event_id], user_id: params[:user_id])
		render json: { attendee: attendee.user.as_json(only: [:id, :first_name, :last_name, :email, :address, :city, :zip, :state, :phone_number, :gender, :username, :avatar]), status: 200 }, status: 200
	end

	def remove_attendee
		attendee = Attendee.where(event_id: params[:event_id], user_id: params[:user_id]).first
		if attendee.present?
			attendee.delete
			render json: { removed: attendee.destroyed?, status: 200 }, status: 200
		end		
	end

	def index
		dateTime = DateTime.now
		# query = "group_id = #{params[:group_id]} AND (DATE(start_date) >= '#{dateTime.strftime("%Y-%m-%d")}' OR repeat != 'None')"
		query = "group_id = #{params[:group_id]} AND (DATE(start_date) >= '#{dateTime.strftime("%Y-%m-%d")}' OR (repeat != 'None' AND DATE(end_date) >= '#{dateTime.strftime("%Y-%m-%d")}'))"
		
		@events = Event.includes(:attendees).where(query).order("start_date DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_events = @events.count
		events = []
		@events.each do |event|
			attendee = event.attendees.find_by_user_id params[:user_id]
			events.push({ id: event.id, subject: event.title, details: event.details, link: event.link, image: event.image.url, start_date: event.start_date, end_date: event.end_date, has_attended: attendee.nil? ? false : true, subtitle: event.subtitle, location: event.location, repeat: event.repeat, repeat_days: event.repeat_days })
		end

		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10
		@paginate = {
			load_more: total_events.eql?(10) ? 1 : 0,
			url: "/api/all_events?group_id=#{params[:group_id]}&off_set=#{off_set}"
		}

		render json: { events: events, load_more: @paginate, status: 200 }, status: 200
	end

	def get_event
		event = Event.find params[:event_id]
		render json: { event: { id: event.id, subject: event.title, details: event.details, link: event.link, image: event.image.url, start_date: event.start_date, end_date: event.start_date, subtitle: event.subtitle, location: event.location }, status: 200 }, status: 200
	end

	def update
		event = Event.find params[:events][:event_id]
		event.update_attributes(event_params)
		render json: { event: { id: event.id, subject: event.title, details: event.details, link: event.link, image: event.image.url, start_date: event.start_date, end_date: event.end_date, subtitle: event.subtitle, location: event.location }, status: 200 }, status: 200
	end

	def remove_event
		event = Event.find params[:event_id].to_i
		if event.present?
			event.delete
			render json: { removed: event.destroyed?, event_id: event.id, status: 200 }, status: 200
		end
	end

	def create
		event_list = EventList.create(user_id: current_user.id, group_id: params[:events][:group_id], event_name: "Untitled Event")
		n_event_param = event_params
		n_event_param[:from_event_list] = event_list.id
		event = Event.create(n_event_param)
		if event.save!
			event_list.destroy!
			
			user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{params[:events][:group_id].to_i}")
			if user_badges.count != 0
				user_badges.find_each do |ub|
					ub.event = ub.event + 1
					ub.save!
				end
			end

			send_to_topic(params[:topics].to_s, { action: "calendar" }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has created an event", event_params[:group_id], ["'group_#{event_params[:group_id]}_#{current_user.id}' in topics"], "event")

			render json: { event: { id: event.id, subject: event.title, details: event.details, link: event.link, image: event.image.url, start_date: event.start_date, end_date: event.end_date, subtitle: event.subtitle, location: event.location }, status: 200 }, status: 200
		end
	end

 	protected
 		def event_params
			params.require(:events).permit(:group_id, :title, :details, :link, :image, :start_date, :end_date, :from_event_list, :subtitle, :location, :repeat, :repeat_days)
		end
end