class Api::V1::UsersController < ApplicationController
	before_action :authenticate_user!, only: [:update_profile, :upload_user_avatar, :user_images, :update_account, :get_user_info, :update_udid, :set_unread_badges, :get_unread_badges, :set_read_badges]
	before_action :authorize_user!, only: [:update_profile, :upload_user_avatar, :user_images, :update_account, :get_user_info, :update_udid, :set_unread_badges, :get_unread_badges, :set_read_badges]
	# require "google/cloud/vision"
	
	def update_profile
		unless user_params.present?
	      raise ActionController::ParameterMissing "user"
	    end
		user = User.find params[:user][:id]
		if !user.nil?
			user.update_attributes user_params
			render json: { data: user.as_json(only: [:id, :first_name, :last_name, :email, :address, :city, :birth_date, :zip, :state, :phone_number, :gender, :username, :avatar]), status: 200 }, status: 200
		end
	end

	def update_user_notification_settings
		setting = Setting.find_by(user_id: params[:setting][:user_id], group_id: params[:setting][:group_id])
		if setting.nil?
			s = Setting.create(user_id: params[:setting][:user_id], group_id: params[:setting][:group_id], inbox: params[:setting][:inbox], event: params[:setting][:event_notif], prayer: params[:setting][:prayer], story: params[:setting][:story], member: params[:setting][:member])
			s.save!
			render json: { edited: true, status: 200 }, status: 200
		else
			setting.update_attributes(inbox: params[:setting][:inbox], event: params[:setting][:event_notif], prayer: params[:setting][:prayer], story: params[:setting][:story], member: params[:setting][:member])
			render json: { edited: true, status: 200 }, status: 200
		end
	end
	def get_user_info
		user = User.find params[:user_id]
		if !user.nil?
			render json: { data: user.as_json(only: [:id, :first_name, :last_name, :email, :address, :city, :birth_date, :zip, :state, :phone_number, :gender, :username, :avatar]), status: 200 }, status: 200
		else
			render json: { error: { message: "This user does not exist." }, status: 404 }, status: 200
		end
	end


	def update_account
		user_name = User.find_by_username params[:user][:username]
		if user_name.nil?
			if params[:user][:password].length < 8
				render json: { error: { message: "Password must be 8 characters." }, status: 404 }, status: 200
			else
				current_user.update_attributes(user_account_params)
				render json: { user: current_user.as_json(only: [:username]), status: 200 }, status: 200
			end
			
		else
			render json: { error: { message: "Username already exist" }, status: 404 }, status: 200
		end
	end

	def retrieve_username
		@user = User.find_by(email: params[:email])
		if @user.present?
		  data = { username: @user.username, email: params[:email] }
		  RetrievePasswordMailer.send_email(data).deliver

		  render json: { is_sent: true, status: 200 }, status: 200
	    else
	      render json: { error: "User not found", status: 404 }, status: 200
	    end
	end

	def reset_password
		user = User.with_reset_password_token(params[:user][:reset_password_token])
		if user.present?
			if params[:user][:password].length < 8
				render json: { error: { message: "Password must be 8 characters." }, status: 300 }, status: 200
			else
				user.update_attributes(user_account_params)
				render json: { title: "Password changed.", message: "Please sign in using your new password", status: 200 }, status: 200
			end
		else
			render json: { error: "Reset token is invalid", status: 404 }, status: 200
		end
	end

	def upload_user_avatar
		if !params[:user].present?
			render json: { error: { message: "Image not found." }, status: 404 }, status: 200
		else
			current_user.update_attributes(user_params)
			render json: { user: current_user.as_json(only: [:id, :first_name, :last_name, :email, :address, :birth_date, :city, :zip, :state, :phone_number, :gender, :username, :avatar]), status: 200 }, status: 200
		end
	end

	def get_unread_badges
		@user_badges = nil
		if UserBadge.exists?(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i)
			@user_badges = UserBadge.where(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i).first
		else
			@user_badges = UserBadge.create(user_id: params[:user_id], group_id: params[:group_id])
		end
		render json: {badge: @user_badges, status: 200}, status: 200
	end

	def set_read_badges
		@user_badges = nil
		if UserBadge.exists?(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i)
			@user_badges = UserBadge.where(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i).first
		else
			@user_badges = UserBadge.create(user_id: params[:user_id], group_id: params[:group_id])
		end

		if params[:tab].to_s != ""
			@user_badges[params[:tab].to_s] = 0
			@user_badges.save!
		end

		render json: {badge: @user_badges, status: 200}, status: 200
	end

	def set_unread_badges
		@user_badges = nil
		if UserBadge.exists?(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i)
			@user_badges = UserBadge.where(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i).first
			@user_badges.inbox = params[:inbox].to_i
			@user_badges.story = params[:story].to_i
			@user_badges.event = params[:event].to_i
			@user_badges.prayer = params[:prayer].to_i
			@user_badges.member = params[:member].to_i
			@user_badges.save
		else
			@user_badges = UserBadge.create(user_id: params[:user_id], group_id: params[:group_id], inbox: params[:inbox].to_i, story: params[:story].to_i, event: params[:event].to_i, prayer: params[:prayer].to_i, member: params[:member].to_i)
		end
		render json: {badge: @user_badges, status: 200}, status: 200
	end

	def update_udid
		u = User.includes(:push_notification_tokens).find params[:user_id]
		if u.push_notification_tokens.where(token: params[:udid].to_s).count == 0
			upnt = u.push_notification_tokens.create(token: params[:udid].to_s)
			if params[:phone_type].present?
				upnt.phone_type = params[:phone_type].to_i
				upnt.save!
			end
		else
			upnt = u.push_notification_tokens.where(token: params[:udid].to_s).first
			if params[:phone_type].present?
				upnt.phone_type = params[:phone_type].to_i
				upnt.save!
			end
		end
		u.update_attributes({udid: params[:udid].to_s, current_group_id: params[:group_id]})
		render json: { user_saved: u.save!, ptp: params[:phone_type].present?, status: 200 }, status: 200
	end

	def remove_profile
		gr = GroupMember.find_by(user_id: params[:user_id].to_i, group_id: params[:group_id].to_i)
		if gr.present?
			gr.delete
			render json: { removed: gr.destroyed?, user_id: params[:user_id].to_i, status: 200 }, status: 200
		end
	end

	def user_images
		@images = []
		@count = GroupMedium.where("group_id=#{params[:group_id]} AND user_id=#{params[:user_id]}").count
		if params[:from_profile].to_s == "true"
			@medium = GroupMedium.includes(:user).where("group_id=#{params[:group_id]} AND user_id=#{params[:user_id]}").order("id DESC").first(3)
		else
			@medium = GroupMedium.includes(:user).where("group_id=#{params[:group_id]} AND user_id=#{params[:user_id]}").order("id DESC")
		end
		
		@medium.each do |medium|
			@images.push({ media: (medium.is_video ? medium.video_name.thumb.url : medium.image.url), user: medium.user, id: medium.id, description: medium.description, is_video: medium.is_video, video: (medium.is_video ? medium.video_name.url : nil) })
		end

		render json: { media: @images, count: @count, status: 200 }, status: 200
	end

	def get_sample
		render json: { is_fetched: true, status: 200 }, status: 200
	end

	protected
		def user_account_params
			params.require(:user).permit(:username, :password)
		end
		
		def user_params
			params.require(:user).permit(:email, :phone_number, :first_name, :last_name, :address, :birth_date, :gender, :city, :zip, :state, :avatar)
		end
end

