class Api::V1::SessionsController < DeviseTokenAuth::SessionsController
  def create
    user = User.includes(:push_notification_tokens).find_by(username: params[:username])
    if user.nil? || !user.valid_password?(params[:password])
      render json: { "error" => "Invalid Username / Password.", status: 404 }, status: 400
    else
      
      group = GroupMember.where("user_id = #{user.id} AND group_id = #{params[:group_id]}").first
      if group.nil?
        render json: { "error" => "Invalid Username / Password.", status: 404 }, status: 400
      else
        client_id = SecureRandom.urlsafe_base64(nil, false)
        token     = SecureRandom.urlsafe_base64(nil, false)

        user.tokens[client_id] = {
          token: BCrypt::Password.create(token),
          expiry: (Time.now + user.token_lifespan).to_i
        }

        new_auth_header = user.build_auth_header(token, client_id)

        response.headers.merge!(new_auth_header)
        
        if params[:udid].present?
          if user.push_notification_tokens.where(token: params[:udid].to_s).count == 0
            upnt = user.push_notification_tokens.create(token: params[:udid].to_s)

            if params[:phone_type].present?
              upnt.phone_type = params[:phone_type].to_i
              upnt.save!
            end
          end
        end
        
        user.udid = params[:udid].to_s
        user.current_group_id = params[:group_id]
        user.save!
        render json: { data: { role: group.role, user: user.as_json(only: [:id, :first_name, :last_name, :email, :address, :city, :birth_date, :zip, :state, :phone_number, :gender, :username, :avatar]) }, status: 200 }, status: 200
      end
    end
  end

  def forgot_password
    @user = User.find_by(email: params[:email])
    if @user.present? 
      @user.send_reset_password_instructions
      render json: { is_sent: true, status: 200 }, status: 200
    else
      render json: { error: "User not found", status: 404 }, status: 200
    end
  end
end
