class Api::V1::MessagesController < ApplicationController
	before_action :authenticate_user!, only: [:send_message, :get_message, :set_read, :like_message, :unlike_message]
	before_action :authorize_user!, only: [:send_message, :get_message, :set_read, :like_message, :unlike_message]

	def send_message
		@image = nil
		@medium = nil

		if !params.nil?
			if (!params[:message][:body].nil? && params[:message][:body] != "") || (params[:message][:image].present? && !params[:message][:image].nil?)
				# @message = ChatroomMessage.includes(:user).create(user_id: params[:message][:user_id], chatroom_id: params[:message][:chatroom_id], body: params[:message][:body], time_sent: DateTime.parse(params[:message][:time_sent].to_s))
				@message = ChatroomMessage.includes(:chatroom, :user).create(user_id: params[:message][:user_id], chatroom_id: params[:message][:chatroom_id], body: params[:message][:body])

				user = @message.user
				if params[:message][:image].present?
					@medium = GroupMedium.create(group_id: params[:message][:group_id].to_i, chatroom_message_id: @message.id, user_id: params[:message][:user_id].to_i, image: params[:message][:image])
				end
				
				dateTime = "Just Now"

				@data = { action: "send_message", data: { id: @message.id, chatroom_type: @message.chatroom.chatroom_type, user: { id: user.id, first_name: user.first_name, last_name: user.last_name, avatar: { url: user.avatar.url }, username: user.username }, message: @message.body, media: @medium.nil? ? nil : @medium.image.url, date_sent: dateTime, like_count: 0, has_liked: false } }
				broadcast("/group/#{params[:message][:group_id]}/chatrooms/#{params[:message][:chatroom_id]}", @data)
				broadcast("/group/#{params[:message][:group_id]}/chatrooms", @data)

				user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{params[:message][:group_id].to_i}")
				if user_badges.count != 0
					user_badges.find_each do |ub|
						ub.inbox = ub.inbox + 1
						ub.save!
					end
				end

				t = params[:message][:image].present? ? params[:message][:topics].to_s : params[:topics].to_s
				ct = @message.chatroom.chatroom_type == 1 ? "Female" : (@message.chatroom.chatroom_type == 2 ? "Male" : "All")
				topics = "'#{t}_iOS_#{ct}' in topics,'#{t}_Android_#{ct}' in topics"
				send_to_topic(topics, { action: "message", params: params, chatroom_type: @message.chatroom.chatroom_type }, "#{user.first_name.nil? ? user.username : user.first_name} has sent a message", params[:message][:group_id].to_i, ["'group_#{params[:message][:group_id].to_i}_#{user.id}' in topics"], "inbox")

				render json: { status: 200, data: { action: "message", chatroom_type: @message.chatroom.chatroom_type }, title: "#{user.first_name.nil? ? user.username : user.first_name} has sent a message", group_id: params[:message][:group_id].to_i, except: user.id }, status: 200
			else
				render json: { error: "Empty parameters", status: 404 }, status: 200
			end
		else
			render json: { error: "Empty parameters", status: 404 }, status: 200
		end
	end

	def get_message
		@messages = []

		messages = ChatroomMessage.includes(:user, :group_medium, chatroom_messages_likes: [:user]).where(chatroom_id: params[:chatroom_id]).order("id DESC").limit(10).offset((params[:off_set].present? ? params[:off_set] : 0))
		total_messages = ChatroomMessage.where(chatroom_id: params[:chatroom_id]).count

		messages.each do |message|
			medium = message.group_medium
			user = message.user
			like_count = message.chatroom_messages_likes.count
			this_current_user = message.chatroom_messages_likes.find_by(user_id: current_user.id)
			if user.present?

				dateTime = "#{time_ago_in_words(message.created_at)} ago"

				@messages.push({ id: message.id, user: { id: user.id, first_name: user.first_name, last_name: user.last_name, username: user.username, avatar: { url: user.avatar.url } }, message: message.body, media: (medium.nil? ? nil : medium.image.url), date_sent: dateTime, like_count: like_count, has_liked: this_current_user.present? })
			end
		end
		
		off_set = (params[:off_set].present? ? params[:off_set].to_i : 0) + 10
		@paginate = {
			load_more: total_messages > off_set ? 1 : 0,
			url: "/api/get_message?chatroom_id=#{params[:chatroom_id]}&off_set=#{off_set}" 
		}
		render json: { messages: @messages, paginate: @paginate, status: 200 }, status: 200
	end

	def set_read
		read_status = ReadStatus.find_by(user_id: current_user.id, chatroom_id: params[:chatroom_id].to_i)
		read_status.last_read_id = params[:chatroom_message_id].to_i
		read_status.save!
		render json: read_status
	end

	def like_message
		@cm = ChatroomMessage.includes(:chatroom_messages_likes).find params[:chatroom_message_id]
		@cml = @cm.chatroom_messages_likes.create(user_id: current_user.id)
		if @cml.save!
			render json: { is_cml: true, like_count: @cm.chatroom_messages_likes.count, status: 200 }, status:200
		else
			render json: { is_cml: false, status: 200 }, status:200
		end
	end

	def unlike_message
		@cm = ChatroomMessage.includes(:chatroom_messages_likes).find params[:chatroom_message_id]
		@cml = @cm.chatroom_messages_likes.where(user_id: params[:user_id])
		if @cml.destroy_all
			render json: { like_count: @cm.chatroom_messages_likes.count, has_liked: false, status: 200 }, status: 200
		end
	end
end
