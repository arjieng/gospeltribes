class Admin::DashboardController < ApplicationController
	before_action :web_authenticate_user!, except: [:ajax_messaging, :ajax_delete_messaging]
	layout 'admin_dashboard'
	def index

		group_creators = GroupCreator.includes(group: [:group_members, :events, :group_medium]).where(user_id: current_user.id)
		@groups = []
		group_creators.each do |gc|
			group = gc.group
			if !group.nil?
				dateTime = DateTime.now
				
				query = "group_id = #{group.id} AND (DATE(start_date) >= '#{dateTime.strftime("%Y-%m-%d")}' OR repeat != 'None')"
    			events_count = group.events.where(query).count


				is_joined = group.group_members.where(user_id: current_user.id).count == 0 ? false : true

				datas = { id: group.id, group_code: group.group_code, group_name: group.group_name, group_image: group.group_image.thumb_160x160, member_count: group.group_members.count, events_count: events_count, media_count: group.group_medium.count, is_joined: is_joined }
				if !@groups.include?(datas)
					@groups.push(datas)
				end
			end
		end
	end

	def prayers
		@prs = []
		prs_ids = []
		@temp = []

		prs = Prayer.includes(:group, user: :group_members).where("from_admin = true AND counter != 0").order('id ASC')
		prs.each do |pr|
			if pr.counter == 1
				if @temp.count != 0
					prs_ids.push(@temp)
					@temp = []
				end
				@prs.push(pr)
				@temp.push(pr.id)
			else
				@temp.push(pr.id)
			end
			
		end
		prs_ids.push(@temp)

		@data = { prs: @prs.reverse!, prs_ids: prs_ids.reverse! }
	end

	def destroy_from_profile
	    @prayer = Prayer.find params[:prayer_id].to_i
	    @prayer.destroy!

	    flash[:notice] = "Prayer removed."
	    redirect_to admin_admin_profile_path
	rescue Exception => e
	    flash[:error] = "Could not delete prayer."
	end

	def inbox
		women_only = []
		women_only_ids = []
		
		men_only = []
		men_only_ids = []
		
		group_text = []
		group_text_ids = []
		
		@chat_messages = []

		last_chat_type = 0
		@temp = []

		cms = ChatroomMessage.includes(:chatroom).where("from_admin = true AND counter != 0")
		cms.each do |cm|
			if !cm.nil?
				case cm.chatroom.chatroom_type
				when 1
					if cm.counter == 1
						if @temp.count != 0
							case last_chat_type
							when 1
								women_only_ids.push(@temp)
							when 2
								men_only_ids.push(@temp)
							when 3
								group_text_ids.push(@temp)
							end
							@temp = []
						end
						last_chat_type = 1
						women_only.push(cm)
						@temp.push(cm.id)
					else
						@temp.push(cm.id)
					end
				when 2
					if(cm.counter == 1)
						if @temp.count != 0
							case last_chat_type
							when 1
								women_only_ids.push(@temp)
							when 2
								men_only_ids.push(@temp)
							when 3
								group_text_ids.push(@temp)
							end
							@temp = []
						end
						last_chat_type = 2
						men_only.push(cm)
						@temp.push(cm.id)
					else
						@temp.push(cm.id)
					end
				when 3
					if(cm.counter == 1)
						if @temp.count != 0
							case last_chat_type
							when 1
								women_only_ids.push(@temp)
							when 2
								men_only_ids.push(@temp)
							when 3
								group_text_ids.push(@temp)
							end
							@temp = []
						end
						last_chat_type = 3
						group_text.push(cm)
						@temp.push(cm.id)
					else
						@temp.push(cm.id)
					end
				end

				
			end
		end
		case last_chat_type
		when 1
			women_only_ids.push(@temp)
		when 2
			men_only_ids.push(@temp)
		when 3
			group_text_ids.push(@temp)
		end

		@chat_messages = { women_only: women_only, men_only: men_only, group_text: group_text, women_only_ids: women_only_ids, men_only_ids: men_only_ids, group_text_ids: group_text_ids }
	end

	def ajax_messaging
		chatrooms = Chatroom.where(chatroom_type: params[:chatroom_type])
		@first_message = nil
		@user = nil
		
		count = 1
		@ids = []
		chatrooms.each do |chatroom|
			message = chatroom.chatroom_messages.includes(:user).create(user_id: params[:user_id], body: params[:body], from_admin: true, counter: count)
			message.save!
			@ids.push(message.id)
			user = message.user
			if(count == 1)
				@first_message = message
				@user = message.user
			end
			
			dateTime = "#{time_ago_in_words(message.created_at)} ago"

		    @data = { action: "send_message", data: { id: message.id, chatroom_type: params[:chatroom_type], user: { id: user.id, first_name: user.first_name, last_name: user.last_name, avatar: { url: user.avatar.url }, username: user.username }, message: message.body, media: nil, date_sent: dateTime } }
	    	broadcast("/group/#{chatroom[:group_id]}/chatrooms/#{chatroom.id}", @data)
	    	broadcast("/group/#{chatroom[:group_id]}/chatrooms", @data)
	    	
	    	ct = params[:chatroom_type].to_i == 1 ? "Female" : (params[:chatroom_type].to_i == 2 ? "Male" : "All")
			topics = "'group_#{chatroom[:group_id].to_i}_iOS_#{ct}' in topics,'group_#{chatroom[:group_id].to_i}_Android_#{ct}' in topics"
			send_to_topic(topics, { action: "message" }, "#{user.first_name.nil? ? user.username : user.first_name} has sent a message", chatroom[:group_id].to_i, ["'group_#{chatroom[:group_id].to_i}_#{user.id}' in topics"], "inbox")
	    	
	    	count += 1
		end

		message = @first_message
	    user = @user
	    dateTime = "#{time_ago_in_words(message.created_at)} ago"
	    @data = { action: "send_message", data: { id: message.id, user: { id: user.id, first_name: user.first_name, last_name: user.last_name, avatar: { url: user.avatar.url }, username: user.username }, message: message.body, media: nil, date_sent: dateTime }, ids: @ids.join(',') }
		render json: @data
	end

	def ajax_delete_messaging
		render json: ChatroomMessage.where("id IN (#{params[:chatroom_message_id]})").destroy_all
	end

	def admin_profile
		@prayers = current_user.prayers.where(is_hidden: false).order("id DESC")
		@media = []

		group_media = GroupMedium.includes(:group).where("user_id=#{current_user.id}").order("id DESC")
		group_media.each do |group_medium|
			group = group_medium.group
			if !group.nil?
				@media.push({ id: group_medium.id, image: group_medium.image.url, group_name: group.group_name, group_code: group.group_code })
			end
		end
	end

	def admin_profile_settings
		
	end

	def edit_personal_info
		current_user.update_attributes(user_params)
		flash[:notice] = "User info updated"
		redirect_to admin_admin_profile_settings_path
	end

	def edit_user_avatar
		current_user.update_attributes(user_params)
		flash[:notice] = "Avatar updated"
		redirect_to admin_admin_profile_settings_path
	end

	def edit_user_account
		if current_user.update_with_password(pass_params)
			bypass_sign_in current_user
			flash[:notice] = "Password successfully updated"
		else
			flash[:error] = "Password not saved"
		end

		redirect_to admin_admin_profile_settings_path
	end

	def create_new_admin
		
	end

	def create_admin
		@user = User.find_by(username: params[:user][:username])
	    if @user.nil?
	    	@user = User.find_by(email: params[:user][:email])
	    	if @user.nil?
	    		if(params[:user][:password].to_s.length < 8)
					flash[:error] = "Password must be 8 characters."
					redirect_to admin_admin_create_new_admin_path
	    		else
	    			group = Group.find 1
					if group.nil?
						flash[:error] = "Group does not exist."
						redirect_to admin_admin_create_new_admin_path
					else
						user = User.create(email: params[:user][:email], password: params[:user][:password], username: params[:user][:username], role: 1)

						group_member = group.group_members.create(user_id: user.id, role: 1)
						if user.save!
							user.settings.create(group_id: group.id, user_id: user.id)               
							flash[:notice] = "Admin created."
							redirect_to admin_admin_create_new_admin_path
						else
							flash[:error] = "Account not created."
							redirect_to admin_admin_create_new_admin_path
						end
					end
	    		end
		    else
				flash[:error] = "Email already exist."
				redirect_to admin_admin_create_new_admin_path
		    end
		    	
	    else
			flash[:error] = "Username already exist."
			redirect_to admin_admin_create_new_admin_path
	    end
	end

	def join_group
		
		g = GroupMember.create(group_id: params[:group_id], user_id: current_user.id, role: 0)
		g.save!
		flash[:notice] = "You have joined the group."
		redirect_to admin_dashboard_index_path
	end

	def create_group
		g = Group.create(group_name: params[:group][:group_name])
		g.save!
		g.group_creators.create(user_id: current_user.id)
		g.save!

		flash[:notice] = "Group successfully created"
		redirect_to admin_dashboard_index_path
	end

	private
		def admin_params
			params.require(:user).permit(:email, :username, :role, :password)
		end
		def user_params
			params.require(:user).permit(:first_name, :last_name, :city, :state, :zip, :phone_number, :birth_date, :email, :gender, :username, :avatar)
		end
		def pass_params
		  	params.require(:pass).permit(:current_password, :password, :password_confirmation, :username)
		  end
end
