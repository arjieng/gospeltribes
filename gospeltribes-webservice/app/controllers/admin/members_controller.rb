class Admin::MembersController < ApplicationController
  before_action :web_authenticate_user!
  def destroy
    begin
      @group = Group.find(params[:dashboard_id])
      @member = @group.group_members.find_by(user_id: params[:id])

      if @member.present?
        @member.destroy!

        flash[:notice] = "Member removed."
      end
    rescue Exception => e
      flash[:error] = "Could not delete member."
    end

    redirect_to admin_dashboard_members_path
  end
end
