class Admin::EventsController < ApplicationController
  	before_action :web_authenticate_user!
	
	def manage_event
		begin
    		if params[:commit].to_s == "Save changes"
				if params[:event_list_id] != ""
					
					params[:share_to].each do |id|
						new_event_params = event_params.as_json(except: :group_id)
	 					new_event_params[:group_id] = id.to_i

					 	new_event = Event.create(new_event_params)
					 	EventList.find(params[:event_list_id].to_i).destroy!

					 	user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{new_event_params[:group_id].to_i}")
						if user_badges.count != 0
							user_badges.find_each do |ub|
								ub.event = ub.event + 1
								ub.save!
							end
						end
					 	
					 	send_to_topic("'group_#{id.to_i}_iOS_All' in topics,'group_#{id.to_i}_Android_All' in topics", { action: "calendar" }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has created an event", id.to_i, ["'group_#{id.to_i}_#{current_user.id}' in topics"], "event")
					 
					end
					flash[:notice] = "Event saved."
				end

				if params[:event_id] != ""
					params[:share_to].each do |id|
						events = Event.where(from_event_list: event_params[:from_event_list].to_i, group_id: id.to_i)

						new_event_params = event_params.as_json(except: :group_id)
	 					new_event_params[:group_id] = id.to_i

						if events.count == 0
						 	new_event = Event.create(new_event_params)
						 	EventList.find(params[:event_list_id].to_i).destroy!
						else
							events.each do |event|
								event.update_attributes(new_event_params)
							end
						end
					end

					all_events_with_same_from_event_list = Event.where("from_event_list = #{event_params[:from_event_list].to_i} AND group_id NOT IN (#{params[:share_to].join(',')})")
					all_events_with_same_from_event_list.delete_all
					flash[:notice] = "Event changes saved."
				end
			else
				event = Event.find params[:event_id].to_i
				event.destroy!
				flash[:notice] = "Event removed."
			end	
	    rescue Exception => e
	    	flash[:error] = "Could not save event."
	    end

	    redirect_to admin_dashboard_events_path
	end

	protected
		def event_params
			params.require(:event).permit(:title, :details, :image, :link, :group_id, :start_date, :end_date, :from_event_list, :location, :subtitle, :repeat, :repeat_days)
		end
end
