class Admin::PrayersController < ApplicationController
  layout 'admin'
  before_action :web_authenticate_user!

  def new
    @group = Group.find(params[:dashboard_id])
    @prayer = Prayer.new
  end

  def create
    @group = Group.find(params[:dashboard_id])
    prayer = @group.prayers.create(create_params)
    prayer.user = current_user
    prayer.is_answered = 0
    prayer.is_hidden = false

    if prayer.save!

      user_badges = UserBadge.where("user_id != #{current_user.id} AND group_id = #{params[:dashboard_id].to_i}")
      if user_badges.count != 0
        user_badges.find_each do |ub|
          ub.prayer = ub.prayer + 1
          ub.save!
        end
      end

      ct = create_params[:share_to].to_i == 1 ? "Female" : (create_params[:share_to].to_i == 2 ? "Male" : "All")
      topics = "'group_#{params[:dashboard_id]}_iOS_#{ct}' in topics,'group_#{params[:dashboard_id]}_Android_#{ct}' in topics"
      send_to_topic(topics, { action: "prayer" }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has created a prayer", params[:dashboard_id], ["'group_#{params[:dashboard_id]}_#{current_user.id}' in topics"], "prayer")
      
      redirect_to admin_dashboard_prayers_path
    end
  end

  def create_to_all
    groups = Group.all
    count = 1
    groups.each do |group|
      prayer = group.prayers.create(create_params)
      prayer.user = current_user
      prayer.is_answered = 0
      prayer.is_hidden = false
      prayer.from_admin = true
      prayer.counter = count
      count += 1
      prayer.save!

      ct = create_params[:share_to].to_i == 1 ? "Female" : (create_params[:share_to].to_i == 2 ? "Male" : "All")
      topics = "'group_#{group.id}_iOS_#{ct}' in topics,'group_#{group.id}_Android_#{ct}' in topics"
      send_to_topic(topics, { action: "prayer" }, "#{current_user.first_name.nil? ? current_user.username : current_user.first_name} has created a prayer", group.id, ["'group_#{group.id}_#{current_user.id}' in topics"], "prayer")
    end
    redirect_to admin_admin_prayers_path
    
  end

  def edit_all
    prs = Prayer.where("id IN (#{params[:prayer_id]})")
    prs.each do |pr|
      pr.update_attributes(create_params)
    end
    flash[:notice] = "Prayer updated."
    redirect_to admin_admin_prayers_path
  end

  def edit
    prayer = Prayer.find params[:prayer_id].to_i
    prayer.update_attributes(create_params)
    flash[:notice] = "Prayer updated."
    redirect_to admin_dashboard_prayers_path
  end

  def update
  end

  def destroy
    @prayer = Prayer.find params[:id].to_i
    @prayer.destroy!

    flash[:notice] = "Prayer removed."
    redirect_to admin_dashboard_prayers_path
  rescue Exception => e
    flash[:error] = "Could not delete prayer."
  end

  def destroy_all
    ps = Prayer.where("id IN (#{params[:id]})")
    ps.destroy_all

    flash[:notice] = "Prayer removed."
    redirect_to admin_admin_prayers_path
  rescue Exception => e
    flash[:error] = "Could not delete prayer."
  end

  private
    def create_params
      params.require(:prayer).permit(:subject, :details, :share_to, :is_answered, :answered_details)
    end
end
