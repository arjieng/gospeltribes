class Admin::PagesController < ApplicationController
  before_action :web_authenticate_user!, except: [:ajax_messaging, :ajax_add_event, :ajax_get_groups, :ajax_delete_messaging, :ajax_delete_media, :ajax_for_sample_page, :ajax_delete_media_from_profile]
  before_action :check_group!, except: [:ajax_messaging, :ajax_add_event, :ajax_get_groups, :ajax_delete_messaging, :ajax_delete_media, :ajax_for_sample_page, :ajax_delete_media_from_profile]
  layout 'admin'

  def index
    @group = Group.includes(chatrooms: [chatroom_messages: [:user, :group_medium]]).find params[:dashboard_id]
  end

  def prayers
    @group = Group.find params[:dashboard_id]
    @prayers_u = Prayer.includes(:group, user: :group_members).where(group_id: params[:dashboard_id], is_answered: 0).order("id DESC")
    @prayers_a = Prayer.includes(:group, user: :group_members).where(group_id: params[:dashboard_id], is_answered: 1).order("id DESC")
  end

  def events
    @group = Group.find params[:dashboard_id]
    @events_list = EventList.where(user_id: current_user.id, group_id: params[:dashboard_id])

    dateTime = DateTime.now
    query = "group_id = #{params[:dashboard_id]} AND (DATE(start_date) >= '#{dateTime.strftime("%Y-%m-%d")}' OR repeat != 'None')"
    @events = Event.where(query).order("start_date DESC")

    @groups = Group.all
  end

  def media
    @group = Group.find params[:dashboard_id]
    @media = GroupMedium.includes(:user).where(group_id: params[:dashboard_id])
  end

  def members
    @group = Group.includes(group_members: [user: [:prayers, :group_medium]]).find params[:dashboard_id]
    @group_members = []
    @group.group_members.each do |member|
      member_user = member.user
      if member_user.present?
        @group_members.push({ id: member_user.id, avatar: member_user.avatar.url, first_name: member_user.first_name, last_name: member_user.last_name, username: member_user.username, prayer_count: member_user.prayers.count, group_medium_count: member_user.group_medium.count, role: member.role })
      end
    end
  end

  def settings
    @group = Group.find params[:dashboard_id]
  end

  def destroy_from_profile
    @prayer = Prayer.find params[:prayer_id].to_i
    @prayer.destroy!

    flash[:notice] = "Prayer removed."
    redirect_to admin_dashboard_profile_path(id: params[:user_id])
  rescue Exception => e
    flash[:error] = "Could not delete prayer."
  end

  def destroy_prayer
    @group = Group.includes(:prayers).find(params[:dashboard_id])
    @prayer = @group.prayers.find_by(id: params[:id])

    @prayer.destroy!

    flash[:notice] = "Prayer removed."
    redirect_to admin_dashboard_prayers_path + (params[:is_answered].to_s == "true" ? "#tab_1_2" : "#tab_1_1")
  rescue Exception => e
    flash[:error] = "Could not delete prayer."
  end

  def profile
    @group = Group.find params[:dashboard_id]
    @user = User.includes(:prayers).find params[:id]
    @prayers = @user.prayers.where(is_hidden: false).order("id DESC")
    @media = []
    group_media = GroupMedium.where(user_id: @user.id, group_id: params[:dashboard_id]).order("id DESC")
    group_media.each do |group_medium|
      group = group_medium.group
      @media.push({ id: group_medium.id, image: group_medium.image.url, is_video: group_medium.is_video, thumb: group_medium.video_name.thumb.url })
    end
  end

  def sample_page_for_testing
    @group = Group.find params[:dashboard_id]
  end
  
  def profile_settings
    @group = Group.find params[:dashboard_id]
    @user = User.includes(:prayers).find params[:id]

  end

  def ajax_for_sample_page
    if params[:search].present?
      render json: URI.parse("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=#{params[:search]}&radius=1000&key=AIzaSyBWV4ntfc6-Tg7-ud7G-Cg9a8btbqZLx18").read
    else
      render json: nil
    end
  end

  def ajax_messaging
    @message = ChatroomMessage.includes(:user).create(user_id: params[:user_id], chatroom_id: params[:chatroom_id], body: params[:body])
    @message.save!
    user = @message.user
    dateTime = "#{time_ago_in_words(@message.created_at)} ago"
    @data = { action: "send_message", data: { id: @message.id, chatroom_type: params[:chatroom_type], user: { id: user.id, first_name: user.first_name, last_name: user.last_name, avatar: { url: user.avatar.url }, username: user.username }, message: @message.body, media: nil, date_sent: dateTime } }
    broadcast("/group/#{params[:group_id]}/chatrooms/#{params[:chatroom_id]}", @data)
    broadcast("/group/#{params[:group_id]}/chatrooms", @data)
    

    user_badges = UserBadge.where("user_id != #{params[:user_id].to_i} AND group_id = #{params[:group_id].to_i}")
    if user_badges.count != 0
      user_badges.find_each do |ub|
        ub.inbox = ub.inbox + 1
        ub.save!
      end
    end

    ct = params[:chatroom_type].to_i == 1 ? "Female" : (params[:chatroom_type].to_i == 2 ? "Male" : "All")
    topics = "'group_#{params[:group_id].to_i}_iOS_#{ct}' in topics,'group_#{params[:group_id].to_i}_Android_#{ct}' in topics"
    send_to_topic(topics, { action: "message" }, "#{user.first_name.nil? ? user.username : user.first_name} has sent a message", params[:group_id].to_i, ["'group_#{params[:group_id]}_#{params[:user_id]}' in topics"], "inbox")


  end

  def ajax_delete_messaging
    cm = ChatroomMessage.find params[:chatroom_message_id]
    if cm.present?
      cm.delete
      render json: { removed: cm.destroyed?, status: 200 }, status: 200
    end
  end

  def ajax_delete_media
    gm = GroupMedium.find params[:media_id]
    if gm.present?
      gm.delete
      @media = GroupMedium.includes(:user).where(group_id: params[:dashboard_id])
      @medium = []
      @media.each do |media|
        @medium.push({ medium: media, user: media.user })
      end
      render json: { media: @medium, status: 200 }, status: 200
    end
  end

  def ajax_delete_media_from_profile
    gm = GroupMedium.find params[:media_id]
    if gm.present?
      gm.delete
      
      @media = []
      group_media = GroupMedium.where(user_id: params[:user_id], group_id: params[:dashboard_id]).order("id DESC")
      group_media.each do |group_medium|
        group = group_medium.group
        @media.push({ id: group_medium.id, image: group_medium.image.url, is_video: group_medium.is_video, thumb: group_medium.video_name.thumb.url })
      end

      render json: { media: @media, status: 200 }, status: 200
    end
  end

  def ajax_add_event
    event = EventList.create(user_id: params[:user_id], group_id: params[:dashboard_id], event_name: params[:event_title])
    render json: event.as_json(only: [:id, :user_id, :group_id, :event_name])
  end

  def ajax_get_groups
    g_is_nil = []
    string_value = []
    groups = Group.includes(:events).all
    groups.each do |group|
      g = group.events.where("from_event_list = #{params["from_event_list_id"]}")
      
      if !g.nil? && g.count != 0
        string_value.push("<option value=\"#{group.id}\" selected=\"true\">#{group.group_name}</option>")
      else
        if group.id == params["dashboard_id"].to_i
          string_value.push("<option value=\"#{group.id}\" selected=\"true\">#{group.group_name}</option>")
        else
          string_value.push("<option value=\"#{group.id}\">#{group.group_name}</option>")
        end
      end
    end

    render json: {strings: string_value}
  end

  def manage_group
    if params[:commit] == "Save Changes"
      group = Group.find(params[:dashboard_id].to_i)
      group.update_attributes(group_event)
      redirect_to admin_dashboard_settings_path
    end
    if params[:commit] == "Delete Group"
      Group.find(params[:dashboard_id].to_i).destroy!
      redirect_to admin_dashboard_index_path
    end
    
  end

  def manage_privilege
    user = User.includes(group_members: :group).find params[:privilege][:user_id]
    gm = user.group_members.find_by_group_id params[:dashboard_id].to_i
    g = gm.group

    case params[:privilege][:privilege_choice].to_s
    when "1"
      user.role = "2"
      gm.role = 1
      user.save!
      gm.save!
    when "2"
      user.role = "2"
      gm.role = 2
      user.save!
      gm.save!
    when "3"
      user.role = "1"
      user.save!
    end
    
    @data = { action: "update_role", data: { user_id: user.id, group: { id: g.id, group_code: g.group_code, group_name: g.group_name, group_image: g.group_image, role: params[:privilege][:privilege_choice] } } }
    broadcast("/group/#{g.id}/chatrooms", @data)
    redirect_to admin_dashboard_profile_settings_path(id: params[:privilege][:user_id].to_i)+"#tab_1_4"
  end

  def edit_personal_info
    user = User.find params[:user_id]
    user.update_attributes(user_params)
    flash[:notice] = "User info updated"
    redirect_to admin_dashboard_profile_settings_path(id: params[:user_id])
  end

  def edit_user_avatar
    user = User.find params[:user_id]
    user.update_attributes(user_params)
    flash[:notice] = "Avatar updated"
    redirect_to admin_dashboard_profile_settings_path(id: params[:user_id])
  end

  def edit_user_account
    user = User.find params[:user_id]
    if user.update_with_password(pass_params)
      if user.id == current_user.id
        bypass_sign_in current_user
      end
      
      flash[:notice] = "Password successfully updated"
    else
      flash[:error] = "Password not saved"
    end

    redirect_to admin_dashboard_profile_settings_path(id: params[:user_id])
  end

  private
    def group_event
      params.require(:group).permit(:group_name, :group_image)
    end
    def user_params
      params.require(:user).permit(:first_name, :last_name, :city, :state, :zip, :phone_number, :birth_date, :email, :gender, :username, :avatar)
    end
    def pass_params
        params.require(:pass).permit(:current_password, :password, :password_confirmation, :username)
      end
end
