class UsersDevise::SessionsController < Devise::SessionsController
   def new
     super
   end

   def create
       # super
   	 admin = User.find_by(email: users_params[:email])
   	 if admin.present? && admin.valid_password?(users_params[:password])
   	 	# admin_as_member = admin.group_members.where("role=1")
   	 	# if admin_as_member.count != 0
 	 		if admin.role == "1"
        resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
   			set_flash_message!(:notice, :signed_in)
   			sign_in_and_redirect(resource_name, resource)
   	 	else
            redirect_to root_path, :alert => "Error with your login or password!"
   	 	end
   	 else
         redirect_to root_path, :alert => "Error with your login or password!"
   	 end
   end


	private
		def sign_in_and_redirect(resource_or_scope, resource=nil)
      scope = Devise::Mapping.find_scope!(resource_or_scope)
      resource ||= resource_or_scope
      sign_in(scope, resource) unless warden.user(scope) == resource
      redirect_to admin_dashboard_index_path
    end
    
		def users_params
			params.require(:user).permit(:email, :password)
		end
end