class UsersDevise::PasswordsController < Devise::PasswordsController
   skip_before_action :require_no_authentication, :only => [:edit, :update]
   def edit
    super
   end

   def update
     self.resource = resource_class.reset_password_by_token(resource_params)

     if resource.errors.empty?
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        set_flash_message(:notice, "Password successfully changed #{resource_params.inspect}")
        redirect_to root_path
     else
        respond_with resource
     end
   end
end
