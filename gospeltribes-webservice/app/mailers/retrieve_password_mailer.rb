class RetrievePasswordMailer < ApplicationMailer
  def send_email(data)
  	@email = data[:email]
  	@username = data[:username]
  	mail( :to => data[:email] ) do |format|
    	format.html{ render prayer_mailer: 'send_email' }
    end
  end
end
