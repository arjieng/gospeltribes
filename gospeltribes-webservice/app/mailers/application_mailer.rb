class ApplicationMailer < ActionMailer::Base
  default from: 'admin@gospeltribes.com'
  layout 'mailer'
end
