class PrayerMailer < ApplicationMailer
  def send_email(data)
  	@prayer = data[:prayer]
  	@message = data[:message]
  	mail( :to => data[:email] ) do |format|
    	format.html{ render prayer_mailer: 'send_email' }
    end
  end
end
