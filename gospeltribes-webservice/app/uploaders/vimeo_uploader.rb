class VimeoUploader < CarrierWave::Uploader::Base
  include CarrierWave::Video
  include CarrierWave::Video::Thumbnailer
  include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    # process :fix_exif_rotation
    # process :strip
    process thumbnail: [{format: 'png', quality: 10, size: 525, strip: false }]
    def full_filename for_file
      png_name for_file, version_name
    end
  end

  def png_name for_file, version_name
    %Q{#{version_name}_#{for_file.chomp(File.extname(for_file))}.png}
  end

  def extension_white_list
    %w(png jpg jpeg png gif bmp tif tiff mp4 mov avi mkv 3gp mpg mpeg)
  end

  def delete!
    remove!
    remove_versions!
  end  

  def filename
    file = model.video_name.url
    extension = model.video_name.present? ? model.video_name.file.extension.downcase : nil
    puts extension

    if extension == "mp4" || extension == "mov" || extension == "avi" || extension == "mkv" || extension == "3gp" || extension == "mpg" || extension == "mpeg" 
      ivar = "@#{mounted_as}_secure_token"    
      token = model.instance_variable_get(ivar) or model.instance_variable_set(ivar, SecureRandom.hex(20/2))
      "#{token}.#{extension}" if original_filename
    else
      ivar = "@#{mounted_as}_secure_token"    
      token = model.instance_variable_get(ivar) or model.instance_variable_set(ivar, SecureRandom.hex(20/2))
      "#{token}.png" if original_filename
    end
  end
end
