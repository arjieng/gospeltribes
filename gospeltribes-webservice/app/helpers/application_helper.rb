module ApplicationHelper
  include ActionView::Helpers::DateHelper
  require 'open-uri'
  require 'net/http'
  require 'fcm'

  def authorize_user!
    if params[:id].present? && current_user.id != params[:id]
      raise Exceptions::UnauthorizedException
    end
  end

  def broadcast(channel, data)
    begin
      message = {:channel => channel, :data => data}
      uri = URI.parse("http://104.248.208.184:9292/faye")
      Net::HTTP.post_form(uri, :message => message.to_json)
    rescue => e
      Rails.logger.debug "#{e.inspect}"
    end
  end


  def send_to_topic(topic, data, title, group_id, exceptions, tab_name)
    fcm = FCM.new("AAAAQUNBeHY:APA91bFp_cFsHbGXZKhzUaBgonQUfHg01yohwZEYrvkoHNTSgUN5vdYdwTrRMuGiG8S6Ao1tqzIrgoDBKM5cacPDkkvH6QIogn6sLrARDdgkubDanuSg_6Ixn_6y6dgsertoGmOMYWEq")

    # data must have `message` params inside
    data.merge!(message: title)
    options_Android = { priority: 'high', data: data }
    options_iOS = { priority: 'high', data: data, notification: { body: title, tab_name: tab_name, sound: 'default', content_available: true } }
    
    if Group.exists? group_id.to_i
      group = Group.includes(group_members: [user: :settings]).find(group_id.to_i)
      members = group.group_members
      members.each do |m|
        if !m.user.nil?
          user = m.user
          if !user.settings.find_by(group_id: group_id).nil?
            user_settings = user.settings.find_by(group_id: group_id)
            if (data[:action].to_s == "message" && user_settings.inbox == false) || (data[:action].to_s == "prayer" && user_settings.prayer == false) || (data[:action].to_s == "calendar" && user_settings.event == false) || (data[:action].to_s == "story" && user_settings.story == false) || (data[:action].to_s == "members" && user_settings.member == false)
              exceptions.push("'group_#{group_id}_#{user.id}' in topics")
            end
          end
        end
      end
    end
    
    # topic[0] = ios, topic[1] = android 
    topic_array = topic.split(",")
    response_iOS = fcm.send_to_topic_condition("#{topic_array[0]} && !(#{exceptions.uniq.join(" || ")})", options_iOS)    
    response_Android = fcm.send_to_topic_condition("#{topic_array[1]} && !(#{exceptions.uniq.join(" || ")})", options_Android)    
    
  end
end
