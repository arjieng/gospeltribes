class StudyModule < ApplicationRecord
	belongs_to :group
	has_many :study_module_comments

	mount_uploader :files, FilesUploader
end
