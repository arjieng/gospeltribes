class ChatroomMessage < ApplicationRecord
	belongs_to :user
	belongs_to :chatroom
	has_one :group_medium
	has_many :chatroom_messages_likes

	mount_uploader :image, AvatarUploader
end
