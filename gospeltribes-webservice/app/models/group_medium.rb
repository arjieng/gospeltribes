class GroupMedium < ApplicationRecord
	belongs_to :group
	belongs_to :chatroom_message, optional: true
	belongs_to :user
	
	mount_uploader :image, AvatarUploader
	mount_uploader :video_name, VimeoUploader
end
