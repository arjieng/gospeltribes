class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  include DeviseTokenAuth::Concerns::User
  


  mount_uploader :avatar, AvatarUploader

  has_many :group_medium
  has_many :group_members
  has_many :groups, through: :group_members
  has_many :chatroom_messages
  has_many :prayers
  has_many :prayer_comments
  has_many :event_lists
  has_many :settings
  has_many :group_creators
  has_many :push_notification_tokens
  has_many :user_badges
end