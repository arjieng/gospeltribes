class Chatroom < ApplicationRecord
	belongs_to :group
	has_many :chatroom_messages
	has_many :read_statuses

	mount_uploader :chatroom_image, AvatarUploader
end
