Rails.application.routes.draw do
  devise_for :users, path: '', controllers: { sessions: "users_devise/sessions", passwords: "users_devise/passwords" }
  
  devise_scope :user do
    root 'users_devise/sessions#new'
    get 'forgot_password' => "api/v1/sessions#forgot_password"
  end

  # namespace :marketing, path: '' do
  get 'marketing' => 'marketing#index'
  # end

  namespace :admin, path: '' do
    
    get 'admin/profile' => 'dashboard#admin_profile'
    get 'admin/profile_settings' => 'dashboard#admin_profile_settings'
    get 'admin/inbox' => 'dashboard#inbox'
    get 'admin/prayers' => 'dashboard#prayers'
    get 'admin/create_new_admin' => 'dashboard#create_new_admin'

    post 'ajax_delete_messaging' => 'dashboard#ajax_delete_messaging'
    post 'ajax_messaging' => 'dashboard#ajax_messaging'
    post 'edit_personal_info' => 'dashboard#edit_personal_info'
    post 'edit_user_avatar' => 'dashboard#edit_user_avatar'
    post 'edit_user_account' => 'dashboard#edit_user_account'
    post 'create_admin' => 'dashboard#create_admin'
    get 'join_group' => 'dashboard#join_group'
    get 'destroy_from_profile' => 'dashboard#destroy_from_profile'
    post 'create_group' => 'dashboard#create_group'
    post 'create_prayer' => 'prayers#create_to_all'
    get 'delete_prayer' => 'prayers#destroy_all'
    post 'edit_prayer' => 'prayers#edit_all'


    resources :dashboard, path: 'admin', only: [:index, :inbox, :prayers] do
      get 'inbox' => 'pages#index'
      get 'prayers' => 'pages#prayers'
      get 'events' => 'pages#events'
      get 'media' => 'pages#media'
      get 'members' => 'pages#members'
      get 'settings' => 'pages#settings'
      get 'profile' => 'pages#profile'
      get 'profile_settings' => 'pages#profile_settings'
      post 'edit_personal_info' => 'pages#edit_personal_info'
      post 'edit_user_avatar' => 'pages#edit_user_avatar'
      post 'edit_user_account' => 'pages#edit_user_account'
      post 'manage_group' => 'pages#manage_group'
      post 'manage_privilege' => 'pages#manage_privilege'
      post 'ajax_add_event' => 'pages#ajax_add_event'
      post 'ajax_get_groups' => 'pages#ajax_get_groups'
      post 'ajax_messaging' => 'pages#ajax_messaging'
      post 'ajax_delete_messaging' => 'pages#ajax_delete_messaging'
      post 'ajax_delete_media' => 'pages#ajax_delete_media'
      post 'ajax_delete_media_from_profile' => 'pages#ajax_delete_media_from_profile'
      
      get 'sample_page_for_testing' => 'pages#sample_page_for_testing'
      post 'ajax_for_sample_page' => 'pages#ajax_for_sample_page'

      post 'manage_event' => 'events#manage_event'

      get 'member_destroy' => 'members#destroy'
      # resources :prayers, only: [:destroy]
      get 'destroy_prayer' => 'pages#destroy_prayer'
      get 'destroy_from_profile' => 'pages#destroy_from_profile'
      post 'create_prayer' => 'prayers#create'
      post 'edit_prayer' => 'prayers#edit'
    end
  end

  #API
  namespace :api do
    scope :v1, path: '' do
      mount_devise_token_auth_for 'User', at: 'auth', controllers: {
        token_validations:  'api/v1/sessions',
        sessions: 'api/v1/sessions',
        registrations: 'api/v1/registrations'
      }
    end
  end

  namespace :api do
  	namespace :v1, path: '' do
  		resources :group, only: [:member_list] do
        get 'group_members' => 'groups#member_list'
      end
      get 'check_group' => 'groups#check_group'
      get 'get_chatrooms' => 'groups#get_chatrooms'
      get 'group_medium' => 'groups#group_medium'
      post 'study_module' => 'groups#study_module'
      post 'edit_study_module' => 'groups#edit_study_module'

      get 'get_study_module' => 'groups#get_study_module'
      get 'delete_study_module' => 'groups#delete_study_module'
      
      post 'add_study_module_comment' => 'groups#add_study_module_comment'
      get 'get_study_module_comment' => 'groups#get_study_module_comment'
      get 'remove_message' => 'groups#remove_message'
      post 'edit_message' => 'groups#edit_message'
      get 'add_group' => 'groups#add_group'
      post 'add_group_image' => 'groups#add_group_image'
      get 'rename_group' => 'groups#rename_group'
      post 'change_chatroom_details' => 'groups#change_chatroom_details'
      get 'remove_group_image' => 'groups#remove_group_image'
      get 'delete_study_module_comment' => 'groups#delete_study_module_comment'
      post 'example_method' => 'groups#example_method'
      get 'set_unread_badges' => 'users#set_unread_badges'
      get 'get_unread_badges' => 'users#get_unread_badges'
      get 'set_read_badges' => 'users#set_read_badges'
      get 'get_sample' => 'users#get_sample'
      
      post 'send_message' => 'messages#send_message'
      get 'get_message' => 'messages#get_message'
      get 'set_read' => 'messages#set_read'
      get 'like_message' => 'messages#like_message'
      get 'unlike_message' => 'messages#unlike_message'
      
      post 'update_user_notification_settings' => 'users#update_user_notification_settings'
      post 'update_account' => 'users#update_account'
      post 'reset_password' => 'users#reset_password'
      get 'retrieve_username' => 'users#retrieve_username'

      get 'user_images' => 'users#user_images'
      get 'update_udid' => 'users#update_udid'
      post 'upload_user_avatar' => 'users#upload_user_avatar'
      get 'get_user_info' => 'users#get_user_info'
      get 'remove_profile' => 'users#remove_profile'
      
      post 'update_profile' => 'users#update_profile'
      post 'create_prayer' => 'prayers#create'
      get 'prayers' => 'prayers#index'
      post 'update_prayer' => 'prayers#update'
      get 'answered_prayers' => 'prayers#answered_prayers'
      get 'unanswered_prayers' => 'prayers#unanswered_prayers'
      get 'prayer_comments' => 'prayers#prayer_comments'
      post 'add_prayer_comment' => 'prayers#add_prayer_comment'
      get 'hide_prayer' => 'prayers#hide_prayer'
      get 'get_all_prayers'=>'prayers#get_all_prayers'
      get 'delete_prayer' => 'prayers#delete_prayer'
      get 'delete_prayer_comment' => 'prayers#delete_prayer_comment'
      
      get 'get_event' => 'events#get_event'
      post 'add_event' => 'events#create'
      get 'all_events' => 'events#index'
      post 'update_event' => 'events#update'
      get 'add_attendee' => 'events#add_attendee'
      get 'attendees' => 'events#get_attendees'
      get 'remove_attendee' => 'events#remove_attendee'
      get 'remove_event' => 'events#remove_event'
  	end
  end
end
